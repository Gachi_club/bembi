<style type="text/css">
	body{margin:0px; background:#ccc; -webkit-margin-before:0em; -webkit-margin-after:0em; -webkit-margin-start:0px; -webkit-margin-end:0px;}
	.error403{height:100%;}
	.error403 .container-fluid{padding-top:5rem;}
	.error403 .row{}
	.error403 h1{padding:1rem 0; font-size:12vw; text-align:center; -webkit-margin-before:0em; -webkit-margin-after:0em; -webkit-margin-start:0px; -webkit-margin-end:0px;}
	.error403 h2{font-size:4vw; text-align:center; -webkit-margin-before:0em; -webkit-margin-after:0em; -webkit-margin-start:0px; -webkit-margin-end:0px;}
	.error403 p{font-size:2vw; padding-top:4rem; text-align:center; -webkit-margin-before:0em; -webkit-margin-after:0em; -webkit-margin-start:0px; -webkit-margin-end:0px;}
	.error403 a{color:#000;}
	.error403 a:hover{text-decoration:none;}
</style>
<section class="error403">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 align-self-center">
				<h1>403</h1>
				<h2>Пользователь не прошел аутентификацию, доступ запрещен</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-12 align-self-center">
				<p><a href="/admin/login/">Перейти на предыдущую страницу</a></p>
			</div>
		</div>
	</div>
</section>