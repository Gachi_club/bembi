<style type="text/css">
	body{margin:0px; background:#ccc; -webkit-margin-before:0em; -webkit-margin-after:0em; -webkit-margin-start:0px; -webkit-margin-end:0px;}
	.error500{height:100%;}
	.error500 .container-fluid{padding-top:5rem;}
	.error500 .row{}
	.error500 h1{padding:1rem 0; font-size:12vw; text-align:center; -webkit-margin-before:0em; -webkit-margin-after:0em; -webkit-margin-start:0px; -webkit-margin-end:0px;}
	.error500 h2{font-size:4vw; text-align:center; -webkit-margin-before:0em; -webkit-margin-after:0em; -webkit-margin-start:0px; -webkit-margin-end:0px;}
	.error500 p{font-size:2vw; padding-top:4rem; text-align:center; -webkit-margin-before:0em; -webkit-margin-after:0em; -webkit-margin-start:0px; -webkit-margin-end:0px;}
	.error500 a{color:#000;}
	.error500 a:hover{text-decoration:none;}
</style>
<section class="error500">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 align-self-center">
				<h1>500</h1>
				<h2>Внутренняя ошибка сервера</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-12 align-self-center">
				<p><a href="/">Перейти на предыдущую страницу</a></p>
			</div>
		</div>
	</div>
</section>