<?php
set_time_limit(0);
ini_set('memory_limit', '-1');
error_reporting(E_ALL & ~E_NOTICE); 
ini_set("display_errors", 1);
header('Content-Type:text/html; charset=utf-8');

// Определяем IP пользователя
if(!empty($_SERVER['HTTP_CLIENT_IP']))
{$user_ip = $_SERVER['HTTP_CLIENT_IP'];}
elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
{$user_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];}
else{$user_ip = $_SERVER['REMOTE_ADDR'];}
if(isset($_SESSION['user_ip']) && $_SESSION['user_ip'] != $user_ip){$_SESSION['user_ip'] = $user_ip;}
else{$_SESSION['user_ip'] = $user_ip;}
$u_ip = explode(",", $_SESSION['user_ip']);
$_SESSION['user_ip'] = $u_ip[0];
// ** Определяем IP пользователя
?>
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="ru" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="ru" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="ru">
<!--<![endif]-->
<!--<html lang="ru">-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="author" content="Elendarien">
	<meta name="robots" content="all">
	<meta name="copywright" content="SinUP.od.ua">
	<meta property="og:title" content="(Error) Ошибка 404. Not Found">
	<meta property="og:description" content="(Error) Ошибка 404 или Not Found — стандартный код ответа HTTP о том, что клиент был в состоянии общаться с сервером, но сервер не может найти данные согласно запросу.">
	<meta property="og:site_name" content="http://sinup.od.ua">
	<meta property="og:image" content="/public/img/404.png">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="/public/img/favicon.ico">
	<title>(Error) Ошибка 404. Not Found</title>
	<meta name="description" content="(Error) Ошибка 404 или Not Found — стандартный код ответа HTTP о том, что клиент был в состоянии общаться с сервером, но сервер не может найти данные согласно запросу.">
	<meta name="keywords" content="error, 404, not found, ошибка">

<!--[if lt IE 9]> 
	<script src="http://css3-mediaqueries-js.googlecode.com/files/css3-mediaqueries.js"></script>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> 
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!--[if lt IE 10]>
	<div style="background:#212121; padding:10px 0; box-shadow:3px 3px 5px 0 rgba(0,0,0,.3); clear:both; text-align:center; position:relative; z-index:1;">
		<a href="https://windows.microsoft.com/en-US/internet-explorer/">
			<img src="/public/img/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.">
		</a>
	</div>
	<script type="text/javascript" src="/public/js/html5shiv.min.js"></script>
<![endif]-->

<link href="https://fonts.googleapis.com/css?family=Dancing+Script:700" rel="stylesheet">
<style>
* {margin:0px; padding:0px;}
html{overflow-x:hidden;}
body{margin:0px; padding:0px; font-family:'Calibri', Arial, sans-serif; -webkit-user-select:none; -moz-user-select:none; -ms-user-select:none;}
body a{cursor:pointer;}
.more {visibility:hidden;}
p {display:block; margin-top:0px; margin-bottom:0rem!important; -webkit-margin-before:0em; -webkit-margin-after:0em; -webkit-margin-start:0px; -webkit-margin-end:0px;}
input:focus, textarea:focus, select:focus{outline-offset:0px;}
:focus{outline:-webkit-focus-ring-color auto 0px;}

.dropdown:hover>.dropdown-menu {display:block;}
.dropdown-menu{margin:0!important;}

@font-face{font-family:'Calibri', Arial, sans-serif;}
html{font-size:1rem;}
@include media-breakpoint-up(sm){html{font-size:1.2rem;}}
@include media-breakpoint-up(md){html{font-size:1.4rem;}}
@include media-breakpoint-up(lg){html{font-size:1.6rem;}}
@include media-breakpoint-up(xl){html{font-size:1.6rem;}}
</style>

</head>

<body class="bg-purple">
        <style>
			@import url('https://fonts.googleapis.com/css?family=Dosis:300,400,500');
			@-moz-keyframes rocket-movement {100%{-moz-transform: translate(1200px,-600px);}}
			@-webkit-keyframes rocket-movement {100%{-webkit-transform: translate(1200px,-600px);}}
			@keyframes rocket-movement {100%{transform: translate(1200px,-600px);}}
			@-moz-keyframes spin-earth {100%{-moz-transform:rotate(-360deg); transition:transform 20s;}}
			@-webkit-keyframes spin-earth {100%{-webkit-transform:rotate(-360deg); transition:transform 20s;}}
			@keyframes spin-earth {100%{-webkit-transform: rotate(-360deg); transform:rotate(-360deg); transition:transform 20s;}}

			@-moz-keyframes move-astronaut {100%{-moz-transform:translate(-160px, -160px);}}
			@-webkit-keyframes move-astronaut {100%{-webkit-transform:translate(-160px, -160px);}}
			@keyframes move-astronaut{100%{-webkit-transform:translate(-160px, -160px); transform:translate(-160px, -160px);}}
			@-moz-keyframes rotate-astronaut {100%{-moz-transform: rotate(-720deg);}}
			@-webkit-keyframes rotate-astronaut {100%{-webkit-transform:rotate(-720deg);}}
			@keyframes rotate-astronaut{100%{-webkit-transform:rotate(-720deg); transform:rotate(-720deg);}}

			@-moz-keyframes glow-star {40%{-moz-opacity:0.3;} 90%,100%{-moz-opacity:1; -moz-transform:scale(1.2);}}
			@-webkit-keyframes glow-star {40%{-webkit-opacity:0.3;} 90%,100%{-webkit-opacity:1; -webkit-transform:scale(1.2);}}
			@keyframes glow-star{40%{-webkit-opacity:0.3; opacity:0.3;} 90%,100%{-webkit-opacity:1; opacity:1; -webkit-transform:scale(1.2); transform:scale(1.2); border-radius:999999px;}}

			.spin-earth-on-hover{transition:ease 200s !important; transform:rotate(-3600deg) !important;}
			html, body{margin:0; width:100%; height:100%; font-family:'Dosis', sans-serif; font-weight:300; -webkit-user-select:none; -moz-user-select:none; -ms-user-select:none; user-select:none;}
			.bg-purple{/*background:url(/public/img/error/bg_purple.png);*/ background-color:#2b005a; background-repeat:repeat-x; background-size:cover; background-position:left top; height:100%; overflow:hidden;}
			.custom-navbar{padding-top:15px;}
			.brand-logo{margin-left:25px; margin-top:5px; display:inline-block;}
			.navbar-links{display:inline-block; float:right; margin-right:15px; text-transform:uppercase;}
			ul {list-style-type:none; margin:0; padding:0; display:flex; align-items:center;}
			li {float:left; padding:0px 15px;}
			li a {display:block; color:white; text-align:center; text-decoration:none; letter-spacing:2px; font-size:12px; -webkit-transition:all 0.3s ease-in; -moz-transition:all 0.3s ease-in; -ms-transition:all 0.3s ease-in; -o-transition:all 0.3s ease-in; transition:all 0.3s ease-in;}
			li a:hover {color:#ffcb39;}
			.btn-request{padding:10px 25px; border:1px solid #FFCB39; border-radius:100px; font-weight:400;}
			.btn-request:hover{background-color:#FFCB39; color:#000; transform:scale(1.05); box-shadow:0px 20px 20px rgba(0,0,0,0.1);}
			.btn-go-home{position:relative; z-index:200; margin:15px auto; width:100px; padding:10px 15px; border:1px solid #FFCB39; border-radius:100px; font-weight:400; display:block; color:white; text-align:center; text-decoration:none; letter-spacing:2px; font-size:11px; -webkit-transition:all 0.3s ease-in; -moz-transition:all 0.3s ease-in; -ms-transition:all 0.3s ease-in; -o-transition:all 0.3s ease-in; transition:all 0.3s ease-in;}
			.btn-go-home:hover{background-color:#FFCB39; color:#000; transform:scale(1.05); box-shadow:0px 20px 20px rgba(0,0,0,0.1);}
			.central-body{padding:17% 5% 10% 5%; text-align:center;}
			.objects img{z-index:90; pointer-events:none;}
			.object_rocket{z-index:95; position:absolute; transform:translateX(-50px); top:75%; pointer-events:none; animation:rocket-movement 200s linear infinite both running;}
			.object_earth{position:absolute; top:20%; left:15%; z-index:90;}
			.object_moon{position:absolute; top:12%; left:25%;}
			.earth-moon{}
			.object_astronaut{animation:rotate-astronaut 200s infinite linear both alternate;}
			.box_astronaut{z-index:110!important; position:absolute; top:60%; right:20%; will-change:transform; animation:move-astronaut 50s infinite linear both alternate;}
			.image-404{position:relative; z-index:100; pointer-events:none;}
			.stars{background:url(/public/img/error/overlay_stars.svg); background-repeat:repeat; background-size:contain; background-position:left top;}
			.glowing_stars .star{position:absolute; border-radius:100%; background-color:#fff; width:3px; height:3px; opacity:0.3; will-change:opacity;}
			.glowing_stars .star:nth-child(1){top:80%; left:25%; animation:glow-star 2s infinite ease-in-out alternate 1s;}
			.glowing_stars .star:nth-child(2){top:20%; left:40%; animation:glow-star 2s infinite ease-in-out alternate 3s;}
			.glowing_stars .star:nth-child(3){top:25%; left:25%; animation:glow-star 2s infinite ease-in-out alternate 5s;}
			.glowing_stars .star:nth-child(4){top:75%; left:80%; animation:glow-star 2s infinite ease-in-out alternate 7s;}
			.glowing_stars .star:nth-child(5){top:90%; left:50%; animation:glow-star 2s infinite ease-in-out alternate 9s;}

			@media only screen and (max-width:600px){
				.navbar-links{display:none;}
				.custom-navbar{text-align:center;}
				.brand-logo img{width:120px;}
				.box_astronaut{top:70%;}
				.central-body{padding-top:25%;}
			}
        </style>
        <div class="stars">
            <div class="custom-navbar">
                <div class="brand-logo">
                    <a href="/" style="text-decoration:none; font-family:'Dancing Script', cursive; color:#fff;">NoEl.od.ua</a>
                </div>
                <div class="navbar-links">
                    <ul>
                      <li><a href="/">Главная</a></li>
                      <li><a href="/cat/nabor_v_krovatku">Набор в кроватку</a></li>
                      <li><a href="/cat/pledy_i_konverty">Пледы и конверты</a></li>
                      <li><a href="/cat/kokony">Коконы</a></li>
                      <li><a href="/cat/postelnye_prinadlezhnosti">Постельные принадлежности</a></li>
                      <li><a href="/cat/pelenki">Пеленки</a></li>
                      <li><a href="/contact" class="btn-request">Контакты</a></li>
                    </ul>
                </div>
            </div>
            <div class="central-body">
                <img class="image-404" src="/public/img/error/404.svg" width="300px">
                <a href="/" class="btn-go-home" target="_blank">На Главную</a>
            </div>
            <div class="objects">
                <img class="object_rocket" src="/public/img/error/rocket.svg" width="40px">
                <div class="earth-moon">
                    <img class="object_earth" src="/public/img/error/earth.svg" width="100px">
                    <img class="object_moon" src="/public/img/error/moon.svg" width="80px">
                </div>
                <div class="box_astronaut">
                    <img class="object_astronaut" src="/public/img/error/astronaut.svg" width="140px">
                </div>
            </div>
            <div class="glowing_stars">
                <div class="star"></div>
                <div class="star"></div>
                <div class="star"></div>
                <div class="star"></div>
                <div class="star"></div>
            </div>
        </div>
    </body>
</html>