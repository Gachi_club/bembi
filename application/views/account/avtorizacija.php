<!--
<style type="text/css">
	.razrabotka img{width:100%;}
</style>
<section class="razrabotka my-5">
	<div class="container">
		<div class="row">
			<div class="col-4">
				<img src="/public/img/razrabotka.png" alt="">
			</div>
			<div class="col-8 align-self-center">
				<h1>Страница находится в разработке</h1>
				<p>Пожалуйста, перейдите на <a href="/">главную страницу</a></p>
			</div>
		</div>
	</div>
</section>
-->

<style type="text/css">
	.acc-login h1{font-size:2vw;}
	.acc-login .row{margin:1rem 0;}
</style>
<section class="acc-login mt-2 mb-5">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1>Страница авторизации</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-6 mx-auto text-center">
				<form action="/account/login" method="post">
					<p class="my-2">
						<input type="text" name="login" class="py-1 px-2 w-50" placeholder="Логин">
					</p>
					<p class="my-2">
						<input type="text" name="password" class="py-1 px-2 w-50" placeholder="Пароль">
					</p>
					<p class="my-2">
						<button type="submit" name="enter" class="btn btn-success py-1 px-4">Войти</button>
					</p>
				</form>
			</div>
		</div>
	</div>
</section>