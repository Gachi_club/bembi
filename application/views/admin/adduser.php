<style type="text/css">
	.adminadd{}
	.adminadd p{margin:.5rem 0;}
	.adminadd input{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.adminadd textarea{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.adminadd select{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.adminadd input[type=date]{border:1px solid #a9a9a9;}
</style>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2"><?=$title;?></h1>
	</div>

	<div class="row adminadd">
		<div class="col-5">
			<form action="/admin/adduser" method="post" enctype="multipart/form-data" id="postadd">
				<p><input type="text" name="alias" placeholder="Alias" class="w-100"></p>
				<p><input type="date" name="date" class="w-100"></p>
				<p><input type="text" name="seo-title" placeholder="SEO Title" class="w-100"></p>
				<p><textarea name="seo-desc" placeholder="SEO Description" class="w-100"></textarea></p>
				<p><input type="text" name="seo-key" placeholder="SEO Keywords" class="w-100"></p>
				<p><input type="text" name="title" placeholder="Title" class="w-100"></p>
				<p><textarea name="text" placeholder="Text" class="w-100"></textarea></p>
				<p><input type="file" name="image"></p>
				<p>
					<select name="visible" class="w-100">
						<option disabled>Выберите видимость:</option>
						<option selected value="нет">Черновик</option>
						<option value="да">Опубликовано</option>
					</select>
				</p>
				<p>
					<button type="submit" class="btn btn-success" form="postadd">Добавить новость</button>
				</p>
			</form>
		</div>
		<div class="col-7">
			
		</div>
	</div>
</main>