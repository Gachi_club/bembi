<style type="text/css">
	.postedd{}
	.postedd p{margin:.5rem 0;}
	.postedd input{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.postedd textarea{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.postedd select{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.postedd input[type=date]{border:1px solid #a9a9a9;}

	input[name=news_alias]{background-color:#e8e8e8!important;}
</style>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2"><?=$title;?></h1>
	</div>
	<div class="row postedd mb-5">
		<div class="col-12">
			<form action="/admin/editpost/<?=$data['news_id'];?>" method="post" enctype="multipart/form-data" id="postedit">
				<input type="hidden" name="editpost" value="1" form="postedit">
				<div class="row">
					<div class="col-6"><p><input type="text" name="news_alias" placeholder="Алиас" value="<?=$data['news_alias'];?>" class="w-100" form="postedit" readonly></p></div>
					<div class="col-6"><p><input type="date" name="news_date" class="w-100" value="<?=$data['news_date'];?>" form="postedit"></p></div>
				</div>
				<div class="row">
					<div class="col-12">
						<p><input type="text" name="news_title" placeholder="Заголовок" value="<?=$data['news_title'];?>" class="w-100" form="postedit"></p>
						<p><textarea name="news_text" class="ckeditor" id="editor" form="postedit"><?=$data['news_text'];?></textarea></p>
					</div>
				</div>
				<?php
				if(file_exists('public/news/'.$data['news_imglink']) && $data['news_imglink'] != ""){
					$pathimg = '/public/news/'.$data['news_imglink']; $figcaption = $data['news_imglink'];
				}else{$pathimg = '/public/img/no_photo.png'; $figcaption = 'Изображение отсутствует';}

				echo '
				<div class="row">
					<div class="col-12 text-center">
						<figure>
							<p><img src="'.$pathimg.'" style="width:250px;"></p>
							<figcaption>'.$figcaption.'</figcaption>
						</figure>
					</div>
				</div>
				';
				?>
				<div class="row">
					<div class="col-6">
						<p><input type="file" name="news_imglink" form="postedit"></p>
					</div>
					<div class="col-6">
						<p>
							<select name="news_visible" class="w-100" form="postedit">
								<?php
								if($data['news_visible'] == 'да'){
									echo '
									<option disabled>Выберите видимость:</option>
									<option value="нет">Черновик</option>
									<option selected value="да">Опубликовано</option>
									';
								}elseif($data['news_visible'] == 'нет'){
									echo '
									<option disabled>Выберите видимость:</option>
									<option selected value="нет">Черновик</option>
									<option value="да">Опубликовано</option>
									';
								}else{
									echo '
									<option selected disabled>Выберите видимость:</option>
									<option value="нет">Черновик</option>
									<option value="да">Опубликовано</option>
									';
								}
								?>
							</select>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-12 text-right">
						<p>
							<button type="submit" class="btn btn-success" form="postedit">Редактировать новость</button>
						</p>
					</div>
				</div>
			</form>
		</div>
	</div>
</main>
<script type="text/javascript">
	var editor = CKEDITOR.replace('editor');
	editor.on( 'change', function(evt){
		var text = evt.editor.getData();
		$("#editor").text(text);
	});
</script>