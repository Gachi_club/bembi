<style>
	.users{}
	.users h1{font-size:2vw;}
	.users h2{font-size:1.8vw;}
	.users h3{font-size:1.6vw;}
	.users h4{font-size:1.4vw;}
	.users h5{font-size:1.2vw;}
	.users p{font-size:1vw;}
</style>
<main role="main" class="users col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1><?=$title;?></h1>
	</div>
	<div class="row mb-3">
		<div class="col-12 font-italic">
			<p><span style="color:red;">Внимание!</span> Для редактирования определенного клиента, перейдите в режим его просмотра. Для этого требуется нажать на изображение "<i class="fas fa-eye"></i>".</p>
			<p>Режим удаления пользователей отключен.</p>
		</div>
	</div>
	<div class="row my-3">
		<div class="col-12">
			<style>
				table.table{border:1px solid #dee2e6!important;}
				.table th, .table td{font-size:1vw; border:1px solid #dee2e6;}
			</style>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th></th>
						<th class="text-center">Дата регистрации</th>
						<th>Имя пользователя</th>
						<th class="text-center">Телефон</th>
						<th class="text-center">E-mail</th>
						<th class="text-center">IPадрес</th>
					</tr>
				</thead>
				<tbody>
			<?php
			foreach($users as $val){
				if($val['p_visible'] == 'да'){$visible = 'Опубликовано';}
				elseif($val['p_visible'] == 'нет'){$visible = 'Черновик';}
				echo '
					<tr>
						<td class="text-center"><a href="/admin/edituser/'.$val['user_id'].'" title="Редактировать пользователя"><i class="fas fa-eye"></i></a></td>
						<td class="text-center">'.$val['user_date'].'</td>
						<td>'.$val['user_name'].'</td>
						<td class="text-center">'.$val['user_phone'].'</td>
						<td class="text-center">'.$val['user_email'].'</td>
						<td class="text-center">'.$val['user_ip'].'</td>
					</tr>
				';
			}
			?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<?php if(!empty($users)){echo $pagination;}?>
		</div>
	</div>
</main>