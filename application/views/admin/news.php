<style>
	.news{}
	.news h1{font-size:2vw;}
	.news h2{font-size:1.8vw;}
	.news h3{font-size:1.6vw;}
	.news h4{font-size:1.4vw;}
	.news h5{font-size:1.2vw;}
	.news p{font-size:1vw;}
	.news table.table{border:1px solid #dee2e6!important;}
	.news table th, .news table td{font-size:1vw; border:1px solid #dee2e6;}
</style>
<main role="main" class="news col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1><?=$title;?></h1>
	</div>
	<div class="row my-3">
		<div class="col-12 text-right">
			<a href="/admin/addpost" class="btn btn-success">Добавить новость</a>
		</div>
	</div>
	<div class="row mb-3">
		<div class="col-12 font-italic">
			<p><span style="color:red;">Внимание!</span> Для редактирования определенной новости, перейдите в режим его просмотра. Для этого требуется нажать на изображение "<i class="fas fa-eye"></i>".</p>
			<p>Режим удаления новостей отключен.</p>
		</div>
	</div>
	<div class="row my-3">
		<div class="col-12">
			<style>
				table.table{border:1px solid #dee2e6!important;}
				.table th, .table td{font-size:1vw; border:1px solid #dee2e6;}
			</style>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th></th>
						<th>Дата создания</th>
						<th>Заголовок</th>
						<th>Текст</th>
						<th>Видимость</th>
						<!-- <th>Удалить</th> -->
					</tr>
				</thead>
				<tbody>
			<?php
			foreach($news as $val){
				if($val['news_visible'] == 'да'){$visible = 'Опубликовано';}
				elseif($val['news_visible'] == 'нет'){$visible = 'Черновик';}

				if(isset($val['news_text']) && !empty($val['news_text']) && $val['news_text'] != ''){
					$news_text = substr($val['news_text'], 0, 250);
					$news_text = rtrim($news_text, '?!,.-');
					$news_text = substr($news_text, 0, strrpos($news_text, ' '));
				}else{$news_text = '';}
		
				echo '
					<tr>
						<td class="text-center"><a href="/admin/editpost/'.$val['news_id'].'" title="Редактировать новость"><i class="fas fa-eye"></i></a></td>
						<td>'.$val['news_date'].'</td>
						<td>'.$val['news_title'].'</td>
						<td>'.$news_text.'</td>
						<td>'.$visible.'</td>
						<!-- <td class="text-center">
							<a href="/admin/deletepost/'.$val['news_id'].'"><img src="/public/img/close.png" alt=""></a>
						</td> -->
					</tr>
				';
			}
			?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<?php if(!empty($news)){echo $pagination;}?>
		</div>
	</div>
</main>