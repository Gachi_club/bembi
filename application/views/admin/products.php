<style>
	.products{}
	.products h1{font-size:2vw;}
	.products h2{font-size:1.8vw;}
	.products h3{font-size:1.6vw;}
	.products h4{font-size:1.4vw;}
	.products h5{font-size:1.2vw;}
	.products p{font-size:1vw;}
</style>
<main role="main" class="products col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1><?=$title;?></h1>
	</div>
	<div class="row my-3">
		<div class="col-12 text-right">
			<a href="/admin/addproduct" class="btn btn-success">Добавить товар</a>
		</div>
	</div>
	<div class="row mb-3">
		<div class="col-12 font-italic">
			<p><span style="color:red;">Внимание!</span> Для редактирования определенного товара, перейдите в режим его просмотра. Для этого требуется нажать на изображение "<i class="fas fa-eye"></i>".</p>
			<p>Режим удаления товара отключен.</p>
		</div>
	</div>
	<div class="row my-3">
		<div class="col-12 table-responsive">
			<style>
				table.table{border:1px solid #dee2e6!important;}
				.table th, .table td{font-size:1vw; border:1px solid #dee2e6;}
				.table td img{width:30px;}
			</style>
			<table class="table table-striped table-sm table-hover">
				<thead>
					<tr>
						<th scope="col"></th>
						<th scope="col" class="text-center">Артикул</th>
						<th scope="col" class="text-center">Фото</th>
						<th>Название</th>
						<th scope="col" class="text-center">Категория</th>
						<th scope="col" class="text-center">Цена</th>
<th scope="col" class="text-center">Наличие</th>
<th scope="col" class="text-center">Главная (да/нет)</th>
						<th scope="col" class="text-center">Видимость</th>
						<!-- <th scope="col" class="text-center">Удалить</th> -->
					</tr>
				</thead>
				<tbody>
			<?php
			$count_prod = count($products);
			for($i=0; $i <= ($count_prod-1); $i++){
			
if($products[$i]['p_nalichie'] == 'да'){$nalichie = 'В наличии';}
elseif($products[$i]['p_nalichie'] == 'нет'){$nalichie = 'Нет в наличии';}

if($products[$i]['p_popular'] == 'да'){$popular = 'Главная';}
elseif($products[$i]['p_popular'] == 'нет'){$popular = 'Нет';}

				if($products[$i]['p_visible'] == 'да'){$visible = 'Опубликовано';}
				elseif($products[$i]['p_visible'] == 'нет'){$visible = 'Черновик';}

				if(file_exists('public/products/'.$products[$i]['p_imglink']) && $products[$i]['p_imglink'] != ""){
					$pathimg = '/public/products/'.$products[$i]['p_imglink'];
				}else{$pathimg = '/public/img/no_photo.png';}
				
				for($z = 0; $z <= count($categories); $z++){
					if($products[$i]['p_catid'] == $categories[$z]['cat_id']){$cat_name = $categories[$z]['cat_title'];}
				}
				
				echo '
					<tr>
						<td class="text-center"><a href="/admin/editproduct/'.$products[$i]['p_id'].'" title="Редактировать товар"><i class="fas fa-eye"></i></a></td>
						<td class="text-center">'.$products[$i]['p_vendor_code'].'</td>
						<td class="text-center"><img src="'.$pathimg.'" alt="'.$products[$i]['p_name'].'"></td>
						<td>'.$products[$i]['p_name'].'</td>
						<td class="text-center">'.$cat_name.'</td>
						<td class="text-center">'.$products[$i]['p_price'].'</td>
<td class="text-center">'.$nalichie.'</td>
<td class="text-center">'.$popular.'</td>
						<td class="text-center">'.$visible.'</td>
						<!-- <td class="text-center"><a href="/admin/deleteproduct/'.$products[$i]['p_id'].'"><img src="/public/img/close.png" alt=""></a></td> -->
					</tr>
				';
			}
			?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<?php if(!empty($products)){echo $pagination;}?>
		</div>
	</div>
</main>