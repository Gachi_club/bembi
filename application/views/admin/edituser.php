<style type="text/css">
	.useredit{}
	.useredit p{margin:.5rem 0;}
	.useredit input{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.useredit textarea{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.useredit select{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.useredit input[type=date]{border:1px solid #a9a9a9;}
</style>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2"><?=$title;?></h1>
	</div>

	<div class="row useredit">
		<div class="col-12">
			<form action="/admin/edituser/<?=$user[0]['user_id'];?>" method="post" enctype="multipart/form-data">
				<p><input type="date" name="date" value="<?=$user[0]['user_date'];?>" class="w-100"></p>
				<p><input type="text" name="login" value="<?=$user[0]['user_login'];?>" class="w-100" required></p>
				<p><input type="text" name="pass" value="<?=$user[0]['user_pass'];?>" class="w-100" required></p>
				<p><input type="text" name="name" value="<?=$user[0]['user_name'];?>" class="w-100" required></p>
				<p><input type="tel" name="phone" value="<?=$user[0]['user_phone'];?>" class="w-100" required></p>
				<p><input type="email" name="email" value="<?=$user[0]['user_email'];?>" class="w-100" required></p>
				<p><input type="text" name="ip" value="<?=$user[0]['user_ip'];?>" class="w-100"></p>
				<p class="text-right">
					<button type="submit" class="btn btn-success">Редактировать пользователя</button>
				</p>
			</form>
		</div>
	</div>
</main>