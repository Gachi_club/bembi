<style type="text/css">
	.admlogin{}
	.adminlogin h1{font-size:2vw;}
	.admlogin .row{margin:.5rem 0;}
	.admlogin p{margin:1rem 0;}
</style>
<section class="admlogin">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 text-center">
				<h1><?=$title;?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-6 mx-auto text-center">
				<form action="/admin/login/" method="post">
					<input type="hidden" name="form_submit" value="login">
					<p><input type="text" name="login" class="py-1 px-2" placeholder="Логин"></p>
					<p><input type="text" name="password" class="py-1 px-2" placeholder="Пароль"></p>
					<p><button type="submit" name="submit" class="py-1 px-4 btn btn-success">Войти</button></p>
				</form>
			</div>
		</div>
	</div>
</section>
