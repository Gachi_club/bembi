<style>
	.editorder{}
	.editorder h1{font-size:2vw;}
	.editorder h2{font-size:1.8vw;}
	.editorder h3{font-size:1.6vw;}
	.editorder h4{font-size:1.4vw;}
	.editorder h5{font-size:1.2vw;}
	.editorder p{font-size:1vw; margin:.5rem 0;}

	.editorder input{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.editorder textarea{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.editorder select{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.editorder input[type=date]{border:1px solid #a9a9a9;}
	.editorder input:read-only, .editorder textarea:read-only{background:#e2e2e2;}
</style>
<main role="main" class="editorder col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1>Редактирование заявки № <?=$this->route['id'];?> от <?=$phone[0]['ph_date'];?></h1>
	</div>
	<div class="row my-3">
		<div class="col-12">
			<form action="/admin/editphones/<?=$phone[0]['ph_id'];?>" method="post">
				<div class="row">
					<div class="col-12">
						<input type="hidden" name="ph_id" value="<?=$phone[0]['ph_id'];?>">
						<p><input type="date" name="ph_date" value="<?=$phone[0]['ph_date'];?>" class="w-100" readonly></p>
						<p><input type="text" name="ph_name" value="<?=$phone[0]['ph_name'];?>" class="w-100" readonly></p>
						<p><input type="email" name="ph_email" value="<?=$phone[0]['ph_email'];?>" class="w-100" readonly></p>
						<p><input type="tel" name="ph_phone" value="<?=$phone[0]['ph_phone'];?>" class="w-100" readonly></p>
						<p><textarea name="ph_text" class="w-100" readonly><?=$phone[0]['ph_text'];?></textarea></p>
						<p>
							<select name="ph_status_id" class="w-100">
								<?php
								if($phone[0]['ps_id'] == '1'){
									echo '
								<option disabled>Выберите статус:</option>
								<option selected value="1">Новый</option>
								<option value="2">Обработан</option>
								<option value="3">Отменен</option>
									';
								}elseif($phone[0]['ps_id'] == '2'){
									echo '
								<option disabled>Выберите статус:</option>
								<option value="1">Новый</option>
								<option selected value="2">Обработан</option>
								<option value="3">Отменен</option>
									';
								}elseif($phone[0]['ps_id'] == '3'){
									echo '
								<option disabled>Выберите статус:</option>
								<option value="1">Новый</option>
								<option value="2">Обработан</option>
								<option selected value="3">Отменен</option>
									';
								}else{
									echo '
								<option selected disabled>Выберите статус:</option>
								<option value="1">Новый</option>
								<option value="2">Обработан</option>
								<option value="3">Отменен</option>
									';
								}
								?>
							</select>
						</p>
					</div>
				</div>
				<div class="row my-3">
					<div class="col-12 text-right">
						<button type="submit" class="btn btn-success">Обновить данные</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</main>