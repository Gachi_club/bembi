<style type="text/css">
	.adminaddslider p{margin:.5rem 0;}
	.adminaddslider input{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.adminaddslider textarea{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.adminaddslider select{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.adminaddslider input[type=date]{border:1px solid #a9a9a9;}
	.adminaddslider input[type=file]{padding:.25rem .5rem; border:1px solid #a9a9a9;}
</style>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2"><?=$title;?></h1>
	</div>

	<div class="row adminaddslider mb-5">
		<div class="col-12">
			<form action="/admin/addslider" method="post" enctype="multipart/form-data" id="slideradd">
				<input type="hidden" name="addslider" class="addform" value="1" form="slideradd">
				<div class="row">
					<div class="col-12">
						<p>
							<input type="text" name="s_link" placeholder="Ссылка на страницу" class="w-100 addform" form="slideradd">
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<p>
							<textarea name="s_alt" placeholder="ALT Описание изображения" class="w-100" form="slideradd"></textarea>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-6 text-center">
						<p><input type="file" name="s_imglink" class="w-100 addform" form="slideradd"></p>
					</div>
					<div class="col-6">
						<p>
							<select name="s_visible" class="w-100 addform" form="slideradd">
								<option selected disabled>Выберите видимость:</option>
								<option value="нет">Черновик</option>
								<option value="да">Опубликовано</option>
							</select>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-12 text-right">
						<p><button type="submit" class="btn btn-success" form="slideradd">Добавить слайдер</button></p>
					</div>
				</div>
			</form>
		</div>
	</div>
</main>