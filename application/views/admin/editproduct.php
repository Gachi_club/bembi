<style type="text/css">
	.admineditproduct{}
	main h1{font-size:2vw;}
	main h2{font-size:1.8vw;}
	main h3{font-size:1.6vw;}
	main h4{font-size:1.4vw;}
	main h5{font-size:1.2vw;}
	main p{font-size:1vw;}
	.admineditproduct p{margin:.5rem 0;}
	.admineditproduct input.addform {font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9; background:#fff;}
	.admineditproduct textarea{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.admineditproduct select{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.admineditproduct input[type=date]{border:1px solid #a9a9a9;}
	.product-select-color{box-sizing:border-box; -moz-box-sizing:border-box; width:100%; height:175px; border-radius:5px; padding:10px; float:right; border:1px solid #407690; background:rgba(0,0,0,.1);}
	.box-color {width:25px; height:25px; border-radius:5px; margin:0 10px 10px 0; float:left;}
	.colorpicker{margin:0 auto;}
	input:read-only{background-color:#e8e8e8!important;}
.admineditproduct label{margin-right:15px;}
.admineditproduct figure{border:1px solid #a9a9a9;}
</style>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1><?=$title;?></h1>
	</div>
<div class="row mb-3">
		<div class="col-12 font-italic">
			<p><span style="color:red;">Внимание!</span> Артикул товара может состоять из цифр и латинских символов [0-9A-Za-z].</p>
		</div>
	</div>
	<div class="row admineditproduct mb-5">
		<div class="col-12">
			<form action="/admin/editproduct/<?=$product['p_id'];?>" method="post" enctype="multipart/form-data" id="productedit">
				<div class="row">
					<div class="col-12">
						<p><input type="text" name="p_vendor_code" value="<?=$product['p_vendor_code'];?>" class="w-100 addform" form="productedit" title="Артикул"></p>
						<p><input type="text" name="p_name" value="<?=$product['p_name'];?>" class="w-100 addform" form="productedit" title="Наименование"></p>
						<p><textarea name="p_text" class="w-100 addform" rows="10" form="productedit" title="Описание товара"><?=$product['p_text'];?></textarea></p>
						<p><input type="text" name="p_colors" value="<?=$product['p_colors'];?>" class="w-100 addform" form="productedit" title="Цвет товара (тект)"></p>
					</div>
				</div>
				<div class="row my-2">
<?php
if(file_exists('public/products/'.$product['p_imglink'].'') && $product['p_imglink'] != ""){
	$pathimg = '/public/products/'.$product['p_imglink'];
}else{$pathimg = '/public/img/no_photo.png';}
echo '<div class="col text-center"><figure><p><img src="'.$pathimg.'" alt="'.$product['p_name'].'" style="width:200px; height:200px;"></p><figcaption><label for="f03">Изображение 1</label><input type="file" name="p_imglink" class="w-100 addform" form="productedit" id="f01"></figcaption></figure></div>';
if(file_exists('public/products/'.$product['p_imglink2'].'') && $product['p_imglink2'] != ""){
	$pathimg2 = '/public/products/'.$product['p_imglink2'];
}else{$pathimg2 = '/public/img/no_photo.png';}
echo '<div class="col text-center"><figure><p><img src="'.$pathimg2.'" alt="'.$product['p_name'].'" style="width:200px; height:200px;"></p><figcaption><label for="f03">Изображение 2</label><input type="file" name="p_imglink2" class="w-100 addform" form="productedit" id="f02"></figcaption></figure></div>';
if(file_exists('public/products/'.$product['p_imglink3'].'') && $product['p_imglink3'] != ""){
	$pathimg3 = '/public/products/'.$product['p_imglink3'];
}else{$pathimg3 = '/public/img/no_photo.png';}
echo '<div class="col text-center"><figure><p><img src="'.$pathimg3.'" alt="'.$product['p_name'].'" style="width:200px; height:200px;"></p><figcaption><label for="f03">Изображение 3</label><input type="file" name="p_imglink3" class="w-100 addform" form="productedit" id="f03"></figcaption></figure></div>';
?>
				</div>
<div class="row">
					<div class="col-6">
						<p>
							<select name="p_parent_catid" class="w-100 addform" form="productedit" title="Основная категория">
								<option disabled>Выберите основную категорию:</option>
							<?php
							for($i=0; $i < count($catparentid); $i++){
								if($catparentid[$i]['cat_id'] == $product['p_parent_catid']){
								echo '
								<option selected value="'.$catparentid[$i]['cat_id'].'">'.$catparentid[$i]['cat_title'].'</option>
								';
								}else{
								echo '
								<option value="'.$catparentid[$i]['cat_id'].'">'.$catparentid[$i]['cat_title'].'</option>
								';
								}
							}
							?>
							</select>
						</p>
					</div>
					<div class="col-6">
						<p>
							<select name="p_catid" class="w-100 addform" form="productedit" title="Суб-категория">
								<option disabled>Выберите дополнительную категорию:</option>
							<?php
							for($i=0; $i < count($catid); $i++){
								if($catid[$i]['cat_id'] == $product['p_catid']){
								echo '
								<option selected value="'.$catid[$i]['cat_id'].'">'.$catid[$i]['cat_title'].'</option>
								';
								}else{
								echo '
								<option value="'.$catid[$i]['cat_id'].'">'.$catid[$i]['cat_title'].'</option>
								';
								}
							}
							?>
							</select>
						</p>
					</div>
					<div class="col-6">
						<p><input type="text" name="p_material" value="<?=$product['p_material'];?>" class="w-100 addform" form="productedit" title="Материал"></p>
					</div>
					<div class="col-6">
						<p><input type="text" name="p_price" value="<?=$product['p_price'];?>" class="w-100 addform" form="productedit" title="Стоимость"></p>
					</div>
					<div class="col-6">
						<p>
							<select name="p_nalichie" class="w-100 addform" form="productedit" title="Наличие">
								<?php
								if($product['p_nalichie'] == 'да'){
									echo '
								<option disabled>Наличие товара (нет/да):</option>
								<option value="нет">Нет в наличии</option>
								<option selected value="да">Есть в наличии</option>
									';
								}elseif($product['p_nalichie'] == 'нет'){
									echo '
								<option disabled>Наличие товара (нет/да):</option>
								<option selected value="нет">Нет в наличии</option>
								<option value="да">Есть в наличии</option>
									';
								}else{
									echo '
								<option selected disabled>Наличие товара (нет/да):</option>
								<option value="нет">Нет в наличии</option>
								<option value="да">Есть в наличии</option>
									';
								}
								?>
							</select>
						</p>
					</div>
					<div class="col-6">
						<p>
							<select name="p_popular" class="w-100 addform" form="productedit" title="Главная страница">
								<?php
								if($product['p_popular'] == 'да'){
									echo '
								<option disabled>Главная страница (нет/да):</option>
								<option value="нет">Нет</option>
								<option selected value="да">Да</option>
									';
								}elseif($product['p_popular'] == 'нет'){
									echo '
								<option disabled>Главная страница (нет/да):</option>
								<option selected value="нет">Нет</option>
								<option value="да">Да</option>
									';
								}else{
									echo '
								<option selected disabled>Главная страница (нет/да):</option>
								<option value="нет">Нет</option>
								<option value="да">Да</option>
									';
								}
								?>
							</select>
						</p>
					</div>
					<div class="col-6">
						<p>
							<select name="p_visible" class="w-100 addform" form="productedit" title="Видимость">
								<?php
								if($product['p_visible'] == 'да'){
									echo '
								<option disabled>Видимость (нет/да):</option>
								<option value="нет">Черновик</option>
								<option selected value="да">Опубликовано</option>
									';
								}elseif($product['p_akciya'] == 'нет'){
									echo '
								<option disabled>Видимость (нет/да):</option>
								<option selected value="нет">Черновик</option>
								<option value="да">Опубликовано</option>
									';
								}else{
									echo '
								<option selected disabled>Видимость (нет/да):</option>
								<option value="нет">Черновик</option>
								<option value="да">Опубликовано</option>
									';
								}
								?>
							</select>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-6">
						<p class="text-center">
							<span class="btn btn-danger" id="btn-clear-input">Очистить форму</span>
						</p>
					</div>
					<div class="col-6">
						<p class="text-center">
							<input type="hidden" value="<?=$product['p_catid'];?>" id="pid">
							<input type="hidden" name="editproduct" value="1" form="productedit">
							<button type="submit" class="btn btn-success" form="productedit">Редактировать товар</button>
						</p>
					</div>
				</div>
			</form>
		</div>
	</div>
</main>
<script type="text/javascript">
	$(document).ready(function(){
		/***** СВЯЗАННЫЕ SELECT'ы *****/
		$("select[name='p_parent_catid']").bind("change", function(){
			$("select[name='p_catid']").empty();
			var value = $("select[name='p_parent_catid']").val();
			$.ajax({
				type:'POST',
				url: $('form').attr('action'),
				data:{
					'select_change':'category',
					'id':value,
				},
				dataType:'json',
				cache:false,
				success:function(result){
					$("select[name='p_catid']").append($('<option selected disabled>Выберите дополнительную категорию:</option>'));
					for(var i in result){
						if(result[i]['cat_id'] == $("#pid").val()){$("select[name='p_catid']").append($('<option selected value="'+result[i]['cat_id']+'">'+result[i]['cat_title']+'</option>'));}
						else{$("select[name='p_catid']").append($('<option value="'+result[i]['cat_id']+'">'+result[i]['cat_title']+'</option>'));}
					}
				}
			});
		});

		/***** ОЧИСТИТЬ (ДОБАВИТЬ ПРОДУКТ / РЕДАКТИРОВАТЬ ПРОДУКТ) *****/
		$('#btn-clear-input').click(function(){
			$('input.addform').val('');
			$('textarea.addform').val('');
			$('select.addform>option:selected').val('');
		});
	});
</script>