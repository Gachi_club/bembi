<style>
	.phones{}
	.phones h1{font-size:2vw;}
	.phones h2{font-size:1.8vw;}
	.phones h3{font-size:1.6vw;}
	.phones h4{font-size:1.4vw;}
	.phones h5{font-size:1.2vw;}
	.phones p{font-size:1vw;}
	.phones table.table{border:1px solid #dee2e6!important;}
	.phones table th, .phones table td{font-size:1vw; border:1px solid #dee2e6;}
</style>
<main role="main" class="phones col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1><?=$title;?></h1>
	</div>
	<div class="row mb-3">
		<div class="col-12 font-italic">
			<p><span style="color:red;">Внимание!</span> Для редактирования определенной заявки, перейдите в режим её просмотра. Для этого требуется нажать на изображение "<i class="fas fa-eye"></i>".</p>
			<p>Режим удаления заявок отключен.</p>
		</div>
	</div>
	<div class="row my-3">
		<div class="col-12">
			<style>
				table.table{border:1px solid #dee2e6!important;}
				.table th, .table td{font-size:1vw; border:1px solid #dee2e6;}
			</style>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th class="text-center"></th>
						<th class="text-center">ID заказа</th>
						<th class="text-center">Дата заявки</th>
						<th class="text-center">Имя</th>
						<th class="text-center">E-mail</th>
						<th class="text-center">Номер телефона</th>
						<th class="text-center">Текст</th>
						<th class="text-center">IP пользователя</th>
						<th class="text-center">Статус заявки</th>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach($phones as $val){
					echo '
					<tr>
						<td class="text-center">
							<a href="/admin/editphones/'.$val['ph_id'].'" title="Редактировать заявку"><i class="fas fa-eye"></i></a>
						</td>
						<td class="text-center">'.$val['ph_id'].'</td>
						<td>'.$val['ph_date'].'</td>
						<td class="text-center">'.$val['ph_name'].'</td>
						<td class="text-center">'.$val['ph_email'].'</td>
						<td class="text-center">'.$val['ph_phone'].'</td>
						<td class="text-center">'.$val['ph_text'].'</td>
						<td class="text-center">'.$val['ph_uip'].'</td>
						<td class="text-center">'.$val['ps_value'].'</td>
					</tr>
					';
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<?php if(!empty($phones)){echo $pagination;}?>
		</div>
	</div>
</main>