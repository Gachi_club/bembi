<style type="text/css">
	.adminaddpost{}
	.adminaddpost p{margin:.5rem 0;}
	.adminaddpost input{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.adminaddpost textarea{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.adminaddpost select{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.adminaddpost input[type=date]{border:1px solid #a9a9a9;}
</style>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2"><?=$title;?></h1>
	</div>

	<div class="row adminaddpost mb-5">
		<div class="col-12">
			<form action="/admin/addpost" method="post" enctype="multipart/form-data" id="postadd">
				<input type="hidden" name="addpost" class="addform" value="1" form="postadd">
				<div class="row">
					<div class="col-12">
						<p><input type="date" name="news_date" value="<?=date('Y-m-d');?>" class="w-100 addform" form="postadd"></p>
						<p><input type="text" name="news_title" placeholder="Заголовок" class="w-100 addform" form="postadd"></p>
						<p><textarea name="news_text" class="ckeditor" id="editor" form="postadd"></textarea></p>
					</div>
				</div>
				<div class="row">
					<div class="col-6">
						<p><input type="file" name="news_imglink" class="w-100 addform" form="postadd"></p>
					</div>
					<div class="col-6">
						<p>
							<select name="news_visible" class="w-100 addform" form="postadd">
								<option selected disabled>Выберите видимость:</option>
								<option value="нет">Черновик</option>
								<option value="да">Опубликовано</option>
							</select>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-12 text-right">
						<p>
							<button type="submit" class="btn btn-success" form="postadd">Добавить новость</button>
						</p>
					</div>
				</div>
			</form>
		</div>
	</div>
</main>
<script type="text/javascript">
	var editor = CKEDITOR.replace('editor');
	editor.on( 'change', function(evt){
		var text = evt.editor.getData();
		$("#editor").text(text);
	});
</script>