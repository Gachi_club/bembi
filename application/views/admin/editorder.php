<style>
	.editorder{}
	.editorder h1{font-size:2vw;}
	.editorder h2{font-size:1.8vw;}
	.editorder h3{font-size:1.6vw;}
	.editorder h4{font-size:1.4vw;}
	.editorder h5{font-size:1.2vw;}
	.editorder p{font-size:1vw; margin:.5rem 0;}

	.editorder input{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.editorder textarea{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.editorder select{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.editorder input[type=date]{border:1px solid #a9a9a9;}
</style>
<main role="main" class="editorder col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1>Редактирование заказа № <?=$this->route['id'];?> от <?=$order[0]['ord_date'];?></h1>
	</div>
	<div class="row my-3">
		<div class="col-12">
			<form action="/admin/editorder/<?=$order[0]['ord_id'];?>" method="post">
				<input type="hidden" name="form_submit" value="editorder">
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="detali-tab" data-toggle="tab" href="#detali" role="tab" aria-controls="detali" aria-selected="true">Детали заказа</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="itogi-tab" data-toggle="tab" href="#itogi" role="tab" aria-controls="itogi" aria-selected="false">Итоги</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="detali" role="tabpanel" aria-labelledby="detali-tab">
						<div class="row">
							<div class="col-12 col-lg-6">
								<p><input type="date" name="ord_date" value="<?=$order[0]['ord_date'];?>" class="w-100"></p>
							</div>
							<div class="col-12 col-lg-6">
								<p>
									<select name="os_id" class="w-100">
<?php
if($order[0]['os_id'] == '1'){echo '<option disabled>Выберите статус:</option><option selected value="1">Новый</option><option value="2">Обработан</option><option value="3">Отменен</option>';}
elseif($order[0]['os_id'] == '2'){echo '<option disabled>Выберите статус:</option><option value="1">Новый</option><option selected value="2">Обработан</option><option value="3">Отменен</option>';}
elseif($order[0]['os_id'] == '3'){echo '<option disabled>Выберите статус:</option><option value="1">Новый</option><option value="2">Обработан</option><option selected value="3">Отменен</option>';}
else{echo '<option selected disabled>Выберите статус:</option><option value="1">Новый</option><option value="2">Обработан</option><option value="3">Отменен</option>';}
?>
									</select>
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<input type="hidden" name="user_id" value="<?=$order[0]['user_id'];?>">
								<p><input type="text" name="user_name" value="<?=$order[0]['user_name'];?>" placeholder="ФИО" class="w-100"></p>
							</div>
							<div class="col-12">
								<p><input type="email" name="user_email" value="<?=$order[0]['user_email'];?>" placeholder="E-mail" class="w-100"></p>
							</div>
							<div class="col-12">
								<p><input type="tel" name="user_phone" value="<?=$order[0]['user_phone'];?>" placeholder="Телефон" class="w-100"></p>
							</div>
							<div class="col-12">
								<p><input type="text" name="ord_city" value="<?=$order[0]['ord_city'];?>" placeholder="Город доставки" class="w-100"></p>
							</div>
							<div class="col-12">
								<p><input type="text" name="ord_address" value="<?=$order[0]['ord_address'];?>" placeholder="Адрес доставки" class="w-100"></p>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-lg-6">
								<p>
									<select name="ord_dostavka" class="w-100">
<?php
if($order[0]['ord_dostavka'] == 'Новая Почта'){echo '<option disabled>Способ доставки</option><option selected value="Новая Почта">Новая Почта</option><option value="Автолюкс">Автолюкс</option><option value="Интайм">Интайм</option><option value="Деливери">Деливери</option><option value="Самовывоз">Самовывоз</option>';}
elseif($order[0]['ord_dostavka'] == 'Автолюкс'){echo '<option disabled>Способ доставки</option><option value="Новая Почта">Новая Почта</option><option selected value="Автолюкс">Автолюкс</option><option value="Интайм">Интайм</option><option value="Деливери">Деливери</option><option value="Самовывоз">Самовывоз</option>';}
elseif($order[0]['ord_dostavka'] == 'Интайм'){echo '<option disabled>Способ доставки</option><option value="Новая Почта">Новая Почта</option><option value="Автолюкс">Автолюкс</option><option selected value="Интайм">Интайм</option><option value="Деливери">Деливери</option><option value="Самовывоз">Самовывоз</option>';}
elseif($order[0]['ord_dostavka'] == 'Деливери'){echo '<option disabled>Способ доставки</option><option value="Новая Почта">Новая Почта</option><option value="Автолюкс">Автолюкс</option><option value="Интайм">Интайм</option><option selected value="Деливери">Деливери</option><option value="Самовывоз">Самовывоз</option>';}
elseif($order[0]['ord_dostavka'] == 'Самовывоз'){echo '<option disabled>Способ доставки</option><option value="Новая Почта">Новая Почта</option><option value="Автолюкс">Автолюкс</option><option value="Интайм">Интайм</option><option value="Деливери">Деливери</option><option selected value="Самовывоз">Самовывоз</option>';}
else{echo '<option selected disabled>Способ доставки</option><option value="Новая Почта">Новая Почта</option><option value="Автолюкс">Автолюкс</option><option value="Интайм">Интайм</option><option value="Деливери">Деливери</option><option value="Самовывоз">Самовывоз</option>';}
?>
									</select>
								</p>
							</div>
							<div class="col-12 col-lg-6">
								<p>
									<select name="ord_payment" class="w-100">
<?php
if($order[0]['ord_payment'] == 'Приват Банк'){echo '<option disabled>Способ оплаты</option><option selected value="Приват Банк">Приват Банк</option><option value="Наложеным платежом">Наложеным платежом</option>';}
elseif($order[0]['ord_payment'] == 'Наложеным платежом'){echo '<option disabled>Способ оплаты</option><option value="Приват Банк">Приват Банк</option><option selected value="Наложеным платежом">Наложеным платежом</option>';}
else{echo '<option selected disabled>Способ оплаты</option><option value="Приват Банк">Приват Банк</option><option value="Наложеным платежом">Наложеным платежом</option>';}
?>
									</select>
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<p>
									<textarea name="ord_user_comment" class="w-100">
<?php
if(!empty($order[0]['ord_user_comment'])){echo $order[0]['ord_user_comment'];}
else{echo 'Комментарий пользователя к заказу...';}
?>
									</textarea>
								</p>
							</div>
							<div class="col-12">
								<p>
									<textarea name="ord_adm_comment" class="w-100">
<?php
if(!empty($order[0]['ord_adm_comment'])){echo $order[0]['ord_adm_comment'];}
else{echo 'Комментарий менеджера к заказу...';}
?>
									</textarea>
								</p>
							</div>
						</div>
					</div>
<style type="text/css">
	#itogi img{width:50px;height:50px;}
	#itogi table.table{border:1px solid #dee2e6!important;}
	#itogi .table th, .table td{font-size:1vw;border:1px solid #dee2e6;}
	#itogi .cart-btn-color{border:1px solid #000;width:20px;height:20px;display:inline-block}
</style>
					<div class="tab-pane fade" id="itogi" role="tabpanel" aria-labelledby="itogi-tab">
						<div class="row">
							<div class="col-12">
								<table class="table table-striped table-sm">
									<thead>
										<tr>
											<!--<th scope="col"></th>-->
											<th scope="col" class="text-center">ID</th>
											<th scope="col" class="text-center">Артикул</th>
											<th scope="col" class="text-center">Фото</th>
											<th scope="col" class="text-center">Наименование</th>
											<th scope="col" class="text-center">Кол-во</th>
											<th scope="col" class="text-center">Цвет</th>
											<th scope="col" class="text-center">Цена за ед.</th>
											<th scope="col" class="text-center">Цена всего</th>
											<!--<th scope="col"></th>-->
										</tr>
									</thead>
									<tbody>
								<?php
for($i = 0;$i < $order[0]['ord_sum_prod'];$i++){
	$prodid = explode("|",$order[0]['ord_prod_id']);
	$prodcnt = explode("|",$order[0]['ord_prod_count']);
	$prodcolor = explode("|",$order[0]['ord_prod_color']);
	foreach($products as $prod){
		if($prod['p_id'] == $prodid[$i]){
			if(file_exists('public/bembiTMP/products/'.$prod['p_vendor_code'].'/'.$prod['p_imglink']) && $prod['p_imglink'] != ""){
				$pathimg = '/public/bembiTMP/products/'.$prod['p_vendor_code'].'/'.$prod['p_imglink'];
			}else{$pathimg = '/public/img/no_photo.png';}
			$sum = $prodcnt[$i] * $prod['p_opt_price'];
			echo '
			<tr>
				<!--<td class="text-center">
					<a href="/admin/editproduct/'.$prod['p_id'].'" title="Редактировать товар"><i class="fas fa-eye"></i></a>
				</td>-->
				<td class="text-center">'.$prod['p_id'].'</td>
				<td class="text-center">'.$prod['p_vendor_code'].'</td>
				<td class="text-center"><img src="'.$pathimg.'" alt="'.$prod['p_name'].'"></td>
				<td class="text-center">
					<a href="/product/'.$prod['p_alias'].'_'.$prod['p_vendor_code'].'" target="_blank">'.$prod['p_name'].'</a>
				</td>
				<td class="text-center">'.$prodcnt[$i].'</td>
				<td class="text-center">
<button class="cart-btn-color" style="background:'.$prodcolor[$i].'"></button>
				</td>
				<td class="text-center">'.$prod['p_opt_price'].'</td>
				<td class="text-center">'.$sum.'</td>
			</tr>
			';
		}
	}
}
								?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="row my-3">
					<div class="col-6">
						<p><strong>Сумма к оплате:</strong> <?=$order[0]['ord_sum_price'];?> грн.</p>
					</div>
					<div class="col-6 text-right">
						<button type="submit" class="btn btn-success">Обновить данные</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</main>