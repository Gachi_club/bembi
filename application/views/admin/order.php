<style>
	.order{}
	.order h1{font-size:2vw;}
	.order h2{font-size:1.8vw;}
	.order p{font-size:1vw; margin:.5rem 0;}
	.order img{width:75px;}
	.order table{width:100%;}
	.order thead{background:rgba(0,0,0,.25);}
	.order thead th{text-align:center; padding:.3rem .5rem; font-size:1vw; font-weight:600;}
	.order tbody td{font-size:1vw;}
</style>
<?php
foreach($order as $val){
?>
<main role="main" class="order col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1><?=$title;?></h1>
	</div>
	<div class="row">
		<div class="col-4">
			<div class="row py-2" style="border:1px solid #000;">
				<div class="col-12">
					<h2>Основная информация</h2>
					<p><strong>Статус:</strong> <?=$val['os_value'];?></p>
					<p><strong>ФИО:</strong> <?=$val['user_name'];?></p>
					<p><strong>E-mail:</strong> <?=$val['user_email'];?></p>
					<p><strong>Номер телефона:</strong> <?=$val['user_phone'];?></p>
					<p><strong>Город доставки:</strong> <?=$val['ord_city'];?></p>
					<p><strong>Адрес доставки:</strong> <?=$val['ord_address'];?></p>
					<p><strong>Оплата:</strong> <?=$val['ord_payment'];?></p>
					<p><strong>Доставка:</strong> <?=$val['ord_dostavka'];?></p>
					<p><strong>Комментарий:</strong> <?=$val['ord_user_comment'];?></p>
					<p><strong>Заметка менеджера:</strong> <?=$val['ord_adm_comment'];?></p>
					<p class="text-center mt-3"><?php echo '<a href="/admin/editorder/'.$this->route['id'].'" class="btn btn-success">Редактировать</a>';?></p>
				</div>
			</div>
			<div class="row mt-3 py-2" style="border:1px solid #000;">
				<div class="col-12">
					<h2>Дополнительная информация</h2>
					<p><strong>IP:</strong> <?=$val['user_ip'];?></p>
					<p><strong>User-Agent:</strong> <i>в работе</i></p>
				</div>
			</div>
		</div>
		<div class="col-8">
			<div class="row ml-1" style="border:1px solid #000;">
				<div class="col-12" style="padding:0;">
					<h2 style="padding:0 1rem;">Товары</h2>
					<table border="1">
						<thead>
							<tr>
								<th>Фото</th>
								<th>Название</th>
								<th>Артикул</th>
								<th>Цена</th>
								<th>Кол-во</th>
								<th>Сумма</th>
							</tr>
						</thead>
						<tbody>
<?php
for($i = 0; $i < $val['ord_sum_prod']; $i++){
	$prodid = explode("|",$val['ord_prod_id']);
	$prodcnt = explode("|",$val['ord_prod_count']);

	$count_prod = count($products);
	for($x = 0; $x <= ($count_prod - 1); $x++){
		if($products[$x]['p_id'] == $prodid[$i]){
			if(file_exists('public/bembiTMP/products/'.$products[$x]['p_vendor_code'].'/'.$products[$x]['p_imglink']) && $products[$x]['p_imglink'] != ""){
				$pathimg = '/public/bembiTMP/products/'.$products[$x]['p_vendor_code'].'/'.$products[$x]['p_imglink'];
			}else{$pathimg = '/public/img/no_photo.png';}
			$sum = $prodcnt[$i] * $products[$x]['p_opt_price'];
			echo '
			<tr>
				<td class="text-center"><img src="'.$pathimg.'" alt=""></td>
				<td class="text-center"><a href="/product/'.$products[$x]['p_alias'].'_'.$products[$x]['p_vendor_code'].'" target="_blank">'.$products[$x]['p_name'].'</a></td>
				<td class="text-center">'.$products[$x]['p_vendor_code'].'</td>
				<td class="text-center">'.$products[$x]['p_opt_price'].' грн.</td>
				<td class="text-center">'.$prodcnt[$i].' шт.</td>
				<td class="text-center">'.$sum.' грн.</td>
			</tr>
			';
		}
	}
}
?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="row ml-1 mt-3 py-2" style="border:1px solid #000;">
				<div class="col-12">
					<p style="margin:0;">
						<strong>К оплате:</strong> <?=$val['ord_sum_price'];?> грн.
					</p>
				</div>
			</div>
		</div>
	</div>
</main>
<?php
}
?>