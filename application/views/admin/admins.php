<style>
	.admins{}
	.admins h1{font-size:2vw;}
	.admins h2{font-size:1.8vw;}
	.admins h3{font-size:1.6vw;}
	.admins h4{font-size:1.4vw;}
	.admins h5{font-size:1.2vw;}
	.admins p{font-size:1vw;}
	.admins table.table{border:1px solid #dee2e6!important;}
	.admins table th, .admins table td{font-size:1vw; border:1px solid #dee2e6;}
</style>
<main role="main" class="admins col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1><?=$title;?></h1>
	</div>
	<div class="row mb-3">
		<div class="col-12 font-italic">
			<p><span style="color:red;">Внимание!</span> Для редактирования определенного администратора, перейдите в режим его просмотра. Для этого требуется нажать на изображение "<i class="fas fa-eye"></i>".</p>
			<p>Режим удаления администраторов отключен.</p>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th class="text-center"></th>
						<th class="text-center">ID</th>
						<th class="text-center">Логин</th>
						<th class="text-center">Имя</th>
						<th class="text-center">E-mail</th>
						<th class="text-center">Телефон</th>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach($admins as $val){
					if($val['adm_role'] != 'Бог'){
					echo '
					<tr>
						<td class="text-center">
							<a href="/admin/editadmin/'.$val['adm_id'].'" title="Редактировать администратора"><i class="fas fa-eye"></i></a>
						</td>
						<td class="text-center">'.$val['adm_id'].'</td>
						<td class="text-center">'.$val['adm_login'].'</td>
						<td class="text-center">'.$val['adm_name'].'</td>
						<td class="text-center">'.$val['adm_email'].'</td>
						<td class="text-center">'.$val['adm_phone'].'</td>
					</tr>
					';
					}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<?php if(!empty($admins)){echo $pagination;}?>
		</div>
	</div>
</main>