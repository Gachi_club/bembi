<style type="text/css">
	.editadmin{}
	.editadmin p{margin:.5rem 0;}
	.editadmin input{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.editadmin textarea{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.editadmin select{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.editadmin input[type=date]{border:1px solid #a9a9a9;}
	.editadmin input:read-only{background-color:#e8e8e8!important;}
</style>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2"><?=$title;?></h1>
	</div>
	<div class="row editadmin mb-5">
		<div class="col-12">
			<form action="/admin/editadmin/<?=$admin['adm_id'];?>" method="post" enctype="multipart/form-data" id="adminedit">
				<input type="hidden" name="editadmin" value="1" form="adminedit">
				<div class="row">
					<div class="col-12">
						<p><input type="text" name="adm_login" value="<?=$admin['adm_login'];?>" class="w-100" placeholder="Логин администратора"></p>
						<p><input type="text" name="adm_pass" class="w-100" placeholder="Новый пароль администратора"></p>
						<p><input type="text" name="adm_name" value="<?=$admin['adm_name'];?>" class="w-100" placeholder="Имя администратора"></p>
						<p><input type="email" name="adm_email" value="<?=$admin['adm_email'];?>" class="w-100" placeholder="E-mail администратора"></p>
						<p><input type="tel" name="adm_phone" value="<?=$admin['adm_phone'];?>" class="w-100" placeholder="Телефон администратора"></p>
					</div>
				</div>
				<div class="row">
					<div class="col-12 text-right">
						<p>
							<button type="submit" class="btn btn-success" form="adminedit">Редактировать админа</button>
						</p>
					</div>
				</div>
			</form>
		</div>
	</div>
</main>