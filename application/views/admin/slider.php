<style>
	.slider{}
	.slider h1{font-size:2vw;}
	.slider h2{font-size:1.8vw;}
	.slider h3{font-size:1.6vw;}
	.slider h4{font-size:1.4vw;}
	.slider h5{font-size:1.2vw;}
	.slider p{font-size:1vw;}
	.slider table.table{border:1px solid #dee2e6!important;}
	.slider table th{vertical-align:middle;}
	.slider table th, .slider table td{font-size:1vw; border:1px solid #dee2e6;}
	.slider table td img.img-fluid{max-width:150px;}
	.slider table td{vertical-align:middle; text-align:center;}
</style>
<main role="main" class="slider col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1><?=$title;?></h1>
	</div>
	<div class="row my-3">
		<div class="col-12 text-right">
			<a href="/admin/addslider" class="btn btn-success">Добавить слайд</a>
		</div>
	</div>
	<div class="row mb-3">
		<div class="col-12 font-italic">
			<p><span style="color:red;">Внимание!</span> Для редактирования определенного слайда, перейдите в режим его просмотра. Для этого требуется нажать на изображение "<i class="fas fa-eye"></i>".</p>
			<p>Режим удаления слайдов отключен.</p>
		</div>
	</div>
	<div class="row my-3">
		<div class="col-12">
			<style>
				table.table{border:1px solid #dee2e6!important;}
				.table th, .table td{font-size:1vw; border:1px solid #dee2e6;}
			</style>
			<table class="table table-striped table-sm">
				<thead>
					<tr class="text-center">
						<th></th>
						<th>Изображение</th>
						<th>URL link</th>
						<th>ALT текст</th>
						<th>Видимость</th>
						<!--<th></th>-->
					</tr>
				</thead>
				<tbody>
			<?php
			foreach($slider as $val){
				if($val['s_visible'] == 'да'){$visible = 'Опубликовано';}
				elseif($val['s_visible'] == 'нет'){$visible = 'Черновик';}

				if(isset($val['s_alt']) && !empty($val['s_alt']) && $val['s_text'] != ''){
					$s_alt = substr($val['s_alt'], 0, 250);
					$s_alt = rtrim($s_alt, '?!,.-');
					$s_alt = substr($s_alt, 0, strrpos($s_alt, ' '));
				}else{$s_alt = 'Нет описания';}

				if(isset($val['s_imglink_webp']) && !empty($val['s_imglink_webp']) && $val['s_imglink_webp']!=''){
					$img_qresult = 'Да';
				}else{$img_qresult = 'Нет';}
		
				echo '
					<tr>
						<td><a href="/admin/editslider/'.$val['s_id'].'" title="Редактировать слайдер"><i class="fas fa-eye"></i></a></td>
						<td><img src="/public/slider/'.$val['s_imglink'].'" class="img-fluid" alt="'.$val['s_alt'].'"></td>
						<td>'.$val['s_link'].'</td>
						<td>'.$s_alt.'</td>
						<td>'.$visible.'</td>
						<!-- <td>
							<a href="/admin/deleteslider/'.$val['s_id'].'"><img src="/public/img/close.png" alt=""></a>
						</td>
						-->
					</tr>
				';
			}
			?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<?php if(!empty($slider)){echo $pagination;}?>
		</div>
	</div>
</main>