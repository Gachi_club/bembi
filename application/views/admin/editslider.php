<style type="text/css">
	.slideredd p{margin:.5rem 0;}
	.sldieredd input{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.slideredd textarea{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.slideredd select{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.slideredd figcaption{font-size:1vw;}
	.slideredd input[type=file]{padding:.25rem .5rem; border:1px solid #a9a9a9;}
</style>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2"><?=$title;?></h1>
	</div>
	<div class="row slideredd mb-5">
		<div class="col-12">
			<form action="/admin/editslider/<?=$data['s_id'];?>" method="post" enctype="multipart/form-data" id="slideredit">
				<input type="hidden" name="editslider" value="1" form="slideredit">
				<div class="row">
					<div class="col-12">
						<p>
							<input type="text" name="s_link" placeholder="Ссылка на страницу сайта. Например: /shop" value="<?=$data['s_link'];?>" class="w-100 addform" form="slideradd">
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<p>
							<textarea name="s_alt" placeholder="Описание изображения для поисковых систем" class="w-100" form="slideradd"><?=$data['s_alt'];?></textarea>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-12 text-center">
						<figure>
							<p><img src="/public/slider/<?=$data['s_imglink'];?>" style="width:250px;"></p>
							<figcaption>Изображение: <?=$data['s_imglink'];?></figcaption>
						</figure>
					</div>
				</div>
				<div class="row">
					<div class="col-6">
						<p><input type="file" name="s_imglink" form="slideredit"></p>
					</div>
					<div class="col-6">
						<p>
							<select name="s_visible" class="w-100" form="slideredit">
								<?php
								if($data['s_visible'] == 'да'){
									echo '
									<option disabled>Выберите видимость:</option>
									<option value="нет">Черновик</option>
									<option selected value="да">Опубликовано</option>
									';
								}elseif($data['s_visible'] == 'нет'){
									echo '
									<option disabled>Выберите видимость:</option>
									<option selected value="нет">Черновик</option>
									<option value="да">Опубликовано</option>
									';
								}else{
									echo '
									<option selected disabled>Выберите видимость:</option>
									<option value="нет">Черновик</option>
									<option value="да">Опубликовано</option>
									';
								}
								?>
							</select>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-12 text-right">
						<p><button type="submit" class="btn btn-success" form="slideredits">Редактировать слайдер</button></p>
					</div>
				</div>
			</form>
		</div>
	</div>
</main>