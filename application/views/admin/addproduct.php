<style type="text/css">
	.adminaddproduct{}
	.adminaddproduct p{margin:.5rem 0;}
	.adminaddproduct input.addform {font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.adminaddproduct textarea{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.adminaddproduct select{font-size:1vw; padding:.25rem .5rem; border:1px solid #a9a9a9;}
	.adminaddproduct input[type=date]{border:1px solid #a9a9a9;}
	
	.product-select-color{box-sizing:border-box; -moz-box-sizing:border-box; width:100%; height:175px; border-radius:5px; padding:10px; float:right; border:1px solid #407690; background:rgba(0,0,0,.1);}
	.box-color {width:25px; height:25px; border-radius:5px; margin:0 10px 10px 0; float:left;}

	.colorpicker{margin:0 auto;}
input:read-only{background-color:grey;}
.adminaddproduct label{margin-right:15px;}
</style>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2"><?=$title;?></h1>
	</div>
<div class="row mb-3">
		<div class="col-12 font-italic">
			<p><span style="color:red;">Внимание!</span> Артикул товара может состоять из цифр и латинских символов [0-9A-Za-z].</p>
		</div>
	</div>
	<div class="row adminaddproduct mb-5">
		<div class="col-12">
			<form action="/admin/addproduct" method="post" enctype="multipart/form-data" id="productadd">
				<div class="row">
					<div class="col-12">
						<p><input type="text" name="p_vendor_code" placeholder="Уникальный артикул товара" class="w-100 addform" form="productadd"></p>
						<p><input type="text" name="p_name" placeholder="Наименование товара" class="w-100 addform" form="productadd"></p>
						<p><textarea name="p_text" class="w-100 addform" rows="10" form="productadd">Описание товара...</textarea></p>
						<p><input type="text" name="p_colors" placeholder="Описание цвета товара" class="w-100 addform" form="productadd"></p>
					</div>
				</div>
				<div class="row">
					<div class="col text-center">
						<p><label for="f01">Изображение 1:</label><input type="file" name="p_imglink" class="addform" form="productadd" id="f01"></p>
					</div>
<div class="col text-center">
						<p><label for="f02">Изображение 2:</label><input type="file" name="p_imglink2" class="addform" form="productadd" id="f02"></p>
					</div>
<div class="col text-center">
						<p><label for="f03">Изображение 3:</label><input type="file" name="p_imglink3" class="addform" form="productadd" id="f03"></p>
					</div>
</div>
<div class="row">
					<div class="col-6">
						<p>
							<select name="p_parent_catid" class="w-100 addform" form="productadd">
								<option selected disabled>Выберите основную категорию:</option>
							<?php
							for($i=0; $i < count($catparentid); $i++){
								echo '
								<option value="'.$catparentid[$i]['cat_id'].'">'.$catparentid[$i]['cat_title'].'</option>
								';
							}
							?>
							</select>
						</p>
					</div>
					<div class="col-6">
						<p>
							<select name="p_catid" class="w-100 addform" form="productadd">
								<option selected disabled>Выберите дополнительную категорию:</option>
							<?php
							for($i=0; $i < count($catid); $i++){
								echo '
								<option value="'.$catid[$i]['cat_id'].'">'.$catid[$i]['cat_title'].'</option>
								';
							}
							?>
							</select>
						</p>
					</div>
					<div class="col-6">
						<p><input type="text" name="p_material" placeholder="Материал товара" class="w-100 addform" form="productadd"></p>
					</div>
					<div class="col-6">
						<p><input type="text" name="p_price" placeholder="Стоимость. Например: 90.50" class="w-100 addform" form="productadd"></p>
					</div>
					<div class="col-6">
						<p>
							<select name="p_nalichie" class="w-100 addform" form="productadd">
								<option selected disabled>Наличие товара (нет/да):</option>
								<option value="нет">Нет в наличии</option>
								<option value="да">Есть в наличии</option>
							</select>
						</p>
					</div>
					<div class="col-6">
						<p>
							<select name="p_popular" class="w-100 addform" form="productadd">
								<option selected disabled>Главная страница (нет/да):</option>
								<option value="нет">Нет</option>
								<option value="да">Да</option>
							</select>
						</p>
					</div>
					<div class="col-6">
						<p>
							<select name="p_visible" class="w-100 addform" form="productadd">
								<option selected disabled>Видимость (нет/да):</option>
								<option value="нет">Черновик</option>
								<option value="да">Опубликовано</option>
							</select>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-6">
						<p class="text-center">
							<span class="btn btn-danger" id="btn-clear-input">Очистить форму</span>
						</p>
					</div>
					<div class="col-6">
						<p class="text-center">
							<input type="hidden" name="addproduct" value="1" form="productadd">
							<button type="submit" class="btn btn-success" form="productadd">Добавить товар</button>
						</p>
					</div>
				</div>
			</form>
		</div>
	</div>
</main>
<script type="text/javascript">
	$(document).ready(function(){
		/***** СВЯЗАННЫЕ SELECT'ы *****/
		$("select[name='p_parent_catid']").bind("change", function(){
			$("select[name='p_catid']").empty();
			var value = $("select[name='p_parent_catid']").val();
			$.ajax({
				type:'POST',
				url:'/admin/addproduct',
				data:{
					'select_change':'category',
					'id':value,
				},
				dataType:'json',
				cache:false,
				success:function(result){
					$("select[name='p_catid']").append($('<option selected disabled>Выберите дополнительную категорию:</option>'));
					for(var i in result){
						$("select[name='p_catid']").append($('<option value="'+result[i]['cat_id']+'">'+result[i]['cat_title']+'</option>'));
					}
				}
			});
		});

		/***** ОЧИСТИТЬ (ДОБАВИТЬ ПРОДУКТ / РЕДАКТИРОВАТЬ ПРОДУКТ) *****/
		$('#btn-clear-input').click(function(){
			$('input.addform').val('');
			$('textarea.addform').val('');
			$('select.addform>option:selected').val('');
		});
	});
</script>