<style>
	.main{}
	.main h1{font-size:2vw;}
	.main h2{font-size:1.8vw;}
	.main h3{font-size:1.6vw;}
	.main h4{font-size:1.4vw;}
	.main h5{font-size:1.2vw;}
	.main p{font-size:1vw;}
	.main ul{margin:0;}
	.main ul li{font-size:1vw;}
	table.table{border:1px solid #dee2e6!important;}
	table th, table td{font-size:1vw; border:1px solid #dee2e6;}
</style>
<main role="main" class="main col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1><?=$ttl;?></h1>
	</div>
	<style>
		.stats p, .stats h3{color:#fff;}
		.stats .box{margin:1rem; padding:1rem; height:100px;}
	</style>
	<div class="row stats">
		<div class="col-3">
			<div class="box" style="background:#00c0ef;">
				<h3><?=$allorders;?></h3>
				<p>Кол-во заказов</p>
			</div>
		</div>
		<div class="col-3">
			<div class="box" style="background:#00a65a;">
				<h3><?=$allprofit.' ₴';?></h3>
				<p>Общая сумма заказов</p>
			</div>
		</div>
		<div class="col-3">
			<div class="box" style="background:#f39c12;">
				<h3><?=$allusers;?></h3>
				<p>Регистраций</p>
			</div>
		</div>
		<div class="col-3">
			<div class="box" style="background:#dd4b39;">
				<h3><?=$allproducts;?></h3>
				<p>Товаров в БД</p>
			</div>
		</div>
	</div>

	<div class="row my-3">
		<div class="col-12">
			<h4>Список заказов</h4>
			<div class="table-responsive">
				<table class="table table-striped table-sm">
					<thead>
						<tr>
							<th class="text-center"></th>
							<th class="text-center">ID заказа</th>
							<th>ФИО</th>
							<th class="text-center">Номер телефона</th>
							<th class="text-center">Статус</th>
							<th class="text-center">Сумма заказа</th>
							<th class="text-center">Дата создания</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach($orders as $val){
						echo '
						<tr>
							<td class="text-center"><a href="/admin/order/'.$val['ord_id'].'"><i class="fas fa-eye"></i></a></td>
							<td class="text-center">'.$val['ord_id'].'</td>
							<td>'.$val['user_name'].'</td>
							<td class="text-center">'.$val['user_phone'].'</td>
							<td class="text-center">'.$val['os_value'].'</td>
							<td class="text-center">'.$val['ord_sum_price'].' грн.</td>
							<td class="text-center">'.$val['ord_date'].'</td>
						</tr>
						';
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	

	<div class="row my-3">
		<div class="col-12">
			<h4>Сумма продаж</h4>
			<style>
				#myChart{width:100%; height:380px;}
			</style>
			<canvas class="my-4" id="myChart"></canvas>
		</div>
	</div>

<style type="text/css">
	#select-ph-status option[selected]{font-weight:bold;}
</style>
	<div class="row">
		<div class="col-12">
			<h4>Список заявок</h4>
			<div class="table-responsive">
				<table class="table table-striped table-sm">
					<thead>
						<tr>
							<th class="text-center">Дата и время заявки</th>
							<th class="text-center">Имя пользователя</th>
							<th class="text-center">Телефон пользователя</th>
							<th class="text-center">E-mail пользователя</th>
							<th class="text-center">Сообщение</th>
							<th class="text-center">IP адрес</th>
							<th class="text-center">Статус заявки</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach($phones as $val){
						echo '
						<tr>
							<td class="text-center">'.$val['ph_datetime'].'</td>
							<td class="text-center">'.$val['ph_name'].'</td>
							<td class="text-center">'.$val['ph_email'].'</td>
							<td class="text-center">'.$val['ph_phone'].'</td>
							<td class="text-center">'.$val['ph_text'].'</td>
							<td class="text-center">'.$val['ph_uip'].'</td>
						';
switch($val['ps_id']){
	case'1':
echo '
<td class="text-center">
<select id="select-ph-status" data-phid="'.$val['ph_id'].'" onchange="PhoneStatus();">
<option value="" disabled>Статус заявки</option>
<option value="1" selected>Новый (выбран)</option>
<option value="2">Обработан</option>
<option value="3">Отменен</option>
</select>
</td>
';
	break;
	case'2':
echo '
<td class="text-center">
<select id="select-ph-status" data-phid="'.$val['ph_id'].'" onchange="PhoneStatus();">
<option value="" disabled>Статус заявки</option>
<option value="1">Новый</option>
<option value="2" selected>Обработан (выбран)</option>
<option value="3">Отменен</option>
</select>
</td>
';
	break;
	case'3':
echo '
<td class="text-center">
<select id="select-ph-status" data-phid="'.$val['ph_id'].'" onchange="PhoneStatus();">
<option value="" disabled>Статус заявки</option>
<option value="1">Новый</option>
<option value="2">Обработан</option>
<option value="3" selected>Отменен (выбран)</option>
</select>
</td>
';
	break;
	default:
echo '
<td class="text-center">
<select id="select-ph-status" data-phid="'.$val['ph_id'].'" onchange="PhoneStatus();">
<option value="" selected disabled>Статус заявки</option>
<option value="1">Новый</option>
<option value="2">Обработан</option>
<option value="3">Отменен</option>
</select>
</td>
';
	break;
}
						echo '
						</tr>
						';
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</main>