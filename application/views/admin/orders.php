<style>
	.orders{}
	.orders h1{font-size:2vw;}
	.orders h2{font-size:1.8vw;}
	.orders h3{font-size:1.6vw;}
	.orders h4{font-size:1.4vw;}
	.orders h5{font-size:1.2vw;}
	.orders p{font-size:1vw;}
	.orders table.table{border:1px solid #dee2e6!important;}
	.orders table th, .orders table td{font-size:1vw; border:1px solid #dee2e6;}
</style>
<main role="main" class="orders col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1><?=$title;?></h1>
	</div>
	<div class="row mb-3">
		<div class="col-12 font-italic">
			<p><span style="color:red;">Внимание!</span> Для редактирования определенного заказа, перейдите в режим его просмотра. Для этого требуется нажать на изображение "<i class="fas fa-eye"></i>".</p>
			<p>Режим удаления заказов отключен.</p>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th class="text-center"></th>
						<th class="text-center">ID заказа</th>
						<th class="text-center">ФИО</th>
						<th class="text-center">Номер телефона</th>
						<th class="text-center">Статус</th>
						<th class="text-center">Сумма заказа</th>
						<th class="text-center">Дата создания</th>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach($orders as $val){
					echo '
					<tr>
						<td class="text-center"><a href="/admin/order/'.$val['ord_id'].'" title="Просмотреть заказ"><i class="fas fa-eye"></i></a></td>
						<td class="text-center">'.$val['ord_id'].'</td>
						<td>'.$val['user_name'].'</td>
						<td class="text-center">'.$val['user_phone'].'</td>
						<td class="text-center">'.$val['os_value'].'</td>
						<td class="text-center">'.$val['ord_sum_price'].' грн.</td>
						<td class="text-center">'.$val['ord_date'].'</td>
					</tr>
					';
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<?php if(!empty($orders)){echo $pagination;}?>
		</div>
	</div>
</main>