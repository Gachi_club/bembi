<?php
/*******************************************************************************
*  Title: Web shop «Bembi» - Default
*  Last update: Nov 12, 2019
*  Author: Nikita Kirilov (Elendarien)
*  Website: https://sinup.od.ua
*  E-mail: kirilovbiz@gmail.com
*******************************************************************************/
set_time_limit(0);
ini_set('memory_limit', '-1');
error_reporting(E_ALL & ~E_NOTICE);
ini_set("display_errors", 1);

switch($_SESSION['lang']){
	case'ua':$default=['head'=>'Ви використовуєте застарілий браузер. Для швидшого, безпечнішого перегляду безкоштовно оновіть Ваш браузер.','header'=>[''=>''],];break;
	case'en':$default=['head'=>'You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.','header'=>[''=>''],];break;
	default:$default=['head'=>'Вы используете устаревший браузер. Для более быстрого и безопасного просмотра бесплатно обновите Ваш браузер.','header'=>[''=>''],];break;
}
if($this->route['action'] == 'product' && isset($val['p_opt_price'])){
	$og_type = '<meta property="og:type" content="product">';
	$og_price_ammount = '<meta property="product:price:amount" content="'.$val['p_opt_price'].'">';
	$og_price_currency = '<meta property="product:price:currency" content="UAH">';
}else{$og_type='';$og_price_ammount='';$og_price_currency='';}
?>
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="<?=$_SESSION['lang'];?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="<?=$_SESSION['lang'];?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="<?=$_SESSION['lang'];?>">
<!--<![endif]-->
<!--<html lang="<?=$_SESSION['lang'];?>">-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="author" content="Кирилов Никита (Elendarien)">
	<meta name="robots" content="all">
	<meta name="copywright" content="Bembi.store">
	<?=$og_type;?>
	<meta property="og:title" content="<?=$og_ttl;?>">
	<meta property="og:description" content="<?=$og_desc;?>">
	<meta property="og:site_name" content="Bembi">
	<meta property="og:image" content="<?=$og_img;?>">
	<meta property="og:image:width" content="300">
	<meta property="og:image:height" content="300">
	<meta property="og:url" content="<?=$canonical;?>">
	<?=$og_price_ammount;?>
	<?=$og_price_currency;?>
	<meta name="twitter:title" content="<?=$og_ttl;?>">
	<meta name="twitter:description" content="<?=$og_desc;?>">
	<meta name="twitter:image" content="<?=$og_img;?>">
	<meta name="twitter:image:width" content="300">
	<meta name="twitter:image:height" content="300">
	<link rel="canonical" href="<?=$canonical;?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="/public/img/favicon.ico">
	<title><?=$title;?></title>
	<meta name="description" content="<?=$desc;?>">
	<meta name="keywords" content="<?=$keys;?>">

<!--[if lt IE 9]> 
	<script src="http://css3-mediaqueries-js.googlecode.com/files/css3-mediaqueries.js"></script>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> 
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!--[if lt IE 10]>
	<div style="background:#212121;padding:10px 0;box-shadow:3px 3px 5px 0 rgba(0,0,0,.3);clear:both;text-align:center;position:relative;z-index:1;">
		<a href="https://windows.microsoft.com/en-US/internet-explorer/">
			<img src="/public/img/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="<?=$default['head'];?>">
		</a>
	</div>
	<script type="text/javascript" src="/public/js/html5shiv.min.js"></script>
<![endif]-->

<!-- ****************************** ВАЖНЫЕ СТИЛИ САЙТА ****************************** -->
<link rel="stylesheet" type="text/css" href="/public/css/normalize.min.css">
<link rel="stylesheet" type="text/css" href="/public/bootstrap_4-4/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/public/css/style.css">
<link rel="stylesheet" type="text/css" href="/public/css/sweetalert.css">
<link rel="stylesheet" type="text/css" href="/public/css/hover-min.css">
<link rel="stylesheet" type="text/css" href="/public/fontawesome/css/all.min.css">
<link rel="stylesheet" type="text/css" href="/public/owlcarousel/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="/public/owlcarousel/owl.theme.default.min.css">
<link rel='stylesheet' type='text/css' href='/public/css/animate.min.css'>
<link rel='stylesheet' type='text/css' href='/public/css/jquery-ui.min.css'>
<link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700&amp;subset=latin-ext" rel="stylesheet">

<!-- ****************************** Подключение вебмастер кабинетов ******************************* -->
</head>
	<body>
		<a href="" id="go-top" class="animated infinite pulse delay-2s" title="Вверх"><i class="fas fa-arrow-up"></i></a>
		<style type="text/css">
			header{}
			.hdr-top{box-shadow:0px 5px 30px rgba(0, 92, 153, 0.15);}
			.hdr-menu{list-style-type:none} .hdr-menu li{float:left} .hdr-menu a{display:block;margin-right:25px;font-size:.8vw} .hdr-menu a:hover{} .hdr-menu img{height:13px;width:auto;margin-right:5px}
			.hdr-social{list-style-type:none} .hdr-social li{float:left} .hdr-social a{display:block; margin:0 7px;} .hdr-social img{height:24px;width:24px}
			.hdr-callback button{background:none;border:none;color:#f25540;font-size:1vw;font-weight:600;padding:0 6px;line-height:14px} .hdr-callback span{border-bottom:1px dotted #f25540}
			.hdr-middle{} .hdr-logo a{font-family:'Dancing Script', cursive;font-size:2.5vw;} .hdr-logo a:hover{text-decoration:none} .hdr-logo img{height:50px} .hdr-slogan{font-family:'Open Sans', Arial, sans-serif;font-weight:400;font-size:13px;letter-spacing:1px}
			.form-contr{height:43px;border-color:#e1e1e1;box-shadow:none;font-weight:600;font-size:14px;padding:11px 25px;position: relative;z-index:2;width:100%;margin-bottom:0;display:block;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out;float:left}
			.form-contr:focus::-webkit-input-placeholder{color:transparent;}
			.form-contr:focus::-moz-placeholder{color:transparent;}
			.input-group-btn{position:absolute;font-size:0;white-space:nowrap;margin-left:-50px;z-index:10} .btn-default{background:#005c99;border-color:#005c99;border-radius:6px;height:43px;width:50px;padding:9px 14px}
			.btn-default img{height:19px;width:auto;vertical-align:baseline}
			.input-group{position:relative;display:table;border-collapse:separate;vertical-align:middle}
			.hdr-phones{list-style-type:none} .hdr-phones a{display:inline-block;text-decoration:none;color:#005c99} .hdr-phones a:hover{color:#000}
			.hdr-buttons{list-style-type:none} .hdr-buttons li{width:67px;padding:.75rem 0} .hdr-buttons .hdr-btns-li p{font-size:.9vw;} .hdr-buttons .hdr-btns-li:hover{background:#fff;box-shadow:0px 2px 7px rgba(0, 92, 153, 0.15)} .hdr-buttons a{display:block;text-align:center;color:#9c9c9c;text-decoration:none;font-size:.8vw;} .hdr-buttons img{width:25px;height:25px;margin:0 auto;display:block} #cart-total{position:absolute;top:0px;right:0;background:#f25540;color:#fff;width:23px;height:23px;border-radius:50%;display:flex;align-items:center;justify-content:center;font-size:12px}
			.menucat{min-width:52.5rem} .menucat-ul{display:inline-block} .menucat-ul li{width:33.33%;float:left} .url-text{font-weight:600;color:#005c99} .url-text:hover{color:#000}
			.hdr-bottom{border-bottom:4px solid #eff6f9;} .hdr-bottom .nav-bembi{background:#005c99} .hdr-bottom .navbar-light .navbar-nav .nav-link{color:rgba(255,255,255,1);font-family:'Open Sans', Arial, sans-serif;text-transform:uppercase;font-size:14px} .hdr-bottom .navbar-light .navbar-nav .nav-link:hover{color:rgba(255,255,255,.8)}
			.navbar-dark .navbar-toggler{font-size:4vw;color:rgba(255,255,255,1);border-color:rgba(255,255,255,.1);}
			.navbar-dark .navbar-nav .nav-link{color:rgba(255,255,255,1);}
@media(max-width:576px){
	.hdr-logo a{font-size:7vw;}
	.menucat{min-width:0rem;max-width:18rem;}
	.menucat-ul li{width:100%;float:none;}
	.hdr-buttons .hdr-btns-li p{font-size:3.5vw;}
}
@media(min-width:576px){}
@media(min-width:768px){
	.hdr-logo a{font-size:4vw;}
	.menucat{min-width:0rem;max-width:46rem;}
	.menucat-ul li{width:50%;}
	.hdr-buttons .hdr-btns-li p{font-size:1.5vw;}
	.navbar-dark .navbar-toggler{font-size:3vw;}
}
@media(min-width:992px){

}
@media(min-width:1024px){
	.hdr-logo a{font-size:2.5vw;}
	.menucat{min-width:52.5rem;}
	.menucat-ul li{width:33.33%;}
	.navbar-expand-lg .navbar-nav .dropdown-menu{left:-7rem;}
	.navbar-expand-lg .navbar-nav .dropdown-menu.menucat.menucat-school{left:-10rem;}
	.navbar-expand-lg .navbar-nav .dropdown-menu.menucat{left:0;}
	.hdr-menu a{font-size:1vw}
}
@media(min-width:1200px){.hdr-buttons .hdr-btns-li p{font-size:.9vw;}.hdr-menu a{font-size:.8vw}}
		</style>
		<header>
			<div class="hdr-top d-none d-lg-block">
				<div class="container py-2">
					<div class="row">
						<div class="col-8" style="align-self:center;">
							<ul class="hdr-menu">
								<li><a href="/o-magazine"><img src="/public/img/icons/shop.svg" alt="О магазине"><span>О магазине</span></a></li>
								<li><a href="/detskaja-odezhda"><img src="/public/img/icons/wallet.svg" alt="Каталог детской и школьной одежды"><span>Каталог</span></a></li>
								<li><a href="/oplata-i-dostavka"><img src="/public/img/icons/delivery-white-blue.svg" alt="Оплата и доставка"><span>Оплата и доставка</span></a></li>
								<li><a href="/sertifikaty"><img src="/public/img/icons/quality.svg" alt="Наши сертификаты"><span>Сертификаты</span></a></li>
								<li><a href="/otzyvy"><img src="/public/img/icons/customer-reviews.svg" alt="Отзывы"><span>Отзывы</span></a></li>
								<li><a href="/kontakty"><img src="/public/img/icons/support.svg" alt="Контакты"><span>Контакты</span></a></li>
							</ul>
						</div>
						<div class="col-2">
							<ul class="hdr-social">
								<li><a href="https://www.facebook.com/bembi.store.ua/" rel="nofollow"><img src="/public/img/icons/facebook.svg" alt="Facebook"></a></li>
								<li><a href="https://www.instagram.com/bembi.store.ua/" rel="nofollow"><img src="/public/img/icons/instagram.svg" alt="Instagram"></a></li>
							</ul>
						</div>
						<div class="col-2 hdr-callback">
							<button data-toggle="modal" data-target="#phoneModal"><span>Заказать звонок</span></button>
						</div>
					</div>
				</div>
			</div>
			<div class="hdr-middle py-3">
				<div class="container-fluid">
					<div class="row">
						<div class="col-6 col-md-4 col-lg-3 text-center">
							<div class="hdr-logo">
								<?php
								if(file_exists('public/bembiTMP/logo.png')){
									echo '<a href="/"><img src="/public/img/no_photo.png" alt="Магазин детской одежды оптом" title="Интернет-магазин детской одежды оптом" class="lazy img-fluid"></a>';
								}else{
									echo '<a href="/" title="Интернет-магазин детской одежды оптом">Bembi.store</a>';
								}
								?>
							</div>
							<div class="hdr-slogan">Детская одежда оптом</div>
						</div>
						<div class="col-4 input-group d-none d-lg-block">
							<input type="text" class="form-contr" placeholder="Что будете искать?" aria-label="Search" autocomplete="off">
							<span class="input-group-btn">
								<button type="button" class="btn btn-default btn-lg" id="search">
									<img src="/public/img/icons/search.svg" alt="search">
								</button>
							</span>

						</div>
						<div class="col-3 col-md-6 col-lg-3 d-none d-md-block">
							<ul class="hdr-phones text-center">
								<li><a href="tel:+380680594606">+38 (068) 059-46-06</a></li>
								<li><a href="tel:+380508486830">+38 (050) 848-68-30</a></li>
								<li>Пн - Пт: <time>10:00</time> - <time>20:00</time></li>
							</ul>
						</div>
						<div class="col-6 col-md-2">
							<ul class="hdr-buttons">
								<!--
								<li><a href="/account/avtorizacija"><img src="/public/img/icons/user.svg" alt="login"><span>Вход</span></a></li>
								<li><a href="/account/izbrannoe"><img src="/public/img/icons/love.svg" alt="login"><span>Желаемое</span></a></li>
								-->
								<li class="hdr-btns-li mx-auto" style="position:relative;">
									<a href="/korzina">
										<img src="/public/img/icons/shopping-cart-empty-side-view.svg" alt="cart">
										<p><span id="cart-total"><?=$basketcount;?></span>Корзина</p>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="hdr-bottom">
				<div class="container-fluid">
					<div class="row">
						<div class="col-12 nav-bembi">
							<nav class="navbar navbar-expand-lg navbar-dark nav-bembi py-1 py-md-0 px-0">
	<button class="navbar-toggler mx-auto mx-md-0 mr-md-auto" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span> МЕНЮ
	</button>
	<div class="collapse navbar-collapse" id="navbarTogglerDemo02">
		<ul class="navbar-nav mx-auto mt-2 mt-lg-0">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Каталог детской одежды</a>
				<div class="dropdown-menu menucat" aria-labelledby="navbarDropdownMenuLink">
					<ul class="list-unstyled menucat-ul w-100">
						<?php
						foreach($allcats as $val){
echo '<li><a class="dropdown-item" href="/detskaja-odezhda/'.$val['cat_alias'].'">'.$val['cat_title'].'</a></li>';
						}
						?>
					</ul>
					<ul class="list-unstyled menucat-ul w-100">
						<li><a class="dropdown-item text-uppercase url-text" href="/detskaja-odezhda">Весь каталог детской одежды</a></li>
						<li><a class="dropdown-item text-uppercase url-text" href="/detskaja-odezhda/?pol=malchik">Детская одежда для мальчиков</a></li>
						<li><a class="dropdown-item text-uppercase url-text" href="/detskaja-odezhda/?pol=devochka">Детская одежда для девочек</a></li>
					</ul>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Школьная форма</a>
				<div class="dropdown-menu menucat menucat-school" aria-labelledby="navbarDropdownMenuLink">
					<ul class="list-unstyled menucat-ul w-100">
						<?php
						foreach($schoolcats as $val){
echo '<li><a class="dropdown-item" href="/detskaja-odezhda/'.$val['cat_alias'].'">'.$val['cat_title'].'</a></li>';
						}
						?>
					</ul>
					<ul class="list-unstyled menucat-ul w-100">
						<li><a class="dropdown-item text-uppercase url-text" href="/detskaja-odezhda/shkolnaja-forma">Вся школьная форма</a></li>
						<li><a class="dropdown-item text-uppercase url-text" href="/detskaja-odezhda/shkolnaja-forma/?pol=malchik" title="Школьная форма для мальчиков">Школьное для мальчиков</a></li>
						<li><a class="dropdown-item text-uppercase url-text" href="/detskaja-odezhda/shkolnaja-forma/?pol=devochka" title="Школьная форма для девочек">Школьное для девочек</a></li>
					</ul>
				</div>
			</li>
			<li class="nav-item"><a class="nav-link" href="/detskaja-odezhda/novinki">Новинки</a></li>
			<li class="nav-item"><a class="nav-link" href="/detskaja-odezhda/rasprodazha">Распродажа</a></li>
			<li class="nav-item"><a class="nav-link" href="/oplata-i-dostavka">Оплата и доставка</a></li>
			<li class="nav-item"><a class="nav-link" href="/otzyvy">Отзывы</a></li>
			<li class="nav-item"><a class="nav-link" href="/kontakty">Контакты</a></li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Инфо</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
					<a class="dropdown-item" href="/o-magazine">О Нас</a>
					<a class="dropdown-item" href="/sertifikaty">Сертификаты</a>
					<a class="dropdown-item" href="/novosti">Новости</a>
					<a class="dropdown-item" href="/kak-zakazat">Как заказать</a>
					<a class="dropdown-item" href="/organizatoram-sp">Организаторам СП</a>
				</div>
			</li>
		</ul>
	</div>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</header>

		<?=$content;?>
		
		<style type="text/css">
			footer{border-top:4px solid #eff6f9;}
			.ftr-logo{} .ftr-logo a{font-family:'Dancing Script', cursive;font-size:2.5vw;} .ftr-logo a:hover{text-decoration:none} .ftr-logo img{height:100px}
			.ftr-social{margin-bottom:10px;display:inline-block;list-style-type:none} .ftr-social li{float:left} .ftr-social a{display:block; margin:0 7px;} .ftr-social img{height:24px;width:24px}
			footer h3{font-weight:600;color:#0e0e0e;font-size:16px;margin:20px 0 15px 0;text-align:left}
			.ftr-menu{list-style-type:none} .ftr-menu li{font-size:14px} .ftr-menu a{color:#005c99;font-size:14px;font-weight:600}
			.ftr-contacts{list-style-type:none} .ftr-contacts li{font-size:14px} .ftr-contacts a{color:#005c99;font-size:14px;font-weight:600}
			.ftr-copyright{font-size:12px} .ftr-copyright a{font-size:12px}
			.btn-callme-ftr{background:-webkit-linear-gradient(#fb822c, #fc6336);background:-o-linear-gradient(#fb822c, #fc6336);background:linear-gradient(#fb822c, #fc6336);padding:12px 18px;text-align:center;line-height:19px;box-shadow:inset 0 -3px 0px #d85028;font-size:14px;font-weight:600;border-radius:5px;border:none;margin:20px 0 25px 0;color:#fff;max-width:180px} .btn-callme-ftr:hover{position:relative;top:1px;color:#fff;box-shadow:none;background:linear-gradient(to bottom, #fc6336, #fb822c)}
			.ftr-callback button{background:none;border:none;color:#f25540;font-size:1.5vw;font-weight:600;}
			.ftr-callback span{border-bottom:1px dotted #f25540}
@media(max-width:576px){
	.ftr-logo a{font-size:5.5vw;}
	.ftr-callback button{font-size:5.5vw;}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
		</style>
		<footer class="pt-3 pb-4">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-4">
<h3>Наши контакты</h3>
<ul class="ftr-contacts">
	<li>Киевстар: <a href="tel:+380680594606">+38 (068) 059-46-06</a></li>
	<li>Viber, WhatsApp: <a href="tel:+380508486830">+38 (050) 848-68-30</a></li>
	<li>Vodafone: <a href="tel:+380508486830">+38 (050) 848-68-30</a></li>
	<li><a href="mailto:die@spammers.die" onclick="window.location='ma'+'il'+'to:'+'ma'+'na'+'ger'+'@'+ 'bem'+'bi.'+'store'">manager@bembi.store</a></li>
	<li>Доставка по всей Украине!</li>
	<li>График работы: ежедневно 10:00 - 20:00</li>
	<li class="ftr-callback text-center text-md-left">
		<button data-toggle="modal" data-target="#phoneModal"><span>Заказать звонок</span></button>
	</li>
</ul>

					</div>
					<div class="col-12 col-md-4 mt-3 mt-md-0 text-center">
<div class="ftr-logo">
	<?php
	if(file_exists('public/bembiTMP/logo.png')){
		echo '<a href="/"><img src="/public/img/no_photo.png" alt="Магазин детской одежды оптом" title="Интернет-магазин детской одежды оптом" class="lazy img-fluid"></a>';
	}else{
		echo '<a href="/" title="Интернет-магазин детской одежды оптом">Bembi.store</a>';
	}
	?>
</div>
<ul class="ftr-social">
	<li><a href="https://www.facebook.com/bembi.store.ua/"><img src="/public/img/icons/facebook.svg" alt="Facebook"></a></li>
	<li><a href="https://www.instagram.com/bembi.store.ua/"><img src="/public/img/icons/instagram.svg" alt="Instagram"></a></li>
</ul>
<div class="ftr-copyright">
						<p>
<?php
if(date("Y") == 2017){echo date("Y");}else{echo '2017 - '.date("Y");}
echo ' © Интернет-магазин детской одежды <a href="/">Bembi.Store</a>';
?>
						</p>
</div>
					</div>
					<div class="col-12 col-md-4 d-none d-md-block">
<h3>Клиентам</h3>
<ul class="ftr-menu">
	<!--
	<li><a href="/account/avtorizacija">Вход</a></li>
	<li><a href="/account/registracija">Регистрация</a></li>
	-->
	<li><a href="/o-magazine">О компании</a></li>
	<li><a href="/novosti">Новости</a></li>
	<li><a href="/kontakty">Контакты</a></li>
	<li><a href="/kak-zakazat">Как заказать</a></li>
	<li><a href="/oplata-i-dostavka">Оплата и доставка</a></li>
	<li><a href="/garantija-i-vozvrat">Гарантия и возврат</a></li>
	<li><a href="/politika-konfidencialnosti">Политика конфиденциальности</a></li>
</ul>
					</div>
				</div>
			</div>
		</footer>

		<!-- Modal phone -->
		<style type="text/css">
			#phoneModal{}
			#phoneModal .modal-body{}
			#phoneModal .before-section{}
			#phoneModal .modal-body div{margin:10px 0;}
			#phoneModal input{border:1px solid #d1d1d1;box-shadow:none;color:#0e0e0e}
			#phoneModal textarea{border:1px solid #d1d1d1;box-shadow:none;color:#0e0e0e}
			#phoneModal .send_btn{background:-webkit-linear-gradient(#fb822c, #fc6336);background:-o-linear-gradient(#fb822c, #fc6336);background:linear-gradient(#fb822c, #fc6336);padding:12px 18px;text-align:center;box-shadow:inset 0 -3px 0px #d85028;font-size:15px;font-weight:bold;border-radius:5px;border:none;color:#fff;box-shadow:inset 0 -4px 0px #d85028}
			#phoneModal .send_btn:hover{box-shadow:none;position:relative;top:1px;color:#fff;box-shadow:none;background:linear-gradient(to bottom, #fc6336, #fb822c)}
		</style>
		<div class="modal fade" id="phoneModal" tabindex="-1" role="dialog" aria-labelledby="phoneModalContent" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body text-center">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h2 class="before-section">Обратный звонок</h2>
						<form action="/" method="post" class="px-4 py-3" enctype="multipart/form-data">
							<input type="hidden" name="product_submit" class="greet_submit" value="phoneadd">
							<input type="hidden" name="greet_page" value="<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>">
							<div><input type="text" name="greet_name" class="greet_name px-2 py-1 w-100" placeholder="Имя"></div>
							<div><input type="email" name="greet_email" class="greet_email px-2 py-1 w-100" placeholder="E-mail"></div>
							<div><input type="tel" name="greet_phone" class="greet_phone px-2 py-1 w-100" placeholder="Телефон"></div>
							<div><textarea name="greet_text" class="greet_text px-2 py-1 w-100" placeholder="Текст сообщения"></textarea></div>
							<span>Код безопасности:</span><img src="/public/important/captcha.php" alt="">
							<div><input type="text" name="greet_captcha" class="greet_captcha px-2 py-1 w-100 w-md-50" placeholder="Введите код безопасности"></div>
							<div><button class="send_btn w-75 w-md-50">Отправить</button></div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- *** Modal phone -->

<script type="text/javascript" src="/public/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="/public/js/popper.min.js"></script>
<script type="text/javascript" src="/public/bootstrap_4-4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/public/js/tooltip.min.js"></script>
<script type="text/javascript" src="/public/js/script.js"></script>
<script async type="text/javascript" src="/public/js/jquery-ui.min.js"></script>
<?php
if($this->route['action'] == 'index' || $this->route['action'] == 'product' || $this->route['action'] == 'search'){
echo '
<script type="text/javascript" src="/public/owlcarousel/owl.carousel.min.js"></script>
<script type="text/javascript">
	$(".index-carousel").owlCarousel({
		items:1, nav:false, navText:["",""], lazyLoad:true, loop:true, margin:10, autoplay:true, autoplayTimeout:3000, autoplayHoverPause:true, responsive:{0:{items:1, nav:true},600:{items:1, nav:true},1000:{items:1, nav:true}}
	});
	$(".novinki-carousel").owlCarousel({
		items:4, nav:false, navText:["",""], lazyLoad:true, loop:true, margin:10, autoplay:true, autoplayTimeout:3000, autoplayHoverPause:true, responsive:{0:{items:1, nav:true},600:{items:2, nav:true},1000:{items:4, nav:true}}
	});
	$(".rasprodazha-carousel").owlCarousel({
		items:4, nav:false, navText:["",""], lazyLoad:true, loop:true, margin:10, autoplay:true, autoplayTimeout:3000, autoplayHoverPause:true, responsive:{0:{items:1, nav:true},600:{items:2, nav:true},1000:{items:4, nav:true}}
	});
	$(".hity-carousel").owlCarousel({
		items:4, nav:false, navText:["",""], lazyLoad:true, loop:true, margin:10, autoplay:true, autoplayTimeout:3000, autoplayHoverPause:true, responsive:{0:{items:1, nav:true},600:{items:2, nav:true},1000:{items:4, nav:true}}
	});
	$(".products-carousel").owlCarousel({
		items:4, nav:false, navText:["",""], lazyLoad:true, loop:true, margin:10, autoplay:true, autoplayTimeout:3000, autoplayHoverPause:true, responsive:{0:{items:1, nav:true},600:{items:2, nav:true},1000:{items:4, nav:true}}
	});
	$(".mini-thumb-carousel").owlCarousel({
		items:4, nav:false, navText:["",""], lazyLoad:true, loop:true, margin:10, autoplay:true, autoplayTimeout:3000, autoplayHoverPause:true,
	});
</script>';
}
?>
<script type="text/javascript" src="/public/js/wow.min.js"></script>
<script type="text/javascript">new WOW().init();</script>
<script type="text/javascript" src="/public/js/modernizr.min.js"></script>
<script type="text/javascript" src="/public/js/jquery.session.js"></script>
<script async type="text/javascript" src="/public/js/sweetalert.min.js"></script>
<?php
if($this->route['action'] == 'product'){
echo '<script async type="text/javascript" src="/public/js/jquery.loupe.min.js"></script>';
}
?>
<script async defer type="text/javascript" src="/public/fontawesome/js/all.min.js"></script>
<?php
if($this->route['action'] == 'product' || $this->route['action'] == 'statja'){
echo '<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5dcc34024464a865"></script>';
}
?>
	</body>
</html>