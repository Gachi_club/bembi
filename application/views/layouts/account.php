<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="ru" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="ru" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="ru">
<!--<![endif]-->
<!--<html lang="ru">-->
<head>
	<meta http-equiv="Content-Type" content="text/html;  charset=utf-8">
	<meta name="author" content="Elendarien">
	<meta name="robots" content="all">
	<meta name="copywright" content="">
	<meta property="og:title" content="<?php echo $og_title; ?>">
	<meta property="og:description" content="<?php echo $og_description; ?>">
	<meta property="og:site_name" content="<?php echo $og_site_name; ?>">
	<meta property="og:image" content="<?php echo $og_image; ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="/public/img/L.png" rel="icon">
	<title><?php echo $title; ?></title>
	<meta name="description" content="<?php echo $description; ?>">
	<meta name="keywords" content="<?php echo $keywords; ?>">

<!--[if lt IE 9]> 
	<script src="http://css3-mediaqueries-js.googlecode.com/files/css3-mediaqueries.js"></script>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> 
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!--[if lt IE 10]>
	<div style="background:#212121; padding:10px 0; box-shadow:3px 3px 5px 0 rgba(0,0,0,.3); clear:both; text-align:center; position:relative; z-index:1;">
		<a href="https://windows.microsoft.com/en-US/internet-explorer/">
			<img src="/public/img/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.">
		</a>
	</div>
	<script type="text/javascript" src="/public/js/html5shiv.min.js"></script>
<![endif]-->

<!-- ****************************** ВАЖНЫЕ СКРИПТЫ САЙТА ****************************** -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js"></script>
<!--
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/v4-shims.js"></script>
-->
<script type="text/javascript" src="/public/js/script.js"></script>
<script type="text/javascript" src="/public/owlcarousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="/public/js/wow.min.js"></script>
<script type="text/javascript">new WOW().init();</script>
<script type="text/javascript" src="/public/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/public/js/form.js"></script>
<script type="text/javascript" src="/public/lightbox/js/lightbox.js"></script>

<!-- ****************************** ВАЖНЫЕ СТИЛИ САЙТА ****************************** -->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/public/css/style.css">
<link rel="stylesheet" type="text/css" href="/public/css/mobile.css">
<link rel="stylesheet" type="text/css" href="/public/owlcarousel/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="/public/owlcarousel/owl.theme.default.min.css">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/public/css/hover.css">
<link rel="stylesheet" type="text/css" href="/public/css/animate.css">
<link rel="stylesheet" type="text/css" href="/public/css/jquery-ui.min.css">
<link href="https://fonts.googleapis.com/css?family=Dancing+Script:700" rel="stylesheet">
<link href="/public/lightbox/css/lightbox.css" rel="stylesheet">

<style>
* {margin:0px; padding:0px;}
html{overflow-x:hidden;}
body{margin:0px; padding:0px; /*-webkit-user-select:none; -moz-user-select:none; -ms-user-select:none;*/}
body a{cursor:pointer;}
.more {visibility:hidden;}
p {display:block; margin-top:0px; margin-bottom:0rem!important; -webkit-margin-before:0em; -webkit-margin-after:0em; -webkit-margin-start:0px; -webkit-margin-end:0px;}
input:focus, textarea:focus, select:focus{outline-offset:0px;}
:focus{outline:-webkit-focus-ring-color auto 0px;}

.dropdown:hover>.dropdown-menu {display: block;}
.dropdown-menu{margin:0!important;}
</style>
</head>
	<body>
		<style type="text/css">
			.hdr-menu{padding:0.1rem 1rem!important;}
			.hdr-menu .form-control{padding:.2rem .5rem; font-size:1vw; font-family:'PT Sans',sans-serif;}
			.hdr-menu .btn-outline-success{padding:.2rem .5rem; font-size:1vw; font-family:'PT Sans',sans-serif;}
			.hdr-menu .nav-item{font-size:1vw;}
			.hdr-menu .dropdown-menu{margin-top:-3px!important;}
		</style>
		<header>
		<div class="bg-light">
		<div class="container">
		<div class="row">
		<div class="col-12">
		<nav class="navbar navbar-expand-lg navbar-light bg-light hdr-menu">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTopMenu" aria-controls="navbarTopMenu" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarTopMenu">
		<ul class="navbar-nav mr-auto">
		<li class="nav-item">
		<a class="nav-link" href="/">Главная</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="/category">Каталог</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="/about">О нас</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="/otzivy">Отзывы</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="/oplatadostavka">Оплата и доставка</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="/garantii">Гарантии</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="/contact">Контакты</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="/kabinet">Личный кабинет</a>
		</li>
		</ul>
		
		<form class="form-inline my-2 my-lg-0">
		<input class="form-control mr-sm-2" type="search" placeholder="Поиск..." aria-label="Search">
		<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Поиск</button>
		</form>
		<ul class="navbar-nav ml-auto">
		<li class="nav-item dropdown">
		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Язык</a>
		<div class="dropdown-menu" aria-labelledby="navbarDropdown">
		<a class="dropdown-item" href="#"><img src="/public/img/ru.svg" style="width:15px; margin-right:5px;" alt="">Русский</a>
		<a class="dropdown-item" href="#"><img src="/public/img/ua.svg" style="width:15px; margin-right:5px;" alt="">Украинский</a>
		<a class="dropdown-item" href="#"><img src="/public/img/gb.svg" style="width:15px; margin-right:5px;" alt="">Английский</a>
		</div>
		</li>
		</ul>
		</div>
		</nav>
		</div>
		</div>
		</div>
		</div>


		<style type="text/css">
			.top-info button{background:#2bc5c3; color:#fff;}
			.top-info button:hover{background:#fb5a86; color:#fff;}
			.top-info .phone{color:#6e6e6e; text-decoration:none;}
		</style>
		<section class="top-info my-4">
			<div class="container">
				<div class="row">
					<div class="col-2">
						<a href="/" style="text-decoration:none;">
							<h2 style="font-family:'Dancing Script', cursive;">NoEl.od.ua</h2>
						</a>
					</div>
					<div class="col-2">
						<p style="color:#fb5a86; font-weight:600;">КИЕВСТАР</p>
						<p><a href="tel:+380976535843" class="phone">+38 (097) 653-58-43</a></p>
					</div>
					<div class="col-2">
						<p style="color:#fb5a86; font-weight:600;">Viber, WhatsApp</p>
						<p><a href="tel:+380976535843" class="phone">+38 (097) 653-58-43</a></p>
					</div>
					<div class="col-2">
						<p style="color:#fb5a86; font-weight:600;">Lifecell</p>
						<p><a href="#" class="phone">+38 (XXX) XXX-XX-XX</a></p>
					</div>
					<div class="col-2 text-center">
						<button type="button" class="btn" data-toggle="modal" data-target="#phoneModal">Заказать звонок</button>
					</div>
					<div class="col-2 text-center">
						<div class="row">
							<div class="col-3"><i class="fas fa-shopping-cart p-2 rounded" style="background:#2bc5c3; color:#fff;"></i></div>
							<div class="col-9">
								<p style="color:#6dced7; font-size:1vw;">ВАША КОРЗИНА</p>
								<p><a href="#" style="color:#000; font-size:.85vw; text-decoration:none;">0 товар(ов) - 0 грн<i class="fas fa-angle-down ml-1"></i></a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


		<style type="text/css">
			.top-menu .nav-item a{font-family:'PT Sans',sans-serif; font-size:1.1vw;}
			.top-menu .nav-item{padding:.3rem .5rem; border-right:1px solid #e6e6e6; border-left:1px solid #e6e6e6;}
			.top-menu .nav-item:first-child{border-right:1px solid #e6e6e6; border-left:0px solid #e6e6e6;}
			.top-menu .nav-item:last-child{border-right:0px solid #e6e6e6; border-left:1px solid #e6e6e6;}
			.top-menu .navbar{padding:0rem 0rem!important;}

			.bef-green{border-top:5px solid #55ec67;}
			.bef-pink{border-top:5px solid #fb5a86;}
			.bef-blue{border-top:5px solid #D3FBFF;}
			.bef-dblue{border-top:5px solid #2bc5c3;}
			.bef-orange{border-top:5px solid #fd8769;}

			.top-menu .dropdown-menu{margin-top:-5px!important;}
		</style>
		<section class="top-menu">
			<div class="container-fluid px-5">
				<div class="row">
					<div class="col-12">
						<nav class="navbar navbar-expand-lg navbar-light bg-light">
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeaderMenu" aria-controls="navbarHeaderMenu" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
						</button>

						<div class="collapse navbar-collapse" id="navbarHeaderMenu">
						<ul class="navbar-nav mx-auto">
						<li class="nav-item dropdown bef-blue">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Набор в кроватку</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="#">Овальная кроватка</a>
						<a class="dropdown-item" href="#">Стандартная кроватка</a>
						</div>
						</li>
						<li class="nav-item dropdown bef-pink">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Пледы и конверты</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="#">Пледы с утеплителем</a>
						<a class="dropdown-item" href="#">Демисезонные</a>
						<a class="dropdown-item" href="#">Пледы без утеплителя</a>
						<a class="dropdown-item" href="#">Пледы-конверты</a>
						</div>
						</li>
						<li class="nav-item bef-orange">
						<a class="nav-link" href="#">Коконы</a>
						</li>
						<li class="nav-item dropdown bef-dblue">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Постельные принадлежности</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="#">Одеяло и подушка</a>
						<a class="dropdown-item" href="#">Простынки на резинке</a>
						<a class="dropdown-item" href="#">Непромокаемые наматрасники</a>
						<a class="dropdown-item" href="#">Балдахин</a>
						<a class="dropdown-item" href="#">Карман</a>
						</div>
						</li>
						<li class="nav-item dropdown bef-green">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Пеленки</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="#">Муслин</a>
						<a class="dropdown-item" href="#">Бязь</a>
						<a class="dropdown-item" href="#">Байка</a>
						<a class="dropdown-item" href="#">Ситец</a>
						<a class="dropdown-item" href="#">Непромокаемые</a>
						</div>
						</li>
						<li class="nav-item bef-blue">
						<a class="nav-link" href="#">Подарочные наборы</a>
						</li>
						<li class="nav-item bef-pink">
						<a class="nav-link" href="#">Подарочные сертификаты</a>
						</li>
						</ul>
						</div>
						</nav>
					</div>
				</div>
			</div>
		</section>
		</header>

		<?php echo $content; ?>

		<style type="text/css">
			footer{}
			footer .contacts, footer .info{padding-top:25px; padding-bottom:5px; border-style:solid; border-width:5px 8px 5px 0; border-image:url(/public/img/stazok.png) 11 1 fill round;}
			footer .contacts ul{list-style-type:none; font-size:1vw; font-family:'PT Sans',sans-serif; color:#6e6e6e;}
			footer .contacts a, footer .info a{text-decoration:none; color:#6e6e6e;}
			footer .info ul{list-style-type:disc; font-size:1vw; font-family:'PT Sans',sans-serif; color:#6e6e6e;}

			.page-text h1{padding:.1rem 0; font-size:1.7vw;}
			.page-text h2{padding:.1rem 0; font-size:1.5vw;}
			.page-text h3{padding:.1rem 0; font-size:1.3vw;}
			.page-text p{padding:.5rem 0; font-size:1vw;}
			.page-text li{padding:.1rem 0; font-size:1vw; margin-left:20px;}

			.r_m{color:#007bff; font-size:1.2vw; font-weight:600; letter-spacing:1px;}
		</style>
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-4 text-right contacts">
						<h5 style="color:#6e6e6e; font-size:1.1vw;">КОНТАКТЫ:</h2>
						<ul>
							<li>Киевстар: <a href="tel:+380976535843">+38 (097) 653-58-43</a></li>
							<li>Viber, WhatsApp: <a href="tel:+380976535843">+38 (097) 653-58-43</a></li>
							<li>Lifecell: <a href="#">+38 (XXX) XXX-XXX-XX</a></li>
							<li><a href="#">manager@noel.od.ua</a></li>
							<li><a href="#">Доставка по всей Украине!</a></li>
							<li>График работы: ежедневно <a href="#">10:00 - 20:00</a></li>
						</ul>
					</div>
					<div class="col-4 text-center align-self-center">
						<p><a href="/" class="my-auto" style="text-decoration:none; font-family:'Dancing Script', cursive; font-size:3vw;">NoEl.od.ua</a><p>
						<p>
							<a href="#" class="mx-2" style="color:#3b5998; font-size:3vw;" title="Facebook"><i class="fab fa-facebook-square"></i></a>
							<a href="#" class="mx-2" style="color:#000; font-size:3vw;" title="Instagram"><i class="fab fa-instagram"></i></a></p>
					</div>
					<div class="col-4 text-left info">
						<h5 style="color:#6e6e6e; font-size:1.1vw;">ИНФОРМАЦИЯ:</h2>
						<ul>
							<li><a href="/about">О нас</a></li>
							<li><a href="/oplatadostavka">Оплата и доставка</a></li>
							<li><a href="/obmenvozvrat">Обмен и возврат</a></li>
							<li><a href="/kakzakazat">Как заказать</a></li>
							<li><a href="/garantii">Гарантии</a></li>
							<li><a href="/contact">Контакты</a></li>
						</ul>
					</div>
				</div>
				<div class="row mt-3">
					<div class="col-12">
						<p style="color:#6e6e6e; font-size:.8vw;">2014 - 2018 © <a href="/">NoEl.od.ua</a></p>
					</div>
				</div>
				<div class="row mb-3">
					<div class="col-12">
						<a href="/soglashenie" style="color:#6e6e6e; font-size:.8vw;" class="pr-2">Лицензионное соглашение</a>
						<a href="/cookies" style="color:#6e6e6e; font-size:.8vw;" class="px-2">Политика Cookies</a>
						<a href="/konfidencialnost" style="color:#6e6e6e; font-size:.8vw;" class="px-2">Политика Конфиденциальности</a>
						<a href="/podderzhka" style="color:#6e6e6e; font-size:.8vw;" class="px-2">Техническая поддержка</a>
						<a href="/reglament" style="color:#6e6e6e; font-size:.8vw;" class="pl-2">Регламент предоставления услуг</a>
					</div>
				</div>
			</div>
		</footer>

		<!-- Modal phone -->
		<div class="modal fade" id="phoneModal" tabindex="-1" role="dialog" aria-labelledby="phoneModalContent" aria-hidden="true">
		<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header">
		<p class="modal-title my-3" id="phoneModalContent" style="font-size:1.2vw;">Оставьте свой контактный номер,<br>
мы перезвоним вам в течение 3х минут и ответим на все вопросы</p>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">&times;</span>
		</button>
		</div>
		<div class="modal-body">
			<form action="/" method="post">
				<p class="pl-1 pb-1" style="color:#7D7979; font-size:1vw; font-weight:600;">Введите ваш номер телефона:</p>
				<p><input type="text" name="phone-number" class="px-2 py-1 w-100" required></p>
				<p class="mt-3 mb-2 text-center"><button type="submit" name="" class="btn btn-primary">Оформить заявку</button></p>
			</form>
		</div>
		</div>
		</div>
		</div>
		<!-- *** Modal phone -->

	</body>
</html>