<?php
/*******************************************************************************
*  Title: Web shop «Bembi» - Default
*  Last update: Nov 12, 2019
*  Author: Nikita Kirilov (Elendarien)
*  Website: https://sinup.od.ua
*  E-mail: kirilovbiz@gmail.com
*******************************************************************************/
set_time_limit(0);
ini_set('memory_limit', '-1');
error_reporting(E_ALL & ~E_NOTICE);
ini_set("display_errors", 1);

switch($_SESSION['hash']['adm_role']){
	case'Бог':$adm_role='god';break;
	case'Менеджер':$adm_role='manage';break;
	default:$adm_role='manage';break;
}
?>
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="ru" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="ru" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="ru">
<!--<![endif]-->
<!--<html lang="ru">-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="author" content="Elendarien">
	<meta name="robots" content="noindex, nofollow">
	<meta name="copywright" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="/public/img/favicon.ico">
	<title><?=$ttl;?></title>
	<meta name="description" content="<?=$desc;?>">
	<meta name="keywords" content="<?=$keys;?>">

<!--[if lt IE 9]> 
	<script src="http://css3-mediaqueries-js.googlecode.com/files/css3-mediaqueries.js"></script>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> 
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!--[if lt IE 10]>
	<div style="background:#212121; padding:10px 0; box-shadow:3px 3px 5px 0 rgba(0,0,0,.3); clear:both; text-align:center; position:relative; z-index:1;">
		<a href="https://windows.microsoft.com/en-US/internet-explorer/">
			<img src="/public/img/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.">
		</a>
	</div>
	<script type="text/javascript" src="/public/js/html5shiv.min.js"></script>
<![endif]-->

<!-- ****************************** ВАЖНЫЕ СТИЛИ САЙТА ****************************** -->
<link rel="stylesheet" type="text/css" href="/public/css/normalize.min.css">
<link rel="stylesheet" type="text/css" href="/public/bootstrap_4-4/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/public/css/style-admin.css">
<link rel="stylesheet" type="text/css" href="/public/css/sweetalert.css">
<link rel="stylesheet" type="text/css" href="/public/owlcarousel/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="/public/owlcarousel/owl.theme.default.min.css">

<link rel="stylesheet" type="text/css" href="/public/css/hover-min.css">
<link rel="stylesheet" type="text/css" href="/public/css/animate.min.css">
<link rel='stylesheet' type='text/css' href='/public/css/jquery-ui.min.css'>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.6/css/all.css">


<style>
* {margin:0px;padding:0px}
html{overflow-x:hidden}
body{margin:0px;padding:0px}
body a{cursor:pointer}
.more {visibility:hidden}
p {display:block; margin-top:0px;margin-bottom:0rem!important;-webkit-margin-before:0em;-webkit-margin-after:0em;-webkit-margin-start:0px;-webkit-margin-end:0px}
input:focus, textarea:focus, select:focus{outline-offset:0px}
:focus{outline:-webkit-focus-ring-color auto 0px}

.dropdown:hover > .dropdown-menu{display:block}
.dropdown-menu{margin:0!important}
</style>
</head>
	<body>
		<style type="text/css">
			.feather{width:16px; height:16px; vertical-align:text-bottom;}
			/* * Sidebar */
			.sidebar{position:fixed; top:0; bottom:0; left:0; z-index:100; /* Behind the navbar */ padding:0; box-shadow:inset -1px 0 0 rgba(0,0,0,.1);}
			.sidebar-sticky{position:-webkit-sticky; position:sticky; top:48px; /* Height of navbar */ height:calc(100vh - 48px); padding-top:.5rem; overflow-x:hidden; overflow-y:auto;/* Scrollable contents if viewport is shorter than content. */}
			.sidebar .nav-link{font-weight:500; color:#333;}
			.sidebar .nav-link .feather{margin-right:4px; color:#999;}
			.sidebar .nav-link.active{color:#007bff;}
			.sidebar .nav-link:hover .feather, .sidebar .nav-link.active .feather{color:inherit;}
			.sidebar-heading{font-size:.75rem; text-transform:uppercase;}
			/* * Navbar */
			.navbar-brand{padding-top:.75rem; padding-bottom:.75rem; font-size:1rem; background-color:rgba(0,0,0,.25); box-shadow:inset -1px 0 0 rgba(0,0,0,.25);}
			.navbar .form-control{padding:.75rem 1rem; border-width:0; border-radius:0;}
			nav.navbar span{font-size:1vw; color:#fff; cursor:pointer;}
			.form-control-dark{color:#fff; background-color:rgba(255,255,255,.1); border-color:rgba(255,255,255,.1);}
			.form-control-dark:focus{border-color:transparent; box-shadow:0 0 0 3px rgba(255,255,255,.25);}
			/* * Utilities */
			.border-top{border-top:1px solid #e5e5e5;}
			.border-bottom{border-bottom:1px solid #e5e5e5;}
		</style>
		<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
			<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="/">Bembi.Store</a>
			<span>Добро пожаловать в Административную панель</span>
			<ul class="navbar-nav px-3">
					<?php
					if($this->route['action'] != 'login'){
						echo '<li class="nav-item text-nowrap"><a class="nav-link" href="/admin/logout">Выход</a></li>';
					}else{echo '<li class="nav-item text-nowrap"><a class="nav-link" href="/admin/login">Авторизация</a></li>';}
					?>
			</ul>
		</nav>
<?php if($this->route['action'] != 'login'){ ?>
		<div class="container-fluid">
			<div class="row">
				<nav class="col-md-2 d-none d-md-block bg-light sidebar">
					<div class="sidebar-sticky">
						<ul class="nav flex-column">
<?php if($adm_role == 'god' || $adm_role == 'manage'){ ?>
							<li class="nav-item">
								<a class="nav-link active" href="/admin/index">
									<span data-feather="home"></span>Панель админа
								</a>
							</li>
<?php }if($adm_role == 'god' || $adm_role == 'manage'){ ?>
							<li class="nav-item">
								<a class="nav-link" href="/admin/orders">
									<span data-feather="file"></span>Заказы
								</a>
							</li>
<?php }if($adm_role == 'god' || $adm_role == 'manage'){ ?>
							<li class="nav-item">
								<a class="nav-link" href="/admin/phones">
									<span data-feather="phone"></span>Заявки
								</a>
							</li>
<?php }if($adm_role == 'god' || $adm_role == 'manage'){ ?>
							<li class="nav-item">
								<a class="nav-link" href="/admin/products">
									<span data-feather="shopping-cart"></span>Товары
								</a>
							</li>
<?php }if($adm_role == 'god'){ ?>
							<li class="nav-item">
								<a class="nav-link" href="/admin/users">
									<span data-feather="users"></span>Клиенты
								</a>
							</li>
<?php }if($adm_role == 'god'){ ?>
							<li class="nav-item">
								<a class="nav-link" href="/admin/slider">
									<span data-feather="box"></span>Слайдер
								</a>
							</li>
<?php }if($adm_role == 'god' || $adm_role == 'manage'){ ?>
							<li class="nav-item">
								<a class="nav-link" href="/admin/news">
									<span data-feather="file-text"></span>Новости
								</a>
							</li>
<?php }if($adm_role == 'god'){ ?>
							<li class="nav-item">
								<a class="nav-link" href="/admin/admins">
									<span data-feather="star"></span>Администраторы
								</a>
							</li>
<?php } ?>
						</ul>
					</div>
				</nav>
			</div>
		</div>
		<?php } ?>

		<?=$content;?>

		<!-- eModal (message/alert/error modal) -->
		<!--
		<div id="e-modal" class="modal hide fade">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<i class="icon-warning-sign"></i>
				<h3>Внимание!!!</h3>
			</div>
			<div class="modal-body">
				<p class="message">Message</p>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button>
			</div>
		</div>
		-->
		<!-- #end eModal -->

<!-- ****************************** ВАЖНЫЕ СКРИПТЫ САЙТА ****************************** -->
<script type="text/javascript" src="/public/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="/public/js/popper.min.js"></script>
<script type="text/javascript" src="/public/bootstrap_4-4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/public/js/tooltip.min.js"></script>
<!-- Charts -->
<?php
if($this->route['action'] == 'index'){
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
<script type="text/javascript" charset="utf-8" async defer>
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx,{
type:'line',
data:{
	labels:["Январь-Февраль", "Март-Апрель", "Май-Июнь", "Июль-Август", "Сентябрь-Октябрь", "Ноябрь-Декабрь",],
	datasets:[{
		data:[<?=$prof1;?>, <?=$prof2;?>, <?=$prof3;?>, <?=$prof4;?>, <?=$prof5;?>, <?=$prof6;?>],
		lineTension:0,backgroundColor:'transparent',borderColor:'#007bff',borderWidth:4,pointBackgroundColor:'#007bff',
	}]
},
options:{
	scales:{
		yAxes:[{ticks:{beginAtZero:false},}]
	},
	legend:{display:false,}
}
});
</script>
<?php
}
?>
<script type="text/javascript" src="/public/js/sweetalert.min.js"></script>
<script type="text/javascript" src="/public/js/script-admin.js"></script>
<script async type="text/javascript" src="/public/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/public/owlcarousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="/public/js/wow.min.js"></script>
<script type="text/javascript">new WOW().init();</script>
<script type="text/javascript" src="/public/js/modernizr.min.js"></script>
<script type="text/javascript" src="/public/js/jquery.session.js"></script>
<script src="//cdn.ckeditor.com/4.11.4/full/ckeditor.js"></script>
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>feather.replace();</script>
<script async defer type="text/javascript" src="/public/fontawesome/js/all.min.js"></script>
	</body>
</html>