<style type="text/css">
	.static-page ul{margin:10px 0;}
	.static-page ol{margin:10px 0;}
	.static-page ul>li{margin-left:20px;}
	.static-page ol>li{margin-left:20px;}
	.static-page p{margin:10px 0;}
@media(max-width:576px){
	.static-page h1{font-size:8vw;}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
<section class="static-page mt-3">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center"><?php if($page_ttl != ""){echo $page_ttl;}else{echo 'Организаторам СП';}?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<ul>
					<li>Минимальный заказ 500 гривен.</li>
					<li>Товар отпускается только упаковками.</li>
					<li>В упаковке один цвет и один размерный ряд (напр.86.92.98).</li>
					<li>Возможен обмен фабричного брака (брак в нашей продукции попадается крайне редко).</li>
					<li>Отправка заказа после полной оплаты на расчётный счёт.</li>
					<li>Так же Вы можете зарезервировать товар, оформив заказ на нашем сайте. После с Вами свяжется менеджер, и Вы оговорите все подробности Вашего заказа. Так же для резерва Вашего заказа, Вам необходимо внести предоплату в размере 10%.</li>
					<li>Для удобства организаторам совместных покупок, мы предоставляем полный архив фотографий, который Вы можете скачать ниже.</li>
					<li>Действует система скидок: от 10.000 гривен 5 % скидка, от 25.000 гривен 10 % скидка.</li>
				</ul>
			</div>
		</div>
		<div class="row mt-3 mb-5">
			<div class="col-12 table-responsive">
				<h2 class="text-center py-3">Размерная сетка</h2>
				<style>
					table{width:75%; margin:0 auto;}
					thead{background-color:rgba(0,0,0,.03);}
					thead th{font-weight:600; padding:.3rem .5rem;}
					thead, tbody{text-align:center;}
					tbody td{}
				</style>
				<table border="1" class="orders table table-hover">
					<thead>
						<tr>
							<th scope="col" style="vertical-align:middle;">Возраст
							</th><th scope="col" style="vertical-align:middle;">Рост, см
						</th></tr>
					</thead>
					<tbody>
						<tr>
							<td style="vertical-align:middle;">1 год</td>
							<td style="vertical-align:middle;">86 см</td>
						</tr>
						<tr>
							<td style="vertical-align:middle;">2 года</td>
							<td style="vertical-align:middle;">92 см</td>
						</tr>
						<tr>
							<td style="vertical-align:middle;">3 года</td>
							<td style="vertical-align:middle;">98 см</td>
						</tr>
						<tr>
							<td style="vertical-align:middle;">4 года</td>
							<td style="vertical-align:middle;">104 см</td>
						</tr>
						<tr>
							<td style="vertical-align:middle;">5 лет</td>
							<td style="vertical-align:middle;">110 см</td>
						</tr>
						<tr>
							<td style="vertical-align:middle;">6 лет</td>
							<td style="vertical-align:middle;">116 см</td>
						</tr>
						<tr>
							<td style="vertical-align:middle;">7 лет</td>
							<td style="vertical-align:middle;">122 см</td>
						</tr>
						<tr>
							<td style="vertical-align:middle;">8 лет</td>
							<td style="vertical-align:middle;">128 см</td>
						</tr>
						<tr>
							<td style="vertical-align:middle;">9 лет</td>
							<td style="vertical-align:middle;">134 см</td>
						</tr>
						<tr>
							<td style="vertical-align:middle;">10 лет</td>
							<td style="vertical-align:middle;">140 см</td>
						</tr>
						<tr>
							<td style="vertical-align:middle;">11 лет</td>
							<td style="vertical-align:middle;">146 см</td>
						</tr>
						<tr>
							<td style="vertical-align:middle;">12 лет</td>
							<td style="vertical-align:middle;">152 см</td>
						</tr>
						<tr>
							<td style="vertical-align:middle;">13 лет</td>
							<td style="vertical-align:middle;">158 см</td>
						</tr>
						<tr>
							<td style="vertical-align:middle;">14 лет</td>
							<td style="vertical-align:middle;">164 см</td>
						</tr>
						<tr>
							<td style="vertical-align:middle;">15 лет</td>
							<td style="vertical-align:middle;">170 см</td>
						</tr>
						<tr>
							<td style="vertical-align:middle;">16 лет</td>
							<td style="vertical-align:middle;">176 см</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>