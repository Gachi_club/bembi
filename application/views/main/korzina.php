<style type="text/css">
	#cart h2{margin-bottom:1rem}
	#cart ul{list-style-type:disc;margin-left:20px}
	#cartfrom p{padding:.3rem 0}
	#cartfrom input, select, textarea{color:grey}
	#cartfrom .card-header{padding:.3rem .75rem}
	#cartfrom .btn.btn-link{color:#000;padding:0rem 0rem!important}
	#cartfrom .btn-link:hover{color:#f1667b;text-decoration:none}
	#cart button[type=submit]{background:#2bc5c3;color:#fff}
	#cart button[type=submit]:hover{background:#fb5a86;color:#fff}
	#cart .tabvalue{color:#2bc5c3}
	#cart .tabvalue:hover{color:#fb5a86;text-decoration:none}
	#cart .tabname{color:#909090}
	#cart .tabresult{color:#000}
	#cart .carter:first-child{margin:0 0 .5rem 0}
	#cart .carter{margin:.5rem 0;box-shadow:0 0 1px rgba(0,0,0,.3);background-color:#f9f9f9}
	#cart .carter:last-child{margin:.5rem 0 0 0}
	#cart .skidka{background:#909090;padding:.46rem!important;top:7px;position:relative}
	#cart .resulter p{color:#4c4c4c}
	#headingOne button, #headinTwo button{white-space:normal}
	#cart .orders{width:100%;border-color:rgba(0,0,0,.125);border-radius:.25rem}
	#cart .orders thead{background-color:rgba(0,0,0,.03)}
	#cart .orders thead th{text-align:center;padding:.3rem 1rem;font-weight:600}
	#cart .orders tbody td{text-align:center}
	#cart .orders tbody td img{max-width:75px;/*padding:.5rem*/}
	#cart .count .btn-minus, #cart .count .btn-plus{border:none;color:#ef0303;font-weight:600;position:relative;display:inline-block;width:30px;font-size:2vw;line-height:normal;vertical-align:middle;background:#efefef}
	#cart .count .btn-minus:hover, #cart .count .btn-plus:hover{}
	#cart .count .counter{font-size:2vw;text-align:center;border:2px solid #005c99;width:40px;height:40px;font-weight:600;color:#ef0303;border-radius:50%}
	#counter:disabled{background-color:transparent}
	#counter{vertical-align:middle;font-size:2vw;}

	#cart .cart-btn-color{border:1px solid #000;width:20px;height:20px;display:inline-block}
@media(max-width:576px){
	#cart .count .btn-minus, #cart .count .btn-plus{font-size:8vw;}
	#cart .count .counter{font-size:8vw;}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
<section id="cart" class="my-4">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-7">
				<div class="row">
					<div class="col-12 text-center">
						<h2>Товары в корзине</h2>
					</div>
					<div class="col-12 text-center font-italic">
						<p>Для выбора другого цвета товара, нажмите на иконку текущего цвета товара в колонке "Цвет".</p>
					</div>
					<div class="col-12 d-md-none">
						<img src="" alt="">
					</div>
				</div>
				<!-- -->
				<div class="row no-gutters">
					<div class="col-12">
						<?php
					if($basketcount == 0){
						echo '
						<div class="row no-gutters">
							<div class="col-12 text-center">
								<p>Корзина пуста</p>
							</div>
						</div>
						';
					}else{
						echo '
							<div class="row">
								<div class="col-12">
									<div class="table-responsive">
									<table border="1" class="orders table table-hover">
										<thead>
											<tr>
												<th scope="col" style="vertical-align:middle;">Фото</th>
												<th scope="col" style="vertical-align:middle;">Наименование</th>
												<th scope="col" style="vertical-align:middle;">Цвет</th>
												<th scope="col" style="vertical-align:middle;">Артикул</th>
												<th scope="col" style="vertical-align:middle;">Цена</th>
												<th scope="col" style="vertical-align:middle;">Кол-во</th>
												<th scope="col" style="vertical-align:middle;">Всего, грн</th>
												<th scope="col"></th>
											</tr>
										</thead>
										<tbody>
						';
						foreach($basketproducts as $val){
							if(!empty($val['p_imglink']) && file_exists('public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink'])){
								$pathimg = '/public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink'];
							}else{$pathimg = '/public/img/no_photo.png';}
							if(!empty($val['p_imglink_webp']) && file_exists('public/bembiTMP/products/webp/'.$val['p_imglink_webp'])){
								$pathimg_webp = '/public/bembiTMP/products/webp/'.$val['p_imglink_webp'];
							}else{$pathimg_webp = '/public/img/no_photo.png';}
							echo '
							<tr>
								<td style="vertical-align:middle;">
									<img src="'.$pathimg.'" alt="'.$val['p_name'].'" class="lazy img-fluid">
								</td>
								<td style="vertical-align:middle;">
									<a href="/product/'.$val['p_alias'].'_'.$val['p_vendor_code'].'" class="tabvalue">'.$val['p_name'].'</a>
								</td>
								<td style="vertical-align:middle;">
									<button class="cart-btn-color" style="background:'.$val['cart_prod_color'].'" data-toggle="modal" data-target="#colorModal" data-active-color="'.$val['cart_prod_color'].'" data-cartid="'.$val['cart_id'].'" data-all-colors="'.$val['p_colors'].'" title="Перейти к выбору другого цвета">
									</button>
								</td>
								<td style="vertical-align:middle;">
									<p>'.$val['p_vendor_code'].'</p>
								</td>
								<td style="vertical-align:middle;">
									<p>'.$val['p_opt_price'].'</p>
								</td>
								<td style="min-width:150px; vertical-align:middle;">
									<p class="count">
<input type="button" value="-" class="btn-minus hvr-pulse" data-value="'.$val['cart_prod_count'].'" data-minstep="'.$val['p_upakovka'].'" data-step="'.$val['p_upakovka'].'" data-cartid="'.$val['cart_id'].'" data-prodid="'.$val['p_id'].'">
<input type="text" disabled id="counter" class="counter korzina-count-'.$val['cart_id'].'" name="count" value="'.$val['cart_prod_count'].'" min="'.$val['p_upakovka'].'" step="'.$val['p_upakovka'].'" title="Кол-во" aria-valuenow="'.$val['cart_prod_count'].'">
<input type="button" value="+" class="btn-plus hvr-pulse" data-value="'.$val['cart_prod_count'].'" data-minstep="'.$val['p_upakovka'].'" data-step="'.$val['p_upakovka'].'" data-cartid="'.$val['cart_id'].'" data-prodid="'.$val['p_id'].'">
									</p>
								</td>
								<td style="vertical-align:middle;">
									<p>'.($val['p_opt_price'] * $val['cart_prod_count']).'</p>
								</td>
								<td id="btn-delete" data-cartid="'.$val['cart_id'].'" data-pid="'.$val['p_id'].'" style="padding:0 .5rem; vertical-align:middle;">
									<i class="fas fa-trash justify-content-center" style="cursor:pointer;" title="Удалить из Корзины"></i>
								</td>
							</tr>
							';
						}
						echo '
										</tbody>
									</table>
									</div>
								</div>
							</div>
						';
					}
						?>
					</div>
				</div>
				
<?php
if($basketcount != 0){
	echo '
				<div class="row mt-3">
					<div class="col-12 resulter">
						<p><strong>К оплате:</strong> <span>'.$basketprice.' грн.</span></p>
					</div>
				</div>
	';
}
?>
				<!-- -->
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
				<div class="row">
					<div class="col-12 text-center">
						<h2>Форма для заказа</h2>
					</div>
				</div>
				<form action="/" method="post" enctype="multipart/form-data">
					<input type="hidden" name="product_submit" value="order">
					<div class="accordion" id="cartfrom">
						<div class="card">
							<div class="card-header" id="headingOne">
								<p class="py-0 mb-0">
									<button class="btn btn-link btn-collapse" type="button" data-toggle="collapse" data-target="#cartOne" aria-expanded="true" aria-controls="cartOne">Основные данные</button>
								</p>
							</div>
							<div id="cartOne" class="collapse show" aria-labelledby="headingOne" data-parent="#cartfrom">
								<div class="card-body">
									<p><input type="text" name="fullname" class="px-2 py-1 w-100" placeholder="Фамилия Имя Отчество *" required></p>
									<p><input type="tel" name="phone" class="px-2 py-1 w-100" placeholder="Мобильный телефон *" required></p>
									<p><input type="email" name="email" class="px-2 py-1 w-100" placeholder="E-mail *" required></p>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headinTwo">
								<p class="py-0 mb-0">
									<button class="btn btn-link btn-collapse collapsed" type="button" data-toggle="collapse" data-target="#cartTwo" aria-expanded="false" aria-controls="cartTwo">Укажите дополнительные данные (не обязательно)</button>
								</p>
							</div>
							<div id="cartTwo" class="collapse" aria-labelledby="headinTwo" data-parent="#cartfrom">
								<div class="card-body">
									<p><input type="text" name="city" class="px-2 py-1 w-100" placeholder="Город доставки"></p>
									<p><input type="text" name="address" class="px-2 py-1 w-100" placeholder="Адрес доставки"></p>
									<p>
										<select name="delivery" class="px-2 py-1 w-100">
											<option value="" selected disabled>Способ доставки</option>
											<option value="Новая Почта">Новая Почта</option>
											<option value="Автолюкс">Автолюкс</option>
											<option value="Интайм">Интайм</option>
											<option value="Деливери">Деливери</option>
											<option value="Самовывоз">Самовывоз</option>
										</select>
									</p>
									<!--
									<p>
										<select name="payment" class="px-2 py-1 w-100">
											<option value="" disabled>Способ оплаты</option>
											<option value="Приват Банк" selected>Приват Банк</option>
										</select>
									</p>
									-->
									<input type="hidden" name="payment" value="Приват Банк">
									<p><textarea name="message" rows="4" class="px-2 py-1 w-100" placeholder="Примечания и пожелания"></textarea></p>
								</div>
							</div>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col-12 align-self-center text-center">
							<input type="hidden" name="basketcount" value="<?=$basketcount;?>">
							<input type="hidden" name="basketprice" value="<?=$basketprice;?>">
							<button type="submit" class="btn btn-collapse" id="formsubmit">Заказать товар</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<!-- Modal color -->
<style type="text/css">
	#colorModal{}
	#colorModal .modal-body{}
	#colorModal .before-section{}
	#colorModal .modal-body div{margin:10px 0;}
	#colorModal input{border:1px solid #d1d1d1;box-shadow:none;color:#0e0e0e}
	#colorModal textarea{border:1px solid #d1d1d1;box-shadow:none;color:#0e0e0e}
	#colorModal .send_btn{background:-webkit-linear-gradient(#fb822c, #fc6336);background:-o-linear-gradient(#fb822c, #fc6336);background:linear-gradient(#fb822c, #fc6336);padding:12px 18px;text-align:center;box-shadow:inset 0 -3px 0px #d85028;font-size:15px;font-weight:bold;border-radius:5px;border:none;color:#fff;box-shadow:inset 0 -4px 0px #d85028}
	#colorModal .send_btn:hover{box-shadow:none;position:relative;top:1px;color:#fff;box-shadow:none;background:linear-gradient(to bottom, #fc6336, #fb822c)}
	#colorModal .radio-item{display:inline-block;position:relative;padding:0 6px}
	#colorModal .radio-item input[type='radio']{display:none}
	#colorModal .radio-item label{color:#666;font-weight:normal}
</style>
<div class="modal fade" id="colorModal" tabindex="-1" role="dialog" aria-labelledby="colorModalContent" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body text-center">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h2 class="before-section">Выберите цвет</h2>
				<div class="modal-color"></div>
			</div>
		</div>
	</div>
</div>
<!-- *** Modal color -->