<style type="text/css">
	#error-page .spin-earth-on-hover{transition:ease 200s !important;transform:rotate(-3600deg)!important;}
	.bg-purple{background-color:#2b005a;background-repeat:repeat-x;background-size:cover;background-position:left top;height:100%;overflow:hidden;position:relative;}
	#error-page .custom-navbar{padding-top:15px;}
	#error-page .brand-logo{margin-left:25px;margin-top:5px;display:inline-block;}
	#error-page .navbar-links{display:inline-block;float:right;margin-right:15px;text-transform:uppercase;}
	#error-page ul{list-style-type:none;margin:0;padding:0;display:flex;align-items:center;}
	#error-page li{float:left;padding:0px 15px;}
	#error-page li a{display:block;color:#fff;text-align:center;text-decoration:none;letter-spacing:2px;font-size:12px;-webkit-transition:all 0.3s ease-in;-moz-transition:all 0.3s ease-in;-ms-transition:all 0.3s ease-in;-o-transition:all 0.3s ease-in;transition:all 0.3s ease-in;}
	#error-page li a:hover{color:#ffcb39;}
	#error-page .btn-request{padding:10px 25px;border:1px solid #FFCB39;border-radius:100px;font-weight:400;}
	#error-page .btn-request:hover{background-color:#FFCB39;color:#000;transform:scale(1.05);box-shadow:0px 20px 20px rgba(0,0,0,0.1);}
	#error-page .btn-go-home{position:relative;z-index:200;margin:15px auto;width:200px;padding:10px 15px;border:1px solid #FFCB39;border-radius:100px;font-weight:400;display:block;color:#fff;text-align:center;text-decoration:none;letter-spacing:2px;font-size:11px;-webkit-transition:all 0.3s ease-in;-moz-transition:all 0.3s ease-in;-ms-transition:all 0.3s ease-in;-o-transition:all 0.3s ease-in;transition:all 0.3s ease-in;}
	#error-page .btn-go-home:hover{background-color:#FFCB39;color:#000;transform:scale(1.05);box-shadow:0px 20px 20px rgba(0,0,0,0.1);}
	#error-page .central-body{padding:17% 5% 10% 5%;text-align:center;}
	#error-page .objects img{z-index:90;pointer-events:none;}
	#error-page .object_rocket{z-index:95;position:absolute;transform:translateX(-50px);top:75%;pointer-events:none;animation:rocket-movement 200s linear infinite both running;}
	#error-page .object_earth{position:absolute;top:20%;left:15%;z-index:90;}
	#error-page .object_moon{position:absolute;top:12%;left:25%;}
	#error-page .earth-moon{}
	#error-page .object_astronaut{animation:rotate-astronaut 200s infinite linear both alternate;}
	#error-page .box_astronaut{z-index:110!important;position:absolute;top:60%;right:20%;will-change:transform;animation:move-astronaut 50s infinite linear both alternate;}
	#error-page .image-404{position:relative;z-index:100;pointer-events:none;}
	#error-page .stars{background:url(/public/img/error/overlay_stars.svg);background-repeat:repeat;background-size:contain;background-position:left top;}
	#error-page .glowing_stars .star{position:absolute;border-radius:100%;background-color:#fff;width:3px;height:3px;opacity:0.3;will-change:opacity;}
	#error-page .glowing_stars .star:nth-child(1){top:80%;left:25%;animation:glow-star 2s infinite ease-in-out alternate 1s;}
	#error-page .glowing_stars .star:nth-child(2){top:20%;left:40%;animation:glow-star 2s infinite ease-in-out alternate 3s;}
	#error-page .glowing_stars .star:nth-child(3){top:25%;left:25%;animation:glow-star 2s infinite ease-in-out alternate 5s;}
	#error-page .glowing_stars .star:nth-child(4){top:75%;left:80%;animation:glow-star 2s infinite ease-in-out alternate 7s;}
	#error-page .glowing_stars .star:nth-child(5){top:90%;left:50%;animation:glow-star 2s infinite ease-in-out alternate 9s;}
	@media only screen and (max-width:600px;){
		#error-page .navbar-links{display:none;}
		#error-page .custom-navbar{text-align:center;}
		#error-page .brand-logo img{width:120px;}
		#error-page .box_astronaut{top:70%;}
		#error-page .central-body{padding-top:25%;}
	}
	@import url('https://fonts.googleapis.com/css?family=Dosis:300,400,500');
	@-moz-keyframes rocket-movement{100%{-moz-transform:translate(1200px,-600px);}}
	@-webkit-keyframes rocket-movement{100%{-webkit-transform:translate(1200px,-600px);}}
	@keyframes rocket-movement{100%{transform:translate(1200px,-600px);}}
	@-moz-keyframes spin-earth{100%{-moz-transform:rotate(-360deg);transition:transform 20s;}}
	@-webkit-keyframes spin-earth{100%{-webkit-transform:rotate(-360deg);transition:transform 20s;}}
	@keyframes spin-earth{100%{-webkit-transform:rotate(-360deg);transform:rotate(-360deg);transition:transform 20s;}}
	@-moz-keyframes move-astronaut{100%{-moz-transform:translate(-160px, -160px);}}
	@-webkit-keyframes move-astronaut{100%{-webkit-transform:translate(-160px, -160px);}}
	@keyframes move-astronaut{100%{-webkit-transform:translate(-160px, -160px);transform:translate(-160px, -160px);}}
	@-moz-keyframes rotate-astronaut{100%{-moz-transform:rotate(-720deg);}}
	@-webkit-keyframes rotate-astronaut{100%{-webkit-transform:rotate(-720deg);}}
	@keyframes rotate-astronaut{100%{-webkit-transform:rotate(-720deg);transform:rotate(-720deg);}}
	@-moz-keyframes glow-star{40%{-moz-opacity:0.3;} 90%,100%{-moz-opacity:1;-moz-transform:scale(1.2);}}
	@-webkit-keyframes glow-star{40%{-webkit-opacity:0.3;} 90%,100%{-webkit-opacity:1;-webkit-transform:scale(1.2);}}
	@keyframes glow-star{40%{-webkit-opacity:0.3;opacity:0.3;} 90%,100%{-webkit-opacity:1;opacity:1;-webkit-transform:scale(1.2);transform:scale(1.2);border-radius:999999px;}}
</style>
<section id="error-page" class="bg-purple">
	<div class="stars">
		<div class="central-body">
			<img class="image-404" src="/public/img/error/404.svg" width="300px">
			<a href="/" class="btn-go-home" target="_blank">На Главную</a>
		</div>
		<div class="objects">
			<img class="object_rocket" src="/public/img/error/rocket.svg" width="40px">
			<div class="earth-moon">
				<img class="object_earth" src="/public/img/error/earth.svg" width="100px">
				<img class="object_moon" src="/public/img/error/moon.svg" width="80px">
			</div>
			<div class="box_astronaut">
				<img class="object_astronaut" src="/public/img/error/astronaut.svg" width="140px">
			</div>
		</div>
		<div class="glowing_stars">
			<div class="star"></div>
			<div class="star"></div>
			<div class="star"></div>
			<div class="star"></div>
			<div class="star"></div>
		</div>
	</div>
</section>