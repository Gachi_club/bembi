<style type="text/css">
	.static-page ul{margin:10px 0;}
	.static-page ol{margin:10px 0;}
	.static-page ul>li{margin-left:20px;}
	.static-page ol>li{margin-left:20px;}
	.static-page p{margin:10px 0;}
@media(max-width:576px){
	.static-page h1{font-size:8vw;}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
<section class="static-page mt-3 mb-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center py-3"><?php if($page_ttl != ""){echo $page_ttl;}else{echo 'Гарантия и возврат';}?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
<div class="static-text">
<p>Детская и подростковая одежда относится к товарам, которые по закону Украины (Постановление КМУ от 19 марта 1994 г. № 172) не подлежат возврату и обмену. Но мы всегда идем на встречу нашим Покупателям и готовы обменять товар или вернуть средства в следующих случаях:</p>
<ol>
	<li>Возврат или обмен товара возможен только в случае, если товар не соответствует качеству, указанному в его описании в результате производственного брака.</li>
	<li>Возврат или обмен товара возможен только в том случае, если он не использовался, сохранен его товарный вид, потребительские свойства, упаковка.</li>
</ol>
<p>Возврат денег осуществляется оговоренным заранее удобным для Покупателя и Продавца способом. При оформлении возврата, доставку оплачивает Покупатель.</p>
</div>
			</div>
		</div>
	</div>
</section>