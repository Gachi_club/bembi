<style type="text/css">
	.kont-inf{font-style:italic;}
	.kontakt-form form>p{margin:.5rem 0;} .kontakt-form input{} .kontakt-form textarea{} .kontakt-form button{}
	.smalltext{font-style:italic;font-size:.9vw}
@media(max-width:576px){
	.smalltext{font-size:3.9vw}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
<section class="kontakty mt-3 mb-3">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1><?php if($page_ttl != ""){echo $page_ttl;}else{echo 'Контакты';}?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-5 align-self-center">
				<div class="kontakt-form">
				<p class="kont-inf">Заполните форму и наш менеджер обязательно свяжется с вами в ближайшее время.</p>
				<form action="/" method="post">
					<input type="hidden" name="greet_submit" value="add">
					<p><input type="text" name="greet_name" class="px-2 py-1 w-100" placeholder="Ваше имя"></p>
					<p><input type="email" name="greet_email" class="px-2 py-1 w-100" placeholder="Ваш e-mail"></p>
					<p><input type="tel" name="greet_phone" class="px-2 py-1 w-100" placeholder="Ваш телефон"></p>
					<p><textarea name="greet_text" class="px-2 py-1 w-100" placeholder="Ваше сообщение"></textarea></p>
					<p class="text-center"><button type="submit" class="btn btn-callme-ftr" name="submit">Отправить письмо</button></p>
				</form>
				</div>
			</div>
			<div class="col-12 col-lg-7 mt-2 mt-lg-0">
				<h5><strong>По телефонам:</strong></h5>
				<ul style="list-style-type:none;margin-bottom:0rem;">
					<li>Киевстар: <a href="tel:+380680594606">+38 (068) 059-46-06</a></li>
					<li>Viber, WhatsApp: <a href="tel:+380508486830">+38 (050) 848-68-30</a></li>
					<li>Vodafone: <a href="tel:+380508486830">+38 (050) 848-68-30</a></li>
				</ul>
				<p class="smalltext"><span style="color:red">*</span> Звонки согласно тарифам Вашего оператора.</p>
				<h5 class="my-2"><strong>По почте или в мессенджерах:</strong></h5>
				<ul style="list-style-type:none;margin-bottom:0rem;">
					<li>Email: <a href="mailto:die@spammers.die" onclick="window.location='ma'+'il'+'to:'+'ma'+'na'+'ger'+'@'+ 'bem'+'bi.'+'store'">manager@bembi.store</a></li>
					<li>Instagram: <a href="https://www.instagram.com/bembi.store.ua/" target="_blank" title="Instagram" rel="nofollow">instagram.com</a></li>
					<li>Facebook: <a href="https://www.facebook.com/bembi.store.ua/" target="_blank" title="Facebook" rel="nofollow">facebook.com</a></li>
				</ul>
			</div>
		</div>
	</div>
</section>