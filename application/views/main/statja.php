<style type="text/css">
	#breadcrumb .breadcrumb{background-color:transparent;font-size:1vw;}
	#statja .main-img{width:250px;float:left;margin-right:1rem}
	#statja h1{margin-bottom:1rem;}
	#statja h2{margin:.5rem 0;text-align:center;font-weight:600}
	#statja h3{margin:.5rem 0}
	#statja h4{margin:.5rem 0}
	#statja h5{margin:.5rem 0}
	#statja p{margin:.5rem 0}
	#statja ul{list-style-type:disc;margin-left:20px}
@media(max-width:576px){
	#breadcrumb .breadcrumb{font-size:4vw;}
	#statja h1{font-size:8vw;}
	#statja h2{font-size:7vw;}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
<section id="breadcrumb" class="mt-lg-3">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
<nav aria-label="breadcrumb" role="navigation">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="/" title="Bembi - интернет-магазин детской одежды оптом"><i class="fas fa-home"></i></a></li>
		<li class="breadcrumb-item"><a href="/novosti">Новости</a></li>
		<li class="breadcrumb-item active" aria-current="page"><?=$art_ttl;?></li>
	</ol>
</nav>
			</div>
		</div>
	</div>
</section>
<section id="statja" class="mb-lg-5 mb-3">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center"><?=$art_ttl;?></h1>
				<img src="<?=$art_img;?>" alt="<?=$art_ttl;?>" class="lazy img-fluid main-img">
				<?=$art_text;?>
			</div>
		</div>
		<div class="row social mt-3">
			<div class="col-12">
				<div class="addthis_inline_share_toolbox"></div>
			</div>
		</div>
	</div>
</section>