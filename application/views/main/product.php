<style type="text/css">
	#product .count .bminus, #product .count .bplus{border:none;color:#ef0303;font-weight:600;position:relative;display:inline-block;width:30px;font-size:2vw;line-height:normal;vertical-align:middle;background:#efefef}
	#product .count .bminus:hover, #product .count .bplus:hover{}
	#product .count .counter{font-size:2vw;text-align:center;border:2px solid #005c99;width:40px;height:40px;font-weight:600;color:#ef0303;border-radius:50%}
	#counter:disabled{background-color:transparent}
	.breadcrumb{background-color:transparent;font-size:1vw;}
	.sidebar{box-shadow:0px 4px 15px rgba(0, 92, 153, 0.20);}
@media(max-width:576px){
	#product .count .bminus, #product .count .bplus{font-size:8vw;}
	#product .count .counter{font-size:8vw;}
	.breadcrumb{font-size:4vw;}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
<section id="product" class="mt-lg-3 mb-lg-5 mb-2">
	<div class="container-fluid">
		<div class="row shop_crumbs">
			<div class="col-12 text-left">
				<nav aria-label="breadcrumb" role="navigation">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="/" title="Bembi - интернет-магазин детской одежды оптом"><i class="fas fa-home"></i></a>
						</li>
<?php
if($val['p_gender']){
	switch($val['p_gender']){
		case'Девочка':$pol_link='/?pol=devochka';$pol_text=' для девочки';break;
		case'Мальчик':$pol_link='/?pol=malchik';$pol_text=' для мальчика';break;
		default:$pol_link='';$pol_text='';break;
	}
}else{$pol_link='';$pol_text='';}

if(!empty($cat['cat_title'])){
	echo '
	<li class="breadcrumb-item"><a href="/detskaja-odezhda'.$pol_link.'" title="Детская и школьная одежда">...</a></li>
	<li class="breadcrumb-item"><a href="/detskaja-odezhda/'.$cat['cat_alias'].$pol_link.'">'.$cat['cat_title'].$pol_text.'</a></li>
	<li class="breadcrumb-item active" aria-current="page">'.$val['p_name'].'</li>
	';
}else{
	echo '<li class="breadcrumb-item active" aria-current="page">Детская и школьная одежда'.$pol_text.'</li>';
}
?>
					</ol>
				</nav>
			</div>
		</div>

		<style type="text/css">
			#sidebar{background:url(/public/img/pattern.png) rgba(244,241,245,.25);box-shadow:0 0 1px rgba(0,0,0,.2) inset;box-shadow:0px 4px 15px rgba(0, 92, 153, 0.20)}
			#sidebar h5{background:#005c99;color:#fff;font-size:1.2vw}
			#sidebar p{background-color:rgba(255,255,255,.8);border:1px solid transparent;color:#005c99;font-size:1vw}
			#sidebar p:hover{background-color:rgba(255,255,255,.8);border:1px solid rgba(150,150,150,.8);color:#005c99;font-size:1vw}
			#sidebar p.active{border:1px solid rgba(150,150,150,.8)}
			#sidebar a{color:#000;transition:.5s;display:block}
			#sidebar a:hover{text-decoration:none;color:#005c99}

			#product .price p{font-weight:600;/*color:#2bc5c3;*/color:#005c99;text-shadow:1px 1px 2px rgba(0,0,0,.51);font-size:3vw;}
			#product .product-name h1{font-size:2vw;font-weight:600}
			#product .nalichie i, #product .nalichie svg{margin-right:5px}
@media(max-width:576px){
	#product .product-name h1{font-size:6vw;}
	#product .price p{font-size:9vw;}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
		</style>
		<div class="row">
			<div class="col-md-3 d-none d-md-block">
				<aside id="sidebar" class="rounded">
<?php
if(isset($allcats) && isset($schoolcats)){
	echo '
					<div class="py-2 px-3">
						<div class="py-2">
							<h5 class="text-center py-2">Каталог детской одежды</h5>
	';
	foreach($allcats as $valcat){
		if($valcat['cat_alias'] != $this->route['cat']){
		echo '
							<p class="my-1 py-1 px-3">
								<a href="/detskaja-odezhda/'.$valcat['cat_alias'].'">'.$valcat['cat_title'].'</a>
							</p>
		';
		}else{echo '<p class="active my-1 py-1 px-3">'.$valcat['cat_title'].'</p>';}
	}
	echo '
						</div>
						<div class="py-2">
							<h5 class="text-center py-2">Каталог одежды для школы</h5>
	';
	foreach($schoolcats as $valcat){
		if($valcat['cat_alias'] != $this->route['cat']){
		echo '
							<p class="my-1 py-1 px-3">
								<a href="/detskaja-odezhda/'.$valcat['cat_alias'].'">'.$valcat['cat_title'].'</a>
							</p>
		';
		}else{echo '<p class="active my-1 py-1 px-3">'.$valcat['cat_title'].'</p>';}
	}
	echo '
						</div>
					</div>
	';
}
?>
				</aside>
			</div>
			<div class="col-md-9">
				<!-- PRODUCT -->
				<div id="product" class="info">
					<div class="row">
						<div class="col-12 col-lg-5">
							<div class="row">
								<div class="col-12">
									<div class="photo text-center align-self-center">
<?php
if(!empty($val['p_imglink']) && file_exists('public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink'])){
	$pathimg = '/public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink'];
}else{$pathimg = '/public/img/no_photo.png';}
echo '<img src="'.$pathimg.'" class="product-image loop w-100 img-fluid lazy" alt="'.$val['p_name'].' ('.$val['p_vendor_code'].')">';
?>
									</div>
								</div>
							</div>
<style type="text/css">
	.img-thumb ul{list-style-type:none;}
	.img-thumb li{float:left; margin:0 3px;}
	.img-thumb li:first-child{margin:0 3px 0 0;}
	.img-thumb li:last-child{margin:0 0 0 3px;}
	.mini-img-p{display:block;background-position:center center;background-size:cover;width:100%;height:100px;cursor:pointer;}
	.mini-img-span{display:inline-block;background-position:center center;background-size:cover;width:100px;height:100px;cursor:pointer;margin:0 10px 0 0;}
@media(max-width:576px){
	.mini-img-span{width:75px;height:75px;margin:0 5px;}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
							<div class="img-thumb row mt-3">
								<div class="col-12">
									
<?php
if(file_exists('public/bembiTMP/products/'.$val['p_vendor_code'])){
	$dir = 'public/bembiTMP/products/'.$val['p_vendor_code'];
	$some = array_diff(scandir($dir), array('..', '.'));
	if(count($some) > 4){
		echo '<div class="owl-carousel mini-thumb-carousel">';
		foreach($some as $gg){
			if(file_exists($dir.'/'.$gg)){
				$small_path = '/'.$dir.'/'.$gg;
				echo '<div><p class="mini-img-p" style="background-image:url('.$small_path.');" data-path="'.$small_path.'"></p></div>';
			}
		}
		echo '</div>';
	}else{
		foreach($some as $gg){
			if(file_exists($dir.'/'.$gg)){
				$small_path = '/'.$dir.'/'.$gg;
				echo '<span class="mini-img-span" style="background-image:url('.$small_path.');" data-path="'.$small_path.'"></span>';
			}
		}
	}
}
?>
								</div>
							</div>
						</div>
						<div class="col-12 col-lg-7">
							<div class="row mt-3 mt-md-0">
								<div class="col-12 product-name">
									<h1><?=$val['p_name'].' ('.$val['p_vendor_code'].')';?></h1>
								</div>
							</div>
<style type="text/css">
#product .radio-item{display:inline-block;position:relative;padding:0 6px}
#product .radio-item input[type='radio']{display:none}
#product .radio-item label{color:#666;font-weight:normal}
</style>
							<div class="row mt-3 mt-lg-3">
								<div class="col-12">
									<p>Выберите требуемый цвет:</p>
<?php
$array = explode(";", $val['p_colors']);
$array = array_filter($array);

if(count($array) > 1){

	for($i = 0; $i < count($array);$i++){
		
		echo '
		<style type="text/css">
		.radio-item label[for=radio-'.$i.']:before{border:8px solid '.$array[$i].';cursor:pointer;-webkit-box-shadow:2px 2px 5px 0px rgba(0,0,0,0.75);-moz-box-shadow:2px 2px 5px 0px rgba(0,0,0,0.75);box-shadow:2px 2px 5px 0px rgba(0,0,0,0.75);content:" ";display:inline-block;position:relative;top:5px;margin:0 5px 0 0;width:35px;height:35px;background-color:transparent}
		#radio-'.$i.':checked + label[for=radio-'.$i.']:after{background:'.$array[$i].';width:23px;height:23px;position:absolute;top:9px;left:4px;content:" ";display:block}
		</style>
		<div class="radio-item product-color">
		';
		if($i == 0){
echo '<input type="radio" name="color" value="'.$array[$i].';" class="product-color-box active-color" id="radio-'.$i.'" checked>
			<label for="radio-'.$i.'" class="hvr-pulse"></label>';
		}else{
echo '<input type="radio" name="color" value="'.$array[$i].';" class="product-color-box" id="radio-'.$i.'">
			<label for="radio-'.$i.'" class="hvr-pulse"></label>';
		}
		echo '
		</div>
		';
	}

}else{
	echo '
	<style type="text/css">
	.radio-item label[for=radio-0]:before{border:8px solid '.$array[0].';cursor:pointer;-webkit-box-shadow:2px 2px 5px 0px rgba(0,0,0,0.75);-moz-box-shadow:2px 2px 5px 0px rgba(0,0,0,0.75);box-shadow:2px 2px 5px 0px rgba(0,0,0,0.75);content:" ";display:inline-block;position:relative;top:5px;margin:0 5px 0 0;width:35px;height:35px;background-color:transparent}
	#radio-0:checked + label[for=radio-0]:after{background:'.$array[0].';width:23px;height:23px;position:absolute;top:9px;left:4px;content:" ";display:block}
	</style>
	<div class="radio-item product-color">
		<input type="radio" name="color" value="'.$array[0].';" class="product-color-box active-color" id="radio-0" checked>
		<label for="radio-0" class="hvr-pulse"></label>
	</div>
	';
}
?>
								</div>
							</div>
<style type="text/css">
#counter{vertical-align:middle;font-size:2vw;}
</style>
							<div class="row my-3 my-md-3">
								<div class="col-6 col-md-4 text-center price">
									<p><span><?=$val['p_opt_price'];?></span> грн.</p>
								</div>
								<div class="col-6 col-md-8 nalichie align-self-center">
<?php
if($val['p_nalichie'] == 'да'){echo '<p style="color:green;"><i class="fas fa-check-square"></i>Есть в наличии</p>';}
elseif($val['p_nalichie'] == 'нет'){echo '<p style="color:grey;"><i class="fas fa-check-square"></i>Нет в наличии</p>';}
else{echo '<p style="color:orange;"><i class="fas fa-check-square"></i>Ошибка</p>';}
?>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<p class="font-italic">Внимание! Стоимость указана за 1 ед.<br>Товар продается только упаковками. В упаковке: <?=$val['p_upakovka'];?> шт.</p>
								</div>
							</div>
							<div class="row my-4">
								<div class="col-12 col-lg-3 count text-center text-md-left mb-2 mb-md-0">
									<div>
<input type="button" value="-" class="bminus hvr-pulse">
<input type="text" disabled id="counter" class="counter" name="count" value="<?=$val['p_upakovka'];?>" min="<?=$val['p_upakovka'];?>" step="<?=$val['p_upakovka'];?>" title="Кол-во" aria-valuenow="<?=$val['p_upakovka'];?>">
<input type="button" value="+" class="bplus hvr-pulse">
									</div>
								</div>
<style type="text/css">
.btn-in-cart{background:-webkit-linear-gradient(#28a745, #1e7e34);background:-o-linear-gradient(#28a745, #1e7e34);background:linear-gradient(#28a745, #1e7e34);padding:12px 18px;text-align:center;line-height:19px;box-shadow:inset 0 -3px 0px #0f401a;font-size:15px;font-weight:600;border-radius:5px;border:none;color:#fff;margin:0 auto;}
.btn-in-cart:hover{position:relative;top:1px;color:#fff;box-shadow:none;background:linear-gradient(to bottom, #1e7e34, #28a745);}
</style>
								<div class="col-12 col-lg-9 buybtn align-self-end text-center mt-2 mt-md-0">
									<p>
<?php
if($val['p_nalichie'] == 'да'){
	echo '<button type="button" class="btn-in-cart btn" data-pid="'.$val['p_id'].'" data-vcode="'.$val['p_vendor_code'].'" data-value="'.$val['p_upakovka'].'" data-price="'.$val['p_opt_price'].'">Добавить в Корзину</button>';
}else{
	echo '<button type="button" class="btn-in-modal btn call_btn px-3 py-1" data-toggle="modal" data-target="#phoneModal" data-pid="'.$val['p_id'].'" data-vcode="'.$val['p_vendor_code'].'" data-value="'.$val['p_upakovka'].'" data-price="'.$val['p_opt_price'].'><span class="d-none d-md-block">Заказать звонок</span></button>';
}
?>
									</p>
								</div>
							</div>
<style type="text/css">
#nav-tab .nav-item a{font-size:1vw;}
#nav-tab .nav-link{color:#005c99;}
#nav-tab .nav-link.active{color:#495057;}
.tab-content h3{font-size:1.2vw;padding:10px 0;font-weight:600;}
.tab-content p{font-size:1vw;padding:5px 0;}
.tab-content ul{}
.tab-content ul>li{font-size:1vw;margin-left:20px;}
.tab-content ol{}
.tab-content ol>li{font-size:1vw;margin-left:20px;}
@media(max-width:576px){
	#nav-tab .nav-item a{font-size:4vw;}
	.tab-content h3{font-size:5vw;}
	.tab-content p{font-size:4vw;}
	.tab-content ul>li{font-size:4vw;}
	.tab-content ol>li{font-size:4vw;}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
							<div class="row">
								<div class="col-12" style="font-family:'El Messiri', sans-serif;">
									<ul class="nav nav-tabs mb-3" id="nav-tab" role="tablist">
										<li class="nav-item">
											<a class="nav-link tab-color active" id="nav-home-tab" data-toggle="pill" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Описание</a>
										</li>
										<li class="nav-item">
											<a class="nav-link tab-color" id="nav-profile-tab" data-toggle="pill" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Как заказать</a>
										</li>
										<li class="nav-item">
											<a class="nav-link tab-color" id="nav-contact-tab" data-toggle="pill" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Оплата</a>
										</li>
									</ul>
									<div class="tab-content" id="nav-tabContent">
<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
<?php
$catarr = explode(";", $val['p_catids']);
$catarr = array_diff($catarr, array(''));

echo '<p class="product-article"><strong>Артикул:</strong> <span>'.$val['p_vendor_code'].'</span></p>';
switch($val['p_gender']){
	case 'Мальчик':echo '<p class="product-sex"><strong>Пол:</strong> <span><a href="/detskaja-odezhda/?pol=malchik">Одежда для мальчиков</a></span></p>';break;
	case 'Девочка':echo '<p class="product-sex"><strong>Пол:</strong> <span><a href="/detskaja-odezhda/?pol=devochka">Одежда для девочек</a></span></p>';break;
}
echo '<p class="product-age"><strong>Размеры:</strong> <span>'.$val['p_razmery'].'</span></p>';
echo '<p class="product-categories"><strong>Категория:</strong> ';
if(count($catarr) < 2){
	echo '<a href="/detskaja-odezhda/'.$cat['cat_alias'].'">'.$cat['cat_title'].'</a>';
}else{
	for($i = 0; $i < count($catarr);$i++){
		for($x = 0; $x < count($cats);$x++){
			if(trim($catarr[$i],"#") == $cats[$x]['cat_alias']){
				echo '<a href="/detskaja-odezhda/'.$cats[$x]['cat_alias'].'">'.$cats[$x]['cat_title'].'</a>';
			}
		}
		if(($i+1) != count($catarr)){echo ', ';}
	}
}
echo '</p>';
if(isset($val['p_text']) && !empty($val['p_text']) && $val['p_text'] != ''){
	echo '<p><strong>Описание:</strong> '.$val['p_text'].'</p>';
}
?>
</div>
<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
	<h3 class="text-center">Как сделать заказ на нашем сайте</h3>
	<p>Посетите интересующий Вас раздел и, выбрав товар, нажмите на него, чтобы просмотреть подробную информацию.</p>
	<p>После выбора нажмите кнопку "Добавить в корзину" и выбранный Вами товар окажется в Корзине. При этом на экране Вы увидите Вашу корзину товаров. Если Ваши покупки не закончены выберите любую категорию в меню.</p>
	<p>Находясь на любой странице сайта, в верхнем правом углу экрана постоянно отображается количество товаров в Вашей Корзине и их суммарная стоимость. Когда Вы решите оформить заказ, просто нажмите на ссылку <a href="/korzina">"Корзина"</a> в правой верхней части экрана. После этого Вы попадете на страницу Корзины, где можете просмотреть добавленные товары, изменить количество каждого заказываемого товара, либо удалить товар.</p>
	<p>Когда все действия в Корзине завершены, там же, рядом с таблицей товаров, заполните Ваши персональные данные в разделе "Форма для заказа", чтобы мы могли связаться с Вами по поводу Вашего заказа.</p>
	<p>Проверьте, правильно ли написаны ваши контактные данные, если все верно, нажимайте кнопку "Оформить заказ". Ваш заказ поступит к нам и мы свяжемся с Вами в самое ближайшее время.</p>
	<p>Также Вы можете сделать заказ, связавшись с нами по телефонам в разделе <a href="/kontakty">Контакты</a>.</p>
</div>
<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
	<h3 class="text-center">Оплата</h3>
	<p>Отправка осуществляется после полной оплаты заказа.</p>
	<ul>
		<li>На карту Приват Банка</li>
		<li>Наложеным платежом</li>
	</ul>
	<h3 class="pt-2 text-center">Доставка</h3>
	<p>Доставка осуществляется по всей территории Украины следующими транспортными компаниями:</p>
	<ul>
		<li>Новая Почта</li>
		<li>Автолюкс</li>
		<li>Интайм</li>
		<li>Деливери</li>
		<li>Самовывоз</li>
	</ul>
	<p>Стоимость доставки зависит от транспортной компании, веса и объема заказа. Поэтому рассчитать точную стоимость не представляется возможным.</p>
	<p>Доставка оплачивается покупателем при получении заказа. Адреса представительств транспортных компаний и присутствие их в Вашем городе можно посмотреть перейдя по ссылкам на их сайты. (нажмите на логотип интересующей Вас компаний).</p>
	<p>Если Вы затрудняетесь выбрать приемлемый вариант доставки, свяжитесь с нами по телефону или с помощью e-mail и мы порекомендуем оптимальный для Вас вариант.</p>
</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- ********* PRODUCT -->
				
				<!-- OWL CAROUSEL -->
<style type="text/css">
	#srch-tabs .nav-pills>li + li:before{color:#e1e1e1;font-weight:300;font-size:2vw}
	#srch-tabs .product-thumb{position:relative;padding:30px 25px 10px 25px;transition:0.3s}
	#srch-tabs .product-thumb:hover{-webkit-box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2);-moz-box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2);box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2);z-index:5}
	#srch-tabs .product-thumb .labels{position:absolute;top:15px;left:15px}
	#srch-tabs .product-thumb .is_new{background:#005c99}
	#srch-tabs .product-thumb .is_rasprodazha{background:#f25540}
	#srch-tabs .product-thumb .is_popular{background:#fb7131}
	#srch-tabs .product-thumb .label-item{color:#fff;font-size:13px;display:flex;align-items:center;justify-content:center;padding:5px 10px;margin-bottom:4px}
	#srch-tabs .product-thumb .image>a{display:block;background-position:center center;background-size:cover;width:100%;height:250px}
	#srch-tabs .product-thumb h4{margin:20px 0 2px 0;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;font-weight:600;font-size:14px}
	#srch-tabs .product-thumb h4>a{color:#005c99}
	#srch-tabs .product-thumb .stock-free{display:flex;align-items:center;min-height:30px;margin-bottom:5px;justify-content:space-between}
	#srch-tabs .product-thumb .stock-free .stock{font-size:13px}
	#srch-tabs .product-thumb .stock.in-stock{color:#4eba06}
	#srch-tabs .product-thumb .stock.out-stock{color:#ff0000}
	#srch-tabs .product-thumb .rating-flex{padding:6px 0 0 0;align-items:center;font-size:12px}
	#srch-tabs .product-thumb .price{font-size:24px;font-weight:600;color:#0e0e0e;margin:15px 0}
	#srch-tabs .product-thumb .price .price-new{font-size:24px;color:#ef0303;font-weight:600}
	#srch-tabs .product-thumb .price .price-old{font-size:18px;font-weight:400;color:#9c9c9c;text-decoration:line-through;margin-left:30px}
	#srch-tabs .product-thumb:hover .hovers{opacity:1;visibility:visible}
	#srch-tabs .product-thumb .btns{display:flex;align-items:center;justify-content:space-between;margin-bottom:20px}
	#srch-tabs .product-thumb .add-to-wish{width:40px;background:none;border:none;padding:7px 0;opacity:0.5}
	#srch-tabs .product-thumb .add-to-cart{background:-webkit-linear-gradient(#fb822c, #fc6336);background:-o-linear-gradient(#fb822c, #fc6336);background:linear-gradient(#fb822c, #fc6336);padding-left:18px;padding-right:18px;text-align:center;line-height:19px;padding-top:12px;padding-bottom:12px;box-shadow:inset 0 -3px 0px #d85028;font-size:15px;font-weight:600;border-radius:5px;border:none;color:#fff;width:150px;margin:0 auto}
	#srch-tabs .product-thumb .add-to-cart:hover{position:relative;top:1px;color:#fff;box-shadow:none;background:linear-gradient(to bottom, #fc6336, #fb822c)}
	#srch-tabs .product-thumb .options{border-top:1px solid #eeeeee;padding:17px 0}
	#srch-tabs .product-thumb .free-ship{display:flex;align-items:center;background:#fcf1ef;height:30px;border-radius:5px;font-family:Arial;font-size:11px;color:#f25540;padding:0 10px}
	#srch-tabs .product-thumb .free-ship img{height:19px;margin-right:5px;width:auto;}
	#srch-tabs .options-title{font-size:14px;color:#9c9c9c;margin-bottom:10px}
	#srch-tabs .options-value{display:flex;flex-wrap:wrap;margin:0 -4px}
	#srch-tabs .nav-pills .nav-link.active, .nav-pills .show>.nav-link{color:#fff;background-color:#005c99}
	#srch-tabs .nav-pills .nav-link{color:#9c9c9c;background-color:transparent;margin:0 5px;padding:.25rem 1rem}
	#srch-tabs .nav-pills .nav-link:hover{color:#fff;background-color:#005c99}
</style>
				<div class="row my-3">
					<div class="col-12">
						<h5 class="text-center text-uppercase py-2" style="color:#fff;background:#005c99;">Другие товары</h5>
					</div>
				</div>
				<div class="row my-3">
					<div class="col-12">
						<div id="srch-tabs" class="product-tabs">
							<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#novinki" role="tab" aria-controls="novinki" aria-selected="true">Новинки</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#rasprodazha" role="tab" aria-controls="rasprodazha" aria-selected="false">Распродажа</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#hity" role="tab" aria-controls="hity" aria-selected="false">Хиты продаж</a>
								</li>
							</ul>
							<div class="tab-content" id="pills-tabContent">
								<div class="tab-pane fade show active" id="novinki" role="tabpanel" aria-labelledby="pills-home-tab">
<?php
if($novinki){
									echo '<div class="owl-carousel novinki-carousel">';
	foreach($novinki as $val){
		if(!empty($val['p_imglink']) && file_exists('public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink'])){
			$pathimg = '/public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink'];
		}else{$pathimg = '/public/img/no_photo.png';}
										echo '
										<div>
										<div class="product-layout">
											<div class="product-thumb">
												<div class="labels"><div class="label-item is_new">новинка</div></div>
												<div class="image">
													<a href="/product/'.$val['p_alias'].'_'.$val['p_vendor_code'].'" style="background-image:url('.$pathimg.');" title="'.$val['p_name'].'"></a>
												</div>
												<h4><a href="/product/'.$val['p_alias'].'_'.$val['p_vendor_code'].'">'.$val['p_name'].'</a></h4>
												<div class="stock-free">
										';
		switch($val['p_nalichie']){
			case 'да':echo '<div class="stock in-stock"><i class="fas fa-check"></i> Есть в наличии</div>';break;
			case 'нет':echo '<div class="stock out-stock"><i class="fas fa-times"></i> Нет в наличии</div>';break;
		}
										echo '
												</div>
												<div class="rating-flex">
													<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Размеры:</strong> '.$val['p_razmery'].'.</p>
													<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Кол-во в упаковке:</strong> '.$val['p_upakovka'].' шт.</p>
													<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Цена:</strong> '.$val['p_opt_price'].' грн. за шт.</p>
												</div>
												<div class="price text-center">
													'.($val['p_opt_price'] * $val['p_upakovka']).' грн.
												</div>
												';
											if($val['p_nalichie'] == 'да'){
												echo '
												<div class="btns">
													<button type="button" class="add-to-cart" data-pid="'.$val['p_id'].'" data-vcode="'.$val['p_vendor_code'].'" data-value="'.$val['p_upakovka'].'" data-price="'.$val['p_opt_price'].'">Купить</button>
												</div>
												';
											}else{
												echo '
												<div class="btns">
													<button type="button" class="btn-in-modal" data-toggle="modal" data-target="#phoneModal" data-pid="'.$val['p_id'].'" data-vcode="'.$val['p_vendor_code'].'" data-value="'.$val['p_upakovka'].'" data-price="'.$val['p_opt_price'].'">Заказать звонок</button>
												</div>
												';
											}
										echo '
											</div>
										</div>
										</div>
										';
	}
									echo '</div>';
}else{echo '<p class="text-center">Нет товара в НОВИНКИ</p>';}
								echo '
								</div>
								<div class="tab-pane fade" id="rasprodazha" role="tabpanel" aria-labelledby="pills-profile-tab">
								';
if($rasprodazha){
									echo '<div class="owl-carousel rasprodazha-carousel">';
	foreach($rasprodazha as $val){
		if(!empty($val['p_imglink']) && file_exists('public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink'])){
			$pathimg = '/public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink'];
		}else{$pathimg = '/public/img/no_photo.png';}
		$price = $val['p_opt_price'] * $val['p_upakovka'];
		$old_price = $price * 1.3;
										echo '
										<div>
										<div class="product-layout">
											<div class="product-thumb">
												<div class="labels"><div class="label-item is_rasprodazha">скидка</div></div>
												<div class="image">
													<a href="/product/'.$val['p_alias'].'_'.$val['p_vendor_code'].'" style="background-image:url('.$pathimg.');" title="'.$val['p_name'].'"></a>
												</div>
												<h4><a href="/product/'.$val['p_alias'].'_'.$val['p_vendor_code'].'">'.$val['p_name'].'</a></h4>
												<div class="stock-free">
										';
		switch($val['p_nalichie']){
			case 'да':echo '<div class="stock in-stock"><i class="fas fa-check"></i> Есть в наличии</div>';break;
			case 'нет':echo '<div class="stock out-stock"><i class="fas fa-times"></i> Нет в наличии</div>';break;
		}
										echo '
												</div>
												<div class="rating-flex">
													<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Размеры:</strong> '.$val['p_razmery'].'.</p>
													<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Кол-во в упаковке:</strong> '.$val['p_upakovka'].' шт.</p>
													<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Цена:</strong> '.$val['p_opt_price'].' грн. за шт.</p>
												</div>
												<div class="price text-center">
													<span class="price-new">'.$price.' грн.</span> <span class="price-old">'.$old_price.' грн.</span>
												</div>
												';
											if($val['p_nalichie'] == 'да'){
												echo '
												<div class="btns">
													<button type="button" class="add-to-cart" data-pid="'.$val['p_id'].'" data-vcode="'.$val['p_vendor_code'].'" data-value="'.$val['p_upakovka'].'" data-price="'.$val['p_opt_price'].'">Купить</button>
												</div>
												';
											}else{
												echo '
												<div class="btns">
													<button type="button" class="btn-in-modal" data-toggle="modal" data-target="#phoneModal" data-pid="'.$val['p_id'].'" data-vcode="'.$val['p_vendor_code'].'" data-value="'.$val['p_upakovka'].'" data-price="'.$val['p_opt_price'].'">Заказать звонок</button>
												</div>
												';
											}
										echo '
											</div>
										</div>
										</div>
										';
	}
									echo '</div>';
}else{echo '<p class="text-center">Нет товара в СКИДКА</p>';}
								echo '
								</div>
								<div class="tab-pane fade" id="hity" role="tabpanel" aria-labelledby="pills-contact-tab">
								';
if($popular){
									echo '<div class="owl-carousel hity-carousel">';
	foreach($popular as $val){
		if(!empty($val['p_imglink']) && file_exists('public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink'])){
			$pathimg = '/public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink'];
		}else{$pathimg = '/public/img/no_photo.png';}
										echo '
										<div>
										<div class="product-layout">
											<div class="product-thumb">
												<div class="labels"><div class="label-item is_popular">хит продаж</div></div>
												<div class="image">
													<a href="/product/'.$val['p_alias'].'_'.$val['p_vendor_code'].'" style="background-image:url('.$pathimg.');" title="'.$val['p_name'].'"></a>
												</div>
												<h4><a href="/product/'.$val['p_alias'].'_'.$val['p_vendor_code'].'">'.$val['p_name'].'</a></h4>
												<div class="stock-free">
										';
		switch($val['p_nalichie']){
			case 'да':echo '<div class="stock in-stock"><i class="fas fa-check"></i> Есть в наличии</div>';break;
			case 'нет':echo '<div class="stock out-stock"><i class="fas fa-times"></i> Нет в наличии</div>';break;
		}
										echo '
												</div>
												<div class="rating-flex">
													<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Размеры:</strong> '.$val['p_razmery'].'.</p>
													<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Кол-во в упаковке:</strong> '.$val['p_upakovka'].' шт.</p>
													<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Цена:</strong> '.$val['p_opt_price'].' грн. за шт.</p>
												</div>
												<div class="price text-center">
													'.($val['p_opt_price'] * $val['p_upakovka']).' грн.
												</div>
												';
											if($val['p_nalichie'] == 'да'){
												echo '
												<div class="btns">
													<button type="button" class="add-to-cart" data-pid="'.$val['p_id'].'" data-vcode="'.$val['p_vendor_code'].'" data-value="'.$val['p_upakovka'].'" data-price="'.$val['p_opt_price'].'">Купить</button>
												</div>
												';
											}else{
												echo '
												<div class="btns">
													<button type="button" class="btn-in-modal" data-toggle="modal" data-target="#phoneModal" data-pid="'.$val['p_id'].'" data-vcode="'.$val['p_vendor_code'].'" data-value="'.$val['p_upakovka'].'" data-price="'.$val['p_opt_price'].'">Заказать звонок</button>
												</div>
												';
											}
										echo '
											</div>
										</div>
										</div>
										';
	}
									echo '</div>';
}else{echo '<p class="text-center">Нет товара в ХИТЫ ПРОДАЖ</p>';}
?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- ******* OWL CAROUSEL -->

				<!-- ANOTHER CATS -->
<style type="text/css">
	.no-webp .srch-cat .cimage1{background-image:url('/public/img/cuteboy.jpg')}
	.webp .srch-cat .cimage1{background-image:url('/public/img/webp/cuteboy.webp')}
	.no-webp .srch-cat .cimage2{background-image:url('/public/img/cutegirl.jpg')}
	.webp .srch-cat .cimage2{background-image:url('/public/img/webp/cutegirl.webp')}
	.no-webp .srch-cat .cimage3{background-image:url('/public/img/happychildrens.png')}
	.webp .srch-cat .cimage3{background-image:url('/public/img/webp/happychildrens.webp')}
	.no-webp .srch-cat .cimage4{background-image:url('/public/img/shok.jpg')}
	.webp .srch-cat .cimage4{background-image:url('/public/img/webp/shok.webp')}
	#srch-cat .category{-webkit-box-shadow: 0px 0px 15px 0px rgba(0, 92, 153, 0.2);-moz-box-shadow: 0px 0px 15px 0px rgba(0, 92, 153, 0.2);box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2)}
	#srch-cat .cat_image{background-position:center;background-repeat:no-repeat;background-size:cover;width:100%;height:262px}
	#srch-cat .cat_image a{width:100%;height:100%;display:block}
	#srch-cat .cat_name{text-decoration:none;color:#000;font-size:20px;}
@media(max-width:576px){
	
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
				<div class="row my-3">
					<div class="col-12">
						<h5 class="text-center text-uppercase py-2" style="color:#fff;background:#005c99;">Другие категории</h5>
					</div>
				</div>
				<div id="srch-cat" class="row srch-cat">
					<div class="col-12 col-lg-3 my-2 my-md-0">
						<div class="category rounded">
							<p class="cat_image cimage1"><a href="/detskaja-odezhda/?pol=malchik"></a></p>
							<h3 class="py-2 text-center">
								<a href="/detskaja-odezhda/?pol=malchik" class="cat_name text-uppercase">Одежда для<br>Мальчиков</a>
							</h3>
						</div>
					</div>
					<div class="col-12 col-lg-3 my-2 my-md-0">
						<div class="category rounded">
							<p class="cat_image cimage2"><a href="/detskaja-odezhda/?pol=devochka"></a></p>
							<h3 class="py-2 text-center">
								<a href="/detskaja-odezhda/?pol=devochka" class="cat_name text-uppercase">Одежда для<br>Девочек</a>
							</h3>
						</div>
					</div>
					<div class="col-12 col-lg-3 my-2 my-md-0">
						<div class="category rounded">
							<p class="cat_image cimage3"><a href="/detskaja-odezhda/novinki"></a></p>
							<h3 class="py-2 text-center">
								<a href="/detskaja-odezhda/novinki" class="cat_name text-uppercase">Наши<br>Новинки</a>
							</h3>
						</div>
					</div>
					<div class="col-12 col-lg-3 my-2 my-md-0">
						<div class="category rounded">
							<p class="cat_image cimage4"><a href="/detskaja-odezhda/rasprodazha"></a></p>
							<h3 class="py-2 text-center">
								<a href="/detskaja-odezhda/rasprodazha" class="cat_name text-uppercase">Уцененная<br>Одежда</a>
							</h3>
						</div>
					</div>
				</div>
				<!-- ******* ANOTHER CATS -->
			</div>
		</div>
	</div>
</section>