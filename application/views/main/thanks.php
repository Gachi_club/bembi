<section id="thanks" class="thanks mt-3">
	<div class="container">
		<?php
		if(isset($orders) && !empty($orders)){
		if($orders['ord_status_id'] == '1'){$ordstatus = 'Новый';}
		elseif($orders['ord_status_id'] == '2'){$ordstatus = 'Обработан';}
		elseif($orders['ord_status_id'] == '3'){$ordstatus = 'Отменен';}
		else{$ordstatus = '';}
		?>
		<div class="row my-3">
			<div class="col-9 col-sm-9 col-md-9 col-lg-9 col-xl-9">
				<h1><?=$orders['user_name'];?>, благодарим Вас за заказ № <?=$orders['ord_id'];?>, от <?=$orders['ord_date'];?></h1>
			</div>
			<div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-right">
				<p class="print"><a href="#print-this-document" id="print"><i class="fas fa-print"></i> Распечатать заказ</a></p>
			</div>
		</div>
		<div class="row">
			<div class="col-12 table-responsive">
				<table border="0" class="table table-striped table-sm table-hover">
					<thead>
						<tr>
							<th scope="col">Наименование</th>
							<th scope="col">Описание</th>
							<th scope="col">Артикул</th>
							<th scope="col">Кол-во</th>
							<th scope="col">Стоимость</th>
						</tr>
					</thead>
					<tbody>
		<?php
		$i = 0;
		while($i <= ($orders['ord_sum_prod']-1)){
			/* Разбираем данные из ордера */
			$prodid = explode("|",$orders['ord_prod_id']);
			$prodcnt = explode("|",$orders['ord_prod_count']);
			$prodcolor = explode("|",$orders['ord_prod_color']);
		
			/* Выбираем конкретный товар. Товар находится в массиве, поэтому используем "-1" */
			$x = $prodid[$i]-1;
			$ttler = $ordprod[$x]['p_name'];

			/* Получаем изображение */
			if(file_exists('public/products/'.$ordprod[$x]['p_imglink'])){
				$pathimg = '/public/products/'.$ordprod[$x]['p_imglink'];
			}else{$pathimg = '/public/img/no_photo.png';}

		echo '
		<tr>
			<td class="text-center">
				<p><img src="'.$pathimg.'" alt="'.$ttler.'" class="lazy img-fluid"></p>
				<p><a href="/product/'.$prodid[$i].'" class="tabvalue">'.$ttler.'</a></p>
			</td>
			<td><p>'.$ordprod[$x]['p_text'].'</p></td>
			<td class="id">'.$ordprod[$x]['p_vendor_code'].'</td>
			<td class="cnt">'.$prodcnt[$i].'</td>
			<td class="cost">'.($ordprod[$x]['p_price']*$prodcnt[$i]).' грн.</td>
		</tr>
		';
			$i++;
		}
		?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-12 text-right">
				<h4><strong>Сумма к оплате:</strong> <?=$orders['ord_sum_price'];?> грн.</h4>
			</div>
		</div>
		<div class="row d-none d-print-flex">
			<div class="col-12 text-left">
				<h5>КОНТАКТЫ:</h5>
				<ul style="list-style-type:none;">
					<li>E-mail: <a href="mailto:die@spammers.die" onClick="<?=$settings[7]['set_value'];?>"><?=$settings[6]['set_value'];?></a></li>
					<li>График работы: ежедневно 10:00 - 20:00</li>
					<li>Телефоны: <a href="<?=$settings[1]['set_value'];?>"><?=$settings[0]['set_value'];?></a>, <a href="<?=$settings[3]['set_value'];?>"><?=$settings[2]['set_value'];?></a></li>
				</ul>
			</div>
		</div>
		<div class="row d-none d-print-flex pt-3" style="border-top:1px solid rgba(0,0,0,.5);">
			<div class="col-6">
				<p><?=$orders['ord_date'];?></p>
			</div>
			<div class="col-6 text-right">
				<a href="/" style="text-decoration:none; display:block;">
					<h2 style="font-family:'Dancing Script', cursive;">NoEl.od.ua</h2>
				</a>
			</div>
		</div>
<script type="text/javascript">
	$("#print").on('click', function(){print(); return false;});
</script>
		<?php
		}else{
		?>
		<div class="row">
			<div class="col-12 text-center">
				<h5>Вы сегодня ничего не заказывали. Через несколько секунд Вас перенаправит на <a href="/">Главную страницу</a>.</h5>
			</div>
		</div>
<script type="text/javascript">
	setTimeout(function(){window.location.href="/";},3000);
</script>
		<?php
		}
		?>
	</div>
</section>