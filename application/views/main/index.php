<style type="text/css">
	.sct1 .flex-height{display:flex;margin-bottom:50px;align-items:stretch}
	.sct1 .index-carousel{box-shadow:0px 4px 15px rgba(0, 92, 153, 0.20);height:100%;}
	.sct1 .index-carousel img{height:450px;}
@media(max-width:576px){}
@media(min-width:576px){}
@media(min-width:768px){
	.sct1 .index-carousel img{height:300px;}
}
@media(min-width:992px){}
@media(min-width:1024px){
	.sct1 .index-carousel img{height:400px;}
}
@media(min-width:1200px){}
</style>
<section class="sct1 mt-3 d-none d-md-block">
	<div class="container">
		<div class="row flex-height">
			<div class="col-12">
				<div class="owl-carousel index-carousel">
					<?php
foreach($slider as $val){
	if(!empty($val['s_imglink']) && file_exists('public/bembiTMP/slider/'.$val['s_imglink'])){
		$pathimg = '/public/bembiTMP/slider/'.$val['s_imglink'];
	}else{$pathimg = '/public/img/no_photo.png';}
					echo '<div>
						<a href="'.$val['s_link'].'">
							<img data-src="'.$pathimg.'" alt="'.$val['s_alt'].'" class="owl-lazy img-fluid">
						</a>
					</div>';
}
					?>
				</div>
			</div>
		</div>
	</div>
</section>

<style type="text/css">
	.sct2 h2{color:#005c99;font-weight:600;font-size:2.3vw;/*margin-bottom:20px*/}
	.sct2 .windows-content > div:first-child .windows-item span{left:auto;right:50px}
	.sct2 .windows-item{height:230px;display:block;position:relative;margin-bottom:30px;background-position:center center;background-size:cover;text-decoration:none!important}
	.sct2 .windows-item span{display:block;position:absolute;left:15px;bottom:17px;color:#005c99;font-weight:600;background:#fff;padding:12px 21px;border-radius:25px;line-height:1}
	.sct2 .windcat1{background-image:url('/public/img/cat1.jpg');}
	.sct2 .windcat2{background-image:url('/public/img/cat2.jpg');}
	.sct2 .windcat3{background-image:url('/public/img/cat3.jpg');}
	.sct2 .windcat4{background-image:url('/public/img/cat4.jpg');}
	.sct2 .windcat5{background-image:url('/public/img/cat5.jpg');}
	.sct2 .windcat6{background-image:url('/public/img/cat6.jpg');}
	.sct2 .windcat7{background-image:url('/public/img/cat7.jpg');}
@media(max-width:576px){
	.sct2 h2{font-size:8.3vw;}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
<section class="sct2 mt-3 mt-md-0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="mb-2 mb-md-4 text-center text-md-left">Популярные категории</h2>
			</div>
		</div>
		<div class="row windows-content">
			<div class="col-12 col-md-6 col-lg-8">
				<a href="/detskaja-odezhda/platja" class="windows-item windcat1" title="Детские платья"><span>Детские платья</span></a>
			</div>
			<div class="col-12 col-md-6 col-lg-4">
				<a href="/detskaja-odezhda/demisezonnye-kostjumy" class="windows-item windcat2" title="Детские костюмы"><span>Детские демисезонные костюмы</span></a>
			</div>
			<div class="col-12 col-md-4 col-lg-4">
				<a href="/detskaja-odezhda/kurtki" class="windows-item windcat3" title="Детские куртки"><span>Детские куртки</span></a>
			</div>
			<div class="col-12 col-md-4 col-lg-4">
				<a href="/detskaja-odezhda/futbolki" class="windows-item windcat4" title="Детские футболки"><span>Детские футболки</span></a>
			</div>
			<div class="col-12 col-md-4 col-lg-4">
				<a href="/detskaja-odezhda/svitera" class="windows-item windcat5" title="Детские свитера"><span>Детские свитера</span></a>
			</div>
			<div class="col-12 col-md-6 col-lg-4">
				<a href="/detskaja-odezhda/losiny" class="windows-item windcat6" title="Детские лосины"><span>Детские лосины</span></a>
			</div>
			<div class="col-12 col-md-6 col-lg-8">
				<a href="/detskaja-odezhda/shkolnaja-forma" class="windows-item windcat7" title="Школьная форма"><span>Школьная форма</span></a>
			</div>
		</div>
	</div>
</section>

<style type="text/css">
	.product-tabs .nav-pills>li + li:before{color:#e1e1e1;font-weight:300;font-size:2vw}
	.product-thumb{position:relative;padding:30px 25px 10px 25px;transition:0.3s}
	.product-thumb:hover{-webkit-box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2);-moz-box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2);box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2);z-index:5}
	.product-thumb .labels{position:absolute;top:15px;left:15px}
	.product-thumb .is_new{background:#005c99}
	.product-thumb .is_rasprodazha{background:#f25540}
	.product-thumb .is_popular{background:#fb7131}
	.product-thumb .label-item{color:#fff;font-size:13px;display:flex;align-items:center;justify-content:center;padding:5px 10px;margin-bottom:4px}
	.product-thumb .image>a{display:block;background-position:center center;background-size:cover;width:100%;height:250px}
	.product-thumb h4{margin:20px 0 2px 0;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;font-weight:600;font-size:14px}
	.product-thumb h4>a{color:#005c99}
	.product-thumb .stock-free{display:flex;align-items:center;min-height:30px;margin-bottom:5px;justify-content:space-between}
	.product-thumb .stock-free .stock{font-size:13px}
	.product-thumb .stock.in-stock{color:#4eba06}
	.product-thumb .stock.out-stock{color:#ff0000}
	.product-thumb .rating-flex{padding:6px 0 0 0;align-items:center;font-size:12px}
	.product-thumb .price{font-size:24px;font-weight:600;color:#0e0e0e;margin:15px 0}
	.product-thumb .price .price-new{font-size:24px;color:#ef0303;font-weight:600}
	.product-thumb .price .price-old{font-size:18px;font-weight:400;color:#9c9c9c;text-decoration:line-through;margin-left:30px}
	.product-thumb:hover .hovers{opacity:1;visibility:visible}
	.product-thumb .btns{display:flex;align-items:center;justify-content:space-between;margin-bottom:20px}
	.product-thumb .add-to-wish{width:40px;background:none;border:none;padding:7px 0;opacity:0.5}
	.product-thumb .add-to-cart{background:-webkit-linear-gradient(#fb822c, #fc6336);background:-o-linear-gradient(#fb822c, #fc6336);background:linear-gradient(#fb822c, #fc6336);padding-left:18px;padding-right:18px;text-align:center;line-height:19px;padding-top:12px;padding-bottom:12px;box-shadow:inset 0 -3px 0px #d85028;font-size:15px;font-weight:600;border-radius:5px;border:none;color:#fff;width:150px;margin:0 auto}
	.product-thumb .add-to-cart:hover{position:relative;top:1px;color:#fff;box-shadow:none;background:linear-gradient(to bottom, #fc6336, #fb822c)}
	.product-thumb .options{border-top:1px solid #eeeeee;padding:17px 0}
	.product-thumb .free-ship{display:flex;align-items:center;background:#fcf1ef;height:30px;border-radius:5px;font-family:Arial;font-size:11px;color:#f25540;padding:0 10px}
	.product-thumb .free-ship img{height:19px;margin-right:5px;width:auto;}
	.options-title{font-size:14px;color:#9c9c9c;margin-bottom:10px}
	.options-value{display:flex;flex-wrap:wrap;margin:0 -4px}
	.nav-pills .nav-link.active, .nav-pills .show>.nav-link{color:#005c99;background-color:transparent}
	.nav-pills .nav-link{color:#9c9c9c;background-color:transparent}
	.nav-pills .nav-link:hover{color:#005c99;background-color:transparent}
@media(max-width:576px){}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
<section class="sct3">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="product-tabs">
					<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#novinki" role="tab" aria-controls="novinki" aria-selected="true">Новинки</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#rasprodazha" role="tab" aria-controls="rasprodazha" aria-selected="false">Распродажа</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#hity" role="tab" aria-controls="hity" aria-selected="false">Хиты продаж</a>
						</li>
					</ul>
					<div class="tab-content" id="pills-tabContent">
						<div class="tab-pane fade show active" id="novinki" role="tabpanel" aria-labelledby="pills-home-tab">
		<?php
if($novinki){
							echo '<div class="owl-carousel novinki-carousel">';
	foreach($novinki as $val){
		if(!empty($val['p_imglink']) && file_exists('public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink'])){
			$pathimg = '/public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink'];
		}else{$pathimg = '/public/img/no_photo.png';}
								echo '
								<div class="product-layout">
									<div class="product-thumb">
										<div class="labels"><div class="label-item is_new">новинка</div></div>
										<div class="image">
											<a href="/product/'.$val['p_alias'].'_'.$val['p_vendor_code'].'" style="background-image:url('.$pathimg.');" title="'.$val['p_name'].'"></a>
										</div>
										<h4><a href="/product/'.$val['p_alias'].'_'.$val['p_vendor_code'].'">'.$val['p_name'].'</a></h4>
										<div class="stock-free">
								';
		switch($val['p_nalichie']){
			case 'да':echo '<div class="stock in-stock"><i class="fas fa-check"></i> Есть в наличии</div>';break;
			case 'нет':echo '<div class="stock out-stock"><i class="fas fa-times"></i> Нет в наличии</div>';break;
		}
								echo '
										</div>
										<div class="rating-flex">
											<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Размеры:</strong> '.$val['p_razmery'].'.</p>
											<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Кол-во в упаковке:</strong> '.$val['p_upakovka'].' шт.</p>
											<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Цена:</strong> '.$val['p_opt_price'].' грн. за шт.</p>
										</div>
										<div class="price text-center">
											'.($val['p_opt_price'] * $val['p_upakovka']).' грн.
										</div>
										';
									if($val['p_nalichie'] == 'да'){
										echo '
										<div class="btns">
											<button type="button" class="add-to-cart" data-pid="'.$val['p_id'].'" data-vcode="'.$val['p_vendor_code'].'" data-value="'.$val['p_upakovka'].'" data-price="'.$val['p_opt_price'].'">Купить</button>
										</div>
										';
									}else{
										echo '
										<div class="btns">
											<button type="button" class="btn-in-modal" data-toggle="modal" data-target="#phoneModal" data-pid="'.$val['p_id'].'" data-vcode="'.$val['p_vendor_code'].'" data-value="'.$val['p_upakovka'].'" data-price="'.$val['p_opt_price'].'">Заказать звонок</button>
										</div>
										';
									}
								echo '
									</div>
								</div>
								';
	}
							echo '</div>';
}else{echo '<p class="text-center">Нет товара в НОВИНКИ</p>';}
		?>
						</div>
						<div class="tab-pane fade" id="rasprodazha" role="tabpanel" aria-labelledby="pills-profile-tab">
		<?php
if($rasprodazha){
							echo '<div class="owl-carousel rasprodazha-carousel">';
	foreach($rasprodazha as $val){
		if(!empty($val['p_imglink']) && file_exists('public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink'])){
			$pathimg = '/public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink'];
		}else{$pathimg = '/public/img/no_photo.png';}
		$price = $val['p_opt_price'] * $val['p_upakovka'];
		$old_price = $price * 1.3;
								echo '
								<div class="product-layout">
									<div class="product-thumb">
										<div class="labels"><div class="label-item is_rasprodazha">скидка</div></div>
										<div class="image">
											<a href="/product/'.$val['p_alias'].'_'.$val['p_vendor_code'].'" style="background-image:url('.$pathimg.');" title="'.$val['p_name'].'"></a>
										</div>
										<h4><a href="/product/'.$val['p_alias'].'_'.$val['p_vendor_code'].'">'.$val['p_name'].'</a></h4>
										<div class="stock-free">
								';
		switch($val['p_nalichie']){
			case 'да':echo '<div class="stock in-stock"><i class="fas fa-check"></i> Есть в наличии</div>';break;
			case 'нет':echo '<div class="stock out-stock"><i class="fas fa-times"></i> Нет в наличии</div>';break;
		}
								echo '
										</div>
										<div class="rating-flex">
											<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Размеры:</strong> '.$val['p_razmery'].'.</p>
											<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Кол-во в упаковке:</strong> '.$val['p_upakovka'].' шт.</p>
											<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Цена:</strong> '.$val['p_opt_price'].' грн. за шт.</p>
										</div>
										<div class="price text-center">
											<span class="price-new">'.$price.' грн.</span> <span class="price-old">'.$old_price.' грн.</span>
										</div>
										';
									if($val['p_nalichie'] == 'да'){
										echo '
										<div class="btns">
											<button type="button" class="add-to-cart" data-pid="'.$val['p_id'].'" data-vcode="'.$val['p_vendor_code'].'" data-value="'.$val['p_upakovka'].'" data-price="'.$val['p_opt_price'].'">Купить</button>
										</div>
										';
									}else{
										echo '
										<div class="btns">
											<button type="button" class="btn-in-modal" data-toggle="modal" data-target="#phoneModal" data-pid="'.$val['p_id'].'" data-vcode="'.$val['p_vendor_code'].'" data-value="'.$val['p_upakovka'].'" data-price="'.$val['p_opt_price'].'">Заказать звонок</button>
										</div>
										';
									}
								echo '
									</div>
								</div>
								';
	}
							echo '</div>';
}else{echo '<p class="text-center">Нет товара в СКИДКА</p>';}
		?>
						</div>
						<div class="tab-pane fade" id="hity" role="tabpanel" aria-labelledby="pills-contact-tab">
		<?php
if($popular){
							echo '<div class="owl-carousel hity-carousel">';
	foreach($popular as $val){
		if(!empty($val['p_imglink']) && file_exists('public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink'])){
			$pathimg = '/public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink'];
		}else{$pathimg = '/public/img/no_photo.png';}
								echo '
								<div class="product-layout">
									<div class="product-thumb">
										<div class="labels"><div class="label-item is_popular">хит продаж</div></div>
										<div class="image">
											<a href="/product/'.$val['p_alias'].'_'.$val['p_vendor_code'].'" style="background-image:url('.$pathimg.');" title="'.$val['p_name'].'"></a>
										</div>
										<h4><a href="/product/'.$val['p_alias'].'_'.$val['p_vendor_code'].'">'.$val['p_name'].'</a></h4>
										<div class="stock-free">
								';
		switch($val['p_nalichie']){
			case 'да':echo '<div class="stock in-stock"><i class="fas fa-check"></i> Есть в наличии</div>';break;
			case 'нет':echo '<div class="stock out-stock"><i class="fas fa-times"></i> Нет в наличии</div>';break;
		}
								echo '
										</div>
										<div class="rating-flex">
											<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Размеры:</strong> '.$val['p_razmery'].'.</p>
											<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Кол-во в упаковке:</strong> '.$val['p_upakovka'].' шт.</p>
											<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Цена:</strong> '.$val['p_opt_price'].' грн. за шт.</p>
										</div>
										<div class="price text-center">
											'.($val['p_opt_price'] * $val['p_upakovka']).' грн.
										</div>
										';
									if($val['p_nalichie'] == 'да'){
										echo '
										<div class="btns">
											<button type="button" class="add-to-cart" data-pid="'.$val['p_id'].'" data-vcode="'.$val['p_vendor_code'].'" data-value="'.$val['p_upakovka'].'" data-price="'.$val['p_opt_price'].'">Купить</button>
										</div>
										';
									}else{
										echo '
										<div class="btns">
											<button type="button" class="btn-in-modal" data-toggle="modal" data-target="#phoneModal" data-pid="'.$val['p_id'].'" data-vcode="'.$val['p_vendor_code'].'" data-value="'.$val['p_upakovka'].'" data-price="'.$val['p_opt_price'].'">Заказать звонок</button>
										</div>
										';
									}
								echo '
									</div>
								</div>
								';
	}
							echo '</div>';
}else{echo '<p class="text-center">Нет товара в ХИТЫ ПРОДАЖ</p>';}
		?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<style type="text/css">
	.sct4{background:#eff6f9;/*padding:80px 0*/}
	.sct4 .utm-item img{height:57px;float:left;margin-right:15px}
	.sct4 .utm-item a{text-decoration:underline;color:#005c99;font-weight:600;/*font-size:1.2vw;*/white-space:nowrap;margin-bottom:7px}
@media(max-width:576px){
	.sct4 .utm-item a{font-size:4.5vw;}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
<section class="sct4 py-5">
	<div class="container py-3 py-md-0">
		<div class="row">
			<div class="col-12 col-lg-4">
				<div class="utm-item">
					<img src="/public/img/icons/piggy-bank.svg" alt="Купить детскую одежду оптом">
					<a href="/organizatoram-sp">А хотите ещё дешевле?</a>
					<p>Хотите купить новую коллекцию детской одежды со скидками?</p>
				</div>
			</div>
			<div class="col-12 col-lg-4 my-5 my-lg-0">
				<div class="utm-item">
					<img src="/public/img/icons/reload.svg" alt="Детская одежда оптом">
					<a href="/garantija-i-vozvrat">14 дней на возврат</a>
					<p>Теперь вы можете не опасаться, что купленная заранее верхняя одежда не подойдет ребенку к сезону</p>
				</div>
			</div>
			<div class="col-12 col-lg-4">
				<div class="utm-item">
					<img src="/public/img/icons/like.svg" alt="Одежда детская оптом">
					<a href="/o-magazine">5 причин купить у нас</a>
					<p>Весомые причины почему нужно делать покупки в «Бемби»</p>
				</div>
			</div>
		</div>
	</div>
</section>

<style type="text/css">
.no-webp .sct5 .cimage1{background-image:url('/public/img/cuteboy.jpg')}
.webp .sct5 .cimage1{background-image:url('/public/img/webp/cuteboy.webp')}
.no-webp .sct5 .cimage2{background-image:url('/public/img/cutegirl.jpg')}
.webp .sct5 .cimage2{background-image:url('/public/img/webp/cutegirl.webp')}
.no-webp .sct5 .cimage3{background-image:url('/public/img/happychildrens.png')}
.webp .sct5 .cimage3{background-image:url('/public/img/webp/happychildrens.webp')}
.no-webp .sct5 .cimage4{background-image:url('/public/img/shok.jpg')}
.webp .sct5 .cimage4{background-image:url('/public/img/webp/shok.webp')}
.sct5 .category{-webkit-box-shadow: 0px 0px 15px 0px rgba(0, 92, 153, 0.2);-moz-box-shadow: 0px 0px 15px 0px rgba(0, 92, 153, 0.2);box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2)}
.sct5 .cat_image{background-position:center;background-repeat:no-repeat;background-size:cover;width:100%;height:262px}
.sct5 .cat_image a{width:100%;height:100%;display:block}
.sct5 .cat_name{text-decoration:none;color:#000;font-size:20px;}
</style>
<section class="sct5 my-3 my-md-5">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6 col-lg-3 my-2 my-lg-0">
				<div class="category rounded">
					<p class="cat_image cimage1"><a href="/detskaja-odezhda/?pol=malchik"></a></p>
					<h3 class="py-2 text-center">
						<a href="/detskaja-odezhda/?pol=malchik" class="cat_name text-uppercase">Одежда для<br>Мальчиков</a>
					</h3>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-3 my-2 my-lg-0">
				<div class="category rounded">
					<p class="cat_image cimage2"><a href="/detskaja-odezhda/?pol=devochka"></a></p>
					<h3 class="py-2 text-center">
						<a href="/detskaja-odezhda/?pol=devochka" class="cat_name text-uppercase">Одежда для<br>Девочек</a>
					</h3>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-3 my-2 my-lg-0">
				<div class="category rounded">
					<p class="cat_image cimage3"><a href="/detskaja-odezhda/novinki"></a></p>
					<h3 class="py-2 text-center">
						<a href="/detskaja-odezhda/novinki" class="cat_name text-uppercase">Наши<br>Новинки</a>
					</h3>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-3 my-2 my-lg-0">
				<div class="category rounded">
					<p class="cat_image cimage4"><a href="/detskaja-odezhda/rasprodazha"></a></p>
					<h3 class="py-2 text-center">
						<a href="/detskaja-odezhda/rasprodazha" class="cat_name text-uppercase">Уцененная<br>Одежда</a>
					</h3>
				</div>
			</div>
		</div>
	</div>
</section>

<style type="text/css">
	.sct6 h2 a{font-size:2.3vw;font-weight:600;color:#005c99}
	.sct6 .news-thumb{height:100%;-webkit-box-shadow: 0px 0px 15px 0px rgba(0, 92, 153, 0.2);-moz-box-shadow: 0px 0px 15px 0px rgba(0, 92, 153, 0.2);box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2)}
	.sct6 .news-thumb .image{width:100%;height:262px}
	.sct6 .news-thumb .image a{display:block;width:100%;height:100%;background-position:center;background-repeat:no-repeat;background-size:cover;}
	.sct6 .news-thumb .posted{color:#9c9c9c;font-size:14px;/*margin-bottom:10px*/}
	.sct6 .news-thumb h4 a{color:#005c99;font-size:14px;font-weight:600}
	.sct6 .show-more{min-width:165px;background:-webkit-linear-gradient(#fb822c, #fc6336);background:-o-linear-gradient(#fb822c, #fc6336);background:linear-gradient(#fb822c, #fc6336);padding:12px 18px;text-align:center;box-shadow:inset 0 -3px 0px #d85028;font-size:15px;font-weight:600;border-radius:5px;border:none;color:#fff}
	.sct6 .show-more:hover{position:relative;top:1px;color:#fff;box-shadow:none;background:linear-gradient(to bottom, #fc6336, #fb822c)}
@media(max-width:576px){
	.sct6 h2 a{font-size:8.3vw;}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
<section class="sct6 mt-3 mt-md-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="mb-2 mb-md-4 text-center text-md-left"><a href="/novosti">Новости и статьи</a></h2>
			</div>
		</div>
		<div class="row">
			<?php
			if($news){
				foreach($news as $val){
					if(!empty($val['news_imglink']) && file_exists('public/bembiTMP/news/'.$val['news_imglink'])){
						$pathimg = '/public/bembiTMP/news/'.$val['news_imglink'];
					}else{$pathimg = '/public/img/no_photo.png';}
			echo '
			<div class="col-12 col-md-6 col-lg-3 my-2 my-lg-0">
				<div class="news-thumb">
					<div class="image">
						<a href="/statja/'.$val['news_alias'].'" style="background-image:url('.$pathimg.');"></a>
					</div>
					<div class="px-3 py-2">
						<div class="posted mb-1">'.$val['news_date'].'</div>
						<h4><a href="/statja/'.$val['news_alias'].'">'.$val['news_title'].'</a></h4>
					</div>
				</div>
			</div>
			';
				}
			}else{echo '<div class="col-12 text-center">Нет новостей</div>';}
			?>
		</div>
		<div class="row mt-4 mb-3">
			<div class="col-12 text-center"><a href="/novosti" class="show-more btn btn-warning">Все новости</a></div>
		</div>
	</div>
</section>

<style type="text/css">
	.sct7{background:#fbeba8;/*padding:70px 0;*/margin-bottom:50px;position:relative;}
	.sct7 .container:before{content:"";background-image:url(/public/img/icons/envelope.svg);display:block;width:95px;height:75px;background-size:contain;background-repeat:no-repeat;position:absolute;transform:rotate(-35deg);bottom:100px;left:50px;}
	.sct7 .s-form{display:flex;align-items:center;justify-content:center;}
	.sct7 .s-form label{color:#0e0e0e;/*margin-right:70px;*/margin-bottom:0;}
	.sct7 .form-subscribe{display: flex;align-items: center;position:relative;}
	.sct7 .s-form input{height:44px;border-color:#e1e1e1;font-weight:600;padding:12px 25px;/*min-width:340px;margin-right:15px*/}
	.sct7 .s-form .btn-subcribe{flex:0 0 165px;padding:12px 18px 11px 18px;background:-webkit-linear-gradient(#fb822c, #fc6336);background:-o-linear-gradient(#fb822c, #fc6336);background:linear-gradient(#fb822c, #fc6336);padding:12px 18px;text-align:center;box-shadow:inset 0 -3px 0px #d85028;font-size:15px;font-weight:600;border-radius:5px;border:none;color:#fff}
	.sct7 .s-form .btn-subcribe:hover{position:relative;top:1px;color:#fff;box-shadow:none;background:linear-gradient(to bottom, #fc6336, #fb822c)}
	.sct7 .form-subscribe #error-msg{position:absolute;bottom:-20px;font-size:13px;left:25px}
	.sct7 .form-control{display:block;width:100%;height:34px;padding:6px 12px;font-size:14px;line-height:1.42857143;color:#555;background-color:#fff;background-image:none;border:1px solid #ccc;border-radius:4px;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.075);box-shadow:inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition:border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;-o-transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s}
	.sct7 .ss-title{color:#0e0e0e;text-align:center;font-size:20px;font-weight:300;margin:35px 0 10px 0}
	.sct7 .ss-sub-title{text-align:center;color:#0e0e0e;font-weight:300}
	.sct7 .ss-sub-title a{color:inherit;text-decoration:underline}
</style>
<section class="sct7 py-5 mb-lg-5 mb-4">
	<div class="container py-3">
		<div class="row">
			<div class="col-12">
				
					<form action="/" method="post">
						<div class="row s-form">
							<div class="col-12 col-md-3 text-center text-md-left">
								<label>Подпишитесь на рассылку</label>
							</div>
							<div class="col-12 col-md-4 my-3 my-md-0">
								<input type="hidden" name="product_submit" value="skidki">
								<input type="text" name="newsletter" id="input-newsletter" class="form-control" placeholder="Введите Ваш email">
							</div>
							<div class="col-12 col-md-5 text-center text-md-left">
								<button id="subcribe" class="btn btn-subcribe">Подписаться</button>
							</div>
						</div>
					</form>
				
			</div>
			<div class="col-12">
				<div class="ss-title">Будьте в курсе выгодных событий и получите скидку 5%</div>
				<div class="ss-sub-title">Нажимая на кнопку вы даете согласие на использование своих персональных данных согласно <a href="/politika-konfidencialnosti">Политике конфиденциальности</a></div>
			</div>
		</div>
	</div>
</section>

<style type="text/css">
	.sct8 .index-text h1{font-size:28px;font-weight:600;margin-bottom:20px;}
	.sct8 .index-text h2{margin:10px 0}
	.sct8 .index-text ul>li{margin-left:20px;margin-bottom:8px;position:relative}
	.sct8 .index-text .slide-btn{cursor:pointer;left:0;bottom:0;border:none;padding:0;background:none;font-size:13px;color:#005c99}
	.sct8 .index-text .slide-btn span{border-bottom:1px dotted #005c99}
	.sct8 .index-text .slide-btn i, .sct8 .index-text .slide-btn svg{font-size:15px;margin:0 0 0 4px;vertical-align:middle}
</style>
<section class="sct8">
	<div class="container">
		<div class="row">
			<div class="col-12 mb-lg-5 mb-3">
<div class="index-text">
<h1>BEMBI: Детская одежда оптом – лучший выбор в Украине</h1>
<p>Детская одежда – один из самых популярнейших и востребованных товаров не только в Украине, но и во всём мире. Интернет-магазин Bembi (Бемби), который находится в Одессе, предлагает купить самую качественную и модную детскую одежду в широком ассортименте оптом по отличной цене в любом уголке Украины: Одесса, Днепр, Киев, Львов, Запорожье, Харьков и др.</p>
<div id="divId" style="display:none">
<p>Низкая цена на опт детской одежды в нашей компании обусловлена прямыми поставками из ведущих фабрик Украины и Турции, без переплаты посредникам. А высокое качество – тщательным отбором партнёров и заботой о клиентах и сохранении собственного имиджа.</p>
<h2>Ассортимент качественной детской одежды оптом</h2>
<p>Каждый сталкивался с проблемой выбора детской одежды. Ассортимент гипермаркетов не всегда доступный, а рыночные раскладки не вызывают доверия вовсе. «Bembi» – это недорогой интернет-магазин детской одежды, где вы найдете широкий ассортимент товаров.</p>
<p>Мы тоже родители, и прекрасно понимает, как важен аспект качества в детских товарах. Сорванцы могут сносить пару хороших брюк за месяц, а некачественные материалы терпят фиаско уже после первых вылазок на прогулку. В каталогах нашего интернет-магазина мы собрали популярную и качественную детскую одежду. Сотни молодых семей отметили наши товары положительной оценкой.</p>
<p>Вашему ребенку понравиться модная детская одежда нашего интернет-магазина. Мы на 100% реализуем все ваши потребительские запросы и предоставим большой выбор товаров от лучших производителей СНГ и зарубежья.</p>
<h2>Чем выгодно сотрудничество с компанией Bembi (Бемби)</h2>
<p>Дети не только быстро растут, но ещё и очень активны, и любая одежда на них, как говорится, просто «горит». Поэтому торговые точки, торгующие детской одеждой, всегда будут прибыльными.</p>
<p>Но для того, чтобы выгодно продать, нужно сперва выгодно купить. И лучший выход для предпринимателей Украины – детская одежда оптом в интернет-магазине компании Bembi (Бемби) с доставкой по всей стране.</p>
<p>Компания имеет множество постоянных клиентов, которые уже давно оценили выгоду сотрудничества с нами. Большинство ведущих популярных магазинов и небольших торговых точек Украины, успешно торгующих в розницу детской одеждой, предпочитают купить её оптом именно у нас.</p>
<p>Мы дорожим нашей репутацией и для нас важно, чтобы вы остались довольны нашим сотрудничеством. Поэтому всегда готовы рассмотреть ваши предложения и найти решение, которое устроит обе стороны.</p>
<p>Уважение и доверие наших покупателей вполне объяснимо, так как компания предоставляет всем, кто к нам обратился, такие преимущества:</p>
<ul>
	<li><strong>Качество.</strong> Главный критерий в детской одежде – качество. Она должна быть удобной, красивой, приятной к телу, достаточно прочной, практичной, и хорошо стираться. Именно такую одежду, имеющую все необходимые сертификаты, вы можете купить у нас оптом.</li>
	<li><strong>Производители.</strong> В нашем ассортименте товары от лучших производителей из Украины и Турции. Мы сотрудничаем только с проверенными и уважаемыми производителями.</li>
	<li><strong>Выбор.</strong> Представлен самый разнообразный ассортимент детской одежды для мальчиков и для девочек. У нас можно найти практически всё, что требуется ребёнку в период от 0 до 16 лет: <a href="/catalog/futbolki" title="Детские футболки">Футболки</a> и <a href="/catalog/shorty" title="Детские шорты">Шорты</a>, <a href="/catalog/rubashki" title="Детские рубашки">Рубашки</a>, <a href="/catalog/demisezonnye-kostjumy" title="Детские демисезонные костюмы">Демисезонные костюмы</a>, <a href="/catalog/shkolnaja-forma" title="Школьная форма">Школьная форма</a>, <a href="/catalog/kurtki" title="Детские куртки">Куртки</a>, <a href="/catalog/platja" title="Детские платья">Платья</a>, <a href="/catalog/tolstovki" title="Детские толстовки">Толстовки</a>, <a href="/catalog/svitera" title="Детские свитера">Свитера</a> и др.</li>
	<li><strong>Консультации.</strong> Обратившись в наш интернет-магазин, вы можете рассчитывать на квалифицированную помощь консультантов. Они помогут вам разобраться в многообразии предлагаемых товаров, а также расскажут о способе его покупки оптом и условиях доставки.</li>
	<li><strong>Цена.</strong> Работая с поставщиками напрямую, мы уверенно можем заявить, что наша цена на опт детской одежды – одна из самых низких в Украине.</li>
	<li><strong>Наличие товара.</strong> Вам не придётся ждать, пока поступит выбранная детская одежда, чтобы получить её оптом – на нашем складе в Одессе постоянно имеется всё, что указано в каталоге интернет-магазина.</li>
	<li><strong>Приятные бонусы.</strong> Чтобы порадовать своих покупателей и выразить им своё уважение и готовность продолжать сотрудничество, компания часто проводит интересные акции и вводит выгодные предложения на опт детской одежды.</li>
</ul>
<h2>Купить детскую одежду оптом Украина</h2>
<p>Bembi (Бемби) – Украинский интернет-магазин детской и подростковой одежды по оптовым ценам, где все заинтересованные в выгодной покупке оптом детской одежды могут заказать её.</p>
<ul>
	<li>Оплата производится через Приват Банк на карту, или наложенным платежом.</li>
	<li>Доставка максимально скоростная. Товар отправляется покупателю в течение суток через Интайм, Новую Почту или Деливери. Если вы решили заказать оптом детскую одежду на сумму от 5 000 грн., то цена доставки оплачивается компанией Bembi (Бемби).</li>
	<li>Время работы. Заказ в интернет-магазине можно оформить круглосуточно в удобное для вас время.</li>
	<li>Обмен и возврат. Мы уверены в качестве нашего товара, но если у вас возникнут претензии, то вы можете вернуть или обменять купленную оптом детскую одежду.</li>
</ul>
<p>Выбирайте для своего бизнеса лучших партнёров – таких, как компания Bembi (Бемби) из Одессы. Простые и доступные покупки детских и подростковых вещей в интернет-магазине «Bembi». Купить качественную детскую одежду оптом по хорошей цене у нас – разумный выбор, который даст вам максимум выгод и удобств.</p>
</div>
<span onclick="anichange('#divId'); return false" class="slide-btn"><span>Читать полностью</span><i class="fas fa-angle-down"></i></span>
</div>
			</div>
		</div>
	</div>
</section>