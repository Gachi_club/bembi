<?php
if(isset($phorders) && !empty($phorders)){
?>
<section id="phthanks" class="phthanks">
	<div class="container">
		<div class="row my-3">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
				<h1>Благодарим за Вашу заявку</h1>
				<p><?=$phorders['ph_name'];?>, наш менеджер свяжется с Вами в ближайшее время.</p>
			</div>
		</div>
	</div>
</section>

<section id="prod_popular" class="py-3">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 text-center">
				<h2>Также Вас может заинтересовать…</h2>
			</div>
		</div>
		<div class="row px-md-5 px-lg-5 px-xl-5">
	<?php
	if(empty($random)){
		echo '
			<div class="col-12 text-center">
				<p>Список товаров пуст</p>
			</div>
		';
	}else{
		foreach($random as $val){
			if(file_exists('public/products/'.$val['p_imglink']) && $val['p_imglink'] !=""){
				$pathimg = '/public/products/'.$val['p_imglink'];
			}else{$pathimg = '/public/img/no_photo.png';}
			echo '
			<style>
				.pimage'.$val['p_id'].'{background:url('.$pathimg.');}
			</style>
			<div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 pop_prod text-center">
				<a href="/product/'.$val['p_id'].'" class="prod_cart">
					<p class="cover-img img-fluid lazy pimage'.$val['p_id'].'"></p>
					<h3>'.$val['p_name'].'</h3>
			';
			if($val['p_catparentid'] != '2'){
				echo '<p class="prod_price"><span>'.$val['p_price'].'</span> грн.</p>';
			}else{
				echo '<p class="prod_price"></p>';
			}
			echo '
					<p class="prod_buylink"><span>Подробнее</span></p>
				</a>
			</div>
			';
		}
	}
	?>
		</div>
	</div>
</section>
<?php
}else{
?>
<section id="phthanks" class="phthanks">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h5>Вы сегодня ничего не заказывали. Через несколько секунд Вас перенаправит на <a href="/">Главную страницу</a>.</h5>
			</div>
		</div>
		<script type="text/javascript">
			setTimeout(function(){window.location.href="/";},3000);
		</script>
	</div>
</section>
<?php
}
?>