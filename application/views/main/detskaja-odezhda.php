<style type="text/css">
	.breadcrumb{background-color:transparent;font-size:1vw;}
@media(max-width:576px){
	.breadcrumb{font-size:4vw;}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
<section class="breadcrumbs mt-lg-3">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
<nav aria-label="breadcrumb" role="navigation">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="/" title="Bembi - интернет-магазин детской одежды оптом"><i class="fas fa-home"></i></a></li>
<?php
if($cat['cat_sex'] == 'devochka' && !isset($_GET['pol'])){$pol_text = '';}
elseif($cat['cat_sex'] == 'devochka' && isset($_GET['pol'])){
	if($cat['cat_sex'] == $_GET['pol']){$pol_text = ' для девочки';}
}elseif($cat['cat_sex'] != 'devochka' && isset($_GET['pol'])){
	switch($_GET['pol']){case'malchik':$pol_text=' для мальчика';break;case'devochka':$pol_text=' для девочки';break;}
}else{$pol_text='';}

if(!empty($cat['cat_title'])){
	echo '
	<li class="breadcrumb-item"><a href="/detskaja-odezhda">Детская и школьная одежда</a></li>
	<li class="breadcrumb-item active" aria-current="page">'.$cat['cat_title'].$pol_text.'</li>
	';
}else{
	echo '<li class="breadcrumb-item"><a href="/detskaja-odezhda" title="Детская и школьная одежда">...</a></li><li class="breadcrumb-item active" aria-current="page">Детская и школьная одежда'.$pol_text.'</li>';
}
?>
	</ol>
</nav>
			</div>
		</div>
	</div>
</section>

<style type="text/css">
	#sidebar{background:url(/public/img/pattern.png) rgba(244,241,245,.25);box-shadow:0 0 1px rgba(0,0,0,.2) inset;box-shadow:0px 4px 15px rgba(0, 92, 153, 0.20)}
	#sidebar h5{background:#005c99;color:#fff;font-size:1.2vw}
	#sidebar p{background-color:rgba(255,255,255,.8);border:1px solid transparent;color:#005c99;font-size:1vw}
	#sidebar p:hover{background-color:rgba(255,255,255,.8);border:1px solid rgba(150,150,150,.8);color:#005c99;font-size:1vw}
	#sidebar p.active{border:1px solid rgba(150,150,150,.8)}
	#sidebar a{color:#000;transition:.5s;display:block}
	#sidebar a:hover{text-decoration:none;color:#005c99}

	#product .product-layout{margin:10px 0}
	#product .product-thumb{position:relative;padding:10px 15px;transition:0.3s}
	.product-thumb:hover{-webkit-box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2);-moz-box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2);box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2);z-index:5}
	#product .product-thumb .labels{position:absolute;top:15px;left:15px}
	#product .product-thumb .is_new{background:#005c99}
	#product .product-thumb .is_rasprodazha{background:#f25540}
	#product .product-thumb .is_popular{background:#fb7131}
	#product .product-thumb .label-item{color:#fff;font-size:13px;display:flex;align-items:center;justify-content:center;padding:5px 10px;margin-bottom:4px}
	#product .product-thumb .image>a{display:block;background-position:center center;background-size:cover;width:100%;height:250px}
	#product .product-thumb h4{margin:20px 0 2px 0;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;font-weight:600;font-size:14px}
	#product .product-thumb h4>a{color:#005c99}
	#product .product-thumb .stock-free{display:flex;align-items:center;min-height:30px;margin-bottom:5px;justify-content:space-between}
	#product .product-thumb .stock-free .stock{font-size:13px}
	#product .product-thumb .stock.in-stock{color:#4eba06}
	#product .product-thumb .stock.out-stock{color:#ff0000}
	#product .product-thumb .rating-flex{padding:6px 0 0 0;align-items:center;font-size:12px}
	#product .product-thumb .price{font-size:24px;font-weight:600;color:#0e0e0e;margin:15px 0}
	#product .product-thumb .price .price-new{font-size:24px;color:#ef0303;font-weight:600}
	#product .product-thumb .price .price-old{font-size:18px;font-weight:400;color:#9c9c9c;text-decoration:line-through;margin-left:30px}
	#product .product-thumb:hover .hovers{opacity:1;visibility:visible}
	#product .product-thumb .btns{display:flex;align-items:center;justify-content:space-between;/*margin-bottom:20px*/}
	#product .product-thumb .add-to-wish{width:40px;background:none;border:none;padding:7px 0;opacity:0.5}
	#product .product-thumb .add-to-cart{background:-webkit-linear-gradient(#fb822c, #fc6336);background:-o-linear-gradient(#fb822c, #fc6336);background:linear-gradient(#fb822c, #fc6336);padding-left:18px;padding-right:18px;text-align:center;line-height:19px;padding-top:12px;padding-bottom:12px;box-shadow:inset 0 -3px 0px #d85028;font-size:15px;font-weight:600;border-radius:5px;border:none;color:#fff;width:150px;margin:0 auto}
	#product .product-thumb .add-to-cart:hover{position:relative;top:1px;color:#fff;box-shadow:none;background:linear-gradient(to bottom, #fc6336, #fb822c)}
	#product .product-thumb .options{border-top:1px solid #eeeeee;padding:17px 0}
	#product .product-thumb .free-ship{display:flex;align-items:center;background:#fcf1ef;height:30px;border-radius:5px;font-family:Arial;font-size:11px;color:#f25540;padding:0 10px}
	#product .product-thumb .free-ship img{height:19px;margin-right:5px;width:auto;}
@media(max-width:576px){
	.cont-title h1{font-size:8vw;}
	#product .product-thumb{-webkit-box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2);-moz-box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2);box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2);}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
<section class="mb-lg-5 mb-4">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 d-none d-md-block">
				<aside id="sidebar" class="rounded">
<?php
if(isset($allcats) && isset($schoolcats)){
	echo '
					<div class="py-2 px-3">
						<div class="py-2">
							<h5 class="text-center py-2">Каталог детской одежды</h5>
	';
	foreach($allcats as $val){
		if($val['cat_alias'] != $this->route['cat']){
		echo '
							<p class="my-1 py-1 px-3">
								<a href="/detskaja-odezhda/'.$val['cat_alias'].'">'.$val['cat_title'].'</a>
							</p>
		';
		}else{echo '<p class="active my-1 py-1 px-3">'.$val['cat_title'].'</p>';}
	}
	echo '
						</div>
						<div class="py-2">
							<h5 class="text-center py-2">Каталог одежды для школы</h5>
	';
	foreach($schoolcats as $val){
		if($val['cat_alias'] != $this->route['cat']){
		echo '
							<p class="my-1 py-1 px-3">
								<a href="/detskaja-odezhda/'.$val['cat_alias'].'">'.$val['cat_title'].'</a>
							</p>
		';
		}else{echo '<p class="active my-1 py-1 px-3">'.$val['cat_title'].'</p>';}
	}
	echo '
						</div>
					</div>
	';
}
?>
				</aside>
			</div>
			<div class="col-12 col-md-9">
				<div class="content">
					<div class="cont-title">
						<div class="row">
							<div class="col-12">
								<h1 class="text-center pb-lg-3">
									<?php if($page_ttl != ""){echo $page_ttl.$pol_text;}else{echo 'Детская и школьная одежда'.$pol_text;}?>
								</h1>
							</div>
						</div>
<style type="text/css">
	.catalog-item{height:150px;display:block;position:relative;background-position:center center;background-size:cover;text-decoration:none!important}
	.windcat-malchik span{display:block;position:absolute;right:15px;bottom:17px;color:#005c99;font-weight:600;background:#fff;padding:12px 21px;border-radius:25px;line-height:1}
	.windcat-devochka span{display:block;position:absolute;left:15px;bottom:17px;color:#005c99;font-weight:600;background:#fff;padding:12px 21px;border-radius:25px;line-height:1}
	.windcat-malchik{background-image:url('/public/img/cat7.jpg');}
	.windcat-devochka{background-image:url('/public/img/cat3.jpg');}
</style>
						<?php
						if($list){
							if(!isset($_GET['pol']) && $cat['cat_sex'] != 'devochka'){
							echo '
							<div class="row d-none d-md-flex">
								<div class="col-6 text-center">
									<a href="/detskaja-odezhda/'.$cat['cat_alias'].'?pol=malchik" class="catalog-item windcat-malchik"><span>'.$cat['cat_title'].' для мальчика</span></a>
								</div>
								<div class="col-6 text-center">
									<a href="/detskaja-odezhda/'.$cat['cat_alias'].'?pol=devochka" class="catalog-item windcat-devochka"><span>'.$cat['cat_title'].' для девочки</span></a>
								</div>
							</div>
							';
							}
						}
						?>
					</div>
					<div id="product" class="cont-products">
						<div class="row no-gutters">
					<?php
					if($list){
						foreach($list as $val){
							if(file_exists('public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink']) && $val['p_imglink'] !=""){
								$pathimg = '/public/bembiTMP/products/'.$val['p_vendor_code'].'/'.$val['p_imglink'];
							}else{$pathimg = '/public/img/no_photo.png';}
							$price = $val['p_opt_price'] * $val['p_upakovka'];
							$old_price = $price * 1.3;
							echo '
							<div class="col-12 col-lg-3">
								<div class="product-layout">
									<div class="product-thumb">
							';
							if($val['p_novinki'] == 'да'){echo '<div class="labels"><div class="label-item is_new">новинка</div></div>';}
							if($val['p_rasprodazha'] == 'да'){echo '<div class="labels"><div class="label-item is_rasprodazha">скидка</div></div>';}
							if($val['p_popular'] == 'да'){echo '<div class="labels"><div class="label-item is_popular">хит продаж</div></div>';}
										echo '
										<div class="image">
											<a href="/product/'.$val['p_alias'].'_'.$val['p_vendor_code'].'" style="background-image:url('.$pathimg.');" title="'.$val['p_name'].'"></a>
										</div>
										<h4><a href="/product/'.$val['p_alias'].'_'.$val['p_vendor_code'].'">'.$val['p_name'].'</a></h4>
										<div class="stock-free">
										';
							switch($val['p_nalichie']){
								case 'да':echo '<div class="stock in-stock"><i class="fas fa-check"></i> Есть в наличии</div>';break;
								case 'нет':echo '<div class="stock out-stock"><i class="fas fa-times"></i> Нет в наличии</div>';break;
							}

$array_colors = explode(";", $val['p_colors']);
$array_colors = array_filter($array_colors);
							echo '
										</div>
										<div class="rating-flex">
											<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Размеры:</strong> '.$val['p_razmery'].'.</p>
											<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Кол-во в упаковке:</strong> '.$val['p_upakovka'].' шт.</p>
											<p style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;"><strong>Цена:</strong> '.$val['p_opt_price'].' грн. за шт.</p>
										</div>
										<div class="price text-center">
							';
							if($val['p_rasprodazha'] == 'да'){
								echo '<span class="price-new">'.$price.' грн.</span> <span class="price-old">'.$old_price.' грн.</span>';
							}else{
								echo $price.' грн.';
							}
										echo '</div>';
									if($val['p_nalichie'] == 'да'){
										echo '
										<div class="btns">
											<button type="button" class="add-to-cart" data-pid="'.$val['p_id'].'" data-vcode="'.$val['p_vendor_code'].'" data-value="'.$val['p_upakovka'].'" data-price="'.$val['p_opt_price'].'" data-colors="'.$array_colors[0].';">Купить</button>
										</div>
										';
									}else{
										echo '
										<div class="btns">
											<button type="button" class="btn-in-modal" data-toggle="modal" data-target="#phoneModal" data-pid="'.$val['p_id'].'" data-vcode="'.$val['p_vendor_code'].'" data-value="'.$val['p_upakovka'].'" data-price="'.$val['p_opt_price'].'">Заказать звонок</button>
										</div>
										';
									}
							echo '
									</div>
								</div>
							</div>
							';
						}
					}else{
						echo '
							<div class="col-12">
								<p class="text-center my-2">Список товаров пуст</p>
							</div>
						';
					}
					?>
						</div>
					</div>
					<div class="cont-pagination mt-3 mt-lg-0">
						<div class="row">
							<div class="col-12">
							<?php if(!empty($list)){echo $pagination;}?>
							</div>
						</div>
					</div>
					<div class="cont-text">
						<div class="row">
							<div class="col-12 mt-3">
<style type="text/css">
	.catalog-text h1{font-size:28px;font-weight:600;margin-bottom:20px;}
	.catalog-text h2{margin:10px 0}
	.catalog-text ul>li{margin-left:20px;margin-bottom:8px;position:relative}
	.catalog-text .slide-btn{cursor:pointer;left:0;bottom:0;border:none;padding:0;background:none;font-size:13px;color:#005c99}
	.catalog-text .slide-btn span{border-bottom:1px dotted #005c99}
	.catalog-text .slide-btn i, .catalog-text .slide-btn svg{font-size:15px;margin:0 0 0 4px;vertical-align:middle}
</style>
<div class="catalog-text">
<p><?php if($page_ttl != ""){echo $page_ttl;}else{echo 'Детская и школьная одежда';}?> – один из самых популярнейших и востребованных товаров не только в Украине, но и во всём мире. Интернет-магазин Bembi (Бемби), который находится в Одессе, предлагает купить самую качественную и модную детскую одежду в широком ассортименте оптом по отличной цене в любом уголке Украины: Одесса, Днепр, Киев, Львов, Запорожье, Харьков и др.</p>
<div id="divId" style="display:none">
<p>Низкая цена на опт детской одежды в нашей компании обусловлена прямыми поставками из ведущих фабрик Украины и Турции, без переплаты посредникам. А высокое качество – тщательным отбором партнёров и заботой о клиентах и сохранении собственного имиджа.</p>
<h2>Ассортимент качественной детской одежды оптом</h2>
<p>Каждый сталкивался с проблемой выбора детской одежды. Ассортимент гипермаркетов не всегда доступный, а рыночные раскладки не вызывают доверия вовсе. «Bembi» – это недорогой интернет-магазин детской одежды, где вы найдете широкий ассортимент товаров.</p>
<p>Мы тоже родители, и прекрасно понимает, как важен аспект качества в детских товарах. Сорванцы могут сносить пару хороших брюк за месяц, а некачественные материалы терпят фиаско уже после первых вылазок на прогулку. В каталогах нашего интернет-магазина мы собрали популярную и качественную детскую одежду. Сотни молодых семей отметили наши товары положительной оценкой.</p>
<p>Вашему ребенку понравиться модная детская одежда нашего интернет-магазина. Мы на 100% реализуем все ваши потребительские запросы и предоставим большой выбор товаров от лучших производителей СНГ и зарубежья.</p>
<h2>Чем выгодно сотрудничество с компанией Bembi (Бемби)</h2>
<p>Дети не только быстро растут, но ещё и очень активны, и любая одежда на них, как говорится, просто «горит». Поэтому торговые точки, торгующие детской одеждой, всегда будут прибыльными.</p>
<p>Но для того, чтобы выгодно продать, нужно сперва выгодно купить. И лучший выход для предпринимателей Украины – детская одежда оптом в интернет-магазине компании Bembi (Бемби) с доставкой по всей стране.</p>
<p>Компания имеет множество постоянных клиентов, которые уже давно оценили выгоду сотрудничества с нами. Большинство ведущих популярных магазинов и небольших торговых точек Украины, успешно торгующих в розницу детской одеждой, предпочитают купить её оптом именно у нас.</p>
<p>Мы дорожим нашей репутацией и для нас важно, чтобы вы остались довольны нашим сотрудничеством. Поэтому всегда готовы рассмотреть ваши предложения и найти решение, которое устроит обе стороны.</p>
<p>Уважение и доверие наших покупателей вполне объяснимо, так как компания предоставляет всем, кто к нам обратился, такие преимущества:</p>
<ul>
	<li><strong>Качество.</strong> Главный критерий в детской одежде – качество. Она должна быть удобной, красивой, приятной к телу, достаточно прочной, практичной, и хорошо стираться. Именно такую одежду, имеющую все необходимые сертификаты, вы можете купить у нас оптом.</li>
	<li><strong>Производители.</strong> В нашем ассортименте товары от лучших производителей из Украины и Турции. Мы сотрудничаем только с проверенными и уважаемыми производителями.</li>
	<li><strong>Выбор.</strong> Представлен самый разнообразный ассортимент детской одежды для мальчиков и для девочек. У нас можно найти практически всё, что требуется ребёнку в период от 0 до 16 лет: <a href="/catalog/futbolki" title="Детские футболки">Футболки</a> и <a href="/catalog/shorty" title="Детские шорты">Шорты</a>, <a href="/catalog/rubashki" title="Детские рубашки">Рубашки</a>, <a href="/catalog/demisezonnye-kostjumy" title="Детские демисезонные костюмы">Демисезонные костюмы</a>, <a href="/catalog/shkolnaja-forma" title="Школьная форма">Школьная форма</a>, <a href="/catalog/kurtki" title="Детские куртки">Куртки</a>, <a href="/catalog/platja" title="Детские платья">Платья</a>, <a href="/catalog/tolstovki" title="Детские толстовки">Толстовки</a>, <a href="/catalog/svitera" title="Детские свитера">Свитера</a> и др.</li>
	<li><strong>Консультации.</strong> Обратившись в наш интернет-магазин, вы можете рассчитывать на квалифицированную помощь консультантов. Они помогут вам разобраться в многообразии предлагаемых товаров, а также расскажут о способе его покупки оптом и условиях доставки.</li>
	<li><strong>Цена.</strong> Работая с поставщиками напрямую, мы уверенно можем заявить, что наша цена на опт детской одежды – одна из самых низких в Украине.</li>
	<li><strong>Наличие товара.</strong> Вам не придётся ждать, пока поступит выбранная детская одежда, чтобы получить её оптом – на нашем складе в Одессе постоянно имеется всё, что указано в каталоге интернет-магазина.</li>
	<li><strong>Приятные бонусы.</strong> Чтобы порадовать своих покупателей и выразить им своё уважение и готовность продолжать сотрудничество, компания часто проводит интересные акции и вводит выгодные предложения на опт детской одежды.</li>
</ul>
<h2>Купить детскую одежду оптом Украина</h2>
<p>Bembi (Бемби) – Украинский интернет-магазин детской и подростковой одежды по оптовым ценам, где все заинтересованные в выгодной покупке оптом детской одежды могут заказать её.</p>
<ul>
	<li>Оплата производится через Приват Банк на карту, или наложенным платежом.</li>
	<li>Доставка максимально скоростная. Товар отправляется покупателю в течение суток через Интайм, Новую Почту или Деливери. Если вы решили заказать оптом детскую одежду на сумму от 5 000 грн., то цена доставки оплачивается компанией Bembi (Бемби).</li>
	<li>Время работы. Заказ в интернет-магазине можно оформить круглосуточно в удобное для вас время.</li>
	<li>Обмен и возврат. Мы уверены в качестве нашего товара, но если у вас возникнут претензии, то вы можете вернуть или обменять купленную оптом детскую одежду.</li>
</ul>
<p>Выбирайте для своего бизнеса лучших партнёров – таких, как компания Bembi (Бемби) из Одессы. Простые и доступные покупки детских и подростковых вещей в интернет-магазине «Bembi». Купить качественную детскую одежду оптом по хорошей цене у нас – разумный выбор, который даст вам максимум выгод и удобств.</p>
</div>
<span onclick="anichange('#divId'); return false" class="slide-btn"><span>Читать полностью</span><i class="fas fa-angle-down"></i></span>
</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>