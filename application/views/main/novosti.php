<style type="text/css">
	#news h1{font-size:2rem;}
	#news .article{-webkit-box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2);-moz-box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2);box-shadow:0px 0px 15px 0px rgba(0, 92, 153, 0.2);padding:10px 15px;margin:15px 0;}
	#news .art-image{background-position:center!important;background-repeat:no-repeat!important;background-size:cover!important;width:100%;height:230px;margin:.5rem 0;}
	#news .art-title h3{font-size:1.5vw!important;line-height:1.5!important;}
	#news .btn-news{background:-webkit-linear-gradient(#fb822c, #fc6336);background:-o-linear-gradient(#fb822c, #fc6336);background:linear-gradient(#fb822c, #fc6336);padding-left:18px;padding-right:18px;text-align:center;line-height:19px;padding-top:12px;padding-bottom:12px;box-shadow:inset 0 -3px 0px #d85028;font-size:15px;font-weight:600;border-radius:5px;border:none;color:#fff;width:150px;margin:0 auto}
	#news .btn-news:hover{position:relative;top:1px;color:#fff;box-shadow:none;background:linear-gradient(to bottom, #fc6336, #fb822c)}
@media(max-width:576px){
	#news h1{font-size:8vw;}
	#news .art-title h3{font-size:5vw!important;}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
<section id="news" class="mt-3 mb-lg-5 mb-4">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center pb-lg-3">
					<?php if($page_ttl != ""){echo $page_ttl;}else{echo 'Все новости';}?>
				</h1>
			</div>
		</div>
		<div class="row">
<?php
if($list){
	foreach($list as $news){
		if(file_exists('public/bembiTMP/news/'.$news['news_imglink']) && $news['news_imglink'] != ""){
			$pathimg = '/public/bembiTMP/news/'.$news['news_imglink'];
		}else{$pathimg = '/public/img/no_photo.png';}

		if(isset($news['news_text']) && !empty($news['news_text']) && $news['news_text'] != ''){
			$news_text = substr($news['news_text'], 0, 315);
			$news_text = rtrim($news_text, '?!,.-');
			$news_text = substr($news_text, 0, strrpos($news_text, ' '));
		}else{$news_text = '';}
		echo '
			<div class="col-12 col-md-6">
				<div class="article">
					<div class="row">
						<div class="col-12 col-md-6">
							<div class="art-image" style="background:url('.$pathimg.');">
								<a href="/statja/'.$news['news_alias'].'" class="d-block w-100 h-100"></a>
							</div>
						</div>
						<div class="col-12 col-md-6">
							<div class="art-title">
								<h3><a href="/statja/'.$news['news_alias'].'">'.$news['news_title'].'</a></h3>
							</div>
							<div class="art-text">'.$news_text.'...</div>
							<div class="art-more d-none">
								<p class="text-right">
									<a href="/statja/'.$news['news_alias'].'" class="btn btn-news">Подробнее</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		';
	}
}else{
	echo '
			<div class="col-12">
				<h2>Спиосок постов пуст</h2>
			</div>
	';
}
?>
		</div>
		<div class="row mt-3">
			<div class="col-12">
				<?php if(!empty($list)){echo $pagination;}?>
			</div>
		</div>
		<div class="row">
			<div class="col-12 mt-3">
<style type="text/css">
	.novosti-text h1{font-size:28px;font-weight:600;margin-bottom:20px;}
	.novosti-text h2{margin:10px 0}
	.novosti-text ul>li{margin-left:20px;margin-bottom:8px;position:relative}
	.novosti-text .slide-btn{cursor:pointer;left:0;bottom:0;border:none;padding:0;background:none;font-size:13px;color:#005c99}
	.novosti-text .slide-btn span{border-bottom:1px dotted #005c99}
	.novosti-text .slide-btn i, .novosti-text .slide-btn svg{font-size:15px;margin:0 0 0 4px;vertical-align:middle}
</style>
<div class="novosti-text">
<p>Детская одежда – один из самых популярнейших и востребованных товаров не только в Украине, но и во всём мире. Интернет-магазин Bembi (Бемби), который находится в Одессе, предлагает купить самую качественную и модную детскую одежду в широком ассортименте оптом по отличной цене в любом уголке Украины: Одесса, Днепр, Киев, Львов, Запорожье, Харьков и др.</p>
<div id="divId" style="display:none">
<p>Низкая цена на опт детской одежды в нашей компании обусловлена прямыми поставками из ведущих фабрик Украины и Турции, без переплаты посредникам. А высокое качество – тщательным отбором партнёров и заботой о клиентах и сохранении собственного имиджа.</p>
<h2>Ассортимент качественной детской одежды оптом</h2>
<p>Каждый сталкивался с проблемой выбора детской одежды. Ассортимент гипермаркетов не всегда доступный, а рыночные раскладки не вызывают доверия вовсе. «Bembi» – это недорогой интернет-магазин детской одежды, где вы найдете широкий ассортимент товаров.</p>
<p>Мы тоже родители, и прекрасно понимает, как важен аспект качества в детских товарах. Сорванцы могут сносить пару хороших брюк за месяц, а некачественные материалы терпят фиаско уже после первых вылазок на прогулку. В каталогах нашего интернет-магазина мы собрали популярную и качественную детскую одежду. Сотни молодых семей отметили наши товары положительной оценкой.</p>
<p>Вашему ребенку понравиться модная детская одежда нашего интернет-магазина. Мы на 100% реализуем все ваши потребительские запросы и предоставим большой выбор товаров от лучших производителей СНГ и зарубежья.</p>
<h2>Чем выгодно сотрудничество с компанией Bembi (Бемби)</h2>
<p>Дети не только быстро растут, но ещё и очень активны, и любая одежда на них, как говорится, просто «горит». Поэтому торговые точки, торгующие детской одеждой, всегда будут прибыльными.</p>
<p>Но для того, чтобы выгодно продать, нужно сперва выгодно купить. И лучший выход для предпринимателей Украины – детская одежда оптом в интернет-магазине компании Bembi (Бемби) с доставкой по всей стране.</p>
<p>Компания имеет множество постоянных клиентов, которые уже давно оценили выгоду сотрудничества с нами. Большинство ведущих популярных магазинов и небольших торговых точек Украины, успешно торгующих в розницу детской одеждой, предпочитают купить её оптом именно у нас.</p>
<p>Мы дорожим нашей репутацией и для нас важно, чтобы вы остались довольны нашим сотрудничеством. Поэтому всегда готовы рассмотреть ваши предложения и найти решение, которое устроит обе стороны.</p>
<p>Уважение и доверие наших покупателей вполне объяснимо, так как компания предоставляет всем, кто к нам обратился, такие преимущества:</p>
<ul>
	<li><strong>Качество.</strong> Главный критерий в детской одежде – качество. Она должна быть удобной, красивой, приятной к телу, достаточно прочной, практичной, и хорошо стираться. Именно такую одежду, имеющую все необходимые сертификаты, вы можете купить у нас оптом.</li>
	<li><strong>Производители.</strong> В нашем ассортименте товары от лучших производителей из Украины и Турции. Мы сотрудничаем только с проверенными и уважаемыми производителями.</li>
	<li><strong>Выбор.</strong> Представлен самый разнообразный ассортимент детской одежды для мальчиков и для девочек. У нас можно найти практически всё, что требуется ребёнку в период от 0 до 16 лет: <a href="/catalog/futbolki" title="Детские футболки">Футболки</a> и <a href="/catalog/shorty" title="Детские шорты">Шорты</a>, <a href="/catalog/rubashki" title="Детские рубашки">Рубашки</a>, <a href="/catalog/demisezonnye-kostjumy" title="Детские демисезонные костюмы">Демисезонные костюмы</a>, <a href="/catalog/shkolnaja-forma" title="Школьная форма">Школьная форма</a>, <a href="/catalog/kurtki" title="Детские куртки">Куртки</a>, <a href="/catalog/platja" title="Детские платья">Платья</a>, <a href="/catalog/tolstovki" title="Детские толстовки">Толстовки</a>, <a href="/catalog/svitera" title="Детские свитера">Свитера</a> и др.</li>
	<li><strong>Консультации.</strong> Обратившись в наш интернет-магазин, вы можете рассчитывать на квалифицированную помощь консультантов. Они помогут вам разобраться в многообразии предлагаемых товаров, а также расскажут о способе его покупки оптом и условиях доставки.</li>
	<li><strong>Цена.</strong> Работая с поставщиками напрямую, мы уверенно можем заявить, что наша цена на опт детской одежды – одна из самых низких в Украине.</li>
	<li><strong>Наличие товара.</strong> Вам не придётся ждать, пока поступит выбранная детская одежда, чтобы получить её оптом – на нашем складе в Одессе постоянно имеется всё, что указано в каталоге интернет-магазина.</li>
	<li><strong>Приятные бонусы.</strong> Чтобы порадовать своих покупателей и выразить им своё уважение и готовность продолжать сотрудничество, компания часто проводит интересные акции и вводит выгодные предложения на опт детской одежды.</li>
</ul>
<h2>Купить детскую одежду оптом Украина</h2>
<p>Bembi (Бемби) – Украинский интернет-магазин детской и подростковой одежды по оптовым ценам, где все заинтересованные в выгодной покупке оптом детской одежды могут заказать её.</p>
<ul>
	<li>Оплата производится через Приват Банк на карту, или наложенным платежом.</li>
	<li>Доставка максимально скоростная. Товар отправляется покупателю в течение суток через Интайм, Новую Почту или Деливери. Если вы решили заказать оптом детскую одежду на сумму от 5 000 грн., то цена доставки оплачивается компанией Bembi (Бемби).</li>
	<li>Время работы. Заказ в интернет-магазине можно оформить круглосуточно в удобное для вас время.</li>
	<li>Обмен и возврат. Мы уверены в качестве нашего товара, но если у вас возникнут претензии, то вы можете вернуть или обменять купленную оптом детскую одежду.</li>
</ul>
<p>Выбирайте для своего бизнеса лучших партнёров – таких, как компания Bembi (Бемби) из Одессы. Простые и доступные покупки детских и подростковых вещей в интернет-магазине «Bembi». Купить качественную детскую одежду оптом по хорошей цене у нас – разумный выбор, который даст вам максимум выгод и удобств.</p>
</div>
<span onclick="anichange('#divId'); return false" class="slide-btn"><span>Читать полностью</span><i class="fas fa-angle-down"></i></span>
</div>
			</div>
		</div>
	</div>
</section>