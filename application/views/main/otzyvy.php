<style type="text/css">
	.otzyv{margin:15px 0;}
	.otzyv-hdr{color:#005c99;}
	.otzyv-name{font-weight:600; margin-right:5px;}
	.otzyv-addr{}
	.otzyv-text{}
</style>
<section class="mt-3 mb-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					<?php if($page_ttl != ""){echo $page_ttl;}else{echo 'Some Title';}?>
				</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
<?php
if($otzyvy){
	echo '
	<div class="row">
	';
	foreach($otzyvy as $otzyv){
		echo '
		<div class="col-12 col-md-6">
			<div class="otzyv">
				<div class="otzyv-hdr">
					<span class="otzyv-name"><i class="fas fa-user"></i> '.$otzyv['r_user_name'].'</span>
					<span class="otzyv-addr"><i class="fas fa-building"></i> '.$otzyv['r_city'].'</span>
				</div>
				<div class="otzyv-btm">
					<p class="otzyv-text">'.$otzyv['r_text'].'</p>
				</div>
			</div>
		</div>
		';
	}
	echo '
	</div>
	';
}else{
	echo '<div class="row"><div class="col-12"><p class="text-center my-2">Нет отзывов</p></div></div>';
}
?>
			</div>
		</div>
	</div>
</section>