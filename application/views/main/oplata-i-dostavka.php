<style type="text/css">
	.static-page ul{margin:10px 0;}
	.static-page ol{margin:10px 0;}
	.static-page ul>li{margin-left:20px;}
	.static-page ol>li{margin-left:20px;}
	.static-page p{margin:10px 0;}
@media(max-width:576px){
	.static-page h1{font-size:8vw;}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
<section class="static-page mt-3 mb-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center"><?php if($page_ttl != ""){echo $page_ttl;}else{echo 'Оплата и доставка';}?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
<h3>Оплата</h3>
<ul>
<li>На карту Приват Банка</li>
<li>Наложеным платежом</li>
</ul>
<p><strong>Минимальная стоимость заказа:</strong> 500 грн.</p>
<p>Отправка осуществляется после полной оплаты заказа.</p>
<h3>Доставка</h3>
<p>Доставка осуществляется по всей территории Украины следующими транспортными компаниями:</p>
<ul>
<li>Новая Почта</li>
<li>Автолюкс</li>
<li>Интайм</li>
<li>Деливери</li>
<li>Самовывоз</li>
</ul>
<p><strong>Стоимость доставки</strong> зависит от транспортной компании, веса и объема заказа. Поэтому рассчитать точную стоимость не представляется возможным.</p>
<p><i><strong>Внимание!</strong> При заказе на сумму более 5000 гривен, доставка бесплатная.</i></p>
<p>Доставка оплачивается покупателем при получении заказа. Адреса представительств транспортных компаний и присутствие их в Вашем городе можно посмотреть перейдя по ссылкам на их сайты. (нажмите на логотип интересующей Вас компаний).</p>
<p>Если Вы затрудняетесь выбрать приемлемый вариант доставки, свяжитесь с нами по телефону или с помощью e-mail и мы порекомендуем <strong>оптимальный для Вас вариант</strong>.</p>
<p>Отправка заказов осуществляется в <span>понедельник</span>, <span>среду</span> и <span>субботу</span>.</p>
			</div>
		</div>
	</div>
</section>