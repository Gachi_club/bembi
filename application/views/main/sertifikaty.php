<style type="text/css">
	.static-page .static-text h1{font-size:28px;font-weight:600;margin-bottom:20px;}
	.static-page .static-text h2{margin:10px 0}
	.static-page .static-text ul>li{margin-left:20px;margin-bottom:8px;position:relative}
	.static-page .static-text .slide-btn{cursor:pointer;left:0;bottom:0;border:none;padding:0;background:none;font-size:13px;color:#005c99}
	.static-page .static-text .slide-btn span{border-bottom:1px dotted #005c99}
	.static-page .static-text .slide-btn i, .static-page .static-text .slide-btn svg{font-size:15px;margin:0 0 0 4px;vertical-align:middle}
@media(max-width:576px){
	.static-page .static-text h1{font-size:8vw;}
}
@media(min-width:576px){}
@media(min-width:768px){}
@media(min-width:992px){}
@media(min-width:1200px){}
</style>
<section class="mt-3 mb-5 static-page">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="mb-3 text-center"><?php if($page_ttl != ""){echo $page_ttl;}else{echo 'Сертификаты';}?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-4">
				<picture>
					<source srcset="/public/bembiTMP/sertificate/webp/1.webp" type="image/webp">
					<source srcset="/public/bembiTMP/sertificate/1.jpg" type="image/jpeg">
					<img data-src="/public/bembiTMP/sertificate/1.jpg" alt="" class="img-fluid">
				</picture>
			</div>
			<div class="col-12 col-lg-4">
				<picture>
					<source srcset="/public/bembiTMP/sertificate/webp/2.webp" type="image/webp">
					<source srcset="/public/bembiTMP/sertificate/2.jpg" type="image/jpeg">
					<img data-src="/public/bembiTMP/sertificate/2.jpg" alt="" class="img-fluid">
				</picture>
			</div>
			<div class="col-12 col-lg-4">
				<picture>
					<source srcset="/public/bembiTMP/sertificate/webp/3.webp" type="image/webp">
					<source srcset="/public/bembiTMP/sertificate/3.jpg" type="image/jpeg">
					<img data-src="/public/bembiTMP/sertificate/3.jpg" alt="" class="img-fluid">
				</picture>
			</div>
		</div>
		<div class="row">
			<div class="col-12 mt-5">
<div class="static-text">
<p>Компания «Bembi» надёжный оптовый поставщик одежды для новорождённых, детей и подростков. Наша компания успешно работает на украинском рынке уже более 10 лет. Мы работаем по всем регионам Украины. Мы работаем с лучшими производителями Турции и осуществляем прямые поставки с их фабрик.</p>
<h2>Особенности продукции Bembi</h2>
<p>Наша продукция - это стильная, комфортная и практичная одежда по доступной цене. В каталоге регулярно обновляется продукция популярных брендов: Bermini, Deco, Safari, Leyz, CitCit, Akkor, Cegisa, Blueland, Toontoy, Umka, Primax, Pink, Gerenimo, Bold, Basak Bebe, Baby Show, Plovdiv, Akkar, ETM, Hakkan Bebe, Lumina, Jojomama. Мы стараемся, чтобы наши клиенты всегда получали самые актуальные и новые модели и могли радовать своих покупателей новинками и разнообразным ассортиментом.</p>
<div id="divId" style="display:none">
<p>О новом поступлении информируем SMS рассылкой или с помощью рассылки на электронную почту. Вся продукция сертифицирована, сертификаты при необходимости прикладываются к заказу по просьбе клиента. Так же при необходимости выдаём полный пакет документов.</p>
<h3>Причины купить детскую и подростковую одежду в интернет-магазине Bembi</h3>
<ul style="list-style-type:disc;">
<li>Большой выбор эксклюзивных вещей, для детского возраста от 0 до 16 лет для любого сезона и случая (повседневная, праздничная, школьная, демисезонная одежда).</li>
<li>Представлены вещи от ведущих мировых брендов детской одежды.</li>
<li>Качественные материалы, натуральные ткани, не вызывающие аллергии, безопасные аксессуары и элементы, используемые в одежде.</li>
<li>Удобная форма просмотра и заказа изделий.</li>
<li>Идеальное соотношение качества и цены.</li>
<li>Гарантия качественного обслуживания и своевременной доставки товара.</li>
<li>Экономия времени и энергии.</li>
</ul>
<h2>Где приобрести детскую и подростковую одежду в Украине?</h2>
<p>Купить детскую и подростковую одежду высокого качества и сшитое с любовью к детям вы можете на нашем сайте всего в несколько простых операций. Обратите внимание на условия доставки и оплаты товара. На сайте вы можете найти номера телефонов, по которым в случае надобности сможете получить исчерпывающую консультацию о товаре, доставке и оплате. Подарите своему чаду радость качества и удобства с «Bembi».</p>
</div>
<span onclick="anichange('#divId'); return false" class="slide-btn"><span>Читать полностью</span><i class="fas fa-angle-down"></i></span>
</div>
			</div>
		</div>
	</div>
</section>