<?php

namespace application\core;

class View{

	public $route;
	public $path;
	public $layout = 'default';

	public function __construct($route){
		$this->route = $route;
		if(strrpos($route['action'], "_") !== true){
			$route['action'] = str_replace("_","-",$route['action']);
		}
		$this->path = $route['controller'].'/'.$route['action'];
	}
	
	public function render($title, $vars = []){
		extract($vars);
		$path = 'application/views/'.$this->path.'.php';
		if(file_exists($path)){
			ob_start(); require $path; $content = ob_get_clean();
			require 'application/views/layouts/'.$this->layout.'.php';
		}
	}
	/*! Редирект */
	public function redirect($url){
		header('Location: '.$url);
		exit;
	}
	/*! 404 код [не используется] */
	public static function errorCode($code){
		http_response_code($code);
		$path = 'application/views/errors/'.$code.'.php';
		if(file_exists($path)){require $path;}
		exit;
	}
	/*! 404 Ошибка */
	public static function errorBembiCode(){
		http_response_code(404);
		header('Location: /error404');
		exit;
	}
	/*! Модальное сообщение */
	public function message($title, $message, $status){
		$result = json_encode(['title'=>$title, 'message'=>$message, 'status'=>$status], JSON_UNESCAPED_UNICODE);
		exit($result);
	}
	/*! Модальное сообщение с таймером и перенаправлением */
	public function relocation($title, $message, $status, $location){
		$result = json_encode(['title'=>$title, 'message'=>$message, 'status'=>$status, 'location'=>$location], JSON_UNESCAPED_UNICODE);
		exit($result);
	}
	/*! Перенаправление */
	public function location($url){
		$result = json_encode(['url'=>$url]);
		exit($result);
	}
}

?>