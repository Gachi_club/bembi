<?php

namespace application\core;

use application\core\View;

class Router{
	protected $routes = [];
	protected $params = [];

	public function __construct(){
		$arr = require 'application/config/routes.php';
		foreach($arr as $key => $val){
			$this->add($key, $val);
		}
	}
	/*! Функция добавления маршрута */
	public function add($route, $params){
		$route = preg_replace('/{([a-z0-9-]+):([^\}?]+)}/', '(?P<\1>\2)', $route);
		$route = '#^'.$route.'$#';
		$this->routes[$route] = $params;
	}
	/*! Функция проверки маршрута */
	public function match(){
		$geturl = strpos($_SERVER['REQUEST_URI'], '?');
		if($geturl == true){
			$getquery = explode('?', $_SERVER['REQUEST_URI']);
			$url = trim($getquery[0], "/");
		}else{
			$url = trim($_SERVER['REQUEST_URI'], "/");
		}
		$url = preg_replace("/\?.+/", "", $url);
		foreach($this->routes as $route => $params){
			if(preg_match($route, $url, $matches)){
				foreach($matches as $key => $match){
					if(is_string($key)){
						if(is_numeric($match)){$match = (int) $match;}
						$params[$key] = $match;
					}
				}
				$this->params = $params;
				return true;
			}
		}
		return false;
	}
	/*! Функция запуска маршрута */
	public function run(){
		if($this->match()){
			$path = 'application\controllers\\'.ucfirst($this->params['controller']).'Controller';
			if(class_exists($path)){
				if(strrpos($this->params['action'], "-") !== true){
					$this->params['action'] = str_replace("-","_",$this->params['action']);
				}
				$action = $this->params['action'].'Action';
				if(method_exists($path, $action)){
					$controller = new $path($this->params);
					$controller->$action();
				}else{/*View::errorCode(404);*/ View::errorBembiCode();}
			}else{/*View::errorCode(404);*/ View::errorBembiCode();}
		}else{/*View::errorCode(404);*/ View::errorBembiCode();}
	}
}
?>