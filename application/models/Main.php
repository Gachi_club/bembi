<?php
namespace application\models;

use application\core\Model;
use application\lib\PHPMailer;
use application\lib\Exception;
use application\lib\Sitemap;

class Main extends Model{
	/*! Объявление переменной ОШИБКА */
	public $error;

	/*! Функция для получения Категорий МЕНЮ */
	public function getIndexAllCats(){
		$result = $this->db->row("SELECT * FROM `prod_cat` WHERE `cat_alias` NOT LIKE '%shkolnye%' AND `cat_index`='нет' AND `cat_visible`='да'");
		return $result;
	}
	/*! Функция для получения Категорий МЕНЮ */
	public function getIndexSchoolCats(){
		$result = $this->db->row("SELECT * FROM `prod_cat` WHERE `cat_alias` LIKE '%shkolnye%' AND `cat_index`='нет' AND `cat_visible`='да'");
		return $result;
	}
	/*! Функция для получения Слайдера ГЛАВНАЯ */
	public function getIndexSlider(){
		$result = $this->db->row("SELECT * FROM `slider` WHERE `s_visible`='да' LIMIT 6");
		return $result;
	}
	/*! Функция для получения Товара ГЛАВНАЯ */
	public function getIndexProduct($table){
		switch($table){
			case 'novinki':$res = "AND `p_novinki`='да'";break;
			case 'rasprodazha':$res = "AND `p_rasprodazha`='да'";break;
			case 'popular':$res = "AND `p_popular`='да'";break;
			default:$res = "";break;
		}
		$result = $this->db->row("SELECT * FROM `products` WHERE `p_nalichie`='да' AND `p_colors`!='' {$res} ORDER BY `p_id` DESC LIMIT 8");
		return $result;
	}
	/*! Функция для получения Новости ГЛАВНАЯ */
	public function getIndexNews(){
		$result = $this->db->row("SELECT * FROM `news` WHERE `news_visible`='да' LIMIT 4");
		return $result;
	}
	/*! Функция для валидации для стр. Контакты [не актив] */
	public function contactValidate($post){
		$nameLen = iconv_strlen($post['name']);
		$messageLen = iconv_strlen($post['message']);
		if($nameLen < 1 or $nameLen > 20){
			$this->error = 'Имя должно содержать от 1 до 20 символов';
			return false;
		}elseif(!filter_var($post['email'], FILTER_VALIDATE_EMAIL)){
			$this->error = 'E-mail введен неверно';
			return false;
		}elseif($messageLen < 10 or $messageLen > 500){
			$this->error = 'Сообщение должно содержать от 10 до 500 символов';
			return false;
		}
		return true;
	}

// *********************************************************************
	/*! Функция SEO для страниц сайта */
	public function getSEOpage($action){
		$result = $this->db->row("SELECT * FROM `pages` WHERE `page_alias`='{$action}' LIMIT 1");
		return $result[0];
	}
	/*! Функция SEO для категорий */
	public function getSEOcategory($route){
		if(!empty($route['cat'])){
			$q = "SELECT * FROM `prod_cat` WHERE `cat_alias`='{$route['cat']}' LIMIT 1";
		}else{$q = "SELECT * FROM `pages` WHERE `page_alias`='{$route['action']}' LIMIT 1";}
		$result = $this->db->row($q);
		return $result[0];
	}
	/*! Функция получения товаров [не актив] */
	public function getProducts($route){
		$result = $this->db->row("SELECT * FROM `products` WHERE `p_catids` LIKE '{$route['cat']};%'");
		return $result;
	}
	/*! Функция SEO для отдельного ТОВАРА */
	public function getSEOproduct($alias){
		$array = explode("_", $alias);
		$array = array_diff($array, array(''));
		//debug($array);
		$result = $this->db->row("SELECT * FROM `products` WHERE `p_alias`='{$array[0]}' AND `p_vendor_code`='bembi_{$array[2]}' LIMIT 1");
		return $result[0];
	}
	/*! Функция получения Категории для отдельного ТОВАРА */
	public function getProdCat($catids){
		$array = explode(";", $catids);
		$array = array_diff($array, array(''));
		$category = trim($array[0],"#");
		
		$result = $this->db->row("SELECT * FROM `prod_cat` WHERE `cat_alias`='{$category}' LIMIT 1");
		return $result[0];
	}
	/*! Функция получения крошек для КАТАЛОГА */
	public function getCatalogCat($route){
		$result = $this->db->row("SELECT * FROM `prod_cat` WHERE `cat_alias`='{$route['cat']}'");
		return $result[0];
	}
	/*! Получаем все категории */
	public function getCategories(){
		$result = $this->db->row("SELECT * FROM `prod_cat`");
		return $result;
	}

// *********************************************************************
	/*! Обновляем цвет товара */
	public function getNewCartColor($post, $user_ip){
		$this->db->query("UPDATE `cart` SET `cart_prod_color`='{$post['cart_color']}' WHERE `cart_user_ip`='{$user_ip}' AND `cart_id`='{$post['cart_id']}'");
		return true;
	}
	/*! Добавляем товар в Корзину */
	public function getProductAdd($post, $user_ip){
		date_default_timezone_set('Europe/Kiev');
		$today = date("Y-m-d H:i:s");

		$result = $this->db->row("SELECT * FROM `cart` WHERE `cart_user_ip`='{$user_ip}' AND `cart_prod_id`='{$post['product_id']}' AND `cart_prod_color`='{$post['product_color']}'");
		if(count($result[0]['cart_id']) > 0){
			$pcount = $result[0]['cart_prod_count'] + $post['product_count'];
			$this->db->query("UPDATE `cart` SET `cart_prod_count`='{$pcount}' WHERE `cart_user_ip`='{$user_ip}' AND `cart_prod_id`='{$post['product_id']}' AND `cart_prod_color`='{$post['product_color']}'");
		}else{
			$this->db->query("INSERT INTO `cart` (`cart_prod_id`, `cart_prod_article`, `cart_prod_price`, `cart_prod_count`, `cart_prod_color`, `cart_datetime`, `cart_user_ip`) VALUES ('{$post['product_id']}', '{$post['product_vendor_code']}', '{$post['product_price']}', '{$post['product_count']}', '{$post['product_color']}', '{$this->modelTime()['datetime']}', '{$user_ip}')");
		}
		return true;
	}
	/*! Увеличение кол-ва товара в Корзине */
	public function getProductUp($post, $user_ip){
		$this->db->query("UPDATE `cart` SET `cart_prod_count`='{$post['product_count']}' WHERE `cart_user_ip`='{$user_ip}' AND `cart_prod_id`='{$post['product_id']}'");
		return true;
	}
	/*! Уменьшение кол-ва товара в Корзине */
	public function getProductDown($post, $user_ip){
		$this->db->query("UPDATE `cart` SET `cart_prod_count`='{$post['product_count']}' WHERE `cart_user_ip`='{$user_ip}' AND `cart_prod_id`='{$post['product_id']}'");
		return true;
	}
	/*! Удаление товара из Корзины */
	public function getProductDelete($post, $user_ip){
		$this->db->row("DELETE FROM `cart` WHERE `cart_prod_id`='{$post['product_id']}' AND `cart_user_ip`='{$user_ip}' AND `cart_id`='{$post['cart_id']}'");
		return true;
	}
	/*! Получаем кол-во продуктов в Корзине */
	public function getBasketCount($user_ip){
		$result = $this->db->row("SELECT COUNT(*) FROM cart,products WHERE cart.cart_user_ip='{$user_ip}' AND products.p_id=cart.cart_prod_id");
		//debug($result);
		return $result[0];
	}
	/*! Получаем все продукты в Корзине */
	public function getBasketProducts($user_ip){
		$result = $this->db->row("SELECT * FROM cart,products WHERE cart.cart_user_ip='{$user_ip}' AND products.p_id=cart.cart_prod_id");
		return $result;
	}

// *********************************************************************
	/*! Функция SEO для отдельной НОВОСТИ [не актив] */
	public function getSEOarticle($alias){
		$result = $this->db->row("SELECT * FROM `news` WHERE `news_alias`='{$alias}' AND `news_visible`='да'");
		return $result[0];
	}
	/*! Функция подсчета кол-ва новостей в таблице [не актив] */
	public function getNewsCount(){
		$result = $this->db->column("SELECT COUNT(news_id) FROM `news` WHERE `news_visible`='да'");
		return $result;
	}
	/*! Функция получения текущего списка новостей исходя из номера страницы [не актив] */
	public function getNewsList($get){
		$max = 6;
		if(!empty($get['page'])){$cpage = $get['page'];}else{$cpage = 1;}
		$start = (($cpage - 1) * $max);
		return $this->db->row("SELECT * FROM `news` WHERE `news_visible`='да' ORDER BY `news_id` DESC LIMIT {$start}, {$max}");
	}
// *********************************************************************
	public function getReviews(){
		$result = $this->db->row("SELECT * FROM `reviews` WHERE `r_visible`='да' LIMIT 16");
		return $result;
	}
// *********************************************************************
	/*! Функция подсчета кол-ва товаров в таблице */
	public function getProductCount($cat_sex, $get, $route){
		if($cat_sex == 'devochka' && !isset($get['pol'])){$sex = "AND (`p_gender`='Девочка' OR `p_gender`='Унисекс')";}
		elseif($cat_sex == 'devochka' && isset($get['pol'])){
			if($cat_sex == $_GET['pol']){$sex = "AND (`p_gender`='Девочка' OR `p_gender`='Унисекс')";}
		}elseif($cat_sex != 'devochka' && isset($get['pol'])){
			switch($get['pol']){case'malchik':$sex="AND (`p_gender`='Мальчик' OR `p_gender`='Унисекс')";break;case'devochka':$sex="AND (`p_gender`='Девочка' OR `p_gender`='Унисекс')";break;default:$sex='';break;}
		}else{$sex='';}

		if(isset($route['cat'])){
			switch($route['cat']){
				case'shkolnaja-forma':$q = "AND `p_catids` LIKE '%#shkolnye%'";break;
				case'novinki':$q = "AND `p_novinki`='да'";break;
				case'rasprodazha':$q = "AND `p_rasprodazha`='да'";break;
				case'popular':$q = "AND `p_popular`='да'";break;
				default:$q = "AND `p_catids` LIKE '%#{$route['cat']};%'";break;
			}
		}else{$q = '';}
		$result = $this->db->column("SELECT COUNT(p_id) FROM `products` WHERE `p_nalichie`='да' AND `p_colors`!='' {$q} {$sex}");
		return $result;
	}
	/*! Функция получения текущего списка товаров исходя из номера страницы */
	public function getProductList($cat_sex, $get, $route, $count){
		if(!empty($count)){$max = $count;}else{$max = 12;}
		if(!empty($get['page'])){$cpage = $get['page'];}else{$cpage = 1;}
		$start = (($cpage - 1) * $max);
	
		if($cat_sex == 'devochka' && !isset($get['pol'])){$sex = "AND (`p_gender`='Девочка' OR `p_gender`='Унисекс')";}
		elseif($cat_sex == 'devochka' && isset($get['pol'])){
			if($cat_sex == $_GET['pol']){$sex = "AND (`p_gender`='Девочка' OR `p_gender`='Унисекс')";}
		}elseif($cat_sex != 'devochka' && isset($get['pol'])){
			switch($get['pol']){case'malchik':$sex="AND (`p_gender`='Мальчик' OR `p_gender`='Унисекс')";break;case'devochka':$sex="AND (`p_gender`='Девочка' OR `p_gender`='Унисекс')";break;default:$sex='';break;}
		}else{$sex='';}

		if(isset($route['cat'])){
			switch($route['cat']){
				case'shkolnaja-forma':$q = "AND `p_catids` LIKE '%#shkolnye%'";break;
				case'novinki':$q = "AND `p_novinki`='да'";break;
				case'rasprodazha':$q = "AND `p_rasprodazha`='да'";break;
				case'popular':$q = "AND `p_popular`='да'";break;
				default:$q = "AND `p_catids` LIKE '%#{$route['cat']};%'";break;
			}
		}else{$q = '';}

		$result = $this->db->row("SELECT * FROM `products` WHERE `p_nalichie`='да' AND `p_colors`!='' {$q} {$sex} ORDER BY `p_id` DESC LIMIT {$start}, {$max}");
		return $result;
	}
	/*! Функция подсчета кол-ва товаров в таблице */
	public function getSearchProductCount($route){
		$query = stripslashes(trim(urldecode($route['q'])));
		$result = $this->db->column("SELECT COUNT(p_id) FROM `products` WHERE `p_nalichie`='да' AND `p_colors`!='' AND (`p_name` LIKE '%{$query}%' OR `p_vendor_code` LIKE '%{$query}%')");
		return $result;
	}
	/*! Функция получения текущего списка товаров исходя из номера страницы */
	public function getSearchProductList($get, $route, $count){
		if(!empty($count)){$max = $count;}else{$max = 12;}
		if(!empty($get['page'])){$cpage = $get['page'];}else{$cpage = 1;}
		$start = (($cpage - 1) * $max);
		$query = stripslashes(trim(urldecode($route['q'])));
		$result = $this->db->row("SELECT * FROM `products` WHERE `p_nalichie`='да' AND `p_colors`!='' AND (`p_name` LIKE '%{$query}%' OR `p_vendor_code` LIKE '%{$query}%') ORDER BY `p_id` DESC LIMIT {$start}, {$max}");
		//debug($result);
		return $result;
	}

	/*! Получаем рандомный товар Главной страницы */
	public function getRandomProduct(){
		$result = $this->db->row("SELECT * FROM `products` WHERE `p_nalichie`='да' ORDER BY RAND() DESC LIMIT 4");
		return $result;
	}
	/*! Получаем "хлебные крошки" [не актив] */
	public function getBreadcrumbs($cat){
		$result = $this->db->row("SELECT * FROM `prod_cat` WHERE `cat_alias`='{$cat}'");
		return $result;
	}
	/*! Получаем заказы для страницы Благодарности [не актив] */
	public function getOrders($user_ip){
		date_default_timezone_set('Europe/Kiev');
		$date = date("Y-m-d");
		$result = $this->db->row("SELECT * FROM orders,users WHERE orders.ord_user_id=users.user_id AND users.user_ip='{$user_ip}' AND orders.ord_date='{$this->modelTime()['date']}' ORDER BY orders.ord_id DESC LIMIT 1");
		return $result[0];
	}
	/*! Получаем товары для страницы Благодарности [не актив] */
	public function getOrdersProducts(){
		$result = $this->db->row("SELECT * FROM `products`");
		return $result;
	}
	/*! Получаем заказы для страницы Благодарности Телефон [не актив] */
	public function getPhones($user_ip){
		$result = $this->db->row("SELECT * FROM `phone` WHERE `ph_uip`='{$user_ip}' ORDER BY `ph_id` DESC LIMIT 1");
		return $result;
	}
	/*! Функция для валидации для стр. Корзниа */
	public function getCartValidate($post){
		$nameLen = iconv_strlen($post['fullname']);
		$phoneLen = iconv_strlen($post['phone']);
		if($nameLen < 1 or $nameLen > 20){
			$this->error = 'Имя должно содержать от 1 до 20 символов'; return false;
		}elseif(!filter_var($post['email'], FILTER_VALIDATE_EMAIL)){
			$this->error = 'E-mail введен неверно'; return false;
		}elseif($phoneLen < 5){
			$this->error = 'Телефон должен содержать от 5 символов'; return false;
		}
		return true;
	}
	/*! Формируем добавление товара в заказы */
	public function getFormOrder($post, $user_ip){
		if(!empty($post['basketcount'])){$basketcount = $post['basketcount'];}else{$basketcount = '';}
		if(!empty($post['basketprice'])){$basketprice = $post['basketprice'];}else{$basketprice = '';}
		if(!empty($post['city'])){$city = $post['city'];}else{$city = 'Неизвестно';}
		if(!empty($post['address'])){$address = $post['address'];}else{$address = 'Неизвестно';}
		if(!empty($post['payment'])){$payment = $post['payment'];}else{$payment = 'Неизвестно';}
		if(!empty($post['delivery'])){$delivery = $post['delivery'];}else{$delivery = 'Неизвестно';}
		if(!empty($post['message'])){$message = $post['message'];}else{$message = 'Неизвестно';}
		/*! Проверка пользователя на существование в БД */
		$usercount = $this->db->row("SELECT COUNT(*) FROM `users` WHERE `user_email`='{$post['email']}'");
		if($usercount[0]['COUNT(*)'] == 1){
			/*! Если пользователь существует в БД, обновляем инфо */
			$this->db->query("UPDATE `users` SET `user_login`='{$post['email']}', `user_name`='{$post['fullname']}', `user_phone`='{$post['phone']}', `user_email`='{$post['email']}', `user_ip`='{$user_ip}' WHERE `user_email`='{$post['email']}'");
		}else{
			/*! Генерируем уникальный пароль для пользователя */
			$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
			$pass = array();
			$alphaLength = strlen($alphabet) - 1;
			for($i = 0; $i < 8; $i++){$n = rand(0, $alphaLength); $pass[] = $alphabet[$n];}
			$new_pass = implode($pass);
			/*! Если пользователя нет, добавляем в БД */
			$this->db->query("INSERT INTO `users` (`user_date`, `user_login`, `user_pass`, `user_name`, `user_phone`, `user_email`, `user_ip`) VALUES ('{$this->modelTime()['date']}', '{$post['email']}', '{$new_pass}', '{$post['fullname']}', '{$post['phone']}', '{$post['email']}', '{$user_ip}')");
		}
		/*! Получаем товар из Корзины и Продуктов */
		$cartprod = $this->db->row("SELECT * FROM cart,products WHERE cart.cart_user_ip='{$user_ip}' AND products.p_id=cart.cart_prod_id");
		for($x = 0; $x < count($cartprod); $x++){
			$prodids[] = $cartprod[$x]['cart_prod_id'];
			$prodcnt[] = $cartprod[$x]['cart_prod_count'];
			$prodarticles[] = $cartprod[$x]['cart_prod_article'];
			$prodcolor[] = $cartprod[$x]['cart_prod_color'];
		}
		$arrprodids = implode("|", $prodids);
		$arrprodcnt = implode("|", $prodcnt);
		$arrprodarticles = implode("|", $prodarticles);
		$arrprodcolor = implode("|", $prodcolor);
		/*! Получаем данные конкретного пользователя */
		$user = $this->db->row("SELECT * FROM `users` WHERE `user_email`='{$post['email']}'");
		/*! Переносим товар в Ордера */
		$this->db->query("INSERT INTO `orders` (`ord_user_id`,`ord_prod_id`,`ord_prod_count`,`ord_prod_color`,`ord_date`,`ord_sum_prod`,`ord_sum_price`,`ord_city`,`ord_address`,`ord_payment`,`ord_dostavka`,`ord_user_comment`) VALUES ('{$user[0]['user_id']}','{$arrprodids}','{$arrprodcnt}','{$arrprodcolor}','{$this->modelTime()['date']}','{$basketcount}','{$basketprice}','{$city}','{$address}','{$payment}','{$delivery}','{$message}')");
		if(!$this->db->lastInsertId()){$this->error = 'Проблема в обработке запроса'; return false;}
			/*! Отправляет данные о заказе на почту пользователя */
			$telegram = $this->modelTelegramOrd($user[0]['user_id'], $arrprodids, $arrprodarticles, $arrprodcnt, $this->modelTime()['date'], $basketcount, $basketprice, $city, $address, $payment, $delivery, $message);
			if($telegram == false){$this->error = 'Проблема в отправке Телеграмм'; return false;}
		/*! Удаляем товар из Корзины */
		$this->db->row("DELETE FROM `cart` WHERE `cart_user_ip`='{$user_ip}'");
		return true;
	}
	/*! Получаем русское наименование для Категории [не актив] */
	public function getCatInfo($cat_alias){
		$result = $this->db->row("SELECT * FROM `prod_cat` WHERE `cat_alias`='{$cat_alias}'");
		return $result[0];
	}
	/*! Получаем русское наименование для SUB-Категории [не актив] */
	public function getSubcatInfo($subcat_alias){
		$result = $this->db->row("SELECT * FROM `prod_cat` WHERE `cat_alias`='{$subcat_alias}'");
		return $result[0];
	}
	/*! Получаем русское наименование для SUB-Категории [не актив] */
	public function getAllSubcatsInfo($allsubcat_alias){
		$result = $this->db->row("SELECT * FROM `prod_cat` WHERE `cat_parent_alias`='{$allsubcat_alias}' AND `cat_alias`!='{$allsubcat_alias}'");
		return $result;
	}

// *********************************************************************
	/*! Функция для валидации для стр. Контакты */
	public function getPhoneValidate($post){
		$nameLen = iconv_strlen($post['greet_name']);
		$phoneLen = iconv_strlen($post['greet_phone']);
		$textLen = iconv_strlen($post['greet_text']);
		if($nameLen < 1 or $nameLen > 20){
			$this->error = 'Имя должно содержать от 1 до 20 символов'; return false;
		}elseif(!filter_var($post['greet_email'], FILTER_VALIDATE_EMAIL)){
			$this->error = 'E-mail введен неверно'; return false;
		}elseif($phoneLen < 5){
			$this->error = 'Телефон должен содержать от 5 символов'; return false;
		}elseif($textLen < 5 or $textLen > 500){
			$this->error = 'Текст сообщения должен содержать от 10 до 500 символов'; return false;
		}elseif($post['greet_captcha'] != $_SESSION['captcha']){
			$this->error = 'Неверный код безопасности'; return false;
		}
		return true;
	}
	/*! Добавляем заявку на перезвон по номеру телефона пользователя */
	public function getAddPhone($post, $user_ip){
		//if(isset($post['greet_name']) && !empty($post['greet_name']) && $post['greet_name'] != ""){
			$name = stripslashes(trim($post['greet_name']));
		//}else{$name = "";}
		//if(isset($post['greet_email']) && !empty($post['greet_email']) && $post['greet_email'] != ""){
			$email = stripslashes(trim($post['greet_email']));
		//}else{$email = "";}
		//if(isset($post['greet_phone']) && !empty($post['greet_phone']) && $post['greet_phone'] != ""){
			$phone = stripslashes(trim($post['greet_phone']));
		//}else{$phone = "";}
		//if(isset($post['greet_text']) && !empty($post['greet_text']) && $post['greet_text'] != ""){
			$text = stripslashes(trim($post['greet_text']));
		//}else{$text = "";}

		$telegram = $this->modelTelegramPhone($this->modelTime()['datetime'], $name, $email, $phone, $text, $user_ip);
		if($telegram == false){$this->error = 'Проблема в отправке Телеграмм'; return false;}

		$this->db->query("INSERT INTO `phone` (`ph_datetime`,`ph_name`,`ph_email`,`ph_phone`,`ph_text`,`ph_uip`) VALUES ('{$this->modelTime()['datetime']}','{$name}','{$email}','{$phone}','{$text}','{$user_ip}')");
		return $this->db->lastInsertId();
	}

// *********************************************************************
	public function getSkidkiAdd($post, $user_ip){
		$this->db->query("INSERT INTO `skidki` SET `skidki_date`='{$this->modelTime()['date']}', `skidki_email`='{$post['newsletter']}', `skidki_ip`='{$user_ip}'");
		return $this->db->lastInsertId();
	}

// *********************************************************************
	/*! Работа с php классом для генерации карты сайты */
	public function getSitemapGenerate($server){
		$sitemap = new sitemap();
		/*! игнорировать ссылки с расширениями: */
		$sitemap->set_ignore(array("javascript:", ".css", ".js", ".ico", ".jpg", ".png", ".jpeg", ".swf", ".gif"));
		/*! ссылка Вашего сайта: */
		$sitemap->get_links($server['REQUEST_SCHEME'].'://'.$server['HTTP_HOST']);
		header("content-type: text/xml");
		$map = $sitemap->generate_sitemap();
		return $map;
	}
	/*! Собираем все настройки для сайта */
	public function getSettings(){
		$result = $this->db->row("SELECT * FROM `settings`");
		return $result;
	}
	/*! Функция получения даты и времени */
	public function modelTime(){
		date_default_timezone_set('Europe/Kiev');
		$array = ['datetime'=>date("Y-m-d H:i:s"),'date'=>date("Y-m-d")];
		return $array;
	}
	/*! Функция транслитерации */
	public function modelTranslit($string){
		$converter = array('а'=>'a', 'б'=>'b', 'в'=>'v', 'г'=>'g', 'д'=>'d', 'е'=>'e', 'ё'=>'jo', 'ж'=>'zh', 'з'=>'z', 'и'=>'i', 'й'=>'j', 'к'=>'k', 'л'=>'l', 'м'=>'m', 'н'=>'n', 'о'=>'o', 'п'=>'p', 'р'=>'r', 'с'=>'s', 'т'=>'t', 'у'=>'u', 'ф'=>'f', 'х'=>'h', 'ц'=>'c', 'ч'=>'ch', 'ш'=>'sh', 'щ'=>'sch', 'ь'=>'', 'ы'=>'y', 'ъ'=>'', 'э'=>'e', 'ю'=>'ju', 'я'=>'ja', 'А'=>'A', 'Б'=>'B', 'В'=>'V', 'Г'=>'G', 'Д'=>'D', 'Е'=>'E', 'Ё'=>'JO', 'Ж'=>'Zh', 'З'=>'Z', 'И'=>'I', 'Й'=>'J', 'К'=>'K', 'Л'=>'L', 'М'=>'M', 'Н'=>'N', 'О'=>'O', 'П'=>'P', 'Р'=>'R', 'С'=>'S', 'Т'=>'T', 'У'=>'U', 'Ф'=>'F', 'Х'=>'H', 'Ц'=>'C', 'Ч'=>'Ch', 'Ш'=>'Sh', 'Щ'=>'Sch', 'Ь'=>'', 'Ы'=>'Y', 'Ъ'=>'', 'Э'=>'E', 'Ю'=>'Yu', 'Я'=>'Ya',);
		$str = strtr($string, $converter);
		$str = strtolower($str);
		$str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
		$str = trim($str, "-");
		return $str;
	}
	/*! Отправляем данные о перезвоне в админ чат Телеграм */
	public function modelTelegramPhone($datetime, $name, $email, $phone, $message, $user_ip){
		/* https://api.telegram.org/bot951463431:AAGGFcKPqzy6oh2F0G6qZolyQMjz7fuDfSo/getUpdates */
		$token = "951463431:AAGGFcKPqzy6oh2F0G6qZolyQMjz7fuDfSo";
		$chat_id = "-1001296226963";
		$arr = array(
			'Дата и время заявки: ' => $datetime,
			'IP пользователя: ' => $user_ip,
			'Имя пользователя: ' => $name,
			'E-mail пользователя: ' => $email,
			'Телефон пользователя: ' => $phone,
			'Сообщение пользователя: ' => $message
		);
		foreach($arr as $key => $val){$txt .= "<b>".$key."</b>".$val."%0A";}
		$sendToTelegram = fopen("https://api.telegram.org/bot{$token}/sendMessage?chat_id={$chat_id}&parse_mode=html&text={$txt}", "r");
		if($sendToTelegram){return true;}else{return false;}
	}
	/*! Отправляем данные о заказе в админ чат Телеграм */
	public function modelTelegramOrd($user_id, $arrprodids, $arrprodarticles, $arrprodcnt, $date, $basketcount, $basketprice, $city, $address, $payment, $delivery, $message){
		/* https://api.telegram.org/bot951463431:AAGGFcKPqzy6oh2F0G6qZolyQMjz7fuDfSo/getUpdates */
		$token = "951463431:AAGGFcKPqzy6oh2F0G6qZolyQMjz7fuDfSo";
		$chat_id = "-1001296226963";
		$user = $this->db->row("SELECT * FROM `users` WHERE `user_id`='{$user_id}'");
		$arr = array(
			'Дата заказа: ' => $date,
			'ID пользователя: ' => $user_id,
			'Имя пользователя: ' => $user[0]['user_name'],
			'Телефон: ' => $user[0]['user_phone'],
			'Email: ' => $user[0]['user_email'],
			'ID товаров: ' => $arrprodids,
			'Артикула товаров: ' => $arrprodarticles,
			'Кол-во товаров: ' => $arrprodcnt,
			'Общее кол-во товаров: ' => $basketcount,
			'Общая сумма за товары: ' => $basketprice.' грн',
			'Город доставки: ' => $city,
			'Адрес доставки: ' => $address,
			'Способ оплаты: ' => $payment,
			'Тип доставки: ' => $delivery,
			'Комментарий пользователя: ' => $message
		);
		foreach($arr as $key => $val){$txt .= "<b>".$key."</b>".$val."%0A";}
		$sendToTelegram = fopen("https://api.telegram.org/bot{$token}/sendMessage?chat_id={$chat_id}&parse_mode=html&text={$txt}", "r");
		if($sendToTelegram){return true;}else{return false;}
	}
}
?>