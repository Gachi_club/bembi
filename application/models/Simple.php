<?php
namespace application\models;

use application\core\Model;
use application\lib\Sitemap;

class Simple extends Model{
	// 
	public $error;
	
	// Функция SEO для страниц сайта
	public function getSEOpage($action){
		$result = $this->db->row("SELECT * FROM `pages` WHERE `page_alias`='{$action}'");
		return $result;
	}

	// Работа с php классом для генерации карты сайты
	public function sitemapGenerate($server){
		$sitemap = new sitemap();

		//игнорировать ссылки с расширениями:
		$sitemap->set_ignore(array("javascript:", ".css", ".js", ".ico", ".jpg", ".png", ".jpeg", ".swf", ".gif"));

		//ссылка Вашего сайта:
		$sitemap->get_links($server['REQUEST_SCHEME'].'://'.$server['HTTP_HOST']);

		$map = $sitemap->generate_sitemap();
		return $map;
	}

}

?>