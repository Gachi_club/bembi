<?php

namespace application\models;

use application\core\Model;
//use Imagick;

class Admin extends Model{
	public $error;

	/*! Функция валидации Логина */
	public function getLoginValidate($post){
		$login = stripslashes(trim($post['login']));
		$pass = stripslashes(trim($post['password']));
		$qhash = md5($login.'::'.$pass.'::KPf0VCthrbwdHGt26aR8bGVH50Wjo3');
		$result = $this->db->row("SELECT * FROM `admin` WHERE `adm_hash`='{$qhash}'");
		foreach($result as $val){
			if($val['adm_login'] != $login || $val['adm_hash'] != $qhash){
				$this->error = 'Логин или Пароль введен не верно';
				return false;
			}
		}
		return $result[0];
	}

// *************************************************** Управление постами
	/*! Функция валидации новости */
	public function postValidate($post){
		$dateLen = iconv_strlen($post['news_date']);
		$titleLen = iconv_strlen($post['title']);
		$textLen = iconv_strlen($post['text']);
		$visibleLen = iconv_strlen($post['news_visible']);
		
		if($titleLen < 3 or $titleLen > 100){$this->error = 'Название должно содержать от 3 до 100 символов'; return false;}
		elseif($dateLen != 10){$this->error = 'Вы не выбрали дату публикации'; return false;}
		elseif($textLen < 500){$this->error = 'Текст должен содержать от 500 символов'; return false;}
		elseif($visibleLen < 4 or $visibleLen > 1){$this->error = 'Выберите видимость новости'; return false;}
		return true;
	}
	/*! Функция добавления новости */
	public function postAdd($post){
		$result = $this->db->row("SELECT * FROM `news` WHERE `news_alias`='{$post['news_alias']}'");
		$news_title = $this->ClearData($post['news_title']);
		$news_alias = $this->str2url($news_title);
		$news_text = $post['news_text'];
		if($result[0]['news_alias'] != $news_alias){
			if(!empty($_FILES['news_imglink']['name'])){
				$imglink_name = $_FILES['news_imglink']['name'];
				$imglink_tmp_name = $_FILES['news_imglink']['tmp_name'];
				$getMime = explode('.', $imglink_name);
				$mime = strtolower(end($getMime));
				$types = array('jpg', 'png', 'jpeg');
				$filePath = 'public/news/';
				$filePath_webp = 'public/news/webp/';
				if(in_array($mime, $types)){
					move_uploaded_file($imglink_tmp_name, $filePath.$news_alias.'.'.$mime);
					$img_result = $news_alias.'.'.$mime;
				}else{$img_result = '';}
			}
			$this->db->query("INSERT INTO `news` (`news_alias`, `news_date`, `news_seo_title`, `news_seo_desc`, `news_seo_key`, `news_imglink`, `news_title`, `news_text`, `news_visible`) VALUES ('{$news_alias}', '{$post['news_date']}', '{$news_title}', '{$news_title}', '{$news_title}', '{$img_result}', '{$news_title}', '{$news_text}', '{$post['news_visible']}')");
			return $this->db->lastInsertId();
		}else{return false;}
	}
	/*! Функция редактирования новости */
	public function postEdit($post, $id){
		$result = $this->db->row("SELECT * FROM `news` WHERE `news_alias`='{$post['news_alias']}' AND `news_id`='{$id}'");
		$news_title = $this->ClearData($post['news_title']);
		$news_alias = $this->str2url($news_title);
		$news_text = $post['news_text'];
		$news_text = html_entity_decode($news_text);
		if($result[0]['news_alias'] == $news_alias){
			if(!empty($_FILES['news_imglink']['name'])){
				$imglink_name = $_FILES['news_imglink']['name'];
				$imglink_tmp_name = $_FILES['news_imglink']['tmp_name'];
				$getMime = explode('.', $imglink_name);
				$mime = strtolower(end($getMime));
				$types = array('jpg', 'png', 'jpeg');
				$filePath = 'public/news/';
				$filePath_webp = 'public/news/webp/';
				if(in_array($mime, $types)){
					move_uploaded_file($imglink_tmp_name, $filePath.$news_alias.'.'.$mime);
					$img_result = $news_alias.'.'.$mime;
				}else{$img_result = '';}
			}
			$this->db->query("UPDATE `news` SET `news_alias`='{$news_alias}', `news_date`='{$post['news_date']}', `news_seo_title`='{$news_title}', `news_seo_desc`='{$news_title}', `news_seo_key`='{$news_title}', `news_imglink`='{$img_result}', `news_title`='{$news_title}', `news_text`='{$news_text}', `news_visible`='{$post['news_visible']}' WHERE `news_id`='{$id}'");
			return true;
		}else{return false;}
	}
	/*! Функция удаления новости */
	public function postDelete($id){
		$result = $this->db->row("SELECT * FROM `news` WHERE `news_id`='{$id}'");
		$this->db->query("DELETE FROM `news` WHERE `news_id`='{$id}'");
		if($result[0]['news_imglink'] != ''){unlink('public/news/'.$result[0]['news_imglink']);}
	}
	/*! Функция проверки новости на существование */
	public function isPostExists($id){
		$result = $this->db->column("SELECT `news_id` FROM `news` WHERE `news_id`='{$id}'");
		return $result;
	}
	/*! Извлекаем данные о новости из БД */
	public function postData($id){
		$result = $this->db->row("SELECT * FROM `news` WHERE `news_id`='{$id}'");
		return $result;
	}
	/*! Функция получения всех новостей */
	public function getAllNews(){
		$result = $this->db->row("SELECT * FROM `news`");
		return $result;
	}
	/*! Функция подсчета кол-ва новостей в таблице */
	public function newsCount(){
		$result = $this->db->column("SELECT COUNT(news_id) FROM `news`");
		return $result;
	}

// *************************************************** Управление слайдером
	/*! Функция добавления слайдера */
	public function sliderAdd($post){
		$s_link = $this->ClearData($post['s_link']);
		$s_alt = $this->ClearData($post['s_alt']);
		$s_visible = $this->ClearData($post['s_visible']);

		$img_result = '';
		if(!empty($_FILES['s_imglink']['name'])){
			$imglink_name = $_FILES['s_imglink']['name'];
			$imglink_tmp_name = $_FILES['s_imglink']['tmp_name'];
			$getMime = explode('.', $imglink_name);
			$mime = strtolower(end($getMime));
			$types = array('jpg', 'png', 'jpeg');
			$filePath = 'public/slider/';
			$filePath_webp = 'public/slider/webp/';
			if(in_array($mime, $types)){
				$file_name = $this->getRandomFileName('public/slider', $mime);
				move_uploaded_file($imglink_tmp_name, $filePath.$file_name.'.'.$mime);
				$img_result = $file_name.'.'.$mime;	
			}
		}
		
		$this->db->query("INSERT INTO `slider` (`s_link`,`s_imglink`,`s_alt`,`s_visible`) VALUES ('{$s_link}','{$img_result}','{$s_alt}','{$s_visible}')");
		return $this->db->lastInsertId();
	}
	/*! Функция редактирования слайдера */
	public function sliderEdit($post, $id){
		$result = $this->db->row("SELECT * FROM `slider` WHERE `s_id`='{$id}'");
		if($result[0]['s_id'] == $post['s_id']){
			$s_link = $this->ClearData($post['s_link']);
			$s_alt = $this->ClearData($post['s_alt']);
			$s_visible = $this->ClearData($post['s_visible']);

			$img_result = '';
			if(!empty($_FILES['s_imglink']['name'])){
				$imglink_name = $_FILES['s_imglink']['name'];
				$imglink_tmp_name = $_FILES['s_imglink']['tmp_name'];
				$getMime = explode('.', $imglink_name);
				$mime = strtolower(end($getMime));
				$types = array('jpg', 'png', 'jpeg');
				$filePath = 'public/news/';
				$filePath_webp = 'public/news/webp/';
				if(in_array($mime, $types)){
					move_uploaded_file($imglink_tmp_name, $filePath.$news_alias.'.'.$mime);
					$img_result = $news_alias.'.'.$mime;
				}
			}
			$this->db->query("UPDATE `slider` SET `s_link`='{$s_link}', `s_alt`='{$s_alt}', `s_imglink`='{$img_result}', `s_visible`='{$s_visible}' WHERE `s_id`='{$id}'");
			return true;
		}else{return false;}
	}
	/*! Функция удаления слайдера */
	public function sliderDelete($id){
		$result = $this->db->row("SELECT * FROM `slider` WHERE `s_id`='{$id}'");
		$this->db->query("DELETE FROM `slider` WHERE `s_id`='{$id}'");
		if($result[0]['s_imglink'] != ''){unlink('public/slider/'.$result[0]['s_imglink']);}
	}
	/*! Функция проверки новости на существование */
	public function isSliderExists($id){
		$result = $this->db->column("SELECT `s_id` FROM `slider` WHERE `s_id`='{$id}'");
		return $result;
	}
	/*! Извлекаем данные о новости из БД */
	public function sliderData($id){
		$result = $this->db->row("SELECT * FROM `slider` WHERE `s_id`='{$id}'");
		return $result;
	}
	/*! Функция получения всех новостей */
	public function getAllSlider(){
		$result = $this->db->row("SELECT * FROM `slider`");
		return $result;
	}
	/*! Функция подсчета кол-ва новостей в таблице */
	public function sliderCount(){
		$result = $this->db->column("SELECT COUNT(s_id) FROM `slider`");
		return $result;
	}
	
// *************************************************** Управление пользователями
	/*! Функция добавления пользователя */
	public function userAdd($post){
		$this->db->query("INSERT INTO `users` (`user_date`, `user_login`, `user_pass`, `user_name`, `user_phone`, `user_email`, `user_ip`) VALUES ('{$post['date']}', '{$post['login']}', '{$post['pass']}', '{$post['name']}', '{$post['phone']}', '{$post['email']}', '{$post['ip']}')");
		return $this->db->lastInsertId();
	}
	/*! Функция редактирования пользователя */
	public function userEdit($post, $id){
		$this->db->query("UPDATE `users` SET `user_date`='{$post['date']}', `user_login`='{$post['login']}', `user_pass`='{$post['pass']}', `user_name`='{$post['name']}', `user_phone`='{$post['phone']}', `user_email`='{$post['email']}', `user_ip`='{$post['ip']}' WHERE `user_id`='{$id}'");
	}
	/*! Функция удаления пользователя */
	public function userDelete($id){
		$this->db->query("DELETE FROM `users` WHERE `user_id`='{$id}'");
		unlink('public/users/'.$id.'.jpg');
	}
	/*! Функция выбора всех пользователей */
	public function getAllUsers(){
		$result = $this->db->row("SELECT * FROM `users`");
		return $result;
	}
	/*! Функция подсчета кол-ва пользователей в таблице */
	public function usersCount(){
		$result = $this->db->column("SELECT COUNT(user_id) FROM `users`");
		return $result;
	}
	/*! Функция проверки пользователя на существование */
	public function isUserExists($id){
		$result = $this->db->column("SELECT `user_id` FROM `users` WHERE `user_id`='{$id}'");
		return $result;
	}
	/*! Функция получения пользователя */
	public function getUser($id){
		$result = $this->db->row("SELECT * FROM `users` WHERE `user_id`='{$id}'");
		return $result;
	}

// *************************************************** Управление продуктом
	/*! Функция добавления продукта */
	public function productAdd($post){
		$result = $this->db->row("SELECT * FROM `products` WHERE `p_vendor_code`='{$post['p_vendor_code']}'");
		if($result[0]['p_vendor_code'] != $post['p_vendor_code']){
			$p_name = $this->ClearData($post['p_name']);
			$p_text = $this->ClearData($post['p_text']);
			$p_colors = $this->ClearData($post['p_colors']);
			$p_catparentid = $post['p_parent_catid'];
			$p_catid = $post['p_catid'];
			$p_vendor_code = $this->ClearData($post['p_vendor_code']);
			$p_material = $this->ClearData($post['p_material']);

			$p_price = $this->ClearData($post['p_price']);
			$pos = strpos($p_price, ",");
			if($pos !== false){$p_price = str_replace(",", ".", $p_price);}
			
			$img_result = '';
			$imglink_name = $_FILES['p_imglink']['name'];
			$imglink_tmp_name = $_FILES['p_imglink']['tmp_name'];
			$getMime = explode('.', $imglink_name);
			$mime = strtolower(end($getMime));
			$types = array('jpg', 'png', 'jpeg');
			$filePath = 'public/products/';
			if(in_array($mime, $types)){
				move_uploaded_file($imglink_tmp_name, $filePath.$p_vendor_code.'.'.$mime);
				$img_result = $p_vendor_code.'.'.$mime;
			}
			/*
			$img_result2 = '';
			$imglink_name2 = $_FILES['p_imglink2']['name'];
			$imglink_tmp_name2 = $_FILES['p_imglink2']['tmp_name'];
			$getMime2 = explode('.', $imglink_name2);
			$mime2 = strtolower(end($getMime2));
			$types2 = array('jpg', 'png', 'jpeg');
			$filePath2 = 'public/products/';
			if(in_array($mime2, $types2)){
				move_uploaded_file($imglink_tmp_name2, $filePath2.$p_vendor_code.'_img2.'.$mime2);
				$img_result2 = $p_vendor_code.'_img2.'.$mime2;
			}
			$img_result3 = '';
			$imglink_name3 = $_FILES['p_imglink3']['name'];
			$imglink_tmp_name3 = $_FILES['p_imglink3']['tmp_name'];
			$getMime3 = explode('.', $imglink_name3);
			$mime3 = strtolower(end($getMime3));
			$types3 = array('jpg', 'png', 'jpeg');
			$filePath3 = 'public/products/';
			if(in_array($mime3, $types3)){
				move_uploaded_file($imglink_tmp_name3, $filePath3.$p_vendor_code.'_img3.'.$mime3);
				$img_result3 = $p_vendor_code.'_img3.'.$mime3;
			}
			*/

			$this->db->query("INSERT INTO `products` (`p_seo_title`, `p_seo_desc`, `p_seo_key`, `p_name`, `p_text`, `p_imglink`, `p_imglink2`, `p_imglink3`, `p_colors`, `p_catparentid`, `p_catid`, `p_vendor_code`, `p_material`, `p_price`, `p_nalichie`, `p_popular`, `p_visible`) VALUES ('{$p_name}', '{$p_name}', '{$p_name}', '{$p_name}', '{$p_text}', '{$img_result}', '{$img_result2}', '{$img_result3}', '{$p_colors}', '{$p_catparentid}', '{$p_catid}', '{$p_vendor_code}', '{$p_material}', '{$p_price}', '{$post['p_nalichie']}', '{$post['p_popular']}', '{$post['p_visible']}')");
			return $this->db->lastInsertId();
		}
	}
	/*! Функция редактирования продукта */
	public function productEdit($post, $id){
		$result = $this->db->row("SELECT * FROM `products` WHERE `p_vendor_code`='{$post['p_vendor_code']}'");
		if($result[0]['p_vendor_code'] == $post['p_vendor_code']){
			$p_name = $this->ClearData($post['p_name']);
			$p_text = $this->ClearData($post['p_text']);
			$p_colors = $this->ClearData($post['p_colors']);
			$p_catparentid = $post['p_parent_catid'];
			$p_catid = $post['p_catid'];
			$p_vendor_code = $this->ClearData($post['p_vendor_code']);
			$p_material = $this->ClearData($post['p_material']);

			$p_price = $this->ClearData($post['p_price']);
			$pos = strpos($p_price, ",");
			if($pos !== false){$p_price = str_replace(",", ".", $p_price);}

			$img_result = '';
			$imglink_name = $_FILES['p_imglink']['name'];
			$imglink_tmp_name = $_FILES['p_imglink']['tmp_name'];
			$getMime = explode('.', $imglink_name);
			$mime = strtolower(end($getMime));
			$types = array('jpg', 'png', 'jpeg');
			$filePath = 'public/products/';
			if(in_array($mime, $types)){
				move_uploaded_file($imglink_tmp_name, $filePath.$p_vendor_code.'.'.$mime);
				$img_result = $p_vendor_code.'.'.$mime;
			}
			/*
			$img_result2 = '';
			$imglink_name2 = $_FILES['p_imglink2']['name'];
			$imglink_tmp_name2 = $_FILES['p_imglink2']['tmp_name'];
			$getMime2 = explode('.', $imglink_name2);
			$mime2 = strtolower(end($getMime2));
			$types2 = array('jpg', 'png', 'jpeg');
			$filePath2 = 'public/products/';
			if(in_array($mime2, $types2)){
				move_uploaded_file($imglink_tmp_name2, $filePath2.$p_vendor_code.'_img2.'.$mime2);
				$img_result2 = $p_vendor_code.'_img2.'.$mime2;
			}
			$img_result3 = '';
			$imglink_name3 = $_FILES['p_imglink3']['name'];
			$imglink_tmp_name3 = $_FILES['p_imglink3']['tmp_name'];
			$getMime3 = explode('.', $imglink_name3);
			$mime3 = strtolower(end($getMime3));
			$types3 = array('jpg', 'png', 'jpeg');
			$filePath3 = 'public/products/';
			if(in_array($mime3, $types3)){
				move_uploaded_file($imglink_tmp_name3, $filePath3.$p_vendor_code.'_img3.'.$mime3);
				$img_result3 = $p_vendor_code.'_img3.'.$mime3;
			}
			*/

			$this->db->query("UPDATE `products` SET `p_seo_title`='{$p_name}', `p_seo_desc`='{$p_name}', `p_seo_key`='{$p_name}', `p_name`='{$p_name}', `p_text`='{$p_text}', `p_imglink`='{$img_result}', `p_imglink2`='{$img_result2}', `p_imglink3`='{$img_result3}', `p_colors`='{$p_colors}', `p_catparentid`='{$p_catparentid}', `p_catid`='{$p_catid}', `p_vendor_code`='{$p_vendor_code}', `p_material`='{$p_material}', `p_price`='{$p_price}', `p_nalichie`='{$post['p_nalichie']}', `p_popular`='{$post['p_popular']}', `p_visible`='{$post['p_visible']}' WHERE `p_id`='{$id}'");
			return true;
		}else{return false;}
	}
	/*! Функция удаления продукта */
	public function productDelete($id){
		$result = $this->db->row("SELECT * FROM `products` WHERE `p_id`='{$id}'");
		if(file_exists('public/products/'.$result[0]['p_imglink']) && $result[0]['p_imglink'] !=""){unlink('public/products/'.$result[0]['p_imglink']);}
		/*
		if(file_exists('public/products/'.$result[0]['p_imglink2']) && $result[0]['p_imglink2'] !=""){unlink('public/products/'.$result[0]['p_imglink2']);}
		if(file_exists('public/products/'.$result[0]['p_imglink3']) && $result[0]['p_imglink3'] !=""){unlink('public/products/'.$result[0]['p_imglink3']);}
		*/
		$this->db->query("DELETE FROM `products` WHERE `p_id`='{$id}'");
	}
	/*! Функция получения всех товаров */
	public function getAllProducts($route){
		$max = 25;
		if(!empty($route['page'])){$cpage = $route['page'];}
		else{$cpage = 1;}
		$start = (($cpage - 1) * $max);
		$result = $this->db->row("SELECT * FROM `products` ORDER BY `p_id` ASC LIMIT {$start}, {$max}");
		return $result;
	}
	/*! Функция получения всех категорий */
	public function getAllCategories(){
		$result = $this->db->row("SELECT * FROM `prod_cat`");
		return $result;
	}
	/*! Функция получения всех товаров */
	public function getProducts(){
		$result = $this->db->row("SELECT * FROM `products`");
		return $result;
	}
	/*! Функция получения одного товара */
	public function getProduct($route){
		$result = $this->db->row("SELECT * FROM `products` WHERE `p_id`='{$route}'");
		return $result;
	}
	/*! Функция подсчета кол-ва видимых товаров в таблице */
	public function productsCount(){
		$result = $this->db->column("SELECT COUNT(p_id) FROM `products` WHERE `p_visible`='да'");
		return $result;
	}
	/*! Функция проверки товара на существование */
	public function isProductExists($id){
		$result = $this->db->column("SELECT `p_id` FROM `products` WHERE `p_id`='{$id}'");
		return $result;
	}
	/*! Функция получения главных категорий */
	public function CatIDGlav(){
		$result = $this->db->row("SELECT * FROM `prod_cat` WHERE `cat_parent_alias`=`cat_alias`");
		return $result;
	}
	/*! Функция получения дополнительных категорий */
	public function CatIDDop($id, $chng){
		$some_cat = $this->db->row("SELECT * FROM `prod_cat` WHERE `cat_id`='{$id}'");
		$result = $this->db->row("SELECT `cat_id`,`cat_title`,`cat_parent_alias`,`cat_alias` FROM `prod_cat` WHERE `cat_parent_alias`='{$some_cat[0]['cat_alias']}' AND `cat_alias`!='{$some_cat[0]['cat_alias']}'");
		if($chng == "change"){echo json_encode($result);}
		elseif($chng == "nochange"){return $result;}
		else{return $result;}
	}
	
//  *************************************************** Управление заказами
	/*! Функция получения всех заказов для Главной */
	public function getAllOrders(){
		$result = $this->db->row("SELECT * FROM orders,users,order_status WHERE orders.ord_user_id=users.user_id AND orders.ord_status_id=order_status.os_id ORDER BY orders.ord_date DESC LIMIT 25");
		return $result;
	}
	/*! Функция получения всех заявок для Главной */
	public function getAllPhones(){
		$result = $this->db->row("SELECT * FROM phone,phone_status WHERE phone.ph_status=phone_status.ps_id ORDER BY phone.ph_datetime DESC LIMIT 25");
		return $result;
	}
	/*! Функция обновления заявки для Главной */
	public function getAllPhonesUpd($id, $value){
		$this->db->query("UPDATE `phone` SET `ph_status`='{$value}' WHERE `ph_id`='{$id}'");
		return true;
	}
	/*! Функция получения всех заказов для страницы Заказов */
	public function getOrders($route){
		$max = 25;
		if(!empty($route['page'])){$cpage = $route['page'];}
		else{$cpage = 1;}
		$start = (($cpage - 1) * $max);
		$result = $this->db->row("SELECT * FROM orders,users,order_status WHERE orders.ord_user_id=users.user_id AND orders.ord_status_id=order_status.os_id ORDER BY orders.ord_id DESC LIMIT {$start}, {$max}");
		return $result;
	}
	/*! Функция просмотра определенного заказа */
	public function getOrder($id){
		$result = $this->db->row("SELECT * FROM orders,users,order_status WHERE orders.ord_user_id=users.user_id AND orders.ord_status_id=order_status.os_id AND orders.ord_id ='{$id}'");
		return $result;
	}
	/*! Функция редактирования определенного заказа */
	public function editOrder($post, $id){
		$this->db->query("UPDATE `orders` SET `ord_date`='{$post['ord_date']}', `ord_status_id`='{$post['os_id']}', `ord_city`='{$post['ord_city']}', `ord_address`='{$post['ord_address']}', `ord_dostavka`='{$post['ord_dostavka']}', `ord_payment`='{$post['ord_payment']}', `ord_user_comment`='{$post['ord_user_comment']}',`ord_update`='{$this->modelTime()['date']}', `ord_adm_comment`='{$post['ord_adm_comment']}' WHERE `ord_id`='{$id}'");
		$this->db->query("UPDATE `users` SET `user_name`='{$post['user_name']}', `user_email`='{$post['user_email']}', `user_phone`='{$post['user_phone']}' WHERE `user_id`='{$post['user_id']}'");
		return true;
	}
	/*! Функция подсчета кол-ва заявок в таблице */
	public function ordersCount(){
		$result = $this->db->column("SELECT COUNT(ord_id) FROM `orders`");
		return $result;
	}

//  *************************************************** Управление заявками
	/*! Функция валидации заявок */
	public function phoneValidate($post){
		$dateLen = iconv_strlen($post['ph_date']);
		$nameLen = iconv_strlen($post['ph_name']);
		$emailLen = iconv_strlen($post['ph_email']);
		$phoneLen = iconv_strlen($post['ph_phone']);
		$textLen = iconv_strlen($post['ph_text']);
		
		if($dateLen != 10){$this->error = 'Отсутствует дата заявки'; return false;}
		elseif($nameLen < 3 or $nameLen > 100){$this->error = 'Отсутствует Имя'; return false;}
		elseif(!filter_var($post['ph_email'], FILTER_VALIDATE_EMAIL)){$this->error = 'E-mail введен неверно'; return false;}
		elseif($phoneLen < 3 or $phoneLen > 100){$this->error = 'Отсутствует номер телефона'; return false;}
		elseif($textLen < 10 or $textLen > 10000){$this->error = 'Текст должен содержать от 10 до 10000 символов'; return false;}
		return true;
	}
	/*! Функция получения всех заказов для страницы Заказов */
	public function getPhones($route){
		$max = 25;
		if(!empty($route['page'])){$cpage = $route['page'];}
		else{$cpage = 1;}
		$start = (($cpage - 1) * $max);
		$result = $this->db->row("SELECT * FROM phone,phone_status WHERE phone.ph_status_id=phone_status.ps_id ORDER BY phone.ph_id DESC LIMIT {$start}, {$max}");
		return $result;
	}
	/*! Функция просмотра определенного заказа */
	public function getPhone($id){
		$result = $this->db->row("SELECT * FROM phone,phone_status WHERE phone.ph_status_id=phone_status.ps_id AND phone.ph_id ='{$id}'");
		return $result;
	}
	/*! Функция редактирования определенного заказа */
	public function editPhone($post, $id){
		date_default_timezone_set('Europe/Kiev');
		$this->db->query("UPDATE `phone` SET `ph_date`='{$post['ph_date']}', `ph_name`='{$post['ph_name']}', `ph_email`='{$post['ph_email']}', `ph_phone`='{$post['ph_phone']}', `ph_text`='{$post['ph_text']}', `ph_status_id`='{$post['ph_status_id']}' WHERE `ph_id`='{$id}'");
		return true;
	}
	/*! Функция подсчета кол-ва заявок в таблице */
	public function phonesCount(){
		$result = $this->db->column("SELECT COUNT(ord_id) FROM `orders`");
		return $result;
	}
//  *************************************************** Управление администраторами
	/*! Функция валидации Логина администратора */
	public function getAdminValidate($post){
		$login = stripslashes(trim($post['login']));
		$pass = stripslashes(trim($post['password']));
		$qhash = md5($login.'::'.$pass.'::KPf0VCthrbwdHGt26aR8bGVH50Wjo3');
		$result = $this->db->row("SELECT * FROM `admin` WHERE `adm_hash`='{$qhash}'");
		return $result[0];
	}
	/*! Функция получения всех админов для страницы Админы */
	public function getAdmins($route){
		$max = 25;
		if(!empty($route['page'])){$cpage = $route['page'];}
		else{$cpage = 1;}
		$start = (($cpage - 1) * $max);
		$result = $this->db->row("SELECT * FROM `admin` ORDER BY `adm_id` DESC LIMIT {$start}, {$max}");
		return $result;
	}
	/*! Функция подсчета кол-ва админов в таблице */
	public function adminsCount(){
		$result = $this->db->column("SELECT COUNT(adm_id) FROM `admin`");
		return $result;
	}
	/*! Функция валидации заявок */
	/*
	public function getAdminValidate($post){
		$loginLen = iconv_strlen($post['adm_login']);
		$passLen = iconv_strlen($post['adm_pass']);
		$nameLen = iconv_strlen($post['adm_name']);
		$emailLen = iconv_strlen($post['adm_email']);
		$phoneLen = iconv_strlen($post['adm_phone']);
		
		if($loginLen < 3 || $loginLen > 25){$this->error = 'Логин должен содержать от 3 до 25 символов'; return false;}
		elseif($passLen < 5 || $passLen > 25){$this->error = 'Пароль должен содержать от 3 до 25 символов'; return false;}
		elseif($nameLen < 3 || $nameLen > 25){$this->error = 'Имя должно содержать от 3 до 25 символов'; return false;}
		elseif(!filter_var($post['adm_email'], FILTER_VALIDATE_EMAIL)){$this->error = 'E-mail введен не верно'; return false;}
		elseif($phoneLen < 5){$this->error = 'Телефон должен содержать от 5 символов'; return false;}
		return true;
	}
	*/
	/*! Функция просмотра определенного админа */
	public function getAdmin($id){
		$result = $this->db->row("SELECT * FROM `admin` WHERE `adm_id`='{$id}'");
		return $result;
	}
	/*! Функция редактирования определенного админа */
	public function editAdmin($post, $id){
		$login = stripslashes(trim($post['adm_login']));
		$pass = stripslashes(trim($post['adm_pass']));
		$admhash = md5($login.'::'.$pass.'::KPf0VCthrbwdHGt26aR8bGVH50Wjo3');
		$this->db->query("UPDATE `admin` SET `adm_hash`='{$admhash}', `adm_login`='{$post['adm_login']}', `adm_name`='{$post['adm_name']}', `adm_email`='{$post['adm_email']}', `adm_phone`='{$post['adm_phone']}' WHERE `adm_id`='{$id}'");
		return true;
	}

//  *************************************************** Данные о продажах по месяцам
	/*! Январь-Февраль */
	public function getProfit1(){
		date_default_timezone_set('Europe/Kiev');
		$year = date("Y");
		if(date("L") == 1){$days = 29;}else{$days = 28;}
		$result = $this->db->row("SELECT `ord_sum_price` FROM `orders` WHERE `ord_date`>='$year-01-01' AND `ord_date`<='$year-02-$days'");
		return $result;
	}
	/*! Март-Апрель */
	public function getProfit2(){
		date_default_timezone_set('Europe/Kiev');
		$year = date("Y");
		$result = $this->db->row("SELECT `ord_sum_price` FROM `orders` WHERE `ord_date`>='$year-03-01' AND `ord_date`<='$year-04-30'");
		return $result;
	}
	/*! Май-Июнь */
	public function getProfit3(){
		date_default_timezone_set('Europe/Kiev');
		$year = date("Y");
		$result = $this->db->row("SELECT `ord_sum_price` FROM `orders` WHERE `ord_date`>='$year-05-01' AND `ord_date`<='$year-06-30'");
		return $result;
	}
	/*! Июль-Август */
	public function getProfit4(){
		date_default_timezone_set('Europe/Kiev');
		$year = date("Y");
		$result = $this->db->row("SELECT `ord_sum_price` FROM `orders` WHERE `ord_date`>='$year-07-01' AND `ord_date`<='$year-08-31'");
		return $result;
	}
	/*! Сентябрь-Октябрь */
	public function getProfit5(){
		date_default_timezone_set('Europe/Kiev');
		$year = date("Y");
		$result = $this->db->row("SELECT `ord_sum_price` FROM `orders` WHERE `ord_date`>='$year-09-01' AND `ord_date`<='$year-10-31'");
		return $result;
	}
	/*! Ноябрь-Декабрь */
	public function getProfit6(){
		date_default_timezone_set('Europe/Kiev');
		$year = date("Y");
		$result = $this->db->row("SELECT `ord_sum_price` FROM `orders` WHERE `ord_date`>='$year-11-01' AND `ord_date`<='$year-12-31'");
		return $result;
	}

//  *************************************************** Прочие данные для админки
	/*! Функция подсчета всех заказов */
	public function modelAllOrders(){
		$result = $this->db->row("SELECT COUNT(ord_id) FROM `orders`");
		return $result[0];
	}
	/*! Функция подсчета стоимости всех заказов */
	public function modelAllProfit(){
		$result = $this->db->row("SELECT `ord_sum_price` FROM `orders`");
		return $result;
	}
	/*! Функция подсчета всех пользователей */
	public function modelAllUsers(){
		$result = $this->db->row("SELECT COUNT(user_id) FROM `users`");
		return $result[0];
	}
	/*! Функция подсчета всех товаров */
	public function modelAllProducts(){
		$result = $this->db->row("SELECT COUNT(p_id) FROM `products`");
		return $result[0];
	}
	/*! Подключаем Статистику к админке */
	public function modelStatistic($interval){
		$result = $this->db->row("SELECT * FROM `stat_visits` ORDER BY `visit_date` DESC LIMIT $interval");
		return $result;
	}

//  *************************************************** Универсальные функции
	/*! Функция очистки данных */
	public function ClearData($data){
		$data = strip_tags($data);
		$data = trim($data);
		$data = htmlspecialchars($data);
		return $data;
	}
	/*! Функция генерации уникального названия */
	public function getRandomFileName($path, $mime){
		$mime = $mime ? '.' . $mime : '';
        $path = $path ? $path . '/' : '';
        do{
            $name = md5(microtime().rand(0, 9999));
            $file = $path.$name.$mime;
        }while(file_exists($file));
        return $name;
	}
	/*! Функция получения даты и времени */
	public function modelTime(){
		date_default_timezone_set('Europe/Kiev');
		$array = ['datetime'=>date("Y-m-d H:i:s"),'date'=>date("Y-m-d")];
		return $array;
	}
	/*! Функция транслитерации */
	public function modelTranslit($string){
		$converter = array('а'=>'a', 'б'=>'b', 'в'=>'v', 'г'=>'g', 'д'=>'d', 'е'=>'e', 'ё'=>'jo', 'ж'=>'zh', 'з'=>'z', 'и'=>'i', 'й'=>'j', 'к'=>'k', 'л'=>'l', 'м'=>'m', 'н'=>'n', 'о'=>'o', 'п'=>'p', 'р'=>'r', 'с'=>'s', 'т'=>'t', 'у'=>'u', 'ф'=>'f', 'х'=>'h', 'ц'=>'c', 'ч'=>'ch', 'ш'=>'sh', 'щ'=>'sch', 'ь'=>'', 'ы'=>'y', 'ъ'=>'', 'э'=>'e', 'ю'=>'ju', 'я'=>'ja', 'А'=>'A', 'Б'=>'B', 'В'=>'V', 'Г'=>'G', 'Д'=>'D', 'Е'=>'E', 'Ё'=>'JO', 'Ж'=>'Zh', 'З'=>'Z', 'И'=>'I', 'Й'=>'J', 'К'=>'K', 'Л'=>'L', 'М'=>'M', 'Н'=>'N', 'О'=>'O', 'П'=>'P', 'Р'=>'R', 'С'=>'S', 'Т'=>'T', 'У'=>'U', 'Ф'=>'F', 'Х'=>'H', 'Ц'=>'C', 'Ч'=>'Ch', 'Ш'=>'Sh', 'Щ'=>'Sch', 'Ь'=>'', 'Ы'=>'Y', 'Ъ'=>'', 'Э'=>'E', 'Ю'=>'Yu', 'Я'=>'Ya',);
		$str = strtr($string, $converter);
		$str = strtolower($str);
		$str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
		$str = trim($str, "-");
		return $str;
	}
}

?>