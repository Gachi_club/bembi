<?php
return [
	'all' => [
		'login',
	],
	'account' => [
		//
	],
	'admin' => [
	/* Доступы для стат. страниц */
		'index',
		'logout',
	/* Доступы для новостей */
		'news',
		'addpost',
		'editpost',
		'deletepost',
	/* Доступы для пользователей */
		//'users',
		//'adduser',
		//'edituser',
		//'deleteuser',
	/* Доступы для товара */
		'products',
		'addproduct',
		'editproduct',
		'deleteproduct',
	/* Доступы для отзывов */
		//'reviews',
		//'addreview',
		//'editreview',
		//'deletereview',
	/* Доступы для заказов */
		'orders',
		'order',
		'editorder',
	/* Доступы для заявок */
		'phones',
		'editphones',
	/* Доступы для слайдера */
		//'slider',
		//'addslider',
		//'editslider',
		//'deleteslider',
	/* Доступы для админов */
		//'admins',
		//'editadmin',
	],
];
?>