<?php
return [
	'all' => [
		'index',
		'o-magazine',
		'oplata-i-dostavka',
		'sertifikaty',
		'kontakty',
		'korzina',
		'kak-zakazat',
		'organizatoram-sp',
		'garantija-i-vozvrat',
		'politika-konfidencialnosti',
		'politika-cookie',
		//'thanks',
		'search',
		'otzyvy',
		'detskaja-odezhda',
		'product',
		'novosti',
		'statja',
		'error404',
	],
	'account' => [
		//
	],
	'admin' => [
		//
	],
];
?>