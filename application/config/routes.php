<?php

return [
	// ********************************** MainController
/* ============================ MAIN Руты для Стат. страниц и функций ============================ */
	'' => ['controller' => 'main', 'action' => 'index',],
	'o-magazine' => ['controller' => 'main', 'action' => 'o-magazine',],
	'oplata-i-dostavka' => ['controller' => 'main', 'action' => 'oplata-i-dostavka',],
	'sertifikaty' => ['controller' => 'main', 'action' => 'sertifikaty',],
	'kontakty' => ['controller' => 'main', 'action' => 'kontakty',],
	'korzina' => ['controller' => 'main', 'action' => 'korzina',],
	'kak-zakazat' => ['controller' => 'main', 'action' => 'kak-zakazat',],
	'organizatoram-sp' => ['controller' => 'main', 'action' => 'organizatoram-sp',],
	'garantija-i-vozvrat' => ['controller' => 'main', 'action' => 'garantija-i-vozvrat',],
	'politika-konfidencialnosti' => ['controller' => 'main', 'action' => 'politika-konfidencialnosti',],
	'politika-cookie' => ['controller' => 'main', 'action' => 'politika-cookie',],
	//'thanks' => ['controller' => 'main', 'action' => 'thanks',],
	'sitemap' => ['controller' => 'simple', 'action' => 'sitemap',],
	'error404' => ['controller' => 'main', 'action' => 'error404',],

/* ============================ MAIN Руты для Поиска ============================ */
	'search' => ['controller' => 'main', 'action' => 'search',],
	'search/{q:([a-zA-Zа-яА-Я0-9_\-\%])+}' => ['controller' => 'main', 'action' => 'search',],

/* ============================ MAIN Руты для Отзывов ============================ */
	'otzyvy' => ['controller' => 'main', 'action' => 'otzyvy',],

/* ============================ MAIN Руты для Магазина ============================ */
	'detskaja-odezhda' => ['controller' => 'main', 'action' => 'detskaja-odezhda',],
	'detskaja-odezhda/{cat:([a-z0-9\-])+}' => ['controller' => 'main', 'action' => 'detskaja-odezhda',],
	//'detskaja-odezhda/{cat:([a-z0-9-])+}/{filter:([a-z0-9_\-\/])+}' => ['controller' => 'main', 'action' => 'detskaja-odezhda',],
	//'detskaja-odezhda/{filter:([a-z0-9_\-\/])+}' => ['controller' => 'main', 'action' => 'detskaja-odezhda',],
	'product/{alias:([a-zа-я0-9_\-])+}' => ['controller' => 'main', 'action' => 'product'],
	
/* ============================ MAIN Руты для Новостей ============================ */
	'novosti' => ['controller' => 'main', 'action' => 'novosti',],
	'statja/{alias:([a-z0-9_\-])+}' => ['controller' => 'main', 'action' => 'statja',],

	// ********************************** AccountController
/* ============================ ACCOUNT Руты для Стат. страниц и функций ============================ */
	'account/avtorizacija' => ['controller' => 'account', 'action' => 'avtorizacija',],
	'account/registracija' => ['controller' => 'account', 'action' => 'registracija',],
	'account/vosstanovlenie' => ['controller' => 'account', 'action' => 'vosstanovlenie',],
	'account/izbrannoe' => ['controller' => 'account', 'action' => 'izbrannoe',],
	// ********************************** AdminController
/* ============================ ADMIN Руты для Стат. страниц и функций ============================ */
	'admin/index' => ['controller' => 'admin', 'action' => 'index',],
	'admin/login' => ['controller' => 'admin', 'action' => 'login',],
	'admin/logout' => ['controller' => 'admin', 'action' => 'logout',],
/* ============================ Руты для Новостей ============================ */
	'admin/news' => ['controller' => 'admin', 'action' => 'news',],
	'admin/addpost' => ['controller' => 'admin', 'action' => 'addpost',],
	'admin/editpost/{id:([0-9])+}' => ['controller' => 'admin', 'action' => 'editpost',],
	'admin/deletepost/{id:([0-9])+}' => ['controller' => 'admin', 'action' => 'deletepost',],
/* ============================ Руты для Пользователей ============================ */
	'admin/users' => ['controller' => 'admin', 'action' => 'users',],
	'admin/adduser' => ['controller' => 'admin', 'action' => 'adduser',],
	'admin/edituser/{id:([0-9])+}' => ['controller' => 'admin', 'action' => 'edituser',	],
	'admin/deleteuser/{id:([0-9])+}' => ['controller' => 'admin', 'action' => 'deleteuser',],
/* ============================ Руты для Товаров ============================ */
	'admin/products' => ['controller' => 'admin', 'action' => 'products',],
	'admin/addproduct' => ['controller' => 'admin', 'action' => 'addproduct',],
	'admin/editproduct/{id:([0-9])+}' => ['controller' => 'admin', 'action' => 'editproduct',],
	'admin/deleteproduct/{id:([0-9])+}' => ['controller' => 'admin', 'action' => 'deleteproduct',],
/* ============================ Руты для Слайдера ============================ */
	'admin/slider' => ['controller' => 'admin', 'action' => 'slider',],
	'admin/addslider' => ['controller' => 'admin', 'action' => 'addslider',],
	'admin/editslider/{id:([0-9])+}' => ['controller' => 'admin', 'action' => 'editslider',],
	'admin/deleteslider/{id:([0-9])+}' => ['controller' => 'admin', 'action' => 'deleteslider',],
/* ============================ Руты для Заказов ============================ */
	'admin/orders' => ['controller' => 'admin', 'action' => 'orders',],
	'admin/order/{id:([0-9])+}' => ['controller' => 'admin', 'action' => 'order',],
	'admin/editorder/{id:([0-9])+}' => ['controller' => 'admin', 'action' => 'editorder',],
/* ============================ Руты для Заказов ============================ */
	'admin/phones' => ['controller' => 'admin', 'action' => 'phones',],
	'admin/editphones/{id:([0-9])+}' => ['controller' => 'admin', 'action' => 'editphones',],
/* ============================ Руты для Заказов ============================ */
	'admin/admins' => ['controller' => 'admin', 'action' => 'admins',],
	'admin/editadmin/{id:([0-9])+}' => ['controller' => 'admin', 'action' => 'editadmin',],
];
?>