<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\Pagination;
use application\lib\Sitemap;
use application\models\Admin;

class SimpleController extends Controller{

	public function __construct($route){
		parent::__construct($route);
		$this->view->layout = 'simple';
		header("content-type: text/xml");
	}

	// Функция для страницы Карта сайта
	public function sitemapAction(){
		$seopage = $this->model->getSEOpage($this->route['action']);
		
		// Работа с php классом для генерации карты сайты
		$sitemap = $this->model->sitemapGenerate($_SERVER);

		$title = $seopage[0]['page_seo_title'];
		if(file_exists('public/pages/'.$this->route['action'].'.jpg')){
			$pathimg = '/public/pages/'.$this->route['action'].'.jpg';
		}else{$pathimg = '/public/img/no_photo.png';}
		$vars = [
			'og_title' => $seopage[0]['page_og_title'],
			'og_description' => $seopage[0]['page_og_desc'],
			'og_image' => $pathimg,
			'description' => $seopage[0]['page_seo_desc'],
			'keywords' => $seopage[0]['page_seo_key'],
			'rtitle' => $seopage[0]['page_title'],
			'text' => $seopage[0]["page_text"],
			'sitemap' => $sitemap,
		];
		$this->view->render($title, $vars);
	}
}

?>