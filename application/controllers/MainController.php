<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\Pagination;
use application\lib\Sitemap;
use application\models\Admin;

class MainController extends Controller{

	public $sitename = " ✔ Интернет магазин «Bembi»";

	public function indexAction(){
		$this->Lang();
		$this->getIP();
		/*! Управление POST данными связанными с покупкой товара */
		if(!empty($_POST['product_submit'])){
			switch($_POST['product_submit']){
				case 'add':$this->model->getProductAdd($_POST, $_SESSION['user_ip']);break;
				case 'produp':$this->model->getProductUp($_POST, $_SESSION['user_ip']);break;
				case 'proddown':$this->model->getProductDown($_POST, $_SESSION['user_ip']);break;
				case 'delete':$this->model->getProductDelete($_POST, $_SESSION['user_ip']);break;
				case 'order':
					if(!$this->model->getCartValidate($_POST)){$this->view->message('Ошибка', $this->model->error, 'error');}
					$order = $this->model->getFormOrder($_POST, $_SESSION['user_ip']);
					if(!$order){$this->view->message('Ошибка', $this->model->error, 'error');}
					$this->view->relocation('Успех', 'Благодарим за оформление заказа, данные отправлены. Наш менеджер свяжется с Вами в ближайшее время.', 'success', 'detskaja-odezhda');
				break;
				case 'phoneadd':
					if(!$this->model->getPhoneValidate($_POST)){$this->view->message('Ошибка', $this->model->error, 'error');}
					$id = $this->model->getAddPhone($_POST, $_SESSION['user_ip']);
					if(!$id){$this->view->message('Ошибка', 'Проблема в обработке запроса', 'error');}
					$this->view->relocation('Успех', 'Благодарим за заявку, данные отправлены. Наш менеджер свяжется с Вами в ближайшее время.', 'success', 'detskaja-odezhda');
				break;
				case 'skidki':
					$skidki = $this->model->getSkidkiAdd($_POST, $_SESSION['user_ip']);
					if(!$skidki){$this->view->message('Ошибка', $this->model->error, 'error');}
					$this->view->relocation('Успех', 'Благодарим вас за участие в программе скидок. После обработки, мы отправим вам код со скидкой.', 'success', 'detskaja-odezhda');
				break;
			}
		}
		if(!empty($_POST['loadcart'])){$baskets = $this->IndexBasket($_SESSION['user_ip']); return $baskets;}

		$seo = $this->model->getSEOpage($this->route['action']);
		$ttl = $seo['page_seo_title'];
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['page_seo_desc'],
			'og_img' => $this->Images('pages', $seo['page_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['page_seo_desc'],
			'keys' => $seo['page_seo_key'],
			'page_ttl' => $seo['page_title'],
			'page_text' => $seo['page_text'],
			'basketcount' => $baskets['bcount'],
			'basketprodcount' => $baskets['bprodcount'],
			'basketproducts' => $baskets['basketproducts'],
			'ballcount' => $baskets['ballcount'],
			'slider' => $this->model->getIndexSlider(),
			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'novinki' => $this->model->getIndexProduct('novinki'),
			'rasprodazha' => $this->model->getIndexProduct('rasprodazha'),
			'popular' => $this->model->getIndexProduct('popular'),
			'news' => $this->model->getIndexNews(),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
	public function o_magazineAction(){
		$this->Lang();
		$this->getIP();
		$seo = $this->model->getSEOpage($this->route['action']);
		$ttl = $seo['page_seo_title'].$this->sitename;
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['page_seo_desc'],
			'og_img' => $this->Images('pages', $seo['page_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['page_seo_desc'],
			'keys' => $seo['page_seo_key'],
			'page_ttl' => $seo['page_title'],
			'page_text' => $seo['page_text'],
			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
	public function oplata_i_dostavkaAction(){
		$this->Lang();
		$this->getIP();
		$seo = $this->model->getSEOpage($this->route['action']);
		$ttl = $seo['page_seo_title'].$this->sitename;
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['page_seo_desc'],
			'og_img' => $this->Images('pages', $seo['page_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['page_seo_desc'],
			'keys' => $seo['page_seo_key'],
			'page_ttl' => $seo['page_title'],
			'page_text' => $seo['page_text'],
			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
	public function sertifikatyAction(){
		$this->Lang();
		$this->getIP();
		$seo = $this->model->getSEOpage($this->route['action']);
		$ttl = $seo['page_seo_title'];
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['page_seo_desc'],
			'og_img' => $this->Images('pages', $seo['page_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['page_seo_desc'],
			'keys' => $seo['page_seo_key'],
			'page_ttl' => $seo['page_title'],
			'page_text' => $seo['page_text'],
			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
	public function kontaktyAction(){
		$this->Lang();
		$this->getIP();
		$seo = $this->model->getSEOpage($this->route['action']);
		$ttl = $seo['page_seo_title'];
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['page_seo_desc'],
			'og_img' => $this->Images('pages', $seo['page_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['page_seo_desc'],
			'keys' => $seo['page_seo_key'],
			'page_ttl' => $seo['page_title'],
			'page_text' => $seo['page_text'],
			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
	public function korzinaAction(){
		$this->Lang();
		$this->getIP();
		if(!empty($_POST['color_submit'])){
			$this->model->getNewCartColor($_POST, $_SESSION['user_ip']);
		}
		$seo = $this->model->getSEOpage($this->route['action']);
		$basket = $this->Basket($_SESSION['user_ip']);
		$ttl = $seo['page_seo_title'];
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['page_seo_desc'],
			'og_img' => $this->Images('pages', $seo['page_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['page_seo_desc'],
			'keys' => $seo['page_seo_key'],
			'page_ttl' => $seo['page_title'],
			'page_text' => $seo['page_text'],
			
			'basketcount' => $basket['bcount'],
			'basketprodcount' => $basket['bprodcount'],
			'basketproducts' => $basket['basketproducts'],
			'ballcount' => $basket['ballcount'],
			'basketprice' => $basket['bprice'],

			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
	public function kak_zakazatAction(){
		$this->Lang();
		$this->getIP();
		$seo = $this->model->getSEOpage($this->route['action']);
		$ttl = $seo['page_seo_title'];
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['page_seo_desc'],
			'og_img' => $this->Images('pages', $seo['page_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['page_seo_desc'],
			'keys' => $seo['page_seo_key'],
			'page_ttl' => $seo['page_title'],
			'page_text' => $seo['page_text'],
			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
	public function organizatoram_spAction(){
		$this->Lang();
		$this->getIP();
		$seo = $this->model->getSEOpage($this->route['action']);
		$ttl = $seo['page_seo_title'];
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['page_seo_desc'],
			'og_img' => $this->Images('pages', $seo['page_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['page_seo_desc'],
			'keys' => $seo['page_seo_key'],
			'page_ttl' => $seo['page_title'],
			'page_text' => $seo['page_text'],
			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
	public function garantija_i_vozvratAction(){
		$this->Lang();
		$this->getIP();
		$seo = $this->model->getSEOpage($this->route['action']);
		$ttl = $seo['page_seo_title'];
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['page_seo_desc'],
			'og_img' => $this->Images('pages', $seo['page_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['page_seo_desc'],
			'keys' => $seo['page_seo_key'],
			'page_ttl' => $seo['page_title'],
			'page_text' => $seo['page_text'],
			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
	public function politika_konfidencialnostiAction(){
		$this->Lang();
		$this->getIP();
		$seo = $this->model->getSEOpage($this->route['action']);
		$ttl = $seo['page_seo_title'];
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['page_seo_desc'],
			'og_img' => $this->Images('pages', $seo['page_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['page_seo_desc'],
			'keys' => $seo['page_seo_key'],
			'page_ttl' => $seo['page_title'],
			'page_text' => $seo['page_text'],
			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
	public function politika_cookieAction(){
		$this->Lang();
		$this->getIP();
		$seo = $this->model->getSEOpage($this->route['action']);
		$ttl = $seo['page_seo_title'];
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['page_seo_desc'],
			'og_img' => $this->Images('pages', $seo['page_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['page_seo_desc'],
			'keys' => $seo['page_seo_key'],
			'page_ttl' => $seo['page_title'],
			'page_text' => $seo['page_text'],
			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
	public function otzyvyAction(){
		$this->Lang();
		$this->getIP();
		$seo = $this->model->getSEOpage($this->route['action']);
		$ttl = $seo['page_seo_title'];
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['page_seo_desc'],
			'og_img' => $this->Images('pages', $seo['page_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['page_seo_desc'],
			'keys' => $seo['page_seo_key'],
			'page_ttl' => $seo['page_title'],
			'page_text' => $seo['page_text'],
			'otzyvy' => $this->model->getReviews(),
			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
	public function detskaja_odezhdaAction(){
		$this->Lang();
		$this->getIP();
		$seo = $this->model->getSEOcategory($this->route);
		$cat = $this->model->getCatalogCat($this->route);

		if($cat['cat_sex'] == 'devochka' && !isset($_GET['pol'])){$sex = '';}
		elseif($cat['cat_sex'] == 'devochka' && isset($_GET['pol'])){
			if($cat['cat_sex'] == $_GET['pol']){$sex = '';}
		}elseif($cat['cat_sex'] != 'devochka' && isset($_GET['pol'])){
			switch($_GET['pol']){case'malchik':$sex=' для мальчиков';break;case'devochka':$sex=' для девочек';break;}
		}else{$sex='';}

		$pagination = new Pagination($_GET, $this->route, $this->model->getProductCount($cat['cat_sex'], $_GET, $this->route), 12);

		if(isset($this->route['cat'])){$ttl = 'Купить '.$seo['cat_seo_title'].$sex.$this->sitename;}
		else{$ttl = $seo['page_seo_title'];}
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['cat_seo_desc'],
			'og_img' => $this->Images('prod_cat', $seo['cat_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['cat_seo_desc'],
			'keys' => $seo['cat_seo_key'],
			'page_ttl' => $seo['cat_title'],
			'page_text' => $seo['cat_text'],
			'cat' => $cat,
			'pagination' => $pagination->get(),
			'list' => $this->model->getProductList($cat['cat_sex'], $_GET, $this->route, 12),
			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
	public function productAction(){
		$this->Lang();
		$this->getIP();
		$seo = $this->model->getSEOproduct($this->route['alias']);
		$cat = $this->model->getProdCat($seo['p_catids']);
		if(isset($seo['p_seo_title']) && !empty($seo['p_seo_title'])){$ttl = $seo['p_seo_title'];}
		else{$ttl = 'Купить '.$seo['p_name'].' (арт.: '.$seo['p_vendor_code'].'). ✔ Интернет магазин «Bembi». Детская одежда оптом в Украине';}
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['p_seo_desc'],
			'og_img' => $this->Images('pages', $seo['page_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['p_seo_desc'],
			'keys' => $seo['p_seo_key'],
			'val' => $seo,
			'cat' => $cat,
			'cats' => $this->model->getCategories(),
			'random' => $this->model->getRandomProduct(),
			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'novinki' => $this->model->getIndexProduct('novinki'),
			'rasprodazha' => $this->model->getIndexProduct('rasprodazha'),
			'popular' => $this->model->getIndexProduct('popular'),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
	public function novostiAction(){
		$this->Lang();
		$this->getIP();
		$seo = $this->model->getSEOpage($this->route['action']);
		$pagination = new Pagination($this->route, $this->model->getNewsCount(), 6);
		$ttl = $seo['page_seo_title'];
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['page_seo_desc'],
			'og_img' => $this->Images('pages', $seo['page_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['page_seo_desc'],
			'keys' => $seo['page_seo_key'],
			'page_ttl' => $seo['page_title'],
			'page_text' => $seo['page_text'],
			'pagination' => $pagination->get(),
			'list' => $this->model->getNewsList($_GET),
			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
	public function statjaAction(){
		$this->Lang();
		$this->getIP();
		$seo = $this->model->getSEOarticle($this->route['alias']);
		$ttl = $seo['news_seo_title'];
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['news_seo_desc'],
			'og_img' => $this->Images('news', $seo['news_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['news_seo_desc'],
			'keys' => $seo['news_seo_key'],
			'art_ttl' => $seo['news_title'],
			'art_text' => $seo['news_text'],
			'art_img' => $this->Images('news', $seo['news_imglink']),
			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
	public function searchAction(){
		$this->Lang();
		$this->getIP();
		$seo = $this->model->getSEOpage($this->route['action']);
		$pagination = new Pagination($_GET, $this->route, $this->model->getSearchProductCount($this->route), 12);
		$ttl = $seo['page_seo_title'];
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['page_seo_desc'],
			'og_img' => $this->Images('pages', $seo['page_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['page_seo_desc'],
			'keys' => $seo['page_seo_key'],
			'page_ttl' => $seo['page_title'],
			'page_text' => $seo['page_text'],
			'pagination' => $pagination->get(),
			'list' => $this->model->getSearchProductList($_GET, $this->route, 8),
			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'novinki' => $this->model->getIndexProduct('novinki'),
			'rasprodazha' => $this->model->getIndexProduct('rasprodazha'),
			'popular' => $this->model->getIndexProduct('popular'),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
	public function error404Action(){
		$this->Lang();
		$this->getIP();
		$seo = $this->model->getSEOpage($this->route['action']);
		$ttl = $seo['page_seo_title'];
		$vars = [
			'og_ttl' => $ttl,
			'og_desc' => $seo['page_seo_desc'],
			'og_img' => $this->Images('pages', $seo['page_imglink']),
			'canonical' => $this->Canonical(),
			'desc' => $seo['page_seo_desc'],
			'keys' => $seo['page_seo_key'],
			'page_ttl' => $seo['page_title'],
			'page_text' => $seo['page_text'],
			'allcats' => $this->model->getIndexAllCats(),
			'schoolcats' => $this->model->getIndexSchoolCats(),
			'settings' => $this->model->getSettings(),
		];
		$this->view->render($ttl, $vars);
	}
// ***** ***** ***** ***** ***** АДМИН СТРАНИЦА ***** ***** ***** ***** *****
	/*! Функция для админ страницы */
	public function postAction(){
		$adminModel = new Admin;
		if(!$adminModel->isPostExists($this->route['id'])){$this->view->errorCode(404);}
		$vars = ['data' => $adminModel->postData($this->route['id'])[0],];
		$this->view->render('Пост', $vars);
	}
// ***** ***** ***** ***** ***** УНИВЕРСАЛЬНЫЕ ФУНКЦИИ ***** ***** ***** ***** *****
	/*! Универсальная функция корзины */
	public function Basket($user_ip){
		$basketcount = $this->model->getBasketCount($user_ip);
		if($basketcount['COUNT(*)'] > 0){
			$basketproducts = $this->model->getBasketProducts($user_ip);
			for($i = 0; $i < $basketcount['COUNT(*)']; $i++){
				$cart_prod_price = $basketproducts[$i]['cart_prod_price']; /*! Получаем цену за ед. */
				$cart_prod_count = $basketproducts[$i]['cart_prod_count']; /*! Получаем цену за кол-во */
				$bprice = $bprice + ($cart_prod_price * $cart_prod_count); /*! Получаем сумму к оплате */
				$bprodcount = $basketproducts[$i]['cart_prod_count'] / 1; /*! Получаем кол-во конкретной позиции в корзине */
				$ballcount = $ballcount + $bprodcount;
			}
			/*! Кол-во позиций в корзине */
			$bcount = $basketcount['COUNT(*)'];
		}else{$bcount = '0'; $bprodcount = '0'; $bprice = '0'; $basketproducts = ''; $ballcount ='0';}
		$array = [
			'bcount' => $bcount,
			'bprice' => $bprice,
			'bprodcount' => $bprodcount,
			'basketproducts' => $basketproducts,
			'ballcount' => $ballcount,
			//'settings' => $this->model->getSettings(),
		];
		return $array;
	}
	/*! Index Ajax loadcart script */
	public function IndexBasket($user_ip){
		$basketcount = $this->model->getBasketCount($user_ip);
		if($basketcount['COUNT(*)'] > 0){
			$basketproducts = $this->model->getBasketProducts($user_ip);
			for($i = 0; $i < $basketcount['COUNT(*)']; $i++){
				$cart_prod_price = $basketproducts[$i]['cart_prod_price'];
				$cart_prod_count = $basketproducts[$i]['cart_prod_count'];
				$bprice = $bprice + ($cart_prod_price * $cart_prod_count);
				$bprodcount = $basketproducts[$i]['cart_prod_count'] / 1;
				$ballcount = $ballcount + $bprodcount;
			}
			$bcount = $basketcount['COUNT(*)'];
		}else{$bcount = '0'; $bprodcount = '0'; $bprice = '0'; $basketproducts = ''; $ballcount ='0';}
		$array = [
			'bcount' => $bcount,
			'bprice' => $bprice,
			'bprodcount' => $bprodcount,
			'basketproducts' => $basketproducts,
			'ballcount' => $ballcount,
			//'settings' => $this->model->getSettings(),
		];
		echo json_encode($array);
	}
	/*! Универсальная функция для изображений */
	public function Images($page, $imglink){
		if(file_exists('public/bembiTMP/'.$page.'/'.$imglink) && $imglink!=""){
			$pathimg = '/public/bembiTMP/'.$page.'/'.$imglink;
		}else{$pathimg = '/public/img/no_photo.png';}
		return $pathimg;
	}
	/*! Функция определения IP */
	public function getIP(){
		if(!empty($_SERVER['HTTP_CLIENT_IP']))
		{$user_ip = $_SERVER['HTTP_CLIENT_IP'];}
		elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		{$user_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];}
		else{$user_ip = $_SERVER['REMOTE_ADDR'];}
		if(isset($_SESSION['user_ip']) && $_SESSION['user_ip'] != $user_ip){$_SESSION['user_ip'] = $user_ip;}
		else{$_SESSION['user_ip'] = $user_ip;}
		$u_ip = explode(",", $_SESSION['user_ip']);
		return $u_ip[0];
	}
	/*! Функция определения Canonical [не актив] */
	public function Canonical(){
		$isSecure = false;
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on'){$isSecure = true;}
		elseif(!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on'){$isSecure = true;}
		$REQUEST_PROTOCOL = $isSecure ? 'https' : 'http';
		if(isset($this->route['cat'])){$a_can = '/'.$this->route['cat'].'/';}
		elseif(isset($this->route['alias'])){$a_can = '/'.$this->route['alias'].'/';}
		elseif(isset($this->route['id'])){$a_can = '/'.$this->route['id'].'/';}
		elseif(isset($this->route['q'])){$a_can = '/'.$this->route['q'].'/';}
		else{$a_can = '';}
		//if(isset($this->route['filter'])){$b_can = '/'.$this->route['filter'];}else{$b_can = '';}
		if(isset($_GET['pol'])){switch($_GET['pol']){case'devochka':$b_can = '/?pol='.$_GET['pol'];break;case'malchik':$b_can = '/?pol='.$_GET['pol'];break;default:$b_can='';break;}}else{$b_can='/';}
		return $REQUEST_PROTOCOL.'://'.$_SERVER['HTTP_HOST'].'/'.$this->route['action'].$a_can.$b_can;
	}
	/*! Функция выбора языка */
	public function Lang(){
		if(isset($_GET['lang']) && !isset($_SESSION['lang'])){
			switch($_GET['lang']){
				case'ua':$_SESSION['lang'] = $_GET['lang']; $lang = $_GET['lang'];break;
				case'en':$_SESSION['lang'] = $_GET['lang']; $lang = $_GET['lang'];break;
				default:$_SESSION['lang'] = 'ru'; $lang = 'ru';break;
			}
		}elseif(isset($_GET['lang']) && isset($_SESSION['lang'])){
			if($_GET['lang'] == $_SESSION['lang']){$lang = $_SESSION['lang'];}
			else{$_SESSION['lang'] = $_GET['lang']; $lang = $_SESSION['lang'];}
		}elseif(!isset($_GET['lang']) && isset($_SESSION['lang'])){
			switch($_SESSION['lang']){
				case'ua':$lang = $_SESSION['lang'];break;
				case'en':$lang = $_SESSION['lang'];break;
				default:$_SESSION['lang'] = 'ru'; $lang = 'ru';break;
			}
		}else{$_SESSION['lang'] = 'ru'; $lang = 'ru';}
		return $lang;
	}
	/*! Функция управления фильтрами [не актив] */
	public function Filter(){
		list($filter['sex'], $filter['age'], $filter['color'], $filter['material']) = explode("/", $this->route['filter']);

		if(strrpos($filter['sex'], "_") !== false){
			$filter['sex'] = explode("_", $filter['sex']);
		}elseif(strrpos($filter['age'], "_") !== false){
			$filter['age'] = explode("_", $filter['age']);
		}elseif(strrpos($filter['color'], "_") !== false){
			$filter['color'] = explode("_", $filter['color']);
		}elseif(strrpos($filter['material'], "_") !== false){
			$filter['material'] = explode("_", $filter['material']);
		}
		$result = array_diff($filter, array(''));
		return $result;
	}

}
?>