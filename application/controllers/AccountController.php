<?php

namespace application\controllers;

use application\core\Controller;

class AccountController extends Controller{

	// Функция входа
	public function loginAction(){
		if(!empty($_POST)){
			$this->view->message('success', 'Форма авторизации работает');
		}
		$title = 'Войти в аккаунт';
		$vars = [
			'og_title' => '',
			'og_description' => '',
			'og_site_name' => '',
			'og_image' => '',
			'description' => '',
			'keywords' => '',
		];
		$this->view->render($title, $vars);
	}

	// Функция регистрации
	public function registerAction(){
		if(!empty($_POST)){
			$this->view->message('success', 'Форма регистрации работает');
		}
		$title = 'Зарегистрировать аккаунт';
		$vars = [
			'og_title' => '',
			'og_description' => '',
			'og_site_name' => '',
			'og_image' => '',
			'description' => '',
			'keywords' => '',
		];
		$this->view->render($title, $vars);
	}

	// Функция восстановления
	public function restoreAction(){
		if(!empty($_POST)){
			$this->view->message('success', 'Форма восстановления работает');
		}
		$title = 'Восстановить аккаунт';
		$vars = [
			'og_title' => '',
			'og_description' => '',
			'og_site_name' => '',
			'og_image' => '',
			'description' => '',
			'keywords' => '',
		];
		$this->view->render($title, $vars);
	}

	// Функция выхода
	public function logoutAction(){
		exit('Выход');
	}

}

?>