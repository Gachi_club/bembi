<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\Pagination;
use application\lib\AdmPagination;
use application\models\Main;

class AdminController extends Controller{

	public function __construct($route){
		parent::__construct($route);
		$this->view->layout = 'admin';
	}

	/*! Функция главной админ страницы */
	public function indexAction(){
		if(!empty($_POST['form_submit'])){
			switch($_POST['form_submit']){
				case'ph-status-update':
					$result = $this->model->getAllPhonesUpd($_POST['ph_id'], $_POST['ph_status']);
					if(!$result){$this->view->message('Ошибка', 'Что-то пошло не так', 'error');}
					//$this->view->relocation('Успех', 'Информация обновлена', 'success', 'admin/index/');
				break;
			}
		}
		$orders = $this->model->getAllOrders();
		$phones = $this->model->getAllPhones();
		if($_GET['interval']){if(!is_numeric($_GET['interval'])){$interval = $_GET['interval'];}else{$interval = '1';}}
		else{$interval = '1';}
		$stat = $this->model->modelStatistic($interval);

		/* Получаем данные о продажах для Чарта */
		$profit1 = $this->model->getProfit1();
		for($i=0; $i < count($profit1); $i++){$prof1 = $prof1 + $profit1[$i]['ord_sum_price'];}
		$profit2 = $this->model->getProfit2();
		for($i=0; $i < count($profit2); $i++){$prof2 = $prof2 + $profit2[$i]['ord_sum_price'];}
		$profit3 = $this->model->getProfit3();
		for($i=0; $i < count($profit3); $i++){$prof3 = $prof3 + $profit3[$i]['ord_sum_price'];}
		$profit4 = $this->model->getProfit4();
		for($i=0; $i < count($profit4); $i++){$prof4 = $prof4 + $profit4[$i]['ord_sum_price'];}
		$profit5 = $this->model->getProfit5();
		for($i=0; $i < count($profit5); $i++){$prof5 = $prof5 + $profit5[$i]['ord_sum_price'];}
		$profit6 = $this->model->getProfit6();
		for($i=0; $i < count($profit6); $i++){$prof6 = $prof6 + $profit6[$i]['ord_sum_price'];}

		/* Получаем: Кол-о заказов, Общая сумма за все время, Кол-во пользователей, Кол-во товара в базе */
		$allorders = $this->model->modelAllOrders();
		$allprofit = $this->model->modelAllProfit();
		if(count($allprofit) != 0){
			for($i=0; $i < count($allprofit); $i++){$allprof = $allprof + $allprofit[$i]['ord_sum_price'];}
		}else{$allprof = 0;}
		$allusers = $this->model->modelAllUsers();
		$allproducts = $this->model->modelAllProducts();

		$ttl = 'Панель администратора - Главная';
		$vars = [
			'desc' => $ttl, 'keys' => '',
			'orders' => $orders,
			'phones' => $phones,
			'prof1' => $prof1, 'prof2' => $prof2, 'prof3' => $prof3, 'prof4' => $prof4, 'prof5' => $prof5, 'prof6' => $prof6,
			'allorders' => $allorders['COUNT(ord_id)'],
			'allprofit' => $allprof,
			'allusers' => $allusers['COUNT(user_id)'],
			'allproducts' => $allproducts['COUNT(p_id)'],
			'stat' => $stat,
		];
		$this->view->render($ttl, $vars);
	}
	
// *************************************************************** Управление доступом
	/*! Функция входа */
	public function loginAction(){
		if(!empty($_POST)){
			if(!empty($_POST['form_submit']) && $_POST['form_submit'] == 'login'){
				$result = $this->model->getLoginValidate($_POST);
				if(!$result){$this->view->message('Ошибка', $this->model->error, 'error');}
				//$_SESSION['hash'] = $this->model->getAdminValidate($_POST);
				$_SESSION['hash'] = $result;
				$_SESSION['admin'] = true;
				$this->view->location('admin/index/');
			}
		}
		if(isset($_SESSION['admin'])){$this->view->redirect('admin/index');}
		$ttl = 'Авторизация';
		$vars = ['desc' => $ttl,'keys' => '',];
		$this->view->render($ttl, $vars);
	}
	/*! Функция выхода */
	public function logoutAction(){
		unset($_SESSION['hash']);
		unset($_SESSION['admin']);
		$this->view->redirect('/admin/login');
	}
	
// *************************************************************** Управление новостями
	/*! Функция просмотра всех новостей */
	public function newsAction(){
		$news = $this->model->getAllNews();
		$pagination = new AdmPagination($this->route, $this->model->getNewsCount(), 25);
		$ttl = 'Панель администратора - Все новости';
		$vars = ['desc' => $ttl, 'keys' => '', 'news' => $news, 'pagination' => $pagination->get(),];
		$this->view->render($ttl, $vars);
	}
	/*! Функция добавления поста */
	public function addpostAction(){
		if(!empty($_POST)){
			if(!empty($_POST['form_submit']) && $_POST['form_submit'] == 'addpost'){
				if(!$this->model->getPostValidate($_POST)){$this->view->message('Ошибка', $this->model->error, 'error');}
				$id = $this->model->getPostAdd($_POST);
				if(!$id){$this->view->message('Ошибка', 'Ошибка обработки запроса', 'error');}
				$this->view->location('admin/news');
			}
		}
		$ttl = 'Панель администратора - Добавление новости';
		$vars = ['desc' => $ttl, 'keys' => '',];
		$this->view->render($ttl, $vars);
	}
	/*! Функция редактирования поста */
	public function editpostAction(){
		if(!empty($_POST)){
			if(!empty($_POST['form_submit']) && $_POST['form_submit'] == 'editpost'){
				$result = $this->model->getPostEdit($_POST, $this->route['id']);
				if($result == true){$this->view->location('admin/editpost/'.$this->route['id']);}
				else{$this->view->message('Ошибка', 'Возможно, новость с таким Alias уже существует', 'error');}
			}
		}
		$post = $this->model->getPostData($this->route['id']);
		$ttl = 'Панель администратора - Редактирование новости, id: '.$this->route['id'];
		$vars = ['desc' => $ttl, 'keys' => '', 'data' => $post[0],];
		$this->view->render($title, $vars);
	}
	/*! Функция удаления поста */
	public function deletepostAction(){
		if(!$this->model->isPostExists($this->route['id'])){$this->view->errorCode(404);}
		$this->model->getPostDelete($this->route['id']);
		$this->view->location('admin/news');
	}

// *************************************************************** Управление слайдером
	/*! Функция просмотра всех слайдов */
	public function sliderAction(){
		$slider = $this->model->getAllSlider();
		$pagination = new AdmPagination($this->route, $this->model->getSliderCount(), 25);
		$ttl = 'Панель администратора - Все слайды';
		$vars = ['desc' => $ttl, 'keys' => '', 'slider' => $slider, 'pagination' => $pagination->get(),];
		$this->view->render($ttl, $vars);
	}
	/*! Функция добавления слайда */
	public function addsliderAction(){
		if(!empty($_POST)){
			if(!empty($_POST['form_submit']) && $_POST['form_submit'] == 'addslider'){
				$id = $this->model->getSliderAdd($_POST);
				if(!$id){$this->view->message('Ошибка', 'Проблема в обработке запроса', 'error');}
				$this->view->location('admin/slider');
			}
		}
		$ttl = 'Панель администратора - Добавление слайда';
		$vars = ['desc' => $ttl, 'keys' => '',];
		$this->view->render($ttl, $vars);
	}
	/*! Функция редактирования слайда */
	public function editsliderAction(){
		if(!empty($_POST)){
			if(!empty($_POST['form_submit']) && $_POST['form_submit'] == 'editslider'){
				$result = $this->model->getSliderEdit($_POST, $this->route['id']);
				if($result == true){$this->view->location('admin/editslider/'.$this->route['id']);}
				else{$this->view->message('Ошибка', 'Проблема в обработке запроса', 'error');}
			}
		}
		$slider = $this->model->getSliderData($this->route['id']);
		$ttl = 'Панель администратора - Редактирование слайдера, id: '.$this->route['id'];
		$vars = ['desc' => $ttl, 'keys' => '', 'data' => $slider[0],];
		$this->view->render($title, $vars);
	}
	/*! Функция удаления поста */
	public function deletesliderAction(){
		if(!$this->model->isSliderExists($this->route['id'])){$this->view->errorCode(404);}
		$this->model->getSliderDelete($this->route['id']);
		$this->view->redirect('/admin/slider');
	}
	
// *************************************************************** Управление пользователями
	/*! Функция просмотра всех пользователей */
	public function usersAction(){
		$users = $this->model->getAllUsers();
		$pagination = new AdmPagination($this->route, $this->model->getUsersCount(), 25);
		$ttl = 'Панель администратора - Все пользователи';
		$vars = ['desc' => $ttl, 'keys' => '', 'users' => $users, 'pagination' => $pagination->get(),];
		$this->view->render($ttl, $vars);
	}
	/*! Функция добавления пользователя */
	public function adduserAction(){
		if(!empty($_POST)){
			if(!empty($_POST['form_submit']) && $_POST['form_submit'] == 'adduser'){
				if(!$this->model->getUserValidate($_POST, 'add')){$this->view->message('Ошибка', $this->model->error, 'error');}
				$id = $this->model->getUserAdd($_POST);
				if(!$id){$this->view->message('Ошибка', 'Проблема в обработке запроса', 'error');}
				$this->model->getUserUploadImage($_FILES['image']['tmp_name'], $id);
				$this->view->message('Успех', 'Пользователь добавлен ID: '.$id, 'success');
			}
		}
		$ttl = 'Панель администратора - Добавление пользователя';
		$vars = ['desc' => $ttl, 'keys' => '',];
		$this->view->render($ttl, $vars);
	}
	/*! Функция редактирования пользователя */
	public function edituserAction(){
		if(!$this->model->isUserExists($this->route['id'])){$this->view->errorCode(404);}
		if(!empty($_POST)){
			if(!empty($_POST['form_submit']) && $_POST['form_submit'] == 'edituser'){
				$this->model->getUserEdit($_POST, $this->route['id']);
				if($_FILES['image']['tmp_name']){$this->model->getUserUploadImage($_FILES['image']['tmp_name'], $this->route['id']);}
				$this->view->message('Успех', 'Пользователь сохранен', 'success');
			}
		}
		// Получаем определенного пользователя
		$user = $this->model->getUser($this->route['id']);
		$ttl = 'Панель администратора - Редактирование пользователя';
		$vars = ['desc' => $ttl, 'keys' => '', 'user' => $user,];
		$this->view->render($ttl, $vars);
	}
	/*! Функция удаления пользователя */
	public function deleteuserAction(){
		if(!$this->model->isUserExists($this->route['id'])){$this->view->errorCode(404);}
		$this->model->getUserDelete($this->route['id']);
		$this->view->redirect('/admin/index');
	}

// *************************************************************** Управление товаром
	/*! Функция просмотра всех продуктов */
	public function productsAction(){
		$products = $this->model->getAllProducts($this->route);
		$categories = $this->model->getAllCategories();
		$pagination = new AdmPagination($this->route, $this->model->getProductsCount(), 25);
		$ttl = 'Панель администратора - Все товары';
		$vars = ['desc' => $title, 'keys' => '', 'products' => $products, 'categories' => $categories, 
			'pagination' => $pagination->get(),];
		$this->view->render($ttl, $vars);
	}
	/*! Функция добавления продукта */
	public function addproductAction(){
		if(!empty($_POST)){
			// ADD PRODUCT
			if(!empty($_POST['form_submit']) && $_POST['form_submit'] == 'addproduct'){
				$id = $this->model->getProductAdd($_POST);
				if(!$id){$this->view->message('Ошибка', 'Возможно товар с таким артикулом уже есть в БД', 'error');}
		        $this->view->message('Успех', 'Товар добавлен. ID: '.$id, 'success');
			}
			// CHANGE CATEGORY
			if(!empty($_POST['form_submit']) && $_POST['form_submit'] == 'changecategory'){
				$p_catid = $this->model->getCatIDDop($_POST['id'], "change");
				return $p_catid;
			}
		}
		// Получаем данные главных категорий
		$p_catparentid = $this->model->getCatIDGlav();
		$ttl = 'Панель администратора - Добавление товара';
		$vars = ['desc' => $ttl, 'keys' => '', 'catparentid' => $p_catparentid, 'catid' => $p_catid,];
		$this->view->render($ttl, $vars);
	}
	/*! Функция редактирования продукта */
	public function editproductAction(){
		if(!$this->model->isProductExists($this->route['id'])){$this->view->errorCode(404);}
		
		if(!empty($_POST)){
			// EDIT PRODUCT
			if(!empty($_POST['form_submit']) && $_POST['form_submit'] == 'editproduct'){
				$result = $this->model->getProductEdit($_POST, $this->route['id']);
				if($result == true){$this->view->message('Успех', 'Товар ID:'.$this->route['id'].', сохранен', 'success');}
				else{$this->view->message('Ошибка', 'Ошибка добавления', 'error');}
			}
			// CHANGE CATEGORY
			if(!empty($_POST['form_submit']) && $_POST['form_submit'] == 'changecategory'){
				$p_catid = $this->model->getCatIDDop($_POST['id'], "change");
				return $p_catid;
			}
		}else{
			// Получаем данные доп. категорий
			$p_catid = $this->model->getCatIDDop($product[0]['p_catparentid'], "nochange");
		}
		// Получаем данные главных категорий
		$product = $this->model->getProduct($this->route['id']);
		$p_catparentid = $this->model->CatIDGlav();
		$ttl = 'Панель администратора - Редактирование товара';
		$vars = ['desc' => $ttl, 'keys' => '', 'product' => $product[0], 'catparentid' => $p_catparentid, 'catid' => $p_catid,];
		$this->view->render($ttl, $vars);
	}
	/*! Функция удаления продукта */
	public function deleteproductAction(){
		if(!$this->model->isProductExists($this->route['id'])){$this->view->errorCode(404);}
		$this->model->getProductDelete($this->route['id']);
		$this->view->redirect('/admin/products');
	}
	
// *************************************************************** Управление заказами
	/*! Функция просмотра всех заказов */
	public function ordersAction(){
		$orders = $this->model->getOrders($this->route);
		$pagination = new AdmPagination($this->route, $this->model->getOrdersCount(), 25);
		$ttl = 'Панель администратора - Все заказы';
		$vars = ['desc' => $ttl, 'keys' => '', 'orders' => $orders, 'pagination' => $pagination->get(),];
		$this->view->render($ttl, $vars);
	}
	/*! Функция просмотра отдельного заказа */
	public function orderAction(){
		$order = $this->model->getOrder($this->route['id']);
		//debug($order);
		$products = $this->model->getProducts();
		$ttl = 'Панель администратора - Информация о заказе';
		$vars = ['desc' => $ttl, 'keys' => '', 'order' => $order, 'products' => $products,];
		$this->view->render($ttl, $vars);
	}
	/*! Функция редактирования отдельного заказа */
	public function editorderAction(){
		if(!empty($_POST)){
			if(!empty($_POST['form_submit']) && $_POST['form_submit'] == 'editorder'){
				$id = $this->model->editOrder($_POST,$this->route['id']);
				$this->view->message('Успех', 'Заказ обновлен', 'success');
			}
		}
		$order = $this->model->getOrder($this->route['id']);
		$products = $this->model->getProducts();
		$categories = $this->model->getAllCategories();
		$ttl = 'Панель администратора - Редактирование заказа';
		$vars = ['desc' => $ttl, 'keys' => '', 'order' => $order, 'products' => $products, 'categories' => $categories,];
		$this->view->render($ttl, $vars);
	}

// *************************************************************** Управление администраторами
	/*! Функция просмотра всех заказов */
	public function adminsAction(){
		$admins = $this->model->getAdmins($this->route);
		$pagination = new AdmPagination($this->route, $this->model->getAdminsCount(), 25);
		$ttl = 'Панель администратора - Все администраторы';
		$vars = ['desc' => $ttl, 'keys' => '', 'admins' => $admins, 'pagination' => $pagination->get(),];
		$this->view->render($ttl, $vars);
	}
	/*! Функция редактирования отдельного заказа */
	public function editadminAction(){
		if(!empty($_POST)){
			if(!empty($_POST['form_submit']) && $_POST['form_submit'] == 'editadmin'){
				if(!$this->model->getAdminValidate($_POST)){$this->view->message('Ошибка', $this->model->error, 'error');}
				$id = $this->model->getEditAdmin($_POST, $this->route['id']);
				$this->view->message('Успех', 'Администратор обновлен', 'success');
			}
		}
		$admin = $this->model->getAdmin($this->route['id']);
		$ttl = 'Панель администратора - Редактирование администратора';
		$vars = ['desc' => $ttl, 'keys' => '', 'admin' => $admin[0],];
		$this->view->render($ttl, $vars);
	}

// *************************************************************** Управление заявками
	/*! Функция просмотра всех заявок */
	public function phonesAction(){
		$phones = $this->model->getPhones($this->route);
		$pagination = new AdmPagination($this->route, $this->model->getPhonesCount(), 25);
		$ttl = 'Панель администратора - Все заказы';
		$vars = ['desc' => $ttl, 'keys' => '', 'phones' => $phones, 'pagination' => $pagination->get(),];
		$this->view->render($ttl, $vars);
	}
	/*! Функция редактирования отдельной заявки */
	public function editphonesAction(){
		if(!empty($_POST)){
			if(!empty($_POST['form_submit']) && $_POST['form_submit'] == 'editphones'){
				if(!$this->model->getPhoneValidate($_POST)){$this->view->message('Ошибка', $this->model->error, 'error');}
				$id = $this->model->getEditPhone($_POST, $this->route['id']);
				if(!$id){$this->view->message('Ошибка', 'Ошибка обработки запроса', 'error');}
				$this->view->message('Успех', 'Заказ обновлен', 'success');
			}
		}
		$phone = $this->model->getPhone($this->route['id']);
		$ttl = 'Панель администратора - Редактирование заказа';
		$vars = ['desc' => $ttl, 'keys' => '', 'phone' => $phone,];
		$this->view->render($ttl, $vars);
	}
}
?>