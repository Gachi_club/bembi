<?php

namespace application\lib;

class Pagination{
    private $max = 5;
    private $get;
    private $route;
    private $index = '';
    private $current_page;
    private $total;
    private $limit;
	public function __construct($get, $route, $total, $limit = 5){
		$this->get = $get;
		$this->route = $route;
		$this->total = $total;
		$this->limit = $limit;
		$this->amount = $this->amount();
		$this->setCurrentPage();
	}

    /*! Создаем начало и конец пагинации, а также определяем неактивной текущую страницу */
	public function get(){
		$links = null;
		$limits = $this->limits();
		$html = '<nav aria-label="Page navigation example"><ul class="pagination">';

		for($page = $limits[0]; $page <= $limits[1]; $page++){
			if($page == $this->current_page){
				$links .= '<li class="page-item active"><span class="page-link">'.$page.'</span></li>';
			}else{$links .= $this->generateHtml($page);}
		}
		if(!is_null($links)){
			if($this->current_page > 1){$links = $this->generateHtml(1, 'Назад').$links;}
			if($this->current_page < $this->amount){$links .= $this->generateHtml($this->amount, 'Вперед');}
		}
		$html .= $links.' </ul></nav>';
		if($this->amount == 1){$html = '';}
		return $html;
	}
    /*! Генерация активных HTML кнопок пагинации */
	private function generateHtml($page, $text = null){
		if(!$text){$text = $page;}
		if(!empty($this->route['action']) && empty($this->route['cat'])){
			if(isset($this->get['pol'])){
				$url = $this->route['action'].'/?pol='.$this->get['pol'].'&page='.$page;
			}else{$url = $this->route['action'].'/?page='.$page;}
			//$url = $this->route['action'].'/?page='.$page;
		}
		if(!empty($this->route['action']) && !empty($this->route['cat'])){
			if(isset($this->get['pol'])){
				$url = $this->route['action'].'/'.$this->route['cat'].'/?pol='.$this->get['pol'].'&page='.$page;
			}else{$url = $this->route['action'].'/'.$this->route['cat'].'/?page='.$page;}
			//$url = $this->route['action'].'/'.$this->route['cat'].'/?page='.$page;
		}
		if(!empty($this->route['action']) && !empty($this->route['q'])){
			$url = $this->route['action'].'/'.$this->route['q'].'/?page='.$page;
		}
		return '<li class="page-item"><a class="page-link" href="/'.$url.'">'.$text.'</a></li>';
	}
    /*! Выставление лимитов */
	private function limits(){
		$left = $this->current_page - round($this->max / 2);
		$start = $left > 0 ? $left : 1;
		if(($start + $this->max) <= $this->amount){
			$end = $start > 1 ? $start + $this->max : $this->max;
		}else{
			$end = $this->amount;
			$start = $this->amount - $this->max > 0 ? $this->amount - $this->max : 1;
		}
		return array($start, $end);
	}
    /*! Определяем текущую страницу */
	private function setCurrentPage(){
		/*
		if(isset($this->route['page'])){$currentPage = $this->route['page'];}
		else{$currentPage = 1;}
		*/

		if(isset($this->get['page'])){$currentPage = $this->get['page'];}
		else{$currentPage = 1;}
		$this->current_page = $currentPage;
		if($this->current_page > 0){
			if($this->current_page > $this->amount){
				$this->current_page = $this->amount;
			}
		}else{$this->current_page = 1;}
	}
	/*! Определяем точное количество */
	private function amount(){return ceil($this->total / $this->limit);}
}
?>