<?php
namespace application\lib;

//use application\lib\SimpleXMLElement;

class Sitemap{
	private $sitemap_urls = array();
	private $base;
	private $protocol;
	private $domain;
	private $check = array();
	private $proxy = "";
	
	//список подстроки для игнорирования в URL-адресах
	public function set_ignore($ignore_list){
		$this->check = $ignore_list;
	}

	//установить прокси-узел и порт (such as someproxy:8080 or 10.1.1.1:8080)
	public function set_proxy($host_port){
		$this->proxy = $host_port;
	}

	//проверка URL-адресов с использованием списка подстрок
	private function validate($url){
		$valid = true;
		//добавьте подстроки url, которые вы не хотите отображать с помощью метода set_ignore()
		foreach($this->check as $val){
			if(stripos($url, $val) !== false){
				$valid = false;
				break;
			}
		}
		return $valid;
	}
	
	//многократные запросы
	private function multi_curl($urls){
		// для завитушек
		$curl_handlers = array();
		// установка обработчиков завитушек
		foreach($urls as $url){
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			if(isset($this->proxy) && !$this->proxy == ''){
				curl_setopt($curl, CURLOPT_PROXY, $this->proxy);
			}
			$curl_handlers[] = $curl;
		}
		// инициирование многопроцессора
		$multi_curl_handler = curl_multi_init();
	
		// добавление всего отдельного обработчика к мульти-обработчику
		foreach($curl_handlers as $key => $curl){
			curl_multi_add_handle($multi_curl_handler,$curl);
		}
		
		// выполнение мультипроцессора
		do{
			$multi_curl = curl_multi_exec($multi_curl_handler, $active);
		}while($multi_curl == CURLM_CALL_MULTI_PERFORM  || $active);
		
		foreach($curl_handlers as $curl){
			//проверка ошибок
			if(curl_errno($curl) == CURLE_OK){
				//если нет ошибки, то получение контента
				$content = curl_multi_getcontent($curl);
				//анализ содержимого
				$this->parse_content($content);
			}
		}
		curl_multi_close($multi_curl_handler);
		return true;
	}

	//функция вызова
	public function get_links($domain){
		//получение базы адреса домена URL
		$this->base = str_replace("http://", "", $domain);
		$this->base = str_replace("https://", "", $this->base);
		$host = explode("/", $this->base);
		$this->base = $host[0];

		//получение правильного имени домена и протокола
		$this->domain = trim($domain);
		if(strpos($this->domain, "http") !== 0){
			$this->protocol = "http://";
			$this->domain = $this->protocol.$this->domain;
		}else{
			$protocol = explode("//", $domain);
			$this->protocol = $protocol[0]."//";
		}
		
		if(!in_array($this->domain, $this->sitemap_urls)){
			$this->sitemap_urls[] = $this->domain;
		}

		//запрос содержимого ссылки с помощью curl
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $this->domain);
		if(isset($this->proxy) && !$this->proxy == ''){
			curl_setopt($curl, CURLOPT_PROXY, $this->proxy);
		}
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$page = curl_exec($curl);
		curl_close($curl);
		$this->parse_content($page);
	}
	
	//анализирует содержимое и проверяет URL-адреса
	private function parse_content($page){
		//получение всех ссылок из атрибутов href
		preg_match_all("/<a[^>]*href\s*=\s*'([^']*)'|".'<a[^>]*href\s*=\s*"([^"]*)"'."/is", $page, $match);

		//сохранение новых ссылок
		$new_links = array();
		for($i = 1; $i < sizeof($match); $i++){
			//прогулки по ссылкам
			foreach($match[$i] as $url){
				//если не начинается с http и не пуст
				if(strpos($url, "http") === false  && trim($url) !== ""){
					//проверка абсолютного пути
					if($url[0] == "/"){$url = substr($url, 1);
					//проверка относительного пути
					}else if($url[0] == "."){
						while($url[0] != "/"){
							$url = substr($url, 1);
						}
						$url = substr($url, 1);
					}
					//преобразование в абсолютный URL
					$url = $this->protocol.$this->base."/".$url;
				}
				//если новый и не пустой
				if(!in_array($url, $this->sitemap_urls) && trim($url) !== ""){
					//если действительный url
					if($this->validate($url)){
						//проверяя, является ли он URL из вашего домена
						if(strpos($url, "http://".$this->base) === 0 || strpos($url, "https://".$this->base) === 0){
							//добавление url в массив sitemap
							$this->sitemap_urls[] = $url;
							//добавление url в новый массив ссылок
							$new_links[] = $url;
						}
					}
				}
			}
		}
		$this->multi_curl($new_links);
		return true;
	}

	//returns array of sitemap URLs
	public function get_array(){
		return $this->sitemap_urls;
	}

	//уведомлять службы, такие как google, bing, yahoo, ask и, кроме того, о вашем обновлении карты сайта
	public function ping($sitemap_url, $title ="", $siteurl = ""){
		$curl_handlers = array();
		
		$sitemap_url = trim($sitemap_url);
		if(strpos($sitemap_url, "http") !== 0){
			$sitemap_url = "http://".$sitemap_url;
		}
		$site = explode("//", $sitemap_url);
		$start = $site[0];
		$site = explode("/", $site[1]);
		$middle = $site[0];
		if(trim($title) == ""){
			$title = $middle;
		}
		if(trim($siteurl) == ""){
			$siteurl = $start."//".$middle;
		}
		//ссылки для ping
		$urls[0] = "http://www.google.com/webmasters/tools/ping?sitemap=".urlencode($sitemap_url);
		$urls[1] = "http://www.bing.com/webmaster/ping.aspx?siteMap=".urlencode($sitemap_url);
		$urls[2] = "http://search.yahooapis.com/SiteExplorerService/V1/updateNotification"."?appid=YahooDemo&url=".urlencode($sitemap_url);
		$urls[3] = "http://submissions.ask.com/ping?sitemap=".urlencode($sitemap_url);
		$urls[4] = "http://rpc.weblogs.com/pingSiteForm?name=".urlencode($title)."&url=".urlencode($siteurl)."&changesURL=".urlencode($sitemap_url);
	
		//установка обработчика
		foreach($urls as $url){
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURL_HTTP_VERSION_1_1, 1);
			$curl_handlers[] = $curl;
		}
		//инициирование многопоточности
		$multi_curl_handler = curl_multi_init();
	
		// добавление всего отдельного обработчика к мульти-обработчику
		foreach($curl_handlers as $key => $curl){
			curl_multi_add_handle($multi_curl_handler,$curl);
		}
		
		// выполнение многопоточности
		do{
			$multi_curl = curl_multi_exec($multi_curl_handler, $active);
		}while($multi_curl == CURLM_CALL_MULTI_PERFORM || $active);
		
		// проверка на ошибки
		$submitted = true;
		foreach($curl_handlers as $key => $curl){
			//вы можете использовать curl_multi_getcontent ($curl); для получения контента
			//и curl_error ($curl); для получения ошибок
			if(curl_errno($curl) != CURLE_OK){
				$submitted = false;
			}
		}
		curl_multi_close($multi_curl_handler);
		return $submitted;
	}
	
	//генерирует карту сайта
	public function generate_sitemap(){
		$xml = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"></urlset>';
		$sitemap = new \SimpleXMLElement($xml);
        foreach($this->sitemap_urls as $url){
            $url_tag = $sitemap->addChild("url");
            $url_tag->addChild("loc", htmlspecialchars($url));
		}
		return $sitemap->asXML();
	}
}
?>