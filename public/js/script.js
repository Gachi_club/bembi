/*******************************************************************************
*  Title: Web shop «Bembi» - Script JS file
*  Last update: Nov 12, 2019
*  Author: Nikita Kirilov (Elendarien)
*  Website: https://sinup.od.ua
*  E-mail: kirilovbiz@gmail.com
*******************************************************************************/

$(document).ready(function(){
	/*! Обращение в Console.log */
	console.log('%cДорогие пользователи Bembi.Store.', 'color: #eb5348; font-size: 28px'); 
	console.log('%cНи при каких обстоятельствах не используйте скрипты, предоставленные Вам кем-либо! Мы не несём ответственности за утерянные данные в таких ситуациях. Также не сообщайте никому ЛЮБОЙ информации, связанной с Вашими сессиями! Просто не давайте ничего, о чём Вас попросили. Выполнение любых из перечисленных действий, скорее всего, приведёт к потере Ваших данных!', 'color:#4d4d4f; font-size:18px');
	console.log('%cС Уважением, администрация интернет магазина Bembi.Store.', 'color:#4d4d4f; font-size:18px');
	/*# Обращение в Console.log */

	/*! Поиск по сайту */
	$('#search').click(function(){
		var	query = $('.form-contr').val();
		window.location.href='/search/'+encodeURIComponent(query);
	});
	$('.form-contr').keydown(function(e){
		if(e.keyCode === 13){
			var query = $('.form-contr').val();
			window.location.href='/search/'+encodeURIComponent(query);
		}
	});
	/*# Поиск по сайту */
	
	/*! FORM.js */
	$('form').submit(function(e){
		var json; e.preventDefault();
		$.ajax({type:$(this).attr('method'), url:$(this).attr('action'), data:new FormData(this), contentType:false, cache:false, processData:false,
			success:function(result){
				json = jQuery.parseJSON(result);
				if(json.url){window.location.href = '/'+json.url;}
				else if(json.location){swal({"title":json.title, "text":json.message, "type":json.status, "timer":2000, "showConfirmButton":false}, function(){window.location.href = '/'+json.location;});}
				else{swal(json.title, json.message, json.status);}
			},
		});
	});
	/*# FORM.js */

	/*! PRODUCT.PHP Уменьшение товара в карточке товара */
	$('.bminus').click(function(e){
		e.preventDefault();
		var input = $('#counter'), value = parseInt(input.val()), minstep = parseInt(input.attr('min'));
		if(value > minstep){value = value - minstep;}else{value = minstep;}
		input.val(value);
		$('#counter').attr('aria-valuenow', value);
		$('.btn-in-cart').attr('data-value', value);
	});
	/*# PRODUCT.PHP Уменьшение товара в карточке товара */

	/*! PRODUCT.PHP Прибавление товара в карточке товара */
	$('.bplus').click(function(e){
		e.preventDefault();
		var input = $('#counter'), value = parseInt(input.val()), minstep = parseInt(input.attr('min'));
		value = value + minstep;
		input.val(value);
		$('#counter').attr('aria-valuenow', value);
		$('.btn-in-cart').attr('data-value', value);
	});
	/*# PRODUCT.PHP Прибавление товара в карточке товара */

	/*! PRODUCT.PHP Мини изображения для товара */
	$('.mini-img-p').click(function(){
		var path = $(this).attr('data-path');
		$('.product-image').attr('src', path);
		$('.loupe > img').attr('src', path);
	});
	$('.mini-img-span').click(function(){
		var path = $(this).attr('data-path');
		$('.product-image').attr('src', path);
		$('.loupe > img').attr('src', path);
	});
	/*# PRODUCT.PHP Мини изображения для товара */

	/*! PRODUCT.PHP + DETSKAJA-ODEZHDA.PHP */
	$('.product-color-box').click(function(){
		$('.product-color-box').removeClass('active-color');
		$(this).addClass('active-color');
	});
	$('.btn-in-cart').click(function(e){
		e.preventDefault();
		var prodColor = $('.active-color').val();
		var prodId = $(this).attr('data-pid'), prodVCode = $(this).attr('data-vcode'), prodCount = parseInt($(this).attr('data-value')), prodPrice = parseInt($(this).attr('data-price'));
		prodADD(prodId, prodVCode, prodColor, prodCount, prodPrice);
	});
	$('.add-to-cart').click(function(e){
		e.preventDefault();
		var prodId = $(this).attr('data-pid'), prodVCode = $(this).attr('data-vcode'), prodCount = parseInt($(this).attr('data-value')), prodPrice = parseInt($(this).attr('data-price')), prodColor = $(this).attr('data-colors');
		prodADD(prodId, prodVCode, prodColor, prodCount, prodPrice);
	});
	/*# PRODUCT.PHP + DETSKAJA-ODEZHDA.PHP */

	/*! MODAL CALL */
	$(".btn-in-modal").click(function(e){
		e.preventDefault();
		$(".greet_pid").val($(this).attr('data-pid')), $(".greet_vcode").val($(this).attr('data-vcode')), $(".greet_value").val(1);
	});
	/*# MODAL CALL */

	/*! KORZINA.PHP Передача цветов в Модальное окно Козины для выбора цвета */
	$('#colorModal').on('show.bs.modal', function(e){
		var button = $(e.relatedTarget);
		var cartId = button.data('cartid'), cartActiveColor = button.data('active-color'), cartAllColors = button.data('all-colors');
		var ArrAllColors = cleanArray(cartAllColors.split(';'));
		$(this).find('.modal-color').html();
		var some = '';
		for(var i = 0; i < ArrAllColors.length; i++){
			if(ArrAllColors[i]+';' == cartActiveColor){
				some = some+'<style type="text/css">.radio-item label[for=radio-'+i+']:before{border:8px solid '+ArrAllColors[i]+';cursor:pointer;-webkit-box-shadow:2px 2px 5px 0px rgba(0,0,0,0.75);-moz-box-shadow:2px 2px 5px 0px rgba(0,0,0,0.75);box-shadow:2px 2px 5px 0px rgba(0,0,0,0.75);content:" ";display:inline-block;position:relative;top:5px;margin:0 5px 0 0;width:35px;height:35px;background-color:transparent;z-index:3}#radio-'+i+':checked + label[for=radio-'+i+']:after{background:'+ArrAllColors[i]+';width:23px;height:23px;position:absolute;top:12px;left:6px;content:" ";display:block;z-index:2}</style><div class="radio-item cart-color"><input type="radio" name="color" value="'+ArrAllColors[i]+';" class="cart-color-box active-color" id="radio-'+i+'" onclick="cartColor(\''+ArrAllColors[i]+';\', \''+cartId+'\');" checked><label for="radio-'+i+'" class="color-label hvr-pulse"></label></div>';
			}else{
				some = some+'<style type="text/css">.radio-item label[for=radio-'+i+']:before{border:8px solid '+ArrAllColors[i]+';cursor:pointer;-webkit-box-shadow:2px 2px 5px 0px rgba(0,0,0,0.75);-moz-box-shadow:2px 2px 5px 0px rgba(0,0,0,0.75);box-shadow:2px 2px 5px 0px rgba(0,0,0,0.75);content:" ";display:inline-block;position:relative;top:5px;margin:0 5px 0 0;width:35px;height:35px;background-color:transparent;z-index:3}#radio-'+i+':checked + label[for=radio-'+i+']:after{background:'+ArrAllColors[i]+';width:23px;height:23px;position:absolute;top:12px;left:6px;content:" ";display:block;z-index:2}</style><div class="radio-item cart-color"><input type="radio" name="color" value="'+ArrAllColors[i]+';" class="cart-color-box" id="radio-'+i+'" onclick="cartColor(\''+ArrAllColors[i]+';\', \''+cartId+'\');"><label for="radio-'+i+'" class="color-label hvr-pulse"></label></div>';
			}
		}
		$(this).find('.modal-color').html(some);
	});
	/*# KORZINA.PHP Передача цветов в Модальное окно Козины для выбора цвета */

	/*! KORZINA.PHP Уменьшение товара в Корзине */
	$(".btn-minus").click(function(e){
		e.preventDefault();
		var value = $(this).attr('data-value'), minstep = $(this).attr('data-minstep'), step = $(this).attr('data-step'), cartid = $(this).attr('data-cartid'), prodid = $(this).attr('data-prodid');
		if(parseInt(value) > parseInt(minstep)){
			value = parseInt(value) - parseInt(minstep);
		}else{value = parseInt(minstep);}
		prodDOWN(cartid, prodid, value);
	});
	/*# KORZINA.PHP Уменьшение товара в Корзине */

	/*! KORZINA.PHP Прибавление товара в Корзине */
	$(".btn-plus").click(function(e){
		e.preventDefault();
		var value = $(this).attr('data-value'), minstep = $(this).attr('data-minstep'), step = $(this).attr('data-step'), cartid = $(this).attr('data-cartid'), prodid = $(this).attr('data-prodid');
		value = parseInt(value) + parseInt(minstep);
		prodUP(cartid, prodid, value);
	});
	/*# KORZINA.PHP Прибавление товара в Корзине */

	/*! KORZINA.PHP Удаление товара из Корзины */
	$(".btn-delete").click(function(e){
		e.preventDefault();
		var cartId = $(this).attr('data-cartid'), prodId = $(this).attr('data-pid');
		prodDELETE(cartId, prodId);
	});
	/*# KORZINA.PHP Удаление товара из Корзины */

	/*! KORZINA.PHP Меняющиеся данные при оформлении заказа */
	$("select[name='some_dostavka']").bind("change", function(){
		$('div.some_dostavka').empty();
		var value = $("select[name='some_dostavka']").val(), result = '';
		if(value == 'Новая почта'){
			result = '<div class="py-2"><p><label for="deliver_city">Ваш город <span>*</span>:</label><input type="text" id="deliver_city"></p><p><label for="deliver_point">Номер отделения:</label><input type="text" id="deliver_point" placeholder="Например: №5"></p><p><label for="comment">Ваши пожелания:</label><input type="text" id="comment" placeholder="Ваши пожелания"></p></div>';
		}else if(value == 'Самовывоз'){
			result = '<div class="py-2" style="color:#fff; border-radius:10px; background-color:#95c11e;"><p class="px-3">Заказ можно забрать по адресу: г. Киев, ул. Дубровицкая, 28, предварительно согласовав дату и время с менеджером после оформления заказа.</p></div>';
		}
		$('div.some_dostavka').append(result);
	});
	$("select[name='some_payment']").bind("change", function(){
		$('div.some_payment').empty();
		var value = $("select[name='some_payment']").val(), result = '';
		if(value == 'Оплата на карту'){result = 'Оплата на карту ПриватБанка: 4149 6293 9763 4643, получатель Деревянченко М.Н. Для подтверждения оплаты сохраните чек или сделайте скриншот.';}
		else if(value == 'Наложенный платеж'){result = 'Оплата при получении заказа на Новой Почте (20 грн за пересылку денег + 2% от суммы платежа).';}
		$('div.some_payment').append('<p>'+result+'</p>');
	});
	/*# KORZINA.PHP Меняющиеся данные при оформлении заказа */

	/*! THANKS.PHP Распечатка заказа */
	$(".printEl").on('click', function(){print(); return false;});
	/*# THANKS.PHP Распечатка заказа */
});

/*! Lazy Load images */
document.addEventListener("DOMContentLoaded", function(){
	var lazyloadImages;if("IntersectionObserver" in window){
		lazyloadImages = document.querySelectorAll(".lazy");
		var imageObserver = new IntersectionObserver(function(entries, observer){
			entries.forEach(function(entry){
				if(entry.isIntersecting){
					var image = entry.target;
					image.classList.remove("lazy");
					imageObserver.unobserve(image);
				}
			});
		});
		lazyloadImages.forEach(function(image){imageObserver.observe(image);});
	}else{
		var lazyloadThrottleTimeout;lazyloadImages = document.querySelectorAll(".lazy");function lazyload(){
			if(lazyloadThrottleTimeout){clearTimeout(lazyloadThrottleTimeout);}
			lazyloadThrottleTimeout = setTimeout(function(){
				var scrollTop = window.pageYOffset;
				lazyloadImages.forEach(function(img){
					if(img.offsetTop < (window.innerHeight + scrollTop)){img.src = img.dataset.src;img.classList.remove('lazy');}
				});
				if(lazyloadImages.length == 0){
					document.removeEventListener("scroll", lazyload);
					window.removeEventListener("resize", lazyload);
					window.removeEventListener("orientationChange", lazyload);
				}
			}, 20);
		}
		document.addEventListener("scroll", lazyload);window.addEventListener("resize", lazyload);window.addEventListener("orientationChange", lazyload);
	}
});
/*# lazy Load images */

/*! Функция для кнопки ВВЕРХ */
/*
$(function(){
	$.fn.scrollToTop = function(){
		$(this).hide().removeAttr("href");
		if($(window).scrollTop() >= "250"){$(this).fadeIn("slow");}
		var scrollDiv = $(this);
		$(window).scroll(function(){
			if($(window).scrollTop() <= "250"){$(scrollDiv).fadeOut("slow");}
			else{$(scrollDiv).fadeIn("slow");}
		});
		$(this).click(function(){$("html, body").animate({scrollTop: 0}, "slow");});
	}
});
$(function(){$("#go-top").scrollToTop();});
*/
/*# Функция для кнопки ВВЕРХ */

/*! Универсальная функция */
/*
function post_query(name,data){
	var str = '', name = name.split('.'), data = data.split('.'), fig = createAsAr(name,data);
	$.each(fig,function(k, v){str += '&'+k+'='+v;});
	$.ajax({type: "POST", url: "/", data: str, dataType: "html", cache: false, success: function(result){alert("post_query"); loadcart();}
		});
}
function createAsAr(arr1, arr2){
	var arr = {};
	for(var i = 0, ii = arr1.length; i<ii; i++){arr[arr1[i]] = arr2[i];}
	return arr;
}
*/
/*# Универсальная функция */

/*! Функция загрузки корзины */
function loadcart(){
	$.ajax({type:"POST", url: "/", data:"loadcart=true", dataType:"json", cache:false,
		success:function(result){
			$.each(result['basketproducts'],function(index,value){
				var sum = value['cart_prod_price'] * value['cart_prod_count'];
				$(".tbl_price-"+value['cart_prod_id']).html(sum);
			});
			if(result['bcount'] == "0"){$("#cart-total").html("0");}
			else{$("#cart-total").html(result['bcount']);}
		}
	});
}
loadcart();
/*# Функция загрузки корзины */

/*! Добавление товара в Корзину */
function prodADD(prodId, prodVCode, prodColor, prodCount, prodPrice){
	$.ajax({type:"POST", url:"/", data:{'product_submit':'add', 'product_id':prodId, 'product_vendor_code':prodVCode, 'product_color':prodColor, 'product_count':prodCount, 'product_price':prodPrice}, success:function(result){loadcart();swal('', 'Товар успешно добавлен в Корзину.', 'success');}
	});
}
/*# Добавление товара в Корзину */

/*! Прибавление товара в Корзине */
function prodUP(cartId, productId, productCount){
	//alert('RARAR');
	$.ajax({type:'POST', url:'/', data:{'product_submit':'produp', 'cart_id':cartId, 'product_id':productId, 'product_count':productCount}, dataType:'html', cache:false, success:function(result){loadcart();/*window.location.href='/korzina';*/}
	});
}
/*# Прибавление товара в Корзине */

/*! Уменьшение товара в Корзину */
function prodDOWN(cartId, productId, productCount){
	$.ajax({type:'POST', url:'/', data:{'product_submit':'proddown', 'cart_id':cartId, 'product_id':productId, 'product_count':productCount}, dataType:'html', cache:false, success:function(result){loadcart();/*window.location.href='/korzina';*/}
	});
}
/*! Уменьшение товара в Корзину */

/*! Удаление товара из Корзины */
function prodDELETE(cartId, productId){
	$.ajax({type:'POST', url:'/', data:{'product_submit':'delete', 'cart_id':cartId, 'product_id':productId}, dataType:"html", cache:false, success:function(result){loadcart(); window.location.href='/korzina';}
	});
}
/*# Удаление товара из Корзины */

/*! Изменение цвета товара Модальное окно Корзина */
function cartColor(zcartColor, zcartId){
	$.ajax({type:'POST', url:'/korzina/', data:{'color_submit':'update', 'cart_id':zcartId, 'cart_color':zcartColor}, dataType:"html", cache:false, success:function(result){window.location.href='/korzina';}
	});
}
/*# Изменение цвета товара Модальное окно Корзина */

/*! Чистка массива от пустоты */
function cleanArray(actual){
	var newArray = new Array();
	for(var i = 0; i < actual.length; i++){
		if(actual[i]){newArray.push(actual[i]);}
	}
	return newArray;
}
/*# Чистка массива от пустоты */

/*! Раскрывающийся текст */
function anichange(objName){
	if($(objName).css('display') == 'none'){$(objName).animate({height:'show'}, 400);$('.slide-btn').html('<span>Скрыть текст</span><i class="fas fa-angle-up"></i>');}
	else{$(objName).animate({height:'hide'}, 200);$('.slide-btn').html('<span>Читать полностью</span><i class="fas fa-angle-down"></i>');}
}
/*# Раскрывающийся текст */