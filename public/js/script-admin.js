/*******************************************************************************
*  Title: Web shop «Bembi» - Script JS file
*  Last update: Nov 12, 2019
*  Author: Nikita Kirilov (Elendarien)
*  Website: https://sinup.od.ua
*  E-mail: kirilovbiz@gmail.com
*******************************************************************************/
$(document).ready(function(){
	/*! Обращение в Console.log */
	console.log('%cДорогие пользователи Bembi.Store.', 'color: #eb5348; font-size: 28px'); 
	console.log('%cНи при каких обстоятельствах не используйте скрипты, предоставленные Вам кем-либо! Мы не несём ответственности за утерянные данные в таких ситуациях. Также не сообщайте никому ЛЮБОЙ информации, связанной с Вашими сессиями! Просто не давайте ничего, о чём Вас попросили. Выполнение любых из перечисленных действий, скорее всего, приведёт к потере Ваших данных!', 'color:#4d4d4f; font-size:18px');
	console.log('%cС Уважением, администрация интернет магазина Bembi.Store.', 'color:#4d4d4f; font-size:18px');
	/*# Обращение в Console.log */
	
	/*! FORM.js */
	$('form').submit(function(e){
		var json; e.preventDefault();
		$.ajax({type:$(this).attr('method'), url:$(this).attr('action'), data:new FormData(this), contentType:false, cache:false, processData:false,
			success:function(result){
				json = jQuery.parseJSON(result);
				if(json.url){window.location.href = '/'+json.url;}
				else if(json.location){swal({"title":json.title, "text":json.message, "type":json.status, "timer":2000, "showConfirmButton":false}, function(){window.location.href = '/'+json.location;});}
				else{swal(json.title, json.message, json.status);}
			},
		});
	});
	/*# FORM.js */
});

/*! Lazy Load images */
document.addEventListener("DOMContentLoaded", function(){
	var lazyloadImages;if("IntersectionObserver" in window){
		lazyloadImages = document.querySelectorAll(".lazy");
		var imageObserver = new IntersectionObserver(function(entries, observer){
			entries.forEach(function(entry){
				if(entry.isIntersecting){
					var image = entry.target;
					image.classList.remove("lazy");
					imageObserver.unobserve(image);
				}
			});
		});
		lazyloadImages.forEach(function(image){imageObserver.observe(image);});
	}else{
		var lazyloadThrottleTimeout;lazyloadImages = document.querySelectorAll(".lazy");function lazyload(){
			if(lazyloadThrottleTimeout){clearTimeout(lazyloadThrottleTimeout);}
			lazyloadThrottleTimeout = setTimeout(function(){
				var scrollTop = window.pageYOffset;
				lazyloadImages.forEach(function(img){
					if(img.offsetTop < (window.innerHeight + scrollTop)){img.src = img.dataset.src;img.classList.remove('lazy');}
				});
				if(lazyloadImages.length == 0){
					document.removeEventListener("scroll", lazyload);
					window.removeEventListener("resize", lazyload);
					window.removeEventListener("orientationChange", lazyload);
				}
			}, 20);
		}
		document.addEventListener("scroll", lazyload);window.addEventListener("resize", lazyload);window.addEventListener("orientationChange", lazyload);
	}
});
/*# lazy Load images */

/*! Чистка массива от пустоты */
function cleanArray(actual){
	var newArray = new Array();
	for(var i = 0; i < actual.length; i++){
		if(actual[i]){newArray.push(actual[i]);}
	}
	return newArray;
}
/*# Чистка массива от пустоты */

/*! Смена статуса телефонной заявки */
function PhoneStatus(){
	var element = document.getElementById('select-ph-status');
	var someid = element.getAttribute('data-phid');
	var somevalue = element.value;
	$.ajax({type:'POST', url:'/admin/index/', data:{'form_submit':'ph-status-update', 'ph_id':someid, 'ph_status':somevalue}, dataType:"html", cache:false,
		success:function(result){
			swal({"title":"Успех", "text":"Данные отправлены", "type":"success", "timer":2000, "showConfirmButton":false}, function(){window.location.href = '/admin/index/';});
		}
	});
	
}
/*# Смена статуса телефонной заявки */